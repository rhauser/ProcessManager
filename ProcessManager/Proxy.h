#ifndef PMG_PROXY_H
#define PMG_PROXY_H

#include <boost/thread.hpp>

#include "ProcessManager/defs.h"
#include "ProcessManager/Handle.h"
#include "ProcessManager/Exceptions.h"

#include <pmgpriv/pmgpriv.hh>

class IPCPartition;

namespace daq {
  namespace pmg {

    class CallBack;

    /**
     * \class Proxy Proxy.h "ProcessManager/Proxy.h"
     *
     * \brief The Proxy class is responsible for caching process state
     *        information and for issuing \e CORBA calls to control the running
     *        process.
     *
     *        The Proxy class is instanciated by the ProxyTable class once per
     *        process that was started here. There are two possible scenarios:
     *        either a callback arrives while the proxy has not been created yet,
     *        or the Singleton tells the ProxyTable that a process was started
     *        and thus requests instantiation of a new Proxy before the first
     *        callback is received. In either case, a new Proxy will only be created
     *        once.
     *
     *        The proxy is responsible for forwarding the process callbacks to all
     *        registered user callbacks. It keeps a list of callbacks, which is
     *        updated exclusively by the ProcessImpl class by means of the
     *        link() and unlink() methods.
     */

    class Proxy {

    public:

      /**
       * \brief The only constructor, taking as parameter the Handle
       *        of the process to be watched.
       *
       *        It inits some variables and creates the DFMutex mutex used to 
       *        protect link() and unlink() methods.
       *
       * \param handle The handle of the process to be watched.
       */
      
      Proxy(const Handle& handle);

      /**
       * \brief The destructor: it destroys the DFMutex mutex.
       */

      ~Proxy();
        
      /**
       * \brief It updates the cached state info. It is called by ProxyTable every time
       *        the process status changes.
       *
       * \param info The daq::pmg::PMGProcessStatusInfo structure containing process status information.
       * \param updateAll If true the cached PMGProcessStatusInfo is replaced by \a info, otherways the process env
       *                  and the start args are not updated. Usually \a updateAll is 'true' when the process is started
       *                  (see Singleton::start_finished()) or when a client connects to a process not started by itself
       *                  (see Singleton::get_process()).
       *
       * \sa ProxyTable::factory(const Handle&, const PMGProcessStatusInfo&).
       */

      void info(const PMGProcessStatusInfo& info, bool updateAll);


      /**
       * \brief It updates the state of the process. To be used only in very specific cases; always prefer
       *        Proxy::info(const PMGProcessStatusInfo& info, bool updateAll).
       *
       *        Typical use case is the need to update the process state after a failure of the host where
       *        the process in running: in this case the process does not exist anymore, and the only reasonable
       *        thing that can be done is to update just its state (the full information is no more available
       *        server-side)
       *
       * \param newState The new process state
       */
      void updateState(PMGProcessState newState);

      /**
       * \brief It gets the cached state info. Usually called by ProcessImpl
       *        to forward it to whoever asked this info through a
       *        Process object.
       *
       * \return A daq::pmg::PMGProcessStatusInfo structure containing process status information.
       */

      PMGProcessStatusInfo info() const;

      /**
       * \brief It gets the handle of the watched process.
       *
       * \return The process Handle.
       */

      const Handle& handle() const ;

      /**
       * \brief It loops through the list of all registered user callbacks
       *        and calls them in order.
       */
      
      void executeCallbacks();

      /**
       * \brief It registers a new user callback. Only called by the ProcessImpl
       *        class when a new user callback function is passed. The CallBack
       *        objects are created by ProcessImpl and unlinked and deleted when
       *        someone calls unlink() on a Process or when the Process/ProcessImpl
       *        objects are deleted.
       *
       * \param callback Pointer to the callback function.
       */

      void link(const CallBack* callback);

      /**
       * \brief It removes a previously registered CallBack object from the
       *        callback list.
       *
       * \param callback Pointer to the callback function.
       */
      
      void unlink(const CallBack* callback);

      /**
       * \brief It returns \c TRUE if this proxy has not yet received a callback
       *        indicating one of the possible process exit states.
       *
       * \return \c TRUE If the process has not exited yet.
       */

      bool exited() const;

      /**
       * \brief It invokes the ProcessManager server to terminate the process via a \e SIGTERM.
       *        It is called by ProcessImpl.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC
       * \throw pmg::Bad_PMG_Server The ProcessManager server is not able to answer
       * \throw pmg::Process_Already_Exited The process already exited
       * \throw pmg::Signal_Not_Allowed Signal not allowed by the AccessManager
       * \throw pmg::Server_Internal_Error Server error while processing the request
       *
       * \sa Server::stop().     
       */

      void stop();

      /**
       * \brief It invokes the ProcessManager server to kill the process via a \e SIGKILL.
       *        It is called by ProcessImpl.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC
       * \throw pmg::Bad_PMG_Server The ProcessManager server is not able to answer
       * \throw pmg::Process_Already_Exited The process already exited
       * \throw pmg::Signal_Not_Allowed Signal not allowed by the AccessManager
       * \throw pmg::Server_Internal_Error Server error while processing the request
       *
       * \sa Server::kill().
       */
      
      void kill();

      /**
       * \brief It invokes the ProcessManager server to "softly" kill the process. The server 
       *        will first send the process a \e SIGTERM and then,
       *        if the process has not exited within a certain amount of time, it will send a \e SIGKILL.
       *        It is called by ProcessImpl.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC
       * \throw pmg::Bad_PMG_Server The ProcessManager server is not able to answer
       * \throw pmg::Process_Already_Exited The process already exited
       * \throw pmg::Signal_Not_Allowed Signal not allowed by the AccessManager
       * \throw pmg::Server_Internal_Error Server error while processing the request
       *
       * \param timeout The time (in seconds) to wait before sending the \e SIGKILL.
       *
       * \sa Server::kill_soft().
       */

      void kill_soft(const int timeout);
      
      /**
       * \brief It invokes the ProcessManager server to send a \e POSIX signal to the process.
       *
       * \param signum \e POSIX signal number.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC
       * \throw pmg::Bad_PMG_Server The ProcessManager server is not able to answer
       * \throw pmg::Process_Already_Exited The process already exited
       * \throw pmg::Signal_Not_Allowed Signal not allowed by the AccessManager
       * \throw pmg::Server_Internal_Error Server error while processing the request
       *
       * \sa Server::signal().
       */

      void signal(const int signum);

      /**
       * \brief It asks the ProcessManager server who started the process to retrieve the content of the process error file.
       *
       * \param fileContent CORBA sequence whose buffer will contain the file contents.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC
       * \throw pmg::Bad_PMG_Server The ProcessManager server is not able to answer
       * \throw pmg::Cannot_Get_File The ProcessManager server is not able to retreive the file
       *
       * \sa Server::errFile()
       */

      void errFile(pmgpriv::File_var& fileContent) const;

      /**
       * \brief It asks the ProcessManager server who started the process to retrieve the content of the process output file.
       *
       * \param fileContent CORBA sequence whose buffer will contain the file contents.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC
       * \throw pmg::Bad_PMG_Server The ProcessManager server is not able to answer
       * \throw pmg::Cannot_Get_File The ProcessManager server is not able to retreive the file
       *
       * \sa Server::outFile()
       */
      
      void outFile(pmgpriv::File_var& fileContent) const;

    protected:

      /**
       * \brief It returns \c TRUE if the passed state is a final process state,
       *        meaning that any reserved resources must be released.
       */

      bool isDeadEndState(const PMGProcessState state) const;

      /**
       * \brief It looks up the \e CORBA server to talk to for the current process.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC.
       *
       * \return A pmgpriv::SERVER_var instance or \e NULL if the server could not be found.
       */
      
      pmgpriv::SERVER_var getServer() const;

      void handleSignalFailure();

    private:

      Handle                            m_handle;                 ///< The process Handle.
      PMGProcessStatusInfo              m_process_info;           ///< Structure containing process status information.
      std::list<CallBack*>              m_callbacks;              ///< Vector containing pointers to all registered callbacks.
      mutable boost::recursive_mutex    m_mutex;                  ///< Mutex used to protect the executeCallbacks(), link() and unlink() methods.
      mutable boost::shared_mutex       m_info_mutex;             ///< Mutex used to protect the process information data structure
    }; // Proxy

    typedef std::list<CallBack*>::iterator       CallBackIterator;         ///< \brief Iterator over the callback list.
    typedef std::list<CallBack*>::const_iterator CallBackConstIterator;    ///< \brief Constant iterator over the callback list.

  } 
} // daq::pmg

#endif
