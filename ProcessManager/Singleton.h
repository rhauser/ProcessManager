#ifndef PMG_SINGLETON_H
#define PMG_SINGLETON_H

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/shared_mutex.hpp>

#include <system/Host.h>

#include <ipc/object.h>

#include "ProcessManager/defs.h"
#include "ProcessManager/Handle.h"
#include "ProcessManager/CallBack.h"

#include <pmgpriv/pmgpriv.hh>

#include <memory>

const int def_timeout = 10; ///< Default timeout in seconds used in kill_partition().

class IPCAlarm;
class IPCPartition;

namespace daq {
namespace pmg {

class Singleton;
class ProxyTable;
class Proxy;
class Process;
class ProcessImpl;
class ProcessDescriptionImpl;
class ProcessList;

/**
 * \class Singleton Singleton.h "ProcessManager/Singleton.h"
 *
 * \brief The Singleton class acts as a communication hub on the client side. It dispatches
 *        client requests and server call-backs. The Singleton also owns the ProxyTable.
 *        Additionally it starts an IPC alarm checking if a linked and running process is correctly
 *        registred in the server and if the client is able to receive callbacks.
 */

class Singleton: public IPCObject<POA_pmgpriv::CLIENT> {

		friend class ProcessDescriptionImpl; // see ProcessDescriptionImpl::start(CallBackFncPtr callback = 0, void* callbackparameter = 0)

	public:

		/**
		 * \brief It creates and initialises the Singleton object. It also activates the CORBA object
		 *        (i.e., calling Singleton::setReference()). This method has to be called at the very beginning
		 *        of the application, before using any method of the ProcessManager client library.
		 *
		 * \throws daq::pmg::Bad_User_Name     Error while getting the name of the current user (this is FATAL).
		 * \throws daq::pmg::Invalid_CORBA_Ref Error while activating the CORBA object (this is FATAL).
		 * \throws daq::pmg::IPC_Port_Error    It means that the file containing the port number to use for server/client
		 *                                     communication exists, but some error occurred while reading it.
		 *                                     In this case the normal IPC communication will be chosen (i.e., using the
		 *                                     IPC lookup method) but the application may decide to not accept this and exit.
		 *                                     This issue is NOT FATAL.
		 * \throws daq::pmg::Already_Init      It means that this function is called more than one time. It is suggested to exit
		 *                                     and fix this problem.
		 *
		 * \sa For more details look at the documentation of the Singleton constructors and of the daq::pmg::utils::serverPort() and
		 *     daq::pmg::utils::loginName() functions.
		 */

		static void init();

		/**
		 * \brief Static method to get the Singleton instance.
		 *
		 * \return Pointer to the Singleton instance.
		 *
		 * \throw daq::pmg::Not_Initialized Exception issued while trying to get the Singleton instance but the
		 *                                  Singleton::init() method has not been called yet.
		 */

		static Singleton* instance();

		/**
		 * \brief Method used to get the pointer to the Process object describing a certain process.
		 *        This is the only way the client should use to get a pointer to the Process object.
		 *        Since Process objects are periodically deleted by the doWhenPMGServerReady() thread
		 *        it is higly reccomented to not store in the client application any Process pointer but
		 *        to always use this method.
		 *        This method behavior depends upon the existence of the process proxy.
		 *        If it already exists then the Process object is just created using the
		 *        Process::Process(Proxy*) constructor (this is what happens when the get_process() method
		 *        is called in ProcessDescriptionImpl::start()). If the proxy does not exist yet then the
		 *        client is added to the ProcessManager server ClientList, the proxy is created, the Process
		 *        object is created and at the end the process status information is updated calling the server (this
		 *        is what happens when trying to connect to a process started by some other clients).
		 *        If a Process object for the \a c_handle handle alredy exists then no new object is created.
		 *
		 * \param handle The process handle.
		 *
		 * \throw pmg::No_PMG_Server      The ProcessManager has not been found in IPC (or there is a communication problem with \e IPC itself)
		 * \throw pmg::Bad_PMG_Server     The ProcessManager is not able to answer the request
		 * \throw pmg::Failed_Get_Process An internal server error occurred while processing the request
		 *
		 * \note Exception are thrown only when you are asking to connect to a process started by a different client (i.e., the proxy
		 *       is not created yet and the server needs to be contacted to acquire information about the process).
		 *
		 * \warning The client application should never try to delete the Process object pointed by the returned pointer.
		 *
		 * \return A pointer to the Process object described by the \a handle.
		 */

		std::shared_ptr<Process> get_process(const Handle& handle);

		/**
		 * \brief It asks the server to "softly" kill all the process in a partition.
		 *        It queries the \e IPC server to find all the ProcessManager servers and for each of
		 *        them the Server::kill_partition() method is called.
		 *        Process are terminated first with a \e SIGTERM and, if the process has not exited
		 *        within \a timeout seconds, then a \e SIGKILL is sent.
		 *
		 * \param partition The name of the partition.
		 * \param timeout Timeout in seconds.
		 *
		 * \throw pmg::Failed_Kill_Partition An error occurred while killing a process in the partition
		 * \throw pmg::No_Agents Failed to get the list of agents from \e IPC.
		 *
		 * \sa Server::kill_partition().
		 */

		void kill_partition(const std::string& partition, const int timeout = def_timeout);

		/**
		 * \brief It asks a server looking for a process matching the search criteria.
		 *        It calls Server::lookup().
		 *
		 * \param hostname The host where the process is running.
		 * \param appname The name of the process.
		 * \param partname The name of the partition the process belongs to.
		 *
		 * \throw pmg::No_PMG_Server      The ProcessManager has not been found in IPC (or there is a communication problem with \e IPC itself)
		 * \throw pmg::Bad_PMG_Server     The ProcessManager is not able to answer the request
		 * \throw pmg::Failed_Lookup      An internal server error occurred while processing the request
		 *
		 * \warning The client application has to take care of deleting the Handle object pointed bu the returned pointer.
		 *
		 * \return A pointer to the process Handle or \e 0 if the process has not been found.
		 */

		std::unique_ptr<Handle> lookup(const std::string& hostname, const std::string& appname, const std::string& partname) const;

		/**
		 * \brief It asks some servers looking for a process matching the search criteria.
		 *        It calls Server::lookup().
		 *
		 * \param hosts Vector containing the name (full qualified name) of the hosts to be queried.
		 * \param appname The name of the process.
		 * \param partname The name of the partition the process belongs to.
		 *
		 * \throw pmg::Failed_Multiple_Lookup An error occurred while asking one of the host in the list
		 *
		 * \return A pointer to the process Handle or \e 0 if the process has not been found.
		 */

		std::unique_ptr<Handle> lookup(const std::vector<std::string>& hosts, const std::string& appname, const std::string& partname) const;

		/**
		 * \brief It asks all the servers found in \e IPC looking for a process matching the search criteria.
		 *        It calls Server::lookup().
		 *
		 * \param appname The name of the process.
		 * \param partname The name of the partition the process belongs to.
		 *
		 * \throw pmg::No_Agents No agents found in \e IPC or \e IPC communication problem
		 * \throw pmg::Failed_Multiple_Lookup An error occurred while asking one of the host in the list
		 *
		 * \return A pointer to the process Handle or \e 0 if the process has not been found.
		 */

		std::unique_ptr<Handle> lookup(const std::string& appname, const std::string& partname) const;

		/**
		 * \brief It asks a ProcessManager server if a certain process exists. It calls
		 *        Server::exists().
		 *
		 * \param handle The process handle.
		 *
		 * \throw pmg::No_PMG_Server      The ProcessManager has not been found in IPC (or there is a communication problem with \e IPC itself)
		 * \throw pmg::Bad_PMG_Server     The ProcessManager is not able to answer the request
		 * \throw pmg::Failed_Exists      An internal server error occurred while processing the request
		 *
		 * \return \c TRUE If the process exists.
		 */

		bool exists_somewhere(const Handle& handle) const;

		/**
		 * \brief It asks the IPC server about the existence of a ProcessManager server.
		 *
		 * \param pmgserver_name Name of the ProcessManager.
		 *
		 * \throw pmg::No_PMG_Server The ProcessManager has not been found in IPC (or there is a communication problem with \e IPC itself)
		 *
		 * \return A reference to the ProcessManager server.
		 */

		pmgpriv::SERVER_var get_server(const std::string& pmgserver_name) const;

		/**
		 * \brief It gets the name of the user running the client.
		 *
		 * \return The user name as a string.
		 */

		const std::string& login_name() const;

        /**
         * \brief Return either token or user name.
         *
         * \return credentials as string.
         */
        std::string credentials() const;

		/**
		 * \brief It gets the name of the host running the client.
		 *
		 * \return The name of the host.
		 */

		const std::string& local_host() const;

		/**
		 * \brief It gets a list of strings containg the handles of all the  processes running on the
		 *        host \a hostname within the \a partition partition. Handles are separated by the pmg::ProcessManagerSeparator string
		 *        - defined in defs.h.
		 *
		 * \param hostname  The name of the host the processes are running
		 * \param partition The name of the partition the processes belong to
		 * \param procList  String which will be filled with the process handles
		 *
		 * \throw pmg::No_PMG_Server         The ProcessManager has not been found in IPC (or there is a communication problem with \e IPC itself)
		 * \throw pmg::Bad_PMG_Server        The ProcessManager is not able to answer the request
		 * \throw pmg::Failed_Listing_Server An internal server error occurred while processing the request
		 *
		 * \note This method implies a \e CORBA call to the ProcessManager server running on the defined host.
		 */

		void askRunningProcesses(const std::string& hostname, const std::string& partition, std::string& procList) const;

		/**
		 * \brief It gets a map containing the handle and status of all the  processes running on the
		 *        host \a hostname within the \a partition partition.
		 *
		 * \param hostname  The name of the host the processes are running
		 * \param partition The name of the partition the processes belong to
		 * \param procMap   The map to be filled
		 *
		 * \throw pmg::No_PMG_Server         The ProcessManager has not been found in IPC (or there is a communication problem with \e IPC itself)
		 * \throw pmg::Bad_PMG_Server        The ProcessManager is not able to answer the request
		 * \throw pmg::Failed_Listing_Server An internal server error occurred while processing the request
		 *
		 * \note This method implies a \e CORBA call to the ProcessManager server running on the defined host.
		 */

		void askRunningProcesses(const std::string& hostname, const std::string& partition, std::map<std::string,
		        pmgpub::ProcessStatusInfo>& procMap) const;

		/**
		 * \brief It gets a list of strings containing the handles of all the  processes running
		 *        within the \a partition partition. Handles are separated by the pmg::ProcessManagerSeparator string
		 *        - defined in defs.h.
		 *
		 * \param  partition The name of the partition the processes belong to
		 * \param  procList  String which will be filled with the process handles
		 *
		 * \throw  pmg::No_Agents Failed getting the agent list from \e IPC
		 * \throw  pmg::Failed_Listing_Partition Some error occurred while contacting some agents
		 *
		 * \note   This method implies a \e CORBA call to all the ProcessManager server published in the \e IPC gloabl partition.
		 */

		void askRunningProcesses(const std::string& partition, std::string& procList) const;
		/**
		 * \brief It gets a map containing the handle and information of all the  processes running
		 *        within the \a partition partition.
		 *
		 * \param  partition The name of the partition the processes belong to
		 * \param  procMap   The map to be filled
		 *
		 * \throw  pmg::No_Agents Failed getting the agent list from \e IPC
		 * \throw  pmg::Failed_Listing_Partition Some error occurred while contacting some agents
		 *
		 * \note   This method implies a \e CORBA call to all the ProcessManager server published in the \e IPC gloabl partition.
		 */

		void askRunningProcesses(const std::string& partition,
		                            std::map<std::string, pmgpub::ProcessStatusInfo>& procMap) const;

	protected:

		/**
		 * \brief It returns the Singleton CORBA reference.
		 *
		 * \return The Singleton CORBA reference.
		 */

		const pmgpriv::CLIENT_var& reference() const;

		/**
		 * \brief It activates the CORBA object (calling _this()) and caches its CORBA reference. Called in Singleton::init().
		 *
		 * \throw It will throw any exception raised by the _this() method.
		 */

		void setReference();

		/**
		 * \brief Method called every time a process status is updated. The process status
		 *        is updated in the Proxy and the user callback (if any) is called.
		 *        It calls ProxyTable::callProxyCallbacks(const std::string&, const PMGProcessStatusInfo&) and
		 *        it is called by ClientList::notify_update().
		 *
		 * \param c_handle The process handle.
		 * \param info The structure containing the process status information.
		 */

		void callback(const char* c_handle, const PMGProcessStatusInfo &info);

		/**
		 * \brief The method calls ProxyTable::factory(const std::string&) to create a new Proxy
		 *        (if not present yet). It is called in ProcessDescriptionImpl::start() before the
		 *        process is actually started. In this way there is no possibility to miss a callback.
		 *        It returns a pointer to a Process instance. Since this method is called when the application
		 *        is started any already existing Process associated with the \a c_handle handle is deleted and
		 *        removed from the Process list.
		 *
		 * \return A pointer to the new fresh created Process object.
		 *
		 * \warning The clint application has not to delete the Process object pointed by the returned pointer.
		 *
		 * \param handle The handle describing the process.
		 * \param info The structure containing the process status information.
		 */

		std::shared_ptr<Process> start_finished(const pmg::Handle& handle, const PMGProcessStatusInfo &info);

		/**
		 * \brief The constructor: it creates the ProxyTable and an IPCAlarm used to monitor the status of processes.
		 *        It also checks on which port the communication with the ProcessManager server should be estabilished
		 *        and gets the name of the current user.
		 *
		 * \throw daq::pmg::Bad_User_Name Error while getting the name of the current user.
		 *
		 * \sa    daq::pmg::utils::loginName().
		 * \sa    daq::pmg::utils::serverPort().
		 */

		Singleton();

		/**
		 * \brief It deletes the ProxyTable and the IPCAlarm.
		 */

		~Singleton();

		/**
		 * \brief It gets a process by name amongst the ones present in the Singleton::m_process_table.
		 *
		 * \param name The name of the process.
		 *
		 * \return A pointer to the process or \e NULL if it is not found.
		 */

		std::shared_ptr<Process> process(const std::string& name) const;

		/**
		 * \brief This function is implemented as an IPCAlarm (executed every SINGLETON_SERVER_QUERY_PERIOD - definition in private_defs.h -
		 *        seconds) in the Singleton class.
		 *        It gets all the processes started by the client and deletes all the Process objects
		 *        representing unlinked and no more running processes.
		 *        For the running processes a check is done if the client is in the server ClientList
		 *        (this is needed in the case of a ProcessManager server failure; in a such a way the client
		 *        is added automatically to the new server ClientList if not already present).
		 *
		 * \param singleton_ptr Pointer to the Singleton instance.
		 *
		 * \return It returns always \c TRUE.
		 */

		friend bool doWhenPMGServerReady(void* singleton_ptr);

		/**
		 * \brief Function registered to be executed at exit using atexit(). It call _destroy() on the Singleton object.
		 */

		friend void singletonCleanup();

		/**
		 * \brief Used to set the client local host.
		 */

		const System::LocalHost* configure_local_host() const;

		/**
		 * \brief Used to set the name of the user the process will belong to. It is the name of the user
		 *        running the client.
		 *
		 * \throw daq::pmg::Bad_User_Name Error while getting the name of the current user.
		 */

		const std::string configure_login_name() const;

	private:

		typedef std::map<std::string, std::shared_ptr<pmg::Process>> process_table_t; ///< \brief Definition of a map containing all the processes.

        const std::string m_login_name; ///< \brief The name of the user running the client.
		const std::unique_ptr<const System::LocalHost> m_local_host; ///< \brief The client local host.
		std::unique_ptr<IPCAlarm> m_PMG_Server_Ready_Alarm; ///< \brief Pointer to the IPCAlarm.
		static Singleton* s_instance; ///< \brief Pointer to the Singleton instance.
		std::unique_ptr<ProxyTable> m_proxy_table; ///< \brief Pointer to the ProxyTable.
		IPCPartition m_global_partition; ///< \brief Pointer to the default IPC partition.
		process_table_t m_process_table; ///< \brief The map containing all the processes.
		mutable boost::recursive_mutex m_mutex; ///< \brief Mutex to protect access to the process map.
		static boost::shared_mutex m_static_mutex;
		unsigned int m_server_port;
		static bool m_failed_server_port;
		static std::string m_port_failed_reason;
		pmgpriv::CLIENT_var m_corba_ref;

}; // Singleton

bool doWhenPMGServerReady(void* singleton_ptr);
void singletonCleanup();

}
} // daq::pmg

#endif
