#ifndef PMG_PROCESS_IMPL_H
#define PMG_PROCESS_IMPL_H

#include <map>
#include <vector>
#include <memory>

#include "ProcessManager/defs.h"
#include "ProcessManager/Handle.h"

namespace daq {
  namespace pmg {

    class Process;
    class Proxy;
    class CallBack;

    /**
     * \class ProcessImpl ProcessImpl.h "ProcessManager/ProcessImpl.h"
     *
     * \brief This class implements all the functionalities of the Process class.
     *        It asks the Proxy to get some process info.
     *        This class must not be used directly by the user.
     *
     * \sa Process, Proxy.
     */

    class ProcessImpl {

    public:

      /**
       * \brief Constructor: it increases the Proxy counter.
       *
       * \param proxy Pointer to the process Proxy object.
       *
       * \sa Proxy, Proxy::incr().
       */

      ProcessImpl(const std::shared_ptr<Proxy>& proxy);

      /**
       * \brief The destructor: if the process is linked then it is unlinked and the
       *        Proxy counter is decremented. If the counter is zero the Proxy is deleted.
       *
       * \sa Proxy::decr(), Singleton::cleanup_unused_proxy(). 
       */

      ~ProcessImpl();

      bool operator==(const ProcessImpl &other) const;   ///< Just operator overloading.
      bool operator!=(const ProcessImpl &other) const;   ///< Just operator overloading.

      /**
       * \brief It gets the process Proxy.
       *
       * \return A pointer to the Proxy or \e 0 if the Proxy does not exist.
       */

      std::shared_ptr<Proxy> proxy() const;

      /**
       * \brief It asks the Proxy to get the process name.
       *
       * \return The process name.
       */

      const std::string name() const;

      /**
       * \brief It asks the Proxy to get the process handle.
       * 
       * \return The process handle.
       */

      const Handle& handle() const;

      /**
       * \brief It asks the Proxy to get process status information. This information is updated
       *        every time the process status changes.
       *
       * \return daq::pmg::PMGProcessStatusInfo structure containing process status information.
       *
       * \sa Proxy::info().
       */

      PMGProcessStatusInfo status() const;

      /**
       * \brief It asks the Proxy to get the executable path.
       *
       * \return The executable path.
       */

      const std::string path() const;

      /**
       * \brief It asks the Proxy to get the process environment.
       *
       * \return Map containing the process environment (variable->value).
       */

      std::map<std::string, std::string> env() const;

      /**
       * \brief It asks the Proxy to get the parameters passed to the process.
       *
       * \return String vector containing all the process parameters.
       */

      std::vector<std::string> params() const;

     /**
       * \brief It asks the Proxy to get the process resource usage.
       *
       * \return A daq::pmg::PMGResourceInfo containing the process resource usage information.
       *
       * \sa ProcessImpl::resource().
       */

      PMGResourceInfo resource() const;

      /**
       * \brief It checks if the process is linked or not. If it linked the pointer to the
       *        callback function is not \e NULL.
       *
       * \return \c TRUE if the process is still running.
       */

      bool is_linked() const;

      /**
       * \brief It asks the proxy about the process status.
       *
       * \return \c TRUE if the process exited.
       */

      bool exited() const;
      
      /**
       * \brief It creates a new CallBack object and stores its pointer into the ProcessImpl::m_callback_ptr
       *        variable. Then the callback is registered into the Proxy.
       *
       * \param callbackowner Pointer to the process to link.
       * \param callback The callback user function. Must be non-null.
       * \param callbackparameter Optional callback parameter to be
       *                          passed to the callback function during
       *                          the actual callback.
       *
       * \sa Proxy::link().
       */
      
      void link(const std::shared_ptr<Process>& callbackowner, CallBackFncPtr callback, void* callbackparameter = 0);

      /**
       * \brief The CallBack object is deleted and the callback is removed in the Proxy.
       *
       * \sa Proxy::unlink().
       */

      void unlink();

      /**
       * \brief It asks the Proxy to terminate the process.
       *
       * \sa Proxy::stop().
       */

      void stop();

      /**
       * \brief It asks the Proxy to kill the process.
       *
       * \sa Proxy::kill().
       */
      
      void kill();

      /**
       * \brief It asks the Proxy to "softly" kill the process.
       *
       * \param timeout The timeout in seconds.
       *
       * \sa Proxy::kill_soft().
       */

      void kill_soft(const int timeout);

      /**
       * \brief It aks the Proxy to send a \e POSIX signal to the process.
       *
       * \param signum \e POSIX signal number.
       *
       * \sa Proxy::signal()
       */

      void signal(const int signum);

      /**
       * \brief It asks the Proxy to get the content of the process error file.
       *
       * \param fileContent CORBA sequence whose buffer will contain the file contents.
       */

      void errFile(pmgpriv::File_var& fileContent) const;

      /**
       * \brief It asks the Proxy to get the content of the process output file.
       *
       * \param fileContent CORBA sequence whose buffer will contain the file contents.
       */

      void outFile(pmgpriv::File_var& fileContent) const;
      
    private:

      std::unique_ptr<CallBack>        m_callback_ptr;   ///< Pointer to the callback function.
      std::shared_ptr<Proxy>           m_proxy_ptr;      ///< Pointer to the process Proxy.

    }; // ProcessImpl
  } 
} // daq::pmg

#endif
