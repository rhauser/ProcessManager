#ifndef PMG_EXCEPTIONS_H
#define PMG_EXCEPTIONS_H

#include <string>

#include <ers/ers.h>

/**
 * \namespace daq
 *
 * \brief This is a wrapping namespace for all ProcessManager exceptions.
 */

namespace daq {

  /**
   * \class pmg::Exception
   *
   * \brief  This is a base class for all ProcessManager exceptions.
   */

    ERS_DECLARE_ISSUE(pmg, Exception, ERS_EMPTY, ERS_EMPTY)

      /*************************************************************/
      /*************ISSUES SENT TO ERROR/WARNING STREAM*************/
      /*************************************************************/

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Manifest_Not_Unmapped,
			   pmg::Exception,
			   "Failed unmapping the manifest " << manifest_name << " for application " << app_name << " in partition " << part_name << ". Reason: " << message,
			   ERS_EMPTY,
			   ((std::string) manifest_name)   // Manifest name.
			   ((std::string) app_name)        // Application name.
			   ((std::string) part_name)       // Partition name.
			   ((std::string) message)         // Failure reason.
			   )

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Manifest_Not_Mapped,
			   pmg::Exception,
			   "Failed mapping the manifest " << manifest_name << ". Reason: " << message,
			   ERS_EMPTY,
			   ((std::string) manifest_name)   // Manifest name.
			   ((std::string) message)         // Failure reason.
			   )

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Reconnection_Error,
			   pmg::Exception,
			   "Failed reconnecting to the application: " << message,
			   ERS_EMPTY,
			   ((std::string) message)        // Failuire reason.
			   )

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Launcher_Timeout,
			   pmg::Exception,
			   "Launcher for the app " << app_name << " (handle " << handle <<  ") not started within the " << timeout << " seconds timeout",
			   ERS_EMPTY,
			   ((std::string) app_name)
			   ((std::string) part_name)
			   ((std::string) handle)
			   ((int) timeout)
			   )

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Status_Update,
			   pmg::Exception,
			   "Failed updating the status for the application " << app_name << " (handle " << handle <<  ") . Reason: " << message,
			   ERS_EMPTY,
			   ((std::string) app_name)
			   ((std::string) part_name)
			   ((std::string) handle)
			   ((std::string) message)
			   )

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Launch_Process_Failed,
			   pmg::Exception,
			   "Error while launching process " << app_name << ". Reason: " << message,
			   ERS_EMPTY,
			   ((std::string) app_name)           // Process name.
			   ((std::string) part_name)
			   ((std::string) message)        // Failure reason.
			   )

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Launcher_Signal_Failed,
			   pmg::Exception,
			   "Signal " << signal << " to " << application << " failed. Reason: " << message,
			   ERS_EMPTY,
			   ((std::string) signal)         // POSIX signal.
			   ((std::string) application)    // Process name.
			   ((std::string) partition)
			   ((int) err_code)
			   ((std::string) message)        // Failuire reason.
			   )

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Launcher_Update_State_Error,
			   pmg::Exception,
			   "Error in sending message to the FIFO " << fifo_name << ". Reason " << message << ".The server will not be notified of process status update.",
			   ERS_EMPTY,
			   ((std::string) fifo_name)           // FIFO name.
			   ((std::string) app_name)
			   ((std::string) part_name)
			   ((int) err_code)
			   ((std::string) message)        // Failure reason.
			   )

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Launcher_FIFO_Error,
			   pmg::Exception,
			   "Error with the FIFO " << fifo_name << ". Reason " << message,
			   ERS_EMPTY,
			   ((std::string) fifo_name)           // FIFO name.
			   ((std::string) app_name)
			   ((std::string) part_name)
			   ((int) err_code)
			   ((std::string) message)        // Failure reason.
			   )

      /*************************************************************/
      /**********************CLIENT SIDE ISSUES*********************/
      /*************************************************************/

  /**
   * \class pmg::No_PMG_Server
   *
   * \brief Exception raised when the ProcessManager server is not found in IPC.
   */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   No_PMG_Server,
			   pmg::Exception,
			   "No PMG Server on host " << host << " found registered in IPC: " << message,
			   ERS_EMPTY,
			   ((std::string) host)    // ProcessManager server host.
			   ((std::string) message) // Failure reason.
			   )

  /**
   * \class pmg::Bad_PMG_Server
   *
   * \brief Exception raised when there is a failure while trying to contact the ProcessManager server.
   */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Bad_PMG_Server,
			   pmg::Exception,
			   "Failed to contact PMG Server on host " << host << ": " << message,
			   ERS_EMPTY,
			   ((std::string) host)     // ProcessManager server host.
			   ((std::string) message)  // Failure reason.
			   )


    /**
     * \class pmg::Failed_Start
     *
     * \brief Exception raised when there is a failure while trying to start the process.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Start,
			   pmg::Exception,
			   "Error while trying to start " << process << " on host " << host << ": " << message,
			   ERS_EMPTY,
			   ((std::string) process) // Process Name.
			   ((std::string) host) // Host Name
			   ((std::string) message) // Failure reason.
			   )

    /**
     * \class pmg::Process_Already_Exited
     *
     * \brief Exception raised when a signal is not sent to a process because the process already exited
     *        (the manifest has already been unmapped, the Application is not found by the server or it is invalid,
     *        the associated launcher is not running)
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Process_Already_Exited,
			   pmg::Exception,
			   "Failed to signal process " << proc_handle << " because already exited: " << reason,
			   ERS_EMPTY,
			   ((std::string)proc_handle) // Process handle
			   ((std::string)proc_name)
			   ((std::string)reason) // Error message
			   )

    /**
     * \class pmg::Signal_Not_Allowed
     *
     * \brief Exception raised when a signal is not sent to a process because not allowed by the AccessManager
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Signal_Not_Allowed,
			   pmg::Exception,
			   "Failed to signal process " << proc_handle << " because not allowed by AM: " << reason,
			   ERS_EMPTY,
			   ((std::string)proc_handle) // Process handle
			   ((std::string)proc_name)
			   ((std::string)reason) // Error message
			   )
    /**
     * \class pmg::Server_Internal_Error
     *
     * \brief Exception raised when a signal is not sent to a process because some error occurred in the server while
     *        processing the request (i.e., not able to write/open the report FIFO
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Server_Internal_Error,
			   pmg::Exception,
			   "Failed to signal process " << proc_handle << " because of an internal server error : " << reason,
			   ERS_EMPTY,
			   ((std::string)proc_handle) // Process handle
			   ((std::string)proc_name)
			   ((std::string)reason) // Error message
			   )

    /**
     * \class pmg:"Failed_Kill_Partition
     *
     * \brief Exception raised by the Singleton::kill_partition method when some error occurs while killing a process in a partition.
     *        The \a proc_handles string contains the handles of the process for which something has gone wrong. The handles are
     *        separated by the pmg::ProcessManagerSeparator string - defined in defs.h.
     *        The \a host_list string contains the name of agents for which a communication error occurred.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Kill_Partition,
			   pmg::Exception,
			   "Some errors occurred while killing partition " << part_name,
			   ERS_EMPTY,
			   ((std::string)part_name)  // Partition name
			   ((std::string)proc_handles)  // Handle list
			   ((std::string)host_list) // Host list
			   )

   /**
    * \class pmg::Failed_Lookup
    *
    * \brief Exception raised by Singleton::lookup(const std::string& hostname, const std::string& appname, const std::string& partname).
    *        It means that some error occurred server side while processing the client request.
    */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Lookup,
			   pmg::Exception,
			   "Error while trying to lookup process " << process << " in partition " << partition << ": " << message,
			   ERS_EMPTY,
			   ((std::string) process) // Name of the process
			   ((std::string) partition)
			   ((std::string) message) // Error message
			   )

   /**
    * \class pmg::Failed_Exists
    *
    * \brief Exception raised by Singleton::exists_somewhere().
    *        It means that some error occurred server side while processing the client request.
    */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Exists,
			   pmg::Exception,
			   "Error while executing exists_somewhere call for process " << process << " in partition " << partition << ": " << message,
			   ERS_EMPTY,
			   ((std::string) process) // Name of the process
			   ((std::string) partition)
			   ((std::string) message) // Error message
			   )

   /**
    * \class pmg::Failed_Get_Process
    *
    * \brief Exception raised by Singleton::get_process().
    *        It means that some error occurred server side while processing the client request.
    */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Get_Process,
			   pmg::Exception,
			   "Error while trying to get process " << process << " in partition " << partition << ": " << message,
			   ERS_EMPTY,
			   ((std::string) process) // Name of the process
			   ((std::string) partition)
			   ((std::string) message) // Error message
			   )

   /**
    * \class pmg::Failed_Listing_Server
    *
    * \brief Exception raised by Singleton::askRunningProcesses(const std::string& hostname, const std::string& partition, std::string& procList).
    *        It means that some error occurred server side while processing the client request.
    */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Listing_Server,
			   pmg::Exception,
			   "Some errors occurred while trying to get process list on host " << host_name << ": " << message,
			   ERS_EMPTY,
			   ((std::string)host_name) // Hostname
			   ((std::string)message)   // Error reason
			   )

   /**
    * \class pmg::Failed_Listing_Partition
    *
    * \brief Exception raised by Singleton::askRunningProcesses(const std::string& partition, std::string& procList).
    *        It means that some ProcessManager server had troubles answering the received request.
    *        The \a agent_list string contains the name of agents for which something has gone wrong. The names are
    *        separated by the pmg::ProcessManagerSeparator string - defined in defs.h.
    */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Listing_Partition,
			   pmg::Exception,
			   "Some errors occurred while trying to get process list for partition " << partition,
			   ERS_EMPTY,
			   ((std::string)partition)  // Partition name
			   ((std::string)agent_list) // Agent list
			   )

    /**
     * \class pmg::No_Agents
     *
     * \brief Exception raised when some error occurs while asking to \e IPC the list of all the agents
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   No_Agents,
			   pmg::Exception,
			   "Failed getting the agents list from IPC " << ": " << message,
			   ERS_EMPTY,
			   ((std::string)message) // Error message
			   )

    /**
     * \class pmg::Failed_Multiple_Lookup
     *
     * \brief Exception raised by Singleton::lookup(const std::string& appname, const std::string& partname).
     *        It means that some ProcessManager server had troubles answering the received request.
     *        The \a agent_list string contains the name of agents for which something has gone wrong. The names are
     *        separated by the pmg::ProcessManagerSeparator string - defined in defs.h.
     *        If the \a handle string is not empty than the process handle was received by some host.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Multiple_Lookup,
			   pmg::Exception,
			   "Some errors occurred while trying to lookup for process " << process << " on multiple hosts",
			   ERS_EMPTY,
			   ((std::string)process)    // Process name
			   ((std::string)handle)     // Found handle
			   ((std::string)agent_list) // Agent list
			   )

    /**
     * \class pmg::Already_Init
     *
     * \brief Exception raised when calling Singleton::init() twice.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Already_Init,
			   pmg::Exception,
			   "Singleton already initialized",
			   ERS_EMPTY, ERS_EMPTY
			   )

    /**
     * \class pmg::Not_Initialized
     *
     * \brief Exception raised when the Singleton::init() method has not been called.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Not_Initialized,
			   pmg::Exception,
			   "Singleton not initialized: Singleton::init() has not been called yet",
			   ERS_EMPTY, ERS_EMPTY
			   )

    /**
     * \class pmg::Cannot_Get_File
     *
     * \brief Exception raised when calling Process::errFile() or Process::outFile().
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Cannot_Get_File,
			   pmg::Exception,
			   "Cannot get file " << fileName << ". Reason: " << reason,
			   ERS_EMPTY,
			   ((std::string)fileName) // File name
			   ((std::string)reason)   // Error reason
			   ((long)code)            // The error code (errno)
			   )


      /*************************************************************/
      /**********************SERVER SIDE ISSUES*********************/
      /*************************************************************/

    /**
     * \class pmg::Failed_Start_Thread
     *
     * \brief Exception raised when there is a failure while trying to start the thread reading on the report FIFO.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Start_Thread,
			   pmg::Exception,
			   "Error while trying to start the thread reading on the report FIFO for process "
			          << process << " in partition " << partition << ": " << message << ". Application control lost!!!",
			   ERS_EMPTY,
			   ((std::string) process) // Process Name.
			   ((std::string) partition)
			   ((std::string) message) // Failure reason.
			   )

    /**
     * \class pmg::Failed_Publish_IS
     *
     * \brief Exception raised when there is a failure while trying to publish process information in IS.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Publish_IS,
			   pmg::Exception,
			   "Error publishing " << who << " info in IS: " << reason,
			   ERS_EMPTY,
			   ((std::string)who) // Who does the information refer to? It can be the agent or the process.
			   ((std::string)reason) // Failure reason.
			   )

    /**
     * \class pmg::Failed_Remove_IS
     *
     * \brief Exception raised when there is a failure while trying to remove process information from IS.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Remove_IS,
			   pmg::Exception,
			   "Error removing " << who << " info from IS: " << reason,
			   ERS_EMPTY,
			   ((std::string)who) // Who does the information refer to? It can be the agent or the process.
			   ((std::string)reason) // Failure reason.
			   )

    /**
     * \class pmg::Failed_Create_IS
     *
     * \brief Exception raised when there is a failure while trying to create process information in IS.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Create_IS,
			   pmg::Exception,
			   "Error creating ISBridge object for the " << who << ": " << reason,
			   ERS_EMPTY,
			   ((std::string)who) // Who does the information refer to? It can be the agent or the process.
			   ((std::string)reason) // Failure reason.
			   )

    /**
     * \class pmg::Failed_Collect_Info
     *
     * \brief Exception raised when there is a failure in collecting process/agent information
     *        to be published in IS.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Collect_Info,
			   pmg::Exception,
			   "Error collecting object information: " << reason,
			   ERS_EMPTY,
			   ((std::string)reason) // Failure reason.
			   )
    /**
     * \class pmg::No_RM_Resources
     *
     * \brief Exception raised when no process resources are available in RM.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   No_RM_Resources,
			   pmg::Exception,
			   "Failed to start " << app_name << " in partition " << partition <<  " because resources are not granted by the RM: " << reason,
			   ERS_EMPTY,
			   ((std::string)app_name) // Application name
			   ((std::string)partition)
			   ((std::string)swobject)
			   ((std::string)reason)   // Reason why no resources are granted by the RM
			   )

    /**
     * \class pmg::RM_Resources_NotFreed
     *
     * \brief Exception raised when RM resources cannot be freed.
     */

	ERS_DECLARE_ISSUE_BASE(pmg,
		   RM_Resources_NotFreed,
		   pmg::Exception,
		   "Failed to free resources for application with PID " << app_pid << " in partition " << partition <<  ": " << reason,
		   ERS_EMPTY,
		   ((unsigned long)app_pid) // Application PID
		   ((std::string)partition)
		   ((std::string)reason)   // Reason why no resources are granted by the RM
		   )


    /**
     * \class pmg::AM_Not_Allowed
     *
     * \brief Exception raised when the AccessManager denies an action on a process.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   AM_Not_Allowed,
			   pmg::Exception,
			   "Failed to start/signal " << app_name << " in partition " << partition_name << " because not allowed by AM: " << message,
			   ERS_EMPTY,
			   ((std::string)app_name) // Application name
			   ((std::string)partition_name) // Partition
			   ((std::string)message)  // AM Status Message
			   )

    /**
     * \class pmg::Binary_Not_found
     *
     * \brief Exception raised when the binary for the application to be started is not found.
     */

	ERS_DECLARE_ISSUE_BASE(pmg,
			   Binary_Not_Found,
			   pmg::Exception,
			   "Failed to find binary for application " << app_name << " in partition " << partition_name << " (binary list is \"" << exec_list << "\")",
			   ERS_EMPTY,
			   ((std::string)app_name) // Application name
			   ((std::string)partition_name) // Partition
			   ((std::string)exec_list)  // Exec list
			   )

    /**
     * \class pmg::Application_Not_Found
     *
     * \brief Exception raised when the server is asked to signal an application but the Application object is not found
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Application_NotFound_Invalid,
			   pmg::Exception,
			   "Failed to send signal to " << app_name << " in partition " << part_name << " because the application has not been found or is not valid",
			   ERS_EMPTY,
			   ((std::string)app_handle) // Application handle
			   ((std::string)app_name)
			   ((std::string)part_name)
			   )

    /**
     * \class pmg::Application_Manifest_Already_Unmapped
     *
     * \brief Exception raised when the server is asked to signal an application but the Application has already exited
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Application_Manifest_Already_Unmapped,
			   pmg::Exception,
			   "Failed to send signal to " << app_name << " in partition " << part_name << " because the application manifest has been already unmapped",
			   ERS_EMPTY,
			   ((std::string)app_handle) // Application handle
			   ((std::string)app_name)
			   ((std::string)part_name)
			   )

    /**
     * \class pmg::Launcher_Not_Running
     *
     * \brief Exception raised when the server is asked to signal an application but the associated launcheris not running
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Launcher_Not_Running,
			   pmg::Exception,
			   "Failed to send signal to " << app_handle << " in partition " << part_name << " because the associated launcher is not running",
			   ERS_EMPTY,
			   ((std::string)app_handle) // Application name
			   ((std::string)app_name)
			   ((std::string)part_name)
			   )

    /**
     * \class pmg::FIFO_Error
     *
     * \brief Exception raised when the server is asked to signal an application but an error occurred while writing in report FIFO
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   FIFO_Error,
			   pmg::Exception,
			   "Failed to send signal to " << app_name << " in partition " << part_name << " because not able to write on the control FIFO. Reason: " << reason,
			   ERS_EMPTY,
			   ((std::string)app_handle) // Application handle
			   ((std::string)app_name)
			   ((std::string)part_name)
			   ((std::string)reason) // Error message
			   )


    /**
     * \class pmg::ApplicationList_Signal_Exception
     *
     * \brief Exception raised in the ApplicationList class when sending a signal to all the processes belonging to the it.
     *        The \a proc_handles string contains the handles of the process for which something has gone wrong. The handles are
     *        separated by the pmg::ProcessManagerSeparator string - defined in defs.h.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   ApplicationList_Signal_Exception,
			   pmg::Exception,
			   "Some errors occurred while sending signals to the ApplicationList " << app_list_name,
			   ERS_EMPTY,
			   ((std::string)app_list_name) // Name of the ApplicationList
			   ((std::string)proc_handles) // String containing process handles
			   )

    /**
     * \class pmg::Partition_Signal_Exception
     *
     * \brief Exception raised in the Partition class when sending a signal to all the processes belonging to the it.
     *        The \a proc_handles string contains the handles of the process for which something has gone wrong. The handles are
     *        separated by the pmg::ProcessManagerSeparator string - defined in defs.h.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Partition_Signal_Exception,
			   pmg::Exception,
			   "Some errors occurred while sending signals to the applications in partition " << part_name,
			   ERS_EMPTY,
			   ((std::string)part_name)     // Partition name
			   ((std::string)proc_handles)  // String containing process handles
			   )

    /**
     * \class pmg::Failed_Kill_All
     *
     * \brief Exception raised by Daemon::kill_all.
     *        The \a proc_handles string contains the handles of the process for which something has gone wrong. The handles are
     *        separated by the pmg::ProcessManagerSeparator string - defined in defs.h.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Failed_Kill_All,
			   pmg::Exception,
			   "Some errors occurred while killing all the processes",
			   ERS_EMPTY,
			   ((std::string)proc_handles) // String containing process handles
			   )


    /**
     * \class pmg::Internal_Server_Error
     *
     * \brief Exception raised when the pmgserver cannot start a process because of an internal error..
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Internal_Server_Error,
			   pmg::Exception,
			   "Internal server error: " << reason,
			   ERS_EMPTY,
			   ((std::string)reason) // String containing process handles
			   )

	ERS_DECLARE_ISSUE_BASE(
	        pmg,
	        CannotNotifyClient,
	        pmg::Exception,
	        "Cannot notify client \"" << clientName << "\" for process with handle \"" << handle << "\"; reason is: " << reason,
	        ERS_EMPTY,
	        ((std::string) clientName)
	        ((std::string) handle)
	        ((std::string) reason)
	)

      /*************************************************************/
      /************************COMMON ISSUES************************/
      /*************************************************************/

    /**
     * \class pmg::Port_Error
     * \brief Exception raised in daq::pmg::utils::serverPort().
     *        This exception is raised when the client or the server is not able to set the port for the CORBA communication.
     *        This happens if the TDAQ_INST_PATH variable cannot be resolved or if the file with the port number exists but some
     *        error happens while trying to read it.
     *        This issue may be actually raised in the Singleton constructor.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   IPC_Port_Error,
			   pmg::Exception,
			   "Error while setting the port number for server/client communication: " << reason,
			   ERS_EMPTY,
			   ((std::string)reason) // Reason
			   )

    /**
     * \class pmg::Bad_User_Name
     *
     * \brief Exception raised if some error occurs when getting the name of the current user.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Bad_User_Name,
			   pmg::Exception,
			   "Error while getting the user name: " << reason,
			   ERS_EMPTY,
			   ((std::string)reason) // Reason
			   )

    /**
     * \class pmg::Invalid_CORBA_Ref
     *
     * \brief Exception raised when some error occurs while calling the CORBA _this() method.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Invalid_CORBA_Ref,
			   pmg::Exception,
			   "Error while getting object CORBA reference: " << reason,
			   ERS_EMPTY,
			   ((std::string)reason) // Reason
			   )

    /**
     * \class pmg::Invalid_CORBA_Ref
     *
     * \brief Exception raised when some error occurs while calling the CORBA _this() method.
     */

    ERS_DECLARE_ISSUE_BASE(pmg,
			   Invalid_Handle,
			   pmg::Exception,
			   "Inavalid process handle: " << reason,
			   ERS_EMPTY,
			   ((std::string)reason) // Reason
			   )


    } // namespace daq

#endif
