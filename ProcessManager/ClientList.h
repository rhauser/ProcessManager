#ifndef PMG_CLIENTLIST_H
#define PMG_CLIENTLIST_H

#include "ProcessManager/Handle.h"
#include "ProcessManager/defs.h"
#include "ProcessManager/Manifest.h"

#include <pmgpriv/pmgpriv.hh>

namespace daq {
  namespace pmg {

    class Manifest ;

    /**
     * \class ClientList ClientList.h "ProcessManager/ClientList.h"
     *  
     * \brief This class manages the list of clients to be notified for a process status change.
     */

    class ClientList {

    public:
    
      /**
       * \brief Empty constructor.
       */

      ClientList();

      /**
       * \brief Empty destructor.
       */
      
      ~ClientList();

      /**
       * \brief It checks if a client is already present in the list.
       *
       * \param client_ref Client \e CORBA reference.
       *
       * \return \c TRUE if the client is already in the list, otherwise \c FALSE.
       */

      bool check(const pmgpriv::CLIENT_var& client_ref) const;

      /**
       * \brief It adds a client to the list.
       *
       * \param client_ref Client \e CORBA reference.
       */

      void add(const pmgpriv::CLIENT_var& client_ref);

      /**
       * \brief It removes the client from the list.
       *
       * \param client_ref Client \e CORBA reference.
       */

      void remove(const pmgpriv::CLIENT_var& client_ref);
      
      /**
       * \brief This method informs all the clients in the list of a process status update.
       *
       *        For all the clients in the list the registered callback function is called (if any).
       *        The \a handle and \a manifest are used to get the current process information and
       *        fill a pmgpub::ProcessStatusInfo structure. This structure can be used in the user
       *        callback function to get process information (remember that the Process pointer
       *        is always passed to the callback function). It is called be Application::notify_update().
       *
       * \param handle Handle describing the process.
       * \param manifest Pointer to the manifest containing process information.
       * \param state Updated process status.
       *
       * \sa Singleton::callback(), CallBack.h, Proxy.h, Singleton.h
       */

      void notify_update(const Handle&   handle,
			 const Manifest* manifest, 
			 const PMGProcessState state) const;

    protected:
      
      std::list<pmgpriv::CLIENT_var> m_clientList;   ///< List containing all the client \e CORBA references.
    
    };
  } 
} // daq::pmg

#endif
