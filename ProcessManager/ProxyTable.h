#ifndef PMG_PROXY_TABLE_H
#define PMG_PROXY_TABLE_H

#include <boost/thread/recursive_mutex.hpp>

#include "ProcessManager/defs.h"
#include "ProcessManager/Handle.h"

#include <memory>

namespace daq {
  namespace pmg {

    class Proxy;

    /**
     * \class ProxyTable ProxyTable.h "ProcessManager/ProxyTable.h"
     *
     * \brief The ProxyTable class is instantiated once by each Singleton
     *        to take care of managing all Proxy instances created here.
     *
     *        The ProxyTable class creates Proxy objects as needed. It receives
     *        notification of process callbacks and process start request completions,
     *        and is responsible for finding/creating the right Proxy, forwarding
     *        callbacks. ProxyTable also provides query methods to retrieve the
     *        Proxy for a certain process handle or to retrieve all stored Proxy
     *        instances (e.g., to stop all processes).
     */

    class ProxyTable {

    public:

      /**
       * \brief It creates a new, empty ProxyTable. Called by the Singleton.
       */

      ProxyTable();

      /**
       * \brief The destructor.
       */

      ~ProxyTable();

      /**
       * \brief Called by Singleton in the ProcessDescriptionImpl::start() method just
       *        after receiving the process handle by the ProcessManager server.
       *        This happens before the process is started and hence before the first process callback.
       *        This method creates the Proxy for the process to be watched if it has not been
       *        created yet.
       *        It is also called by Singleton in Singleton::get_process() when a client tries
       *        to connect to a process not started by itself.
       *
       * \param handle The handle of the process to be watched, as returned
       *               by the \e CORBA call on the remote ProcessManager server.
       *
       * \return The Proxy corresponding to the passed handle.
       *
       * \sa ProcessDescriptionImpl::start(), Singleton::start_finished(), Singleton::get_process().
       */

      std::shared_ptr<Proxy> factory(const pmg::Handle& handle);

      /**
       * \brief Called by Singleton when a process callback arrives.
       *        It looks for the proxy, creates it if needed and updates
       *        it with the new status info.
       *
       * \param handle The handle of the process being updated.
       * \param info The newest status information of the process
       *             referenced by the passed Handle.
       *
       * \return The proxy corresponding to the passed handle.
       *
       * \sa Singleton::callback().
       */

      void callProxyCallbacks(const std::string& handle, const PMGProcessStatusInfo& info);

      /**
       * \brief It implements the same functionality as ProxyTable::callProxyCallbacks(const std::string& handle, const PMGProcessStatusInfo& info)
       *        with the difference that only the state of the process in updated.
       *
       *        Note that no Proxy is created if it does not exist.
       *
       * \param handle The handle of the process being updated
       * \param state  The new state of the process
       *
       * \sa Singleton::doWhenPMGServerReady(void* pSingleton)
       */
      void callProxyCallbacks(const std::string& handle, PMGProcessState state);

      /**
       * \brief It removes the proxy from the table and deletes it.
       *
       * \param handle The process handle corresponding to the Proxy to be removed.
       */

      void remove(const std::string& handle);

      /**
       * \brief It looks up the Proxy object for a given Handle.
       *
       * \return A pointer to a Proxy object or \e NULL if no corresponding
       *         Proxy could be found.
       *
       * \param handle The process handle. 
       */
      
      std::shared_ptr<Proxy> find(const std::string& handle) const;

      /**
       * \brief Returns all stored proxies.
       *
       * \return A non-null list (possibly empty) of Proxy pointers.
       */

      std::list<std::shared_ptr<Proxy>> find_all() const;

    private:

      std::map<std::string, std::shared_ptr<Proxy>>     m_proxyMap;   ///< \brief Map containing all the proxies associated to the process handles they represent.
      mutable boost::recursive_mutex                    m_mutex;

    }; // ProxyTable

    typedef std::map<std::string, std::shared_ptr<Proxy>>::iterator       proxyMapIterator;        ///< \brief Iterator over the proxy map.
    typedef std::map<std::string, std::shared_ptr<Proxy>>::const_iterator proxyMapConstIterator;   ///< \brief Constant iterator over the proxy map.

  } 
} // daq::pmg

#endif
