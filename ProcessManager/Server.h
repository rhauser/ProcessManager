#ifndef PMG_SERVER_H
#define PMG_SERVER_H

#include <ipc/object.h>
#include <owl/semaphore.h>

#include <pmgpriv/pmgpriv.hh>

#include "ProcessManager/defs.h"
#include "ProcessManager/Exceptions.h"

#include <system/System.h>

#include <string_view>

class IPCPartition;
class IPCAlarm;

namespace daq {
namespace pmg {

class Daemon;

/**
 * \class Server Server.h "ProcessManager/Server.h"
 *
 * \brief This class encapsulates the functionality of the Server part of the ProcessManager.
 *        It exports some methods to the \e IDL \e CORBA interface.
 *        Almost all methods are implemented by the Daemon class.
 */

class Server: public IPCNamedObject<POA_pmgpriv::SERVER, ::ipc::multi_thread, ::ipc::persistent>
{

	public:

		/**
		 * \brief The constructor: it publishes the ProcessManager in the IPC server and starts an alarm
		 *        to test if the global IPC server is up (if it is up but the ProcessManager is not published then it does it).
		 *        Before publishing in IPC it tries to reconnect to already existing processes (this is needed after
		 *        a failure). To do that the file system is scanned looking for valid Manifest files. A Manifest is
		 *        considered as valid if the Launcher associated to it is alive and the report and control FIFOs
		 *        are available too.
		 *
		 * \param server_id The Process manager server name (i.e., AGENT_FullHostName).
		 * \param directory The main directory used to store manifests and FIFOs.
		 * \param port The port the agent is listening to (if zero, than the port is not fixed)
		 * \param canExit Whether to allow the agent to exit or not on an explicit user request
		 *
		 * \throw daq::pmg::Bad_User_Name Error in getting the name of the current user.
		 *
		 * \sa Manifest, Launcher.
		 */

		Server(const std::string& server_id,
		       const System::File& directory,
		       const unsigned int port,
		       bool canExit);

		/**
		 * \brief The destructor: it unpublishes the ProcessManager from IPC and stop all the report threads.
		 */

		~Server();

		/**
		 * \brief It stops the server.
		 */

		void shutdown();

		/**
		 * \brief Simple control method (to check if the server is alive).
		 *
		 * \return It returns always \c TRUE.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 */

		bool alive();

		/**
		 * \brief It executes Daemon::request_start() and it is called by ProcessDescriptionImpl::start().
		 *
		 * \return The handle of the process to be started.
		 *
		 * \param requestInfo Structure containing the information needed to start the process.
		 *
		 * \throw pmgpriv::start_ex IDL exception raised if some error occurred.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa For a detailed description see Daemon::request_start().
		 */

		char* request_start(const pmgpriv::ProcessRequestInfo& requestInfo);

		/**
		 * \brief It executes Daemon::really_start() and it is called by ProcessDescriptionImpl::start().
		 *
		 * \param handle The handle of the process to start.
		 *
		 * \throw pmgpriv::start_ex IDL exception raised if some error occurred.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa For a detailed description see Daemon::really_start().
		 */

		void really_start(const char* handle);

		/**
		 * \brief A \e POSIX signal is sent to the process.
		 *        It executes Daemon::signal() and it is called by Proxy::signal().
		 *        The user usually uses it by means of the Process class (Process::signal()).
		 *
		 * \param handle The process handle.
		 * \param signum The \e POSIX signal number.
		 * \param userName The name of the user requesting the action.
		 * \param hostName The name of the host the request comes from.
		 *
		 * \throw pmgpriv::signal_ex IDL exception raised if some error occurred.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa For a detailed description see Daemon::signal().
		 */

		void signal(const char* handle,
		            const CORBA::Long signum,
		            const char* userName,
		            const char* hostName);

		/**
		 * \brief The process is terminated via a \e SIGTERM.
		 *        It executes Daemon::stop() and it is called by Proxy::stop().
		 *        The user usually uses it by means of the Process class (Process::stop()).
		 *
		 * \param handle The process handle.
		 * \param userName The name of the user requesting the action.
		 * \param hostName The name of the host the request comes from.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \throw pmgpriv::signal_ex IDL exception raised if some error occurred.
		 *
		 * \sa For a detailed description see Daemon::stop().
		 */

		void stop(const char* handle, const char* userName, const char* hostName);

		/**
		 * \brief The process is terminated via a \e SIGKILL.
		 *        It executes Daemon::kill() and it is called by Proxy::kill().
		 *        The user usually uses it by means of the Process class (Process::kill()).
		 *
		 * \param handle The process handle.
		 * \param userName The name of the user requesting the action.
		 * \param hostName The name of the host the request comes from.
		 *
		 * \throw pmgpriv::signal_ex IDL exception raised if some error occurred.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa For a detailed description see Daemon::kill().
		 */

		void kill(const char* handle, const char* userName, const char* hostName);

		/**
		 * \brief It terminates the process in a "soft" way.
		 *        It executes Daemon::kill_soft() and it is called by Proxy::kill_soft().
		 *        The user usually uses it by means of the Process class (Process::kill_soft()).
		 *
		 * \param handle The process handle.
		 * \param timeout See Daemon::kill_soft().
		 * \param userName The name of the user requesting the action.
		 * \param hostName The name of the host the request comes from.
		 *
		 * \throw pmgpriv::signal_ex IDL exception raised if some error occurred.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa For a detailed description see Daemon::kill_soft().
		 */

		void kill_soft(const char* handle,
		               const CORBA::Long timeout,
		               const char* userName,
		               const char* hostName);

		/**
		 * \brief It "softly" kills all the started process belonging to a particular partition.
		 *        It executes Daemon::kill_partition() and it is available to the user
		 *        via the Singleton class (Singleton::kill_partition()).
		 *
		 * \return \c FALSE If the partition has not been found.
		 *
		 * \param partition The name of the partition.
		 * \param timeout See Daemon::kill_soft().
		 * \param userName The name of the user requesting the action.
		 * \param hostName The name of the host the request comes from.
		 *
		 * \throw pmgpriv::kill_partition_ex IDL exception raised if some error occurred
		 *        while killing a process in the partition.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa For a detailed description see Daemon::kill_partition().
		 */

		bool kill_partition(const char* partition,
		                    const CORBA::Long timeout,
		                    const char* userName,
		                    const char* hostName);

		/**
		 * \brief It "softly" kills all the started processes.
		 *        It executes Daemon::kill_all().
		 *
		 * \param timeout See Daemon::kill_soft().
		 * \param userName The name of the user requesting the action.
		 * \param hostName The name of the host the request comes from.
		 *
		 * \throw pmgpriv::kill_all_ex IDL exception raised if some error occurred while killing
		 *        a process.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa For a detailed description see Daemon::kill_all().
		 */

		void kill_all(const CORBA::Long timeout, const char* userName, const char* hostName);

		/**
		 * \brief It looks for a process in a particular partition.
		 *        It executes Daemon::lookup() and is used by Singleton::lookup().
		 *
		 * \return The handle of the process (or an empty string if it has not been found).
		 *
		 * \param app_name Name of the process as defined in ProcessDescription.
		 * \param part_name Name of the partition.
		 *
		 * \throw pmgpriv::lookup_ex IDL exception raised if some error occurred.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa For a detailed description see Daemon::lookup().
		 */

		char* lookup(const char* app_name, const char* part_name);

		/**
		 * \brief It looks for all the processes in a partition.
		 *        It executes Daemon::processes().
		 *
		 * \return A string containing the handles of all the processes (they are separated by the pmg::ProcessManagerSeparator string - defined in defs.h).
		 *         The string is empty if no process has been found or the requested partition does not exist.
		 *
		 * \param partition Name of the partition the processes should belong.
		 *
		 * \throw pmgpriv::processes_ex IDL exception raised if some error occurred.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa Daemon::processes().
		 */

		char* processes(const char* partition);

		/**
		 * \brief It looks for all the processes in a partition.
		 *        It executes Daemon::processes_info().
		 *
		 * \param procInfoList List containing the handles of all the processes and their corresponding information.
		 *         The list is empty if no process has been found or the requested partition does not exist.
		 *
		 * \param partition Name of the partition the processes should belong.
		 *
		 * \throw pmgpriv::processes_ex IDL exception raised if some error occurred.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa Daemon::processes_info().
		 */

		void processes_info(const char* partition, pmgpriv::proc_info_list_out procInfoList);

		/**
		 * \brief It looks for all the running processes in all the partitions.
		 *        It executes Daemon::allprocesses().
		 *
		 * \return A string containing the handles of all the processes (they are separated by the pmg::ProcessManagerSeparator string - defined in defs.h).
		 *         The string is empty if no process has been found.
		 *
		 * \throw pmgpriv::all_processes_ex IDL exception raised if some error occurred.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa Daemon::all_processes().
		 */

		char* all_processes();

		/**
		 * \brief It looks for all the running processes in all the partitions.
		 *        It executes Daemon::all_processes_info().
		 *
		 * \param  procInfoList List containing the handles of all the processes and their corresponding information.
		 *         The list is empty if no process has been found.
		 *
		 * \throw pmgpriv::all_processes_ex IDL exception raised if some error occurred.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa Daemon::all_processes_info().
		 */

		void all_processes_info(pmgpriv::proc_info_list_out procInfoList);

		/**
		 * \brief It checks if a certain process exists.
		 *        It calls Daemon::exists() and it is used by Singleton::exists_somewhere().
		 *
		 * \return \c TRUE If the process exists.
		 *
		 * \param handle The process handle.
		 *
		 * \throw pmgpriv::exists_ex IDL exception raised if some error occurred.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa For a detailed description see Daemon::exists().
		 */

		bool exists(const char* handle);

		/**
		 * \brief It adds a client to a certain application ClientList.
		 *        It calls Daemon::link_client().
		 *
		 * \param requestInfo Structure containing information about the application and the client.
		 *
		 * \return \c TRUE If the client has been added to the ClientList, \c FALSE if not (i.e., the
		 *         client was already in the list).
		 *
		 * \throw pmgpriv::link_client_ex IDL exception raised if some error occurred.
		 *        pmgpriv::app_not_found_ex IDL exception raised if the application does not exist anymore.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa For a detailed description see Daemon::link_client().
		 */

		bool link_client(const pmgpriv::LinkRequestInfo& requestInfo);

		/**
		 * \brief It gets all available information about a process.
		 *        It calls Daemon::get_info() and it is used in Singleton::get_process()
		 *        when a client tries to connect to a process started by somebody else.
		 *
		 * \param handle The process handle.
		 * \param info The structure to be filled with process information.
		 *
		 * \throw pmgpriv::get_info_ex IDL exception raised if some error occurred.
		 *
		 * \note This method is exported in idl and it is available for remote calls.
		 *
		 * \sa For a detailed description see Daemon::get_info().
		 */

		bool get_info(const char* handle, pmgpub::ProcessStatusInfo_out info);

		/**
		 * \brief It gets the content of the file whose name is \a fileName.
		 *        This method compares the value of \a maxMsgSize passed by the client and of omniORB::giopMaxMsgSize()
		 *        for this server and calculates the maximum number of bytes that can be transferred. If this value is
		 *        smaller than the file size, then only the tail of the file is transferred.
		 *
		 * \param fileName The name of the file to retrieve.
		 * \param maxMsgSize The client max allowed CORBA message size.
		 * \param binFile CORBA sequence whose internal buffer will contain the file contents.
		 *
		 * \throw pmgpriv::procFiles_ex IDL exception raised if some error occurred.
		 */

		void procFiles(const char* fileName, CORBA::Long maxMsgSize, pmgpriv::File_out binFile);

		/**
		 * \brief It gets some info about this agent and the host where the agent is running on.
		 *
		 * \param info CORBA structure containing requested info.
		 *
		 * \throw pmgpriv::agentInfo_ex IDL exception raised if some error occurred.
		 */

		void agentInfo(pmgpub::AgentInfo_out info);

		/**
		 * \brief It will cause the server to exit (only if the pmgserver is started with the "-k" option)
		 *
		 * \throws pmgpriv::exit_ex The pmgserver will not exit because not started with the right command line flag
		 */
		void exit();

		/**
		 * \brief Method to get the name of the user running the ProcessManager server.
		 *
		 * \return The user name as a string.
		 */

		void run();

		static const std::string& login_name();

	protected:

		/**
		 * \brief It checks if the global IPC server is ready.
		 *
		 * \return \c TRUE If the gloabl IPC server is ready.
		 */

		bool globalIPCServerReady();

		/**
		 * \brief It checks if the ProcessManager reference in IPC is valid.
		 *
		 * \return \c TRUE If the ProcessManager reference in IPC is valid.
		 */

		bool checkMyIPCReference();

		/**
		 * \brief It publishes the ProcessManager in IPC.
		 */

		void publishInGlobalIPC();

		/**
		 * \brief It gets the IPC reference file.
		 *
		 * \return \c TRUE If the file has been found.
		 */

		bool getIPCRefValue();

		/**
		 * \brief It connects to already existing applications when the ProcessManager server starts.
		 *        This is needed after a server failure to not lose the control of running processes.
		 *        The direcory \a dirName (and all its sub-dirs) is scanned looking for valid manifest files. If the Launcher
		 *        holding the manifest is still running then the manifest information is used to build the
		 *        Application objects describing the processes (in particular all the needed threads reading
		 *        the report FIFO are spawn to allow process status change notifications).
		 *
		 * \param dirName Name of the directory where manifest files are looked for.
		 */

		void connectLaunchers(const std::string& dirName);

		/**
		 * \brief It scans \a dirName (and all its sub-dirs) is scanned looking for valid manifest files.
		 *        It is used by connectLaunchers().
		 *
		 * \param dirName Name of the directory where manifest files are looked for.
		 */

		void scan(const std::string& dirName);

		/**
		 * \brief Method internally used to get the name of the user running the server.
		 *
		 * \throw daq::pmg::Bad_User_Name Error while getting the name of the current user.
		 */

		void configure_login_name() const;

        /**
         * \brief Verify credentials of request.
         *
         * \throws daq::tokens::CannotVerify.
         *
         * \returns User name of client.
         */
         std::string verify_credentials(std::string_view credentials);

	private:

		OWLSemaphore m_sem;
		bool m_i_can_die; ///< Bool to decide whether the agent can exit on explicit user request
		IPCPartition* m_global_part; ///< Pointer to the global \e IPC partition.
		std::string m_IPC_name; ///< Name used to identify the ProcessManager in \e IPC (i.e., AGENT_FullHostName).
		std::string m_host_name; ///< The name of the host the Processmanager is running on.
		std::string m_ipc_ref_value; ///< The name of the \e IPC reference file.
		IPCAlarm* m_IPC_Ready_Alarm; ///< \e IPC alarm used to check if the global \e IPC server is alive.
		Daemon* m_Daemon_ptr; ///< Pointer to the Daemon.
		const unsigned int m_port;

		std::vector<std::string> manifestVect; ///< Vector containing all the valid manifet files found when the server starts.
		static std::string server_login_name; ///< The name of the user running the pmgserver.

		friend bool doWhenGlobalIPCReady(void* server_p);

}; // Server

/**
 * \brief It checks the presence of the global \e IPC server and publishes the ProcessManager
 *        if not already done. This function is implemented as an IPCAlarm in the daq::pmg::Server
 *        class. It is executed every one second.
 *
 * \param server_p Pointer to the daq::pmg::Server.
 */

bool doWhenGlobalIPCReady(void* server_p);

}
} // daq::pmg

#endif
