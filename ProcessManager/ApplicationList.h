/*
 *  ApplicationList.h
 *
 *  Created by Matthias Wiesmann on 07.04.05.
 *
 */

#ifndef PMG_APPLICATIONLIST_H
#define PMG_APPLICATIONLIST_H

#include <map>

#include "ProcessManager/Application.h"

namespace daq {
  namespace pmg {

    class Partition;
    
    /**
     * \class ApplicationList ApplicationList.h "ProcessManager/ApplicationList.h"
     *
     * \brief The ApplicationList class contains a set of applications sharing the same name.
     *        It is removed and deleted by the parent Partition destructor.
     *
     * \sa Application.h
     */

    class ApplicationList {
    
    public:

      /**
       * \brief The constructor: it sets some internal class members.
       *
       * \param name The application name.
       * \param parent A pointer to the Partition object the application belongs to.
       * \param identity The starting value of the application unique id: it is \e 0 by default and a different value is used only when 
       *                 the object is created to reconnect to a process after a ProcessManager server failure (in this
       *                 case the id of the already existing process is kept).
       *
       * \sa Partition.h  
       */

      ApplicationList(const std::string &name, Partition *parent, pmg_id_t identity=(pmg_id_t)0);

      /**
       * \brief The destructor: when called it deletes all the still present Application objects. 
       */

      ~ApplicationList();

      /**
       * \brief It terminates all the applications with a \e SIGTERM.
       *
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::ApplicationList_Signal_Exception Some error occurred while sending the signal to an
       *        application in the ApplicationList
       *
       * \sa Application::stop()
       */

      void stop(const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It sends all the applications a \e POSIX signal.
       *
       * \param signum The number of the \e POSIX signal to be sent.
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *       
       * \throw pmg::ApplicationList_Signal_Exception Some error occurred while sending the signal to an
       *        application in the ApplicationList
       *
       * \sa Application::signal()
       */

      void signal(int signum, const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It terminates all the applications with a \e SIGKILL.
       *
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.   
       *
       * \throw pmg::ApplicationList_Signal_Exception Some error occurred while sending the signal to an
       *        application in the ApplicationList
       *
       * \sa Application::kill()
       */

      void kill(const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It sends all the applications a \e SIGTERM signal and then, if the application is not exited
       *        within a certain amount of time, a \e SIGKILL signal.
       *
       * \param timeout The time (in seconds) to wait for the application to exit before sending the \e SIGKILL signal.
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::ApplicationList_Signal_Exception Some error occurred while sending the signal to an
       *        application in the ApplicationList
       *
       * \sa Application::kill_soft()
       */

      void kill_soft(int timeout, const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It looks for an Application with name \a app_name.
       *
       * \param app_name The name of the application to look for.
       *
       * \return \li A pointer to the Application object describing the application with name \a app_name.
       *         \li \c A null pointer ('0') if the Application with name \a app_name is not found.
       */

      Application* lookup(const std::string& app_name) const;
      
      /**
       * \brief It returns a pointer to the Application with the unique id equal to \a identity.
       *
       * \param identity The unique id of the Application object you are looking for.
       *
       * \return \li A pointer to the Application object with the unique id equal to \a identity.
       *         \li \e A null pointer ('0') if the corresponding Application is not found.
       */

      Application* application(pmg_id_t identity) const;

      /**
       * \brief It creates a new Application object with the unique id equal to \a identity.
       *
       *        If an Application with the same \a identity already exists it returns a pointer
       *        to it, otherwise a new Application is created and is added to the ApplicationList::m_application_list.
       *
       *        The behavior of this methods is different depending on the value of \a identity:
       *        \li If \e 0 (the default value) then a new Application is created after having incremented the id
       *            used for the previous Application (i.e., the id is monotonically increasing within the
       *            scope of a given partition and a given application name).
       *        \li If not \e 0 then the a new Application is created (if it does not already exists) with the 
       *            unique id equal to \a identity. This happens when the ProcessManager server is reconnecting 
       *            to an already existing application after a failure. In this case the Application unique id must be kept.
       *
       * \warning Use this method with \a identity not equal to \e 0 only when trying to reconnect
       *          to a process after a server crash. 
       *
       * \param identity The Application unique id.
       *
       * \return \li If \a identity is different than \e 0 a pointer to the new (or already existing) 
       *             Application object with unique id equal to \a identity.
       * \return \li If \a identity is equal to \e 0 a pointer to the a new Application object with unique id
       *             incremented by one with respect to the previous value.
       */

      Application* application_factory(pmg_id_t identity=(pmg_id_t)0);

      /**
       * \brief It gets all the applications present in the list.
       *
       * \param collection The \c application_collection_t container where to put the Application objects.
       *
       * \return The \a collection container.
       *
       */

      application_collection_t& all_applications(application_collection_t& collection) const;

      /**
       * \brief It gets the application name.
       *
       * \return The application name as a string.
       */
      
      const std::string& name() const;

      /**
       * \brief It gets the main directory where application files will be placed
       *        (i.e., \e /tmp/ProcessManager/partition_name/application_name).
       *
       * \return A System::File object describing the directory. 
       */

      const System::File& directory() const;

      /**
       * \brief It gets the Partition the applications are running in.
       *
       * \return A pointer to the Partition object.
       */

      const Partition* parent() const;

      /**
       * \brief It gets the number of valid processes.
       *
       * \return The number of active processes.
       *         It also flags an Application as invalid (and unmaps the manifest) if it is in the NOTAV state for more
       *         than APP_NOTAV_STATE_TIMEOUT (definition in private_defs.h). Usually this is not
       *         needed because the Application is set invalid when the corresponding process cannot be
       *         started by any reason. The only bad case is a client crash between the daq::pmg::Server::request_start()
       *         and the daq::pmg::Server::really_start() calls (or just a CORBA timeout in daq::pmg::Server::request_start()).
       *         In this case the process is not started and the Application would remain valid.
       *
       * \sa Application::invalid()
       */

      unsigned int active_processes() const;
    
      /**
       * \brief It prints all the applications with their id to a stream.
       *
       * \param stream The stream where to send the information.
       */

      void print_to(std::ostream& stream) const;

      /**
       * \brief It removes and deletes the Application object identified by the \c appId id.
       *        It also scans the Application list and removes and deletes any other invalid Application object.
       *        It is called by runReportThread().
       *
       * \param appId The Application object id. 
       */

      void removeAndDelete(pmg_id_t appId);

    private:
      
      typedef std::map<pmg_id_t,Application*> applist_t;            ///< \brief Definition for map containing the Application objects associated to their ids.
      std::string                             m_name;               ///< \brief Name of the applications.
      System::File                            m_directory;          ///< \brief The main directory where application files will be placed.
      pmg_id_t                                m_last_id;            ///< \brief The last used application unique id.
      applist_t                               m_application_list;   ///< \brief Map containing pointers to all the owned Application objects. 
      Partition*                              m_parent;             ///< \brief Partition owning all the applications.

    protected:

      /**
       * \brief It increases by one the last used application unique id.
       *
       * \return The last used application unique id incremented by one.
       */

      pmg_id_t next_identity();

      /**
       * \brief It builds a System::File object describing the main directory where application files will be placed.
       *
       * \return A System::File object describing the main directory where application files will be placed.
       */

      static System::File build_directory(const std::string &name, const Partition *parent);

    };
  } 
} // daq::pmg

std::ostream& operator<<(std::ostream& stream, const daq::pmg::ApplicationList& list);

#endif
