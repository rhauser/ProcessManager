/*
 *  ProcessData.h
 *
 *  Created by Matthias Wiesmann on 15.02.05.
 *
 */

#ifndef PMG_PROCESS_DATA_H
#define PMG_PROCESS_DATA_H

#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "ProcessManager/defs.h"

#define PMG_CORE_PROCESS_DATA_LEN 49152  ///< Size of the memory mapped file (the manifest).

namespace daq {
  namespace pmg {
    
    typedef unsigned int offset_t;      ///< A definition for an unsigned int.

    /**
     * \brief It converts the \c daq::pmg::p_state_t enum to a meaningful string.
     *
     * \return String representing the process status.
     */

    const char* state_string(PMGProcessState state);

    /** 
     * \brief This structure defines the data mapping of the actual manifest.
     *        All fields are either scalar types, or integer references to a memory area that contains strings.
     * 
     * \sa Manifest.h
     */

    typedef struct {
      char                  m_signature[4] ;             ///< Signature should contain the string pmg following by a zero.
      unsigned int          m_version ;                  ///< Version number of manifest file format.
      PMGProcessState       m_process_state ;            ///< State of the process. 
      pid_t                 m_process_id  ;              ///< Process id of the managed process. 
      pid_t                 m_launcher_id ;              ///< Process id of the launcher for the process.
      uid_t                 m_user_id ;                  ///< User id of the process. 
      int                   m_exit_code ;                ///< Exit code (if applicable) of the process.
      long                  m_rm_token ;                 ///< Resource Manager token.
      unsigned int          m_signal_code ;              ///< Signal code (if applicable) of the process.
      unsigned int          m_options ;                  ///< Option flags (unused).
      struct rusage         m_rusage ;                   ///< Resource usage structure.
      time_t                m_start_time ;               ///< Start time of the process.
      time_t                m_stop_time ;                ///< Stop time of the process. 
      offset_t              m_control_path_offset ;      ///< Path to control FIFO.
      mode_t                m_control_permission ;       ///< Permissions of control FIFO.
      offset_t              m_report_path_offset ;       ///< Path to report FIFO.
      offset_t              m_binary_name_offset ;       ///< Binary name.
      offset_t              m_working_directory_offset ; ///< Working directory.
      offset_t              m_input_path_offset ;        ///< Path to input stream.
      offset_t              m_output_path_offset ;       ///< Path to output stream.
      offset_t              m_error_path_offset ;        ///< Path to error stream.
      mode_t                m_output_permission ;        ///< Permissions for both output and error streams.
      offset_t              m_env_offset ;               ///< Environment list (string table).
      offset_t              m_param_offset ;             ///< Parameter list (string list).
      int                   m_init_timeout ;             ///< Init timeout.
      int                   m_auto_kill_timeout ;        ///< The "automatic kill" timeout.
      offset_t              m_error_offset ;             ///< Error message (if applicable).
      offset_t              m_handle_offset ;            ///< PMG handle.
      offset_t              m_rm_swobject_offset ;       ///< Process RM swobject.
      offset_t              m_requesting_host_offset;    ///< Host the request to start the process comes from.
      offset_t              m_last_string_offset ;       ///< index of the last string in memory. 
    } ProcessDataHeader; 

#define PMG_CORE_PROCESS_DATA_DATA_LEN PMG_CORE_PROCESS_DATA_LEN-sizeof(ProcessDataHeader)
    
    /** 
     * \brief This structure represents the content of the manifest file used to communicate 
     *        between the ProcessManager server and the Launcher. It contains all information needed
     *        to start a process. 
     * 
     *        The structure is basically built around two parts, a fixed length part which 
     *        contains either scalars, or references (for strings). The second part is a large
     *        array of characters (\c m_data).
     *        Strings are represented by an offset in the \c m_data area. 
     */
    
    typedef struct {
      ProcessDataHeader m_header; 
      char m_data[PMG_CORE_PROCESS_DATA_DATA_LEN]; 
    } ProcessData; 
        
  } 
}  // daq::pmg

#endif 

