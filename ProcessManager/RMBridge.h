#ifndef PMG_RM_BRIDGE_H
#define PMG_RM_BRIDGE_H

#include <ResourceManager/RM_Client.h>

namespace daq {
namespace pmg {

/**
 * \class RMBridge RMBridge.h "ProcessManager/RMBridge.h"
 *
 * \brief The RMBridge class is used to talk to
 *        the Resource Manager package in a convenient way.
 *        Resources are asked when a request to start a process is received and
 *        are released once the application has exited.
 *
 */

class RMBridge {

	public:

		/**
		 * \brief Reserves resources for a particular application in
		 *        a given partition running on a particular host.
		 *
		 * \param partition The partition where the app runs.
		 * \param swobject The software object describing the application (as a string).
		 * \param host The host where the application will run (as a string).
		 * \param user The user the application will belong to (as a string).
		 * \param app_name The application name
		 *
		 * \return \li A positive integer if resources were reserved;
		 *         \li \e 0 If nothing was reserved;
		 *
		 * \throw pmg::No_RM_Resources No resources are present in RM to start the process.
		 */

		long get_resources(const std::string& partition,
		                   const std::string& swobject,
		                   const std::string& host,
		                   const std::string& user,
		                   const std::string& app_name);

		/**
		 * \brief Frees previously reserved resources.
		 *
		 * \param itoken The token previously returned by get_resources()
		 *        (the RM will only be queried if this param is > 0).
		 */

		void free_resources(long itoken);

		/**
		 * \brief It Frees previously reserved resources.
		 * This method should be called only if 'handleId' is greater than 0. It is useful
		 * to free resources using both the RM handle and the process PID with a single remote
		 * call. If 'handleId' is equal to zero then the 'free_resource_by_pid' method
		 * should be used.
		 *
		 * \param handleId The token previously returned by get_resources()
		 * \param partName The name of the partition the process is running in
		 * \param hostName The name of the host where the process is executed
		 * \param pid	   The process PID
		 *
		 */
		void free_resources(long handleId,
		                    const std::string& partName,
		                    const std::string& hostName,
		                    unsigned long pid);

		/**
		 * \brief Free process resources using its PID.
		 *
		 * \param partitionName The name of the partition the process belongs to.
		 * \param hostName      The name of the host the process is running on.
		 * \param pid           The process pid.
		 */

		void free_resources_by_pid(const std::string& partitionName,
		                           const std::string& hostName,
		                           unsigned long pid);

		/**
		 * \brief It frees all the resources acquired by the local host for all the applications in all the partitions.
		 *        It is used in the Server class when the pmgserver i restarted after a machine crash (in this case the
		 *        pmgserver was not able to free application resources before exiting).
		 */

		void freeLocalHostResources();

		/**
		 * \brief The constructor
		 */

		RMBridge();

		/**
		 * \brief The destructor
		 */

		~RMBridge();

	private:

		rmgr::RM_Client* m_rm_client; ///< Pointer to the rm::RM_Client object.

}; // RMBridge
}
} // daq::pmg

#endif
