#ifndef ERS_SYSLOG_STREAM_H
#define ERS_SYSLOG_STREAM_H

#include <string>

#include <ers/OutputStream.h>
#include <ers/Issue.h>

namespace daq {
  namespace pmg {
    
    class SyslogStream : public ::ers::OutputStream {
    
    public:
      SyslogStream(const std::string& syslogLocalFacility);
      void write(const ::ers::Issue& issue);

    protected:
      void trimStr(std::string& str);

    private:
      int localFacility_;
      static boost::recursive_mutex mutex_;
      static unsigned int msgID;
      const size_t maxLen_;

    };
    
  }
}

#endif
