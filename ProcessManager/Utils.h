#ifndef PMG_UTILS_H
#define PMG_UTILS_H

#include <string>
#include <vector>

#include "ProcessManager/Exceptions.h"

namespace daq { namespace pmg { namespace utils {

/**
 * \brief It tokenizes the \a text string using \a separators as a separator.
 *
 * \param text The string to tokenize.
 * \param separators The string to use as a separator.
 * \param result The string vector where to put results of the tokenize action.
 */

void tokenize(const std::string& text, const std::string& separators, std::vector<std::string>& result);

/**
 * \brief It setup the port number to use for client/server CORBA communication.
 *
 *        It first checks for the existence of the SERVER_PORT_FILE (see private_defs.h) file
 *        in the TDAQ_INST_PATH directory and, if the file exists, reads the port number to use.
 *
 * \throw daq::pmg::IPC_Port_Error This exception is raised when one of the following error occurs:
 *                                 \li The TDAQ_INST_PATH variable cannot be resolved;
 *                                 \li The TDAQ_INST_PATH/SERVER_PORT_FILE exists but cannot be opened;
 *                                 \li The TDAQ_INST_PATH/SERVER_PORT_FILE exists, can be opened but some I/O
 *                                     error occurs while reading it.
 *
 * \return The port number to use or 0 if the file which defines the port to use is not present.
 */

unsigned int serverPort();

/**
 * \brief It gets the name of the user running the binary.
 *
 * \throw daq::pmg::Bad_User_Name Some error occurred.
 */

const std::string loginName();

/**
 * \brief It translates the errno code number in a string.
 *
 * \param errno The errno.
 *
 * \return The errno meaning.
 *
 * \note It internally uses the strerror_r system call which is thread safe.
 */

const std::string errno2String(int errNo);

/**
 * \brief It gives a string representation of the host mounting points occupancy.
 */

const std::string mntPointInfo();

/**
 * \brief Structure containing host information.
 */

struct HostInfo {
        long uptime;             ///< Host uptime
        unsigned long totalram;  ///< Total RAM amount (in kB)
        unsigned long totalswap; ///< Total swap amount (in kB)
        unsigned int used_ram;   ///< Host used RAM (in %)
        unsigned int used_swap;  ///< Host used swap (in %)
        unsigned short procs;    ///< Total number of processes running on the host
        unsigned long freeram;   ///< Available memory size (in kB)
        unsigned long sharedram; ///< Amount of shared memory (in kB)
        unsigned long bufferram; ///< Memory used by buffers (in kB)
        unsigned long cachedram; ///< Memory used for caches (in kB)
        unsigned long freeswap;  ///< Swap space still available (in kB)
};

/**
 * \brief It gives info about the host.
 */

bool hostInfo(HostInfo& info);

}}}

#endif
