#ifndef PMG_PROCESS_DESCRIPTION_H
#define PMG_PROCESS_DESCRIPTION_H

#include <system/Host.h>

#include "ProcessManager/defs.h"
#include "ProcessManager/Exceptions.h"

#include <memory>

namespace daq {
  namespace pmg {

    class ProcessDescriptionImpl;
    class Process;

    /**
     * \class ProcessDescription ProcessDescription.h "ProcessManager/ProcessDescription.h"
     *
     * \brief The ProcessDescription class holds request information for starting processes.
     *
     *        The ProcessDescription class is the class responsible for containing the
     *        information about the process which the user wishes to start, information
     *        which has been obtained by passing the information in the constructor.
     *
     * \note  This class does not own the real code, but its actions are implemented in
     *        the ProcessDescriptionImpl class. It creates a ProcessDescriptionImpl
     *        object and uses it to execute all the tasks.
     *
     * \sa ProcessDescriptionImpl.h
     */

    class ProcessDescription {

    public:

      /**
       * \brief Simple constructor where all info is passed.
       *        Constructor to which all the information about the process is passed directly to it.
       *
       * \param  host_name     The fully qualified hostname on which to run the process.
       * \param  app_name      A unique name for the process to be started
       *                           (eg. system_ls for the /bin/ls system command).
       * \param  partition     Name of partition in which this application is defined and will run.
       * \param  exec_list     A list of possible fully qualified executables.
       * \param  wd            The working directory from which the process should be started.
       * \param  start_args    The command line arguments used to start this process.
       *
       * \param  env           The environment variables which are needed to start this process.
       * \param  log_path      The log file directory path. Some directory structure and the
       *                           log file name is appended to it.
       * \param  input_dev     The redirection of stdin.
       * \param  rm_swobject   The swobject describing the process.
       *                       Empty string -> DO not use RM; Not empty string -> USE RM.
       * \param  null_logs     Whether to write out the log at all
       * \param  init_time_out The initial timeout after which to give up
       * \param  auto_kill_time_out If different than zero, the process will be automatically killed after this timeout
       *                            once it has reached the running state (e.g., it may be after the init timeout, if defined)
       *
       * \throw  daq::pmg::Not_Initialized Exception issued when trying to create a ProcessDescription bu the the Singleton::init()
       *                                   method has been called yet.
       */

      ProcessDescription(const std::string& host_name,
			 const std::string& app_name,
			 const std::string& partition,
			 const std::string& exec_list,
			 const std::string& wd,
			 const std::vector<std::string>& start_args,
			 const std::map<std::string,std::string>& env,
			 const std::string& log_path,
			 const std::string& input_dev,
			 const std::string& rm_swobject,
			 bool null_logs,
			 unsigned long init_time_out,
			 unsigned long auto_kill_time_out = 0);

      /**
       * \brief Variant of the previous constructor taking a System::Host object rather
       *        than a string to describe the host on which to run the process.
       *
       * \throw  daq::pmg::Not_Initialized Exception issued when trying to create a ProcessDescription bu the the Singleton::init()
       *                                   method has been called yet.
       *
       */

      ProcessDescription(const System::Host& host,
			 const std::string& app_name,
			 const std::string& partition,
			 const std::string& exec_list,
			 const std::string& wd,
			 const std::vector<std::string>& start_args,
			 const std::map<std::string,std::string>& env,
			 const std::string& log_path,
			 const std::string& input_dev,
			 const std::string& rm_swobject,
			 bool null_logs,
			 unsigned long init_time_out,
			 unsigned long auto_kill_time_out = 0);

      /**
       * \brief Variant of the previous constructor taking the same objects
       *        as the old PMG (ie vector of strings instead of strings etc...).
       *
       * \throw  daq::pmg::Not_Initialized Exception issued when trying to create a ProcessDescription bu the the Singleton::init()
       *                                   method has been called yet.
       *
       */

      ProcessDescription(const std::string& host_name,
			 const std::string& app_name,
			 const std::string& partition,
			 const std::vector<std::string>& exec_list,
			 const std::string& wd,
			 const std::string& start_args,
			 const std::map<std::string,std::string>& env,
			 const std::string& log_path,
			 const std::string& input_dev,
			 const std::string& rm_swobject,
			 bool null_logs,
			 unsigned long init_time_out,
			 unsigned long auto_kill_time_out = 0);

      /**
       * \brief Variant of the previous constructor taking the same objects
       *        as the old PMG (ie vector of strings instead of strings etc...).
       * \throw  daq::pmg::Not_Initialized Exception issued when trying to create a ProcessDescription bu the the Singleton::init()
       *                                   method has been called yet.
       */

      ProcessDescription(const System::Host& host,
			 const std::string& app_name,
			 const std::string& partition,
			 const std::vector<std::string>& exec_list,
			 const std::string& wd,
			 const std::string& start_args,
			 const std::map<std::string,std::string>& env,
			 const std::string& log_path,
			 const std::string& input_dev,
			 const std::string& rm_swobject,
			 const bool null_logs,
			 const unsigned long init_time_out,
			 unsigned long auto_kill_time_out = 0);

      /**
       * \brief Copy constructor.
       *
       * \throw  daq::pmg::Not_Initialized Exception issued when trying to create a ProcessDescription bu the the Singleton::init()
       *                                   method has been called yet.
       */

      ProcessDescription(const ProcessDescription &other);

      /**
       * \brief The destructor.
       */

      virtual ~ProcessDescription();

      bool operator==(const ProcessDescription &other) const;   ///< Just operator overloading.
      bool operator!=(const ProcessDescription &other) const;   ///< Just operator overloading.

      /**
       * \brief Used to start this process. Start parameters will be passed.
       *        It causes a remote \e CORBA call to the ProcessManager server.
       *
       * \param callback Optional callback function.
       * \param callbackparameter Optional callback parameter to be
       *                          passed to the callback function during
       *                          the actual callback.
       *
       * \return A Process pointer. The user has not to take care of this object to be deleted. But if the created
       *         Process is a linked one (i.e., a callback is registered) the user should unlink it once the process has exited.
       *
       * \throw daq::pmg::No_PMG_Server The ProcessManager server is not found in \e IPC;
       * \throw daq::pmg::Bad_PMG_Server There is a failure while trying to contact the ProcessManager server;
       * \throw daq::pmg::Failed_start A problem occurs while starting the process.
       *
       * \sa The Process class, ProcessDescriptionImpl::start(), Exceptions.h
       */

      std::shared_ptr<Process> start(CallBackFncPtr callback = 0, void* callbackparameter = 0);


      /**
       * \brief Returns a string representation of all parameters of this
       *        process, including start arguments and environment vars.
       *
       * \sa ProcessDescriptionImpl::toString().
       */

      const std::string toString() const;

      /**
       * \brief It returns the name of the process.
       *
       * \sa ProcessDescriptionImpl::app_name().
       */

      const std::string app_name() const;

      /**
       * \brief It prints all the ProcessDescription information to a stream.
       *
       * \param stream The stream the information will be send to.
       * \param prettyPrint Enable a pretty print way.
       */

      void printTo(std::ostream &stream, const bool prettyPrint=false) const;

      static const std::vector<std::string> emptyArgs;           ///< Static string vector to use to pass the process empty arguments.
      static const std::map<std::string,std::string> emptyEnv;   ///< Static map used to pass the process an empty environment.


    private:

      std::unique_ptr<ProcessDescriptionImpl> m_impl;            ///< Pointer to the ProcessDescriptionImpl instance.

    }; // ProcessDescription
  }
} // daq::pmg

std::ostream& operator<<(std::ostream& stream, const daq::pmg::ProcessDescription& description);


#endif
