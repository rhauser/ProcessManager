#ifndef PMG_BINARYPATH_H
#define PMG_BINARYPATH_H

#include <string>
#include <list>


namespace daq {
  namespace pmg {

    /** 
     * \class BinaryPath BinaryPath.h "ProcessManager/BinaryPath.h"
     *
     * \brief This class encapsulates the functionality for selecting the first appropriate
     *        binary from the list of binaries.
     * 
     * \author Marc Dobson
     */

    class BinaryPath {

    public:
    
      /**
       * \brief The constructor fills a list with all the available binary paths.
       *
       * \param path A string containing all the binary paths.
       *
       * \sa create()
       */

      BinaryPath(const std::string& path);

      /**
       * \brief The destructor.
       */

      ~BinaryPath();

      /**
       * \brief It looks for a valid binary in the list of all availble binaries.
       *
       * \return A string describing the valid found binary. The string is empty if no valid binary is found.
       */

      std::string get_binary();

      /**
       * \brief It creates (from a string) a list containing all the possible binary paths.
       *
       * \param path A string containing all the possible binary paths (the '\e :' char is assumed to be the separator).
       */

      void create(const std::string& path);

      /**
       * \brief It prints to the standard output all the possible binary paths.
       */

      void print() const;

    private:

      /**
       * \brief It checks if the binary path is absolute (i.e., it starts with '\e/').
       *
       * \param exe The binary path to check.
       *
       * \return \c TRUE if the path is absolute, \c FALSE if it is not.
       */

      bool is_absolute(const std::string& exe) const;

      /**
       * \brief It checks if the file the binary path refers to exists.
       *
       * \param exe The file to check.
       *
       * \return \c TRUE if the file exists, \c FALSE if it does not.
       */

      bool is_accesible(const std::string& exe) const;

      /**
       * \brief It checks if the file the binary path refers to is executable.
       *
       * \param exe The file to check.
       *
       * \return \c TRUE if the file is executable, \c FALSE if it is not.
       */
      
      bool is_executable(const std::string& exe) const;

      /**
       * \brief It checks if the binary path represents a file.
       *
       * \param exe The binary path to check.
       *
       * \return \c TRUE if the path represents a file, \c FALSE if it does not.
       */
      
      bool is_file(const std::string& exe) const;
      
      /**
       * \brief It makes a global check on the binary path.
       *
       * \param exe The binary path to check.
       *
       * \return \c TRUE if the binary path exists and represents a file which can be executed, otherwise \c FALSE.
       */

      bool check_binary(const std::string& exe) const;

    private:
      std::list<std::string> m_binary_path_list;   ///< \brief A list containing all the possible binary paths.
      const std::string m_null_path;               ///< \brief An empty string.
      static const std::string m_path_seperation;  ///< \brief Path separator (i.e., '\e :'). 
    }; //BinaryPath
    
  } 

} // daq::pmg

#endif
