/*
 *  Application.h
 *
 *  Created by Matthias Wiesmann on 07.04.05.
 *
 */

#ifndef PMG_APPLICATION_H
#define PMG_APPLICATION_H

#include <system/Process.h>
#include <system/File.h>

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/bind/bind.hpp>

#include "ProcessManager/ClientList.h"
#include "ProcessManager/Handle.h"
#include "ProcessManager/defs.h"

#include <pmgpriv/pmgpriv.hh>

namespace System {
  class FIFOConnection;
}

namespace daq {
  namespace pmg {

    class ApplicationList;
    class Partition;
    class ProcessIS;
    class Manifest;

    /**
     * \class Application Application.h "ProcessManager/Application.h"
     *
     * \brief The Application class (owned by the ProcessManager server) is designed to be an interface to the spawn process.
     *
     *        For each process to be started an Application object is created. The Application class includes
     *        methods to start the Launcher (and then the process) and send signals and receive updates from the process.
     *        Each Application object spawns a thread reading from a \e FIFO used to get information
     *        about the process status changes. Every time an update is received the ClientList
     *        class is invoked to notify all the registered clients.
     *        Commands (\e POSIX signals) are sent to the controlled process using a named pipe.
     *        The Application class owns the manifest in read/write mode until the Launcher is started. Then
     *        the manifest is mapped in read-only mode and the manifest is owned by the Launcher in
     *        read/write mode.
     *        The Application lifetime is the same as the effective process one: the Application exists when the
     *        process is started and it is deleted when the process exits. If the Application object is created
     *        but the corresponding process cannot be started the object is set "invalid" and will be deleted when
     *        the Partition is removed.
     *        Before sending any signal to the Process the AccessManager is asked about user authorization to execute
     *        such an action.
     *
     * \sa ApplicationList.h
     */

    class Application {

    public:

      /**
       * \brief The constructor: it inits some variables.
       *
       * \param identity Unique identity number: within the scope of a given partition and given application name
       *                 it is monotonically increasing.
       * \param parent   Pointer to the ApplicationList object the Application belongs to.
       * \param hasManifest Boolean indicating if the manifest file already exists when the Application is created. It is \c FALSE
       *                    by default and it is used when the server reconnects to an application after a failure.
       * \param hasReport Boolean indicating if the report \e FIFO (the one used to get process updates) already exists
       *                  when the Application is created. It is \c FALSE by default and it is used when the server reconnects
       *                  to an application after a failure.
       */

      Application(pmg_id_t identity, ApplicationList *parent, bool hasManifest = false, bool hasReport = false);

      /**
       * \brief The destructor: it unmaps (if not yet done) and deletes the manifest and deletes the report \e FIFO objet.
       */

      virtual ~Application();

      /**
       * \brief It causes the thread reading on the report FIFO to return. It DOES NOT kill the running process.
       *        Called by Daemon::stopAllReportThreads().
       */

      void stopReportThread() const;

      /**
       * \brief It sends the Launcher a command to update process statistics (i.e., \c getrusage is called).
       */

      void update_data() const;

      /**
       * \brief It sends the Launcher a command to stop the process (i.e., the \e POSIX signal \e SIGTERM).
       *
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::Launcher_Not_Running Signal not sent because the associated Launcher is not running
       * \throw pmg::FIFO_Error Signal not sent because some error occurred while writing/opening the report FIFO
       * \throw pmg::AM_Not_Allowed Signal not sent because non allowed by the AccessManager
       * \throw pmg::Application_Manifest_Already_Unmapped Signal not sent because the manifest has already been unmapped
       */

      void stop(const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It tells the Launcher to send the process a \e POSIX signal.
       *
       * \param signum The number of the \e POSIX signal to be sent.
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::Launcher_Not_Running Signal not sent because the associated Launcher is not running
       * \throw pmg::FIFO_Error Signal not sent because some error occurred while writing/opening the report FIFO
       * \throw pmg::AM_Not_Allowed Signal not sent because non allowed by the AccessManager
       * \throw pmg::Application_Manifest_Already_Unmapped Signal not sent because the manifest has already been unmapped
       */

      void signal(int signum, const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It sends the Launcher a command to kill the process (i.e., the \e POSIX signal \e SIGKILL).
       *
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::Launcher_Not_Running Signal not sent because the associated Launcher is not running
       * \throw pmg::FIFO_Error Signal not sent because some error occurred while writing/opening the report FIFO
       * \throw pmg::AM_Not_Allowed Signal not sent because non allowed by the AccessManager
       * \throw pmg::Application_Manifest_Already_Unmapped Signal not sent because the manifest has already been unmapped
       */

      void kill(const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It tells the Launcher to first send the process a \e SIGTERM signal and then, if the process is not exited
       *        within a certain amount of time, a \e SIGKILL signal.
       *
       * \param timeout The time (in seconds) to wait for the process to exit before sending the \e SIGKILL signal.
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::Launcher_Not_Running Signal not sent because the associated Launcher is not running
       * \throw pmg::FIFO_Error Signal not sent because some error occurred while writing/opening the report FIFO
       * \throw pmg::AM_Not_Allowed Signal not sent because non allowed by the AccessManager
       * \throw pmg::Application_Manifest_Already_Unmapped Signal not sent because the manifest has already been unmapped
       */

      void kill_soft(int timeout, const std::string& userName, const std::string& hostName) const;

      /**
       * \brief Method to get the process unique id.
       *
       * \return The process unique id.
       */

      pmg_id_t get_id() const;

      /**
       * \brief Method to get the manifest.
       *
       * \return \li A pointer to the Manifest object describing the manifest.\n
       *         \li \c Null pointer ('0') if the manifest is not mapped.
       *
       * \sa Manifest.h
       */

      Manifest* manifest() const;

      /**
       * \brief Method to get the partition the application belongs to.
       *
       * \return A pointer to the Partition object owning the partition.
       *
       * \sa Partition.h
       */

      const Partition* partition() const;

      /**
       * \brief Method to get a System::Process object representing the Launcher.
       *
       * \return A System::Process object representing the Launcher.
       */

      System::Process launcher() const;

      /**
       * \brief Method telling if the process is active or not.
       *
       * \return \li \c FALSE If the manifest does not exist (or is not mapped or loaded), or
       *         the process's already exited or failed.\n
       *         \li \c TRUE If the manifest exists (and is loaded and mapped), and the process is in one of the
       *         following stati: requested, running, launching or created.
       */

      bool is_active() const;

      /**
       * \brief Method to get the directory where manifest and \e FIFOs are created.
       *
       * \return A string describing the directory containing manifest and \e FIFO files.
       */

      std::string toString() const;

      /**
       * \brief Method to get the handle describing the process.
       *
       * \return A pointer to the process Handle.
       *
       * \sa Handle.h
       *
       */

      pmg::Handle* toHandle() const;

      /**
       * \brief Method to initialize the manifest.
       *
       *        The manifest is created and is mapped in memory. Some fields (mostly strings) are filled.
       *
       * \throw System::PosixIssue, ers::Issue, std::exception or any other unexpected exception occurred while operating with the manifest.
       */

      void init_manifest();

      /**
       * \brief This method starts the Launcher.
       *
       *        It is called by the Daemon::really_start(const std::string&) method and takes care of starting the Launcher with
       *        the right parameters. The manifest is here unmapped and mapped again in read-only mode. The report \e FIFO
       *        is created and a thread (implemented by daq::pmg::runReportThread) reading from it for process updates is spawn.
       *        Information about the process is published in \e IS.
       *
       * \throw pmg::Failed_Start_Thread  An error while starting the report thread
       * \throw System::PosixIssue, ers::Issue, std::exception or any other unexpected exception occurred while operating with the manifest
       *        and \e FIFO or starting the Launcher.
       *
       * \sa PMGLauncher.cxx
       */

      void start();

      /**
       * \brief This method prints application information (mainly manifest entries) to a stream.
       *
       * \param stream The stream where to put information.
       */

      void print_to(std::ostream &stream) const;

      /**
       * \brief This method adds a client to the list of clients that wants to be notified of a process status change.
       *
       * \param client_ref Client \e CORBA reference.
       *
       * \sa ClientList.h
       */

      void add_client_ref(const pmgpriv::CLIENT_var& client_ref);

      /**
       * \brief This method checks if a client is in the ClientList.
       *
       * \param client_ref Client \e CORBA reference.
       *
       * \return \li \c TRUE If the client is already in the list.
       *         \li \c FALSE If the client is not in the list yet.
       *
       * \sa ClientList.h
       */

      bool is_in_client_list(const pmgpriv::CLIENT_var& client_ref) const;

      /**
       * \brief This method return a reference to the mutex used to protect manifest accesses.
       */

      boost::recursive_mutex& mutex() const;

     /**
      * \brief This method opens the report \e FIFO in read/write non-blocking mode.
      *
      * \throw System::OpenFileIssue An error occurred while trying to open the report \e FIFO.
      */

      void open_report_fifo();

      /**
       * \brief It sets the Application object valid or not. Used to flag an Application whose corresponding
       *        process was not able to be started.
       *
       * \param isInvalid \c TRUE to flag the Application object as invalid; FALSE to flag it as valid.
       */

      void invalid(bool isInvalid);

      /**
       * \brief It gets the status of the Application object: valid or not.
       */

      bool invalid() const;

    protected:

      /**
       * \brief This method is used to notify the registered clients of a process status change.
       *
       *        It is called by daq::pmg::runReportThread() every time a process changes its status. All the clients (present in the ClientList)
       *        which have registred a callback are notified.
       *        Process status information is updated in \e IS too.
       *
       * \param state The process updated state.
       *
       * \sa ClientList.h
       */

      void notify_update(const PMGProcessState state);

      /**
       * \brief This method contains the code executed by the thread spawn in the start() method.
       *
       *        It first checks if the Launcher has been started: this is done by looking for a change in the process
       *        status. If the process does not changes its status within a fixed timeout (defined in private_defs.h) then it is assumed that
       *        the Launcher has not been correctly started and a warning is sent to ers::warning stream.
       *        Then the thread starts looping on reading from the report \e FIFO
       *        for process status changes. The loop ends when the \e q character is received (it is sent by the Launcher when
       *        the process has terminated its execution). To avoid an endless loop in case of a Launcher failure the report
       *        \e FIFO is read only when some data is available. To do that the \e select system function is called with
       *        a default timeout defined in private_defs.h. If no data is available after this period of time then a check on the Launcher is
       *        done: if the Launcher is still alive then the loop restarts, otherways the method returns.
       *        It calls the Application::notify_update method every time a process status change is received.
       *        When it finishes reading from the report FIFO it takes care of freeing the resources in the \e RM,
       *        unmapping and unlinking the manifest, unlinking the report \e FIFO, removing the directory containing manifest and \e FIFOs and
       *        removing the process information from \e IS. It also asks the parent ApplicationList to remove and delete the
       *        Application object.
       *
       * \param pApplication Pointer to the application object.
       */

      friend void runReportThread(Application* pApplication);

      /**
       * \brief Method to get the directory where \e FIFOs and manifest are stored for each process.
       *
       * \param identity The application unique id.
       * \param parent_dir Common directory for all applications with the same name (i.e., \e /tmp/ProcessManager/user_name/partition/application_name).
       *
       * \return A System::File object describing the directory containg \e FIFOs and the manifest file.
       */

      static System::File get_directory(pmg_id_t identity, const System::File &parent_dir);

      /**
       * \brief Method to get the manifest file
       *
       * \param parent_dir Common directory for all applications with the same name (i.e., \e /tmp/ProcessManager/user_name/partition/application_name).
       *
       * \return A System::File object describing the manifest file.
       */

      static System::File get_manifest_file(const System::File &parent_dir);

      /**
       * \brief Method to build the application handle.
       *
       * \return A Handle object describing the process handle.
       */

      pmg::Handle buildHandle() const;

      /**
       * \brief This methods asks the Acces Manger about the authorization to send a signal to a process.
       *
       * \param pManifest Pointer to the manifest containing process information.
       * \param signal Number of the \e POSIX signal to send to the process.
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \return \c TRUE if the Access Manager has given the authorization.
       *
       * \throw Any exception thrown by AMBridge::allowSignal(int)
       *
       * \note The Access Manager is asked only of the owner of the process is different than the one of the
       *       ProcessManager server.
       */

      bool signalAllowed(const Manifest* pManifest, int signal, const std::string& userName, const std::string& hostName) const;

      void freeRMResources();

    private:

      System::File                      m_directory;            ///< \brief Common directory for all applications with the same name (i.e., /tmp/ProcessManager/<partition>/<application name>).
      pmg_id_t                          m_id;                   ///< \brief Application unique id.
      ClientList                        m_clientList;           ///< \brief List of the registred client for the process.
      pmg::Manifest*                    m_manifest;             ///< \brief Pointer to the Manifest object describing the manifest file.
      System::Process                   m_launcher_process;     ///< \brief System::Process object describing the Launcher process.
      ApplicationList*                  m_parent;               ///< \brief Pointer to the ApplicationList object the Application belongs to.
      System::FIFOConnection*           m_report_fifo;          ///< \brief Pointer to the System::FIFOConnection objet describing the report FIFO.
      ProcessIS*                        is_info_process;        ///< \brief Pointer to the ISBridge object used to publish process information in IS.
      mutable boost::recursive_mutex 	m_mutex;                ///< \brief Mutex used to protect accesses to the manifest.
      bool                              isAppInvalid;           ///< \brief Simple flagf to set this object as invalid.
      pmg::Handle*                      m_handle;               ///< \brief The process handle.
      std::string                       m_stop_thread_code;     ///< \brief Code used to let the report thread return.
      bool                              m_fifo_open;            ///< \brief Flag set when the report FIFO has been opened.
      bool                              m_readStopMessage;      ///< \brief Flag used to notify the main thread that the report thread has been stopped.
      mutable boost::condition          m_condVar;              ///< \brief Condition variable.
      mutable boost::mutex              m_condVarMutex;         ///< \brief Mutex associated to the condition variable.
      mutable boost::mutex              m_openFIFOMutex;        ///< \brief Mutex used when the report FIFO is opened.

    }; // Application

    typedef std::vector<pmg::Application *> application_collection_t;   ///< \brief A definition for a vector whose elements are pointers to Application objects.

    void runReportThread(Application* pApplication);

  } } // pmg

std::ostream& operator<<(std::ostream& stream, const daq::pmg::Application& app);

#endif
