#!/bin/sh


# The help
help() {
	echo "Usage: start_PartitionTest -d <database_file> -p <partition_name> [-j <path_to_testPmg.jar]"
}

if [ $# -lt 4 ] ; then
	help 
	exit
fi

while test $# != 0; do
	case "$1" in
		-[h?x]*) help ; exit ;;
		-d)     shift; db="$1" ;;
		-p)     shift; partition="$1" ;;
		-j)     shift; testJar="$1" ;;
	esac
	shift;
done

# Partition is mandatory
if [ -z "$partition" ]; then
	partition="$TDAQ_PARTITION"
fi

# Check the java bin
javaBin="$TDAQ_JAVA_HOME"/bin/java
if [ -z "$TDAQ_JAVA_HOME" ]; then
	echo "Could not find java binary. Please, check the TDAQ_JAVA_HOME value."
	exit
fi

# Check classpath
classpath="$TDAQ_CLASSPATH"
if [ -z "$TDAQ_CLASSPATH" ]; then
	echo "Could not find java classpath! Exiting..."
	exit
fi

# Disable the ERS signal handler (the VM may crash)
export TDAQ_ERS_NO_SIGNAL_HANDLERS=1


# Start the application
echo "$testJar:$classpath"
"$javaBin" -cp "$testJar:$classpath:$CLASSPATH" test.PartitionTest -d "$db" -p "$partition" &

