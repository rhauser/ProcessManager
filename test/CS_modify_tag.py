#!/usr/bin/env python

import os,re,time 

print 'INFO [CS_modify_tag] Creating temp.data.xml with Tag %s' % os.environ['CMTCONFIG']

inp=open(os.environ['TEST_DB'],'r')
out=open('temp.data.xml','w')

rexp = re.compile('^\s+"Tag"\s+".+\"\s+$')
for line in inp.readlines():
	match=rexp.match(line)
	if match:
		out.write('  "Tag" "%s"\n' % os.environ['CMTCONFIG'])
	else:
		out.write(line)
