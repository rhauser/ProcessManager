#include "ProcessManager/Utils.h"
#include "ProcessManager/private_defs.h"
#include "ProcessManager/Exceptions.h"

#include <system/User.h>
#include <system/exceptions.h>

#include <fstream>
#include <sstream>
#include <string>

#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>
#include <sys/statvfs.h>
#include <sys/sysinfo.h>

void daq::pmg::utils::tokenize(const std::string & text,
                               const std::string & separators,
                               std::vector<std::string>& result)
{
    if(text.empty())
        return;

    std::string::size_type start_p = 0;
    std::string::size_type end_p = 0;

    while(start_p < text.size()) {
        end_p = text.find(separators, start_p);
        if(end_p == std::string::npos) {
            end_p = text.length();
        }
        std::string extract = text.substr(start_p, end_p - start_p);
        if(extract.size() > 0) {
            result.push_back(text.substr(start_p, end_p - start_p));
        }
        start_p = end_p + separators.size();
    }
}

unsigned int daq::pmg::utils::serverPort() {
    // Check if the file with the pmgserver port number to use exists

    unsigned int portNumber = 0;
  	const char* instPath = ::getenv("TDAQ_INST_PATH");

    if(instPath != NULL) {
        std::string portFileName = std::string(instPath) + SERVER_PORT_FILE;
        int fileExists = ::access(portFileName.c_str(), F_OK);

        if(fileExists == 0) {
            std::ifstream fileStream(portFileName.c_str(), std::ios::in);

            if(fileStream.good()) {
                fileStream >> portNumber;
                if(portNumber != 0) {
                    ERS_DEBUG(1, "Communications with the pmgserver will be estabilished on port " << portNumber);
                } else {
                    std::string eMsg = "I/O error while reading from file " + portFileName;
                    throw daq::pmg::IPC_Port_Error(ERS_HERE, eMsg);
                }
            } else {
                std::string eMsg = "File " + portFileName + " exists but cannot be open.";
                throw daq::pmg::IPC_Port_Error(ERS_HERE, eMsg);
            }

            fileStream.close();
        }

    } else {
        throw daq::pmg::IPC_Port_Error(ERS_HERE, std::string("Unable to resolve the TDAQ_INST_PATH variable"));
    }

    return portNumber;
}

const std::string daq::pmg::utils::loginName() {
    System::User user;
    try {
        return user.name();
    }
    catch(System::PosixIssue &ex) {
        std::string eMsg(ex.message());
        if(ex.get_error() != 0) {
            eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ") ";
        }
        throw daq::pmg::Bad_User_Name(ERS_HERE, eMsg, ex);
    }
}

const std::string daq::pmg::utils::errno2String(int errNo) {
    char errStrBuf[512];
    return std::string(strerror_r(errNo, errStrBuf, 512));
}

const std::string daq::pmg::utils::mntPointInfo() {
    std::ifstream mounts("/proc/mounts"); // Open /proc/mounts
    std::ostringstream stream;

    if(mounts.good()) {
        while(!mounts.eof()) {
            std::string device;
            std::string mntpoint;
            mounts >> device >> mntpoint;

            // Look only for lines starting with '/dev/'
            // It means that the mounted fs is local

            std::size_t find = device.find("/dev/");

            if(find != std::string::npos) {
                struct statvfs fs_struct;
                int stat_result = statvfs(mntpoint.c_str(), &fs_struct);
                if(stat_result != -1) {
                    float fs_used_blocks = fs_struct.f_blocks - fs_struct.f_bfree;
                    float fs_free_blocks = fs_struct.f_bfree;
                    unsigned int used_percentage = (unsigned int) ((fs_used_blocks) * 100.
                            / (fs_used_blocks + fs_free_blocks) + 0.5);
                    stream << "* " << mntpoint << ": " << used_percentage << "% * ";
                } else {
                    stream << mntpoint << ": " << "NO ACCESS" << " * ";
                }
            }
        } // while
    } // if std::ifstream

    mounts.close();

    return stream.str();
}

bool daq::pmg::utils::hostInfo(daq::pmg::utils::HostInfo& info) {
    bool memoryRead(true);
    bool sysinfoRead(true);

    std::ifstream fileStream("/proc/meminfo", std::ios::in);
    if((bool) fileStream) {
        std::string token;

        while(fileStream.good()) {
            fileStream >> token;
            if(token == "MemTotal:") {
                fileStream >> info.totalram;
            } else if(token == "MemFree:") {
                fileStream >> info.freeram;
            } else if(token == "Buffers:") {
                fileStream >> info.bufferram;
            } else if(token == "Cached:") {
                fileStream >> info.cachedram;
            } else if(token == "SwapTotal:") {
                fileStream >> info.totalswap;
            } else if(token == "SwapFree:") {
                fileStream >> info.freeswap;
            } else if(token == "Shmem:") {
                fileStream >> info.sharedram;
            }
        }

        if(info.totalram != 0) {
            info.used_ram = (unsigned int) ((info.totalram - (info.freeram + info.bufferram + info.cachedram)) * 100. / (double) info.totalram);
        } else {
            memoryRead = false;
            info.used_ram = 0;

            ERS_LOG("Cannot get the amount of available memory on the machine");
        }

        if(info.totalswap != 0) {
            info.used_swap = (unsigned int) ((info.totalswap - info.freeswap) * 100. / (double) info.totalswap);
        } else {
            info.used_swap = 0;
        }
    } else {
        // meminfo could not be read
        memoryRead = false;
        ERS_LOG("Cannot retrieve information about the status of the memory of the machine");
    }

    struct sysinfo sys__info;
    int sysinfo_result = sysinfo(&sys__info);
    if(sysinfo_result != -1) {
        info.uptime = sys__info.uptime;
        info.procs = sys__info.procs;
    } else {
        // sysinfo call failure
        sysinfoRead = false;
        ERS_LOG("Failure in getting the uptime and the number of running processes in the machine. Error code is " + std::to_string(sysinfo_result));
    }

    return (memoryRead && sysinfoRead);
}
