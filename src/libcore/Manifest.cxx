/*
 *  Manifest.cxx
 *
 *  Created by Matthias Wiesmann on 15.02.05.
 *
 */

#include <iostream>
#include <sstream>
#include <limits>

#include <ers/ers.h>
#include <system/exceptions.h>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>

#include "ProcessManager/Manifest.h"

using namespace daq ;

using boost::posix_time::ptime;
using boost::posix_time::from_time_t;
using boost::posix_time::to_simple_string;
using boost::date_time::c_local_adjustor;

const char * const pmg::Manifest::DEFAULT_PATH = "/dev/null" ; 
const char * const pmg::Manifest::SIGNATURE = "pmg" ; 
const char * const pmg::Manifest::FILENAME = "pmg.manifest" ; 
const char * const pmg::Manifest::DEFAULT_CONTROL = "pmg.control" ; 
const char * const pmg::Manifest::DEFAULT_OUT = "/dev/null" ; 
const char * const pmg::Manifest::DEFAULT_REPORT = "pmg.report" ; 
const char * const pmg::Manifest::DEFAULT_ERR_MSG = "None" ;

daq::pmg::offset_t invalidOffset = std::numeric_limits<unsigned int>::max();

pmg::Manifest::Manifest(const System::File &file, bool read_mode, bool write_mode, mode_t perm)  : System::MapFile(file,PMG_CORE_PROCESS_DATA_LEN,0,read_mode,write_mode,perm) {
  ERS_DEBUG(3,"Create manifest "<< file);
} // Manifest

const pmg::ProcessData *pmg::Manifest::process_read_data() const {
  ERS_ASSERT_MSG(is_loaded(),c_full_name() << " is not loaded") ;
    return (pmg::ProcessData *) m_map_address ; 
} // process_data

pmg::ProcessData *pmg::Manifest::process_write_data() const {
  ERS_ASSERT_MSG(is_loaded(),c_full_name() << " is not loaded") ;
  ERS_ASSERT_MSG(m_map_write,c_full_name() << " is not writable");
    return (pmg::ProcessData *) m_map_address ; 
} // process_data

const char * pmg::Manifest::string_area_read() const {
    const ProcessData *p = process_read_data() ; 
    return &(p->m_data[0]) ; 
} // string_area_read
    
char* pmg::Manifest::string_area_write() {
    ProcessData *p = process_write_data() ;
    return &(p->m_data[0]) ; 
} // string_area_write

System::StringMemoryArea::offset_t pmg::Manifest::last_string() const  {
     return process_read_data()->m_header.m_last_string_offset ; 
} // last_string

void pmg::Manifest::last_string(offset_t offset) {
    process_write_data()->m_header.m_last_string_offset = offset ; 
} // last_string
    
size_t pmg::Manifest::string_area_size() const {
    return PMG_CORE_PROCESS_DATA_DATA_LEN ; 
} // string_area_size

void pmg::Manifest::sign() const {
    ProcessData *ptr = process_write_data() ; 
    char *sign_str = ptr->m_header.m_signature ; 
    strcpy(sign_str,SIGNATURE); 
    ptr->m_data[0] = (char) 0xff ; 
    ptr->m_header.m_version = PMG_MANIFEST_VERSION ; 
} // sign

bool pmg::Manifest::check_sign() const {
    const ProcessData *ptr = process_read_data() ; 
    const char *sign_str = ptr->m_header.m_signature ; 
    const int c = strcmp(sign_str,SIGNATURE); 
    if (c!=0) return false ; 
    if (ptr->m_data[0]!=(char) 0xff) return false ; 
    return (ptr->m_header.m_version == PMG_MANIFEST_VERSION) ; 
} // check_sign

unsigned int  pmg::Manifest::version() const {
    return process_read_data()->m_header.m_version ; 
} // version


// ==========================
// Process Information
// ==========================


PMGProcessState pmg::Manifest::process_state() const {
    return process_read_data()->m_header.m_process_state ; 
} // process_state

void pmg::Manifest::process_state(PMGProcessState state) {
    process_write_data()->m_header.m_process_state = state ; 
} // process_state

System::Process pmg::Manifest::process_id() const {
    return System::Process(process_read_data()->m_header.m_process_id) ; 
} // process_id

void pmg::Manifest::process_id(const System::Process &p) {
    process_write_data()->m_header.m_process_id = p.process_id(); 
} // process_id

System::Process pmg::Manifest::launcher_pid() const {
    return System::Process(process_read_data()->m_header.m_launcher_id) ; 
} // launcher_id

void pmg::Manifest::launcher_pid(const System::Process &process) {
     process_write_data()->m_header.m_launcher_id = process.process_id() ; 
} // launcher_id
 
int pmg::Manifest::exit_code() const {
    return process_read_data()->m_header.m_exit_code ; 
} // exit_code

void pmg::Manifest::exit_code(int code) {
    process_write_data()->m_header.m_exit_code = code ;
} // exit_code

unsigned int pmg::Manifest::exit_signal() const {
    return process_read_data()->m_header.m_signal_code ;
} // exit_signal

void pmg::Manifest::exit_signal(unsigned int code) {
    process_write_data()->m_header.m_signal_code = code ;
} // exit_signal

long pmg::Manifest::rm_token() const {
    return process_read_data()->m_header.m_rm_token ;
} // rm_token

void pmg::Manifest::rm_token(long token) {
    process_write_data()->m_header.m_rm_token = token ;
} // rm_token

System::User pmg::Manifest::user() const {
    return System::User(process_read_data()->m_header.m_user_id) ;
} // user
 
void pmg::Manifest::user(const System::User &u) {
    process_write_data()->m_header.m_user_id = u.identity() ; 
} // user_id
    
struct rusage * pmg::Manifest::resource_usage_ptr()  {
    return &(process_write_data()->m_header.m_rusage) ; 
} // resource_usage_ptr

const struct rusage * pmg::Manifest::resource_usage_ptr() const  {
    return &(process_read_data()->m_header.m_rusage) ; 
} // resource_usage_ptr

time_t *pmg::Manifest::start_time_ptr() {
    return &(process_write_data()->m_header.m_start_time) ; 
}

const time_t *pmg::Manifest::start_time_ptr() const {
    return &(process_read_data()->m_header.m_start_time) ; 
}

time_t *pmg::Manifest::stop_time_ptr()  {
    return &(process_write_data()->m_header.m_stop_time) ; 
}

const time_t *pmg::Manifest::stop_time_ptr() const {
    return &(process_read_data()->m_header.m_stop_time) ; 
}


// ==========================
//   Program Parameters 
// ==========================


System::Executable pmg::Manifest::executable() const {
    const offset_t offset = process_read_data()->m_header.m_binary_name_offset ;
    return System::Executable(get_string(offset)) ;
} // executable

void pmg::Manifest::executable(const System::Executable &exec) {
    offset_t *offset = &(process_write_data()->m_header.m_binary_name_offset) ;
    insert(offset,exec) ; 
} // executable

System::File pmg::Manifest::working_directory() const {
    const offset_t offset = process_read_data()->m_header.m_working_directory_offset ;
    return System::File(get_string(offset)); 
} // working_directory

void pmg::Manifest::working_directory(const System::File &dir) {
    offset_t *offset = &(process_write_data()->m_header.m_working_directory_offset) ;
    insert(offset,dir) ; 
} // working_directory

System::File pmg::Manifest::output_file() const {
    const offset_t offset = process_read_data()->m_header.m_output_path_offset ;
    const char *path = get_string(offset); 
    if (path) {
	return System::File(path) ; 
    } else {
	return System::File(DEFAULT_PATH); 
    } 
} // output_file
    
void pmg::Manifest::input_file(const System::File &in_file) {
    pmg::ProcessData *ptr = process_write_data() ; 
    offset_t *offset = &(ptr->m_header.m_input_path_offset) ;
    insert(offset,in_file) ; 
} // input_file

System::File pmg::Manifest::input_file() const {
    const offset_t offset = process_read_data()->m_header.m_input_path_offset ;
    const char *path = get_string(offset); 
    if (path) {
	return System::File(path) ; 
    } else {
	return System::File(DEFAULT_PATH); 
    } 
} // output_file
    
void pmg::Manifest::output_file(const System::File &out_file) {
    pmg::ProcessData *ptr = process_write_data() ; 
    offset_t *offset = &(ptr->m_header.m_output_path_offset) ;
    insert(offset,out_file) ; 
} // output_file

System::File pmg::Manifest::error_file() const {
    const offset_t offset = process_read_data()->m_header.m_error_path_offset ;
    const char *path = get_string(offset); 
    if (path) {
	return System::File(path) ; 
    } else {
	return System::File(DEFAULT_PATH); 
    } 
} // error_file

void pmg::Manifest::error_file(const System::File &error) {
    pmg::ProcessData *ptr = process_write_data() ; 
    offset_t *offset = &(ptr->m_header.m_error_path_offset) ; 
    insert(offset,error);
} // output_file


mode_t pmg::Manifest::output_file_permission() const {
    return process_read_data()->m_header.m_output_permission ; 
} // output_file_permission

void pmg::Manifest::output_file_permission(mode_t perm) {
    process_write_data()->m_header.m_output_permission = perm ; 
} // output_file_permission

// ==========================
//   Launcher Control 
// ==========================

System::File pmg::Manifest::control_file() const {
    const offset_t offset = process_read_data()->m_header.m_control_path_offset ;
    const char *path = get_string(offset); 
    if (path) {
	return System::File(path) ; 
    } else {
	return parent().child(DEFAULT_CONTROL); 
    } // if
} // control_file

void pmg::Manifest::control_file(const System::File &control) {
    pmg::ProcessData *ptr = process_write_data() ; 
    offset_t *offset = &(ptr->m_header.m_control_path_offset) ; 
    insert(offset,control);
} // control_file

mode_t pmg::Manifest::control_file_permission() const {
    return process_read_data()->m_header.m_control_permission ; 
} // output_file_permission

void pmg::Manifest::control_file_permission(mode_t perm) {
    process_write_data()->m_header.m_control_permission = perm ; 
} // output_file_permission

System::File pmg::Manifest::report_file() const {
    const offset_t offset = process_read_data()->m_header.m_report_path_offset ;
    const char *path = get_string(offset); 
    if (path) {
	return System::File(path) ; 
    } else {
	return System::File(DEFAULT_REPORT); 
    } // if
} // report_file

void pmg::Manifest::report_file(const System::File &report) {
    pmg::ProcessData *ptr = process_write_data() ; 
    offset_t *offset = &(ptr->m_header.m_report_path_offset) ; 
    insert(offset,report);
} // report_file

// ==========================
// Handle
// ==========================

void pmg::Manifest::handle(const std::string &name) {
    pmg::ProcessData *ptr = process_write_data() ; 
    offset_t *offset = &(ptr->m_header.m_handle_offset) ; 
    insert(offset,name.c_str());
} // handle

std::string pmg::Manifest::handle() const {
    const offset_t offset = process_read_data()->m_header.m_handle_offset  ;
    const char *str = get_string(offset); 
    if (str) {
	return std::string(str); 
    } else {
	return "" ; 
    } 
} // handle
    
// ==========================
// RM swobject
// ==========================

/** Sets the process swobject used for RM 
  * \param name the swobject string
  * \note The swobject is considered an opaque character string 
  */

void pmg::Manifest::rm_swobject(const std::string &swobject) {
  pmg::ProcessData *ptr = process_write_data() ; 
  offset_t *offset = &(ptr->m_header.m_rm_swobject_offset) ; 
  insert(offset,swobject.c_str());
} // swobject

/** Gets the process swobject 
 * \return the swobject string
 * \note The swobject is considered an opaque character string 
 */

std::string pmg::Manifest::rm_swobject() const {
  const offset_t offset = process_read_data()->m_header.m_rm_swobject_offset  ;
  const char *str = get_string(offset); 
  if (str) {
    return std::string(str); 
  } else {
    return "" ; 
  } 
} // swobject

void pmg::Manifest::requesting_host(const std::string &hostName) {
  pmg::ProcessData *ptr = process_write_data() ;
  offset_t *offset = &(ptr->m_header.m_requesting_host_offset) ;
  insert(offset,hostName.c_str());
} 

std::string pmg::Manifest::requesting_host() const {
  const offset_t offset = process_read_data()->m_header.m_requesting_host_offset  ;
  const char *str = get_string(offset);
  if (str) {
    return std::string(str);
  } else {
    return "" ;
  }
} 

// ==========================
//   Error Reporting 
// ==========================

const char* pmg::Manifest::error_msg() const {
    const offset_t offset = process_read_data()->m_header.m_error_offset ;
    return get_string(offset); 
} // error_msg

void pmg::Manifest::error_msg(const char* msg) {
    pmg::ProcessData *ptr = process_write_data() ; 
    offset_t *offset = &(ptr->m_header.m_error_offset) ;
    insert(offset,msg) ; 
} // error_msg

void pmg::Manifest::parameters(const System::Executable::param_collection &params) {
  // TODO: this is basically broken. It is enough to call it first with an
  // empty vector and then with a non-empty one. The first call sets the
  // offset to "invalid" and the second call just fails because of this.
  if(params.empty()) {
    process_write_data()->m_header.m_param_offset = invalidOffset;
  } else {
    offset_t *offset = &(process_write_data()->m_header.m_param_offset) ;
    insert(offset,params);
  }
} // set_param

System::Executable::param_collection pmg::Manifest::parameters() const {
    const offset_t offset = process_read_data()->m_header.m_param_offset;
    if(offset != invalidOffset) {
      return get_vector(offset);
    } else {
      System::Executable::param_collection emptyVect;
      return emptyVect;
    }
} // get_parameter

void pmg::Manifest::environnements(const System::Executable::env_collection &envs) {
    offset_t *offset = &(process_write_data()->m_header.m_env_offset) ;
    insert(offset,envs) ; 
} // environnements

System::Executable::env_collection pmg::Manifest::environnements() const {
    const offset_t offset = process_read_data()->m_header.m_env_offset ;
    return get_map(offset); 
} // environnements

void pmg::Manifest::init_timeout(const int timeout) {
    process_write_data()->m_header.m_init_timeout = timeout ;
} // init_timeout

int pmg::Manifest::init_timeout() const {
    return process_read_data()->m_header.m_init_timeout ;
} // init_timeout

void pmg::Manifest::auto_kill_timeout(const int timeout) {
    process_write_data()->m_header.m_auto_kill_timeout = timeout ;
} // init_timeout

int pmg::Manifest::auto_kill_timeout() const {
    return process_read_data()->m_header.m_auto_kill_timeout ;
} // init_timeout


void pmg::Manifest::print(std::ostream &stream) const {
    const System::Executable exec = executable(); 
    stream << "--- COMMAND:   " << exec.to_string(parameters()) << std::endl ;
    stream << "--- WD:        " << working_directory().full_name() << std::endl ; 
    std::string out_perm_str = System::File::pretty_permissions(output_file_permission()) ;
    stream << "--- OUTPUT_S:  " << output_file().full_name() << " (" << out_perm_str << ")" << std::endl ; 
    stream << "--- ERROR_S:   " << error_file().full_name() << " (" << out_perm_str << ")" << std::endl ; 
    std::string ctrl_perm_str = System::File::pretty_permissions(control_file_permission()); 
    stream << "--- CONTROL:   " << control_file().full_name() << " (" << ctrl_perm_str << ")" << std::endl ; 
    stream << "--- REPORT:    " << report_file().full_name() << std::endl ;
    stream << "--- REQ HOST:  " << requesting_host() << std::endl ;
    stream << "--- USER:      " << user() << std::endl ; 
    stream << "--- PROCESS:   " << process_id() << std::endl ; 
    stream << "--- LAUNCHER:  " << launcher_pid() << std::endl ; 
    stream << "--- STATUS:    " << state_string(process_state()) << std::endl ; 
    stream << "--- EXIT:      " << System::Process::exit_pretty(exit_code()) << std::endl ;
    stream << "--- SIGNAL:    " << strsignal(exit_signal()) << std::endl ; 

    ptime startTime(c_local_adjustor<ptime>::utc_to_local(from_time_t(*start_time_ptr())));
    stream << "--- START:     " << to_simple_string(startTime) << std::endl;

    ptime stopTime(c_local_adjustor<ptime>::utc_to_local(from_time_t(*stop_time_ptr())));
    stream << "--- STOP:      " << to_simple_string(stopTime) << std::endl; 

    const struct rusage *r = resource_usage_ptr() ;
    stream << "--- MEMORY:    " << System::File::pretty_size(r->ru_maxrss,true)  << std::endl ; 
    stream << "--- SWAPS:     " << r->ru_nswap << std::endl ; 
    stream << "--- PAGES:     " << r->ru_majflt << " faults " << r->ru_minflt << " reclaims" <<  std::endl ; 
    stream << "--- CONTEXT:   " << r->ru_nvcsw << " involuntary " << r->ru_nivcsw << " voluntary" << std::endl ; 
    stream << "--- SYSTEM:    " << r->ru_stime.tv_sec << " s. " <<  r->ru_stime.tv_usec << " µs." <<  std::endl ; 
    stream << "--- USER:      " << r->ru_utime.tv_sec << " s. " <<  r->ru_utime.tv_usec << " µs." <<  std::endl ; 
    stream << "--- HANDLE:    " << handle() <<  std::endl ;
    //    stream << "sw_object: \t" << rm_swobject() << std::endl;
    const char* msg = error_msg() ; 
    if (msg) {
	stream << "--- ERROR MSG:   " << msg << std::endl ; 
    }
    stream << "*** START DUMPING THE ENVIRONMENT ***" << std::endl ;
    const System::Executable::env_collection environment = environnements() ;
    for(System::Executable::env_collection::const_iterator it = environment.begin(); it != environment.end(); ++it) {
      stream << " --- " << it->first << "=" << it->second << std::endl ;
    }
 } // print

std::ostream& operator<<(std::ostream& stream, const pmg::Manifest& manifest) {
    manifest.print(stream);
    return stream ; 
} // operator<<


