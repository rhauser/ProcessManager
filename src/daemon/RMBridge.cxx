#include <ers/ers.h>
#include <ResourceManager/exceptions.h>
#include <system/Host.h>

#include "ProcessManager/RMBridge.h"
#include "ProcessManager/Exceptions.h"

#include <ostream>

using namespace daq;

pmg::RMBridge::RMBridge() {
    m_rm_client = new rmgr::RM_Client();
} // RMBridge

pmg::RMBridge::~RMBridge() {
    delete m_rm_client;
} // ~RMBridge

long pmg::RMBridge::get_resources(const std::string& partition_string,
                                  const std::string& swobject_string,
                                  const std::string& host_string,
                                  const std::string& user_string,
                                  const std::string& app_name_string)

{
    long itoken = 0;

    try {
        itoken = m_rm_client->requestResources(partition_string, swobject_string, host_string, user_string);
    }
    catch(daq::rmgr::Exception& ex) {
        ERS_DEBUG(1, "Exception while asking the RM for resources: " << ex.message());
        throw pmg::No_RM_Resources(ERS_HERE, app_name_string, partition_string, swobject_string, ex.message(), ex);
    }
    catch(ers::Issue& ex) {
        ERS_DEBUG(1,"Unexpected exception while asking the RM for resources: " << ex.message());
        throw pmg::No_RM_Resources(ERS_HERE, app_name_string, partition_string, swobject_string, ex.message(), ex);
    }
    catch(std::exception& ex) {
        ERS_DEBUG(1,"Unexpected exception while asking the RM for resources: " << ex.what());
        throw pmg::No_RM_Resources(ERS_HERE, app_name_string, partition_string, swobject_string, ex.what(), ex);
    }
    catch(...) {
        ERS_DEBUG(1,"Unknown exception while asking the RM for resources");
        throw pmg::No_RM_Resources(ERS_HERE, app_name_string, partition_string, swobject_string, "Unknown exception");
    }

    if(itoken < 0) {
        throw pmg::No_RM_Resources(ERS_HERE, app_name_string, partition_string, swobject_string, "Resource not available");
    }

    return itoken;
}

void pmg::RMBridge::freeLocalHostResources() {
    try {
        m_rm_client->freeComputer(System::LocalHost::full_local_name(), false);
    }
    catch(daq::rmgr::Exception &ex) {
        std::string errorMessage = "Failed to free RM resources for local host " + ex.message();
        ers::warning(pmg::Exception(ERS_HERE, errorMessage, ex));
    }
    catch(ers::Issue &ex) {
        std::string errorMessage = "Failed to free RM resources for local host " + ex.message();
        ers::warning(pmg::Exception(ERS_HERE, errorMessage, ex));
    }
    catch(...) {
        std::string errorMessage = "Failed to free RM resources for local host: unknown exception";
        ers::warning(pmg::Exception(ERS_HERE, errorMessage));
    }
}

void pmg::RMBridge::free_resources(long itoken) {
    if(itoken > 0) {
        try {
            m_rm_client->freeResources(itoken, false);
        }
        catch(daq::rmgr::Exception &ex) {
            std::string errorMessage = "Failed to free RM resources " + ex.message();
            ers::error(pmg::Exception(ERS_HERE, errorMessage, ex));
        }
        catch(ers::Issue &ex) {
            std::string errorMessage = "Failed to free RM resources " + ex.message();
            ers::error(pmg::Exception(ERS_HERE, errorMessage, ex));
        }
        catch(std::exception &ex) {
            std::string errorMessage = std::string("Failed to free RM resources ") + std::string(ex.what());
            ers::error(pmg::Exception(ERS_HERE, errorMessage, ex));
        }
        catch(...) {
            std::string errorMessage = "Failed to free RM resources, unknown exception";
            ers::error(pmg::Exception(ERS_HERE, errorMessage));
        }
    }
}

void pmg::RMBridge::free_resources(long handleId,
                                   const std::string& partitionName,
                                   const std::string& hostName,
                                   unsigned long pid)
{
    try {
        m_rm_client->freeAllProcessResources(handleId, partitionName, hostName, pid, false);
    }
    catch(daq::rmgr::CommunicationException& ex) {
        ers::error(pmg::RM_Resources_NotFreed(ERS_HERE, pid, partitionName, ex.message(), ex));
    }
    catch(daq::rmgr::ProcessNotFound &ex) {
        std::ostringstream stream;
        stream << "Failed to free RM resources for process running in partition "
               << partitionName << " and PID " << pid << ": " << ex.message();
        ERS_DEBUG(1, stream.str());
    }
    catch(daq::rmgr::PartitionNotFound &ex) {
        std::ostringstream stream;
        stream << "Failed to free RM resources for process running in partition "
               << partitionName << " and PID " << pid << ": " << ex.message();
        ERS_DEBUG(1, stream.str());
    }
    catch(daq::rmgr::Exception &ex) {
        ers::error(pmg::RM_Resources_NotFreed(ERS_HERE, pid, partitionName, ex.message(), ex));
    }
    catch(ers::Issue &ex) {
        ers::error(pmg::RM_Resources_NotFreed(ERS_HERE, pid, partitionName, ex.message(), ex));
    }
    catch(std::exception &ex) {
        ers::error(pmg::RM_Resources_NotFreed(ERS_HERE, pid, partitionName, ex.what(), ex));
    }
    catch(...) {
        ers::error(pmg::RM_Resources_NotFreed(ERS_HERE, pid, partitionName, "unknown exception"));
    }
}

void pmg::RMBridge::free_resources_by_pid(const std::string& partitionName,
                                          const std::string& hostName,
                                          unsigned long pid)
{
    try {
        m_rm_client->freeProcessResources(partitionName, hostName, pid, false);
    }
    catch(daq::rmgr::CommunicationException& ex) {
        ers::error(pmg::RM_Resources_NotFreed(ERS_HERE, pid, partitionName, ex.message(), ex));
    }
    catch(daq::rmgr::ProcessNotFound &ex) {
        std::ostringstream stream;
        stream << "Failed to free RM resources for process running in partition " << partitionName << " and PID "
               << pid << ": " << ex.message();
        ERS_DEBUG(1, stream.str());
    }
    catch(daq::rmgr::PartitionNotFound &ex) {
        std::ostringstream stream;
        stream << "Failed to free RM resources for process running in partition " << partitionName << " and PID "
               << pid << ": " << ex.message();
        ERS_DEBUG(1, stream.str());
    }
    catch(daq::rmgr::Exception &ex) {
        ers::error(pmg::RM_Resources_NotFreed(ERS_HERE, pid, partitionName, ex.message(), ex));
    }
    catch(ers::Issue &ex) {
        ers::error(pmg::RM_Resources_NotFreed(ERS_HERE, pid, partitionName, ex.message(), ex));
    }
    catch(std::exception &ex) {
        ers::error(pmg::RM_Resources_NotFreed(ERS_HERE, pid, partitionName, ex.what(), ex));
    }
    catch(...) {
        ers::error(pmg::RM_Resources_NotFreed(ERS_HERE, pid, partitionName, "unknown exception"));
    }
}
