/*
 *  Daemon.cxx
 *
 *  Created by Matthias Wiesmann on 08.04.05.
 *
 */

#include <sstream>
#include <map>
#include <memory>
#include <utility>
#include <filesystem>

#include <ers/ers.h>
#include <system/System.h>
#include <system/exceptions.h>
#include <owl/time.h>
#include <ipc/alarm.h>
#include <pmg/pmg_syncMacros.h>

#include "ProcessManager/Daemon.h"
#include "ProcessManager/Server.h"
#include "ProcessManager/ApplicationList.h"
#include "ProcessManager/Partition.h"
#include "ProcessManager/BinaryPath.h"
#include "ProcessManager/Exceptions.h"
#include "ProcessManager/ISBridge.h"
#include "ProcessManager/AMBridge.h"
#include "ProcessManager/RMBridge.h"
#include "ProcessManager/private_defs.h"

boost::recursive_mutex daq::pmg::Daemon::partition_table_mutex;
boost::recursive_mutex daq::pmg::Daemon::application_table_mutex;

using is::repository;

namespace daq {
namespace pmg {

Daemon::Daemon(const System::File directory) :
	m_directory(directory)
{

	m_Update_IS = new IPCAlarm(IS_UPDATE_PERIOD, update_all_is, this);

} // Daemon()

Daemon::~Daemon() {

	delete m_Update_IS;

} // ~Daemon

const System::File &Daemon::directory() const {
	return m_directory;
} // directory

unsigned int Daemon::active_processes() const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);

	unsigned int count = 0;
	partition_table_t::const_iterator pos;

	for(pos = m_partition_table.begin(); pos != m_partition_table.end(); ++pos) {
		count += pos->second->active_processes();
	} // if

	return count;

} // running_nbr

Partition *Daemon::partition(const std::string &name) const {

	Partition* part_ptr = 0;

	partition_table_t::const_iterator pos = m_partition_table.find(name);
	if(pos != m_partition_table.end()) {
		part_ptr = pos->second;
	} // if

	return part_ptr;

} // partition

Partition *Daemon::partition_factory(const std::string &name) {

	Partition *p = partition(name);

	if(p == 0) {
		p = new Partition(name, this);
		ERS_ASSERT(p);
		m_partition_table[name] = p;
	}

	return p;

} // partition_factory

Application *Daemon::application(const std::string &partition_name,
                                 const std::string &application_name,
                                 pmg_id_t identity) const
{

	Application* app_ptr = 0;

	const Partition *p = partition(partition_name);
	if(p) {
		app_ptr = p->application(application_name, identity);
	}

	return app_ptr;

} // application

Application *Daemon::application(const Handle &handle) const {
	return application(handle.partitionName(), handle.applicationName(), handle.id());
} // application

Application *Daemon::build_app(const Handle &handle) {
	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
	return application_factory(handle.partitionName(), handle.applicationName(), handle.id());
} // application

application_collection_t Daemon::all_applications() const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);

	application_collection_t collection;
	partition_table_t::const_iterator pos;
	for(pos = m_partition_table.begin(); pos != m_partition_table.end(); ++pos) {
		if(pos->second) {
			collection = pos->second->all_applications(collection);
		}
	} // for

	return collection;

} // all_applications

Application *Daemon::application_factory(const std::string & partition_name,
                                         const std::string &application_name,
                                         pmg_id_t identity)
{
	Partition *p = partition_factory(partition_name);
	ERS_ASSERT(p);
	Application* app = p->application_factory(application_name, identity);
	return app;
} // application_factory


// ----------------------------------------------------------------
// Control Methods
// ----------------------------------------------------------------

void Daemon::stop(const std::string& userName, const std::string& hostName) const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);

	unsigned int failures = 0;
	std::string handles;

	partition_table_t::const_iterator pos;
	for(pos = m_partition_table.begin(); pos != m_partition_table.end(); ++pos) {
		if(pos->second) {
			try {
				pos->second->stop(userName, hostName);
			}
			catch(Partition_Signal_Exception &ex) {
				++failures;
				handles += ex.get_proc_handles().c_str();
			}
		}
	} // for

	if(failures > 0) {
		throw pmg::Failed_Kill_All(ERS_HERE, handles);
	}

} // stop

void Daemon::kill(const int timeout, const std::string& userName, const std::string& hostName) const
{

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);

	unsigned int failures = 0;
	std::string handles;

	partition_table_t::const_iterator pos;
	for(pos = m_partition_table.begin(); pos != m_partition_table.end(); ++pos) {
		if(pos->second) {
			try {
				pos->second->kill_soft(timeout, userName, hostName);
			}
			catch(Partition_Signal_Exception &ex) {
				++failures;
				handles += ex.get_proc_handles().c_str();
			}
		}
	} // for

	if(failures > 0) {
		throw pmg::Failed_Kill_All(ERS_HERE, handles);
	}

} // kill

void Daemon::print_to(std::ostream& stream) const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);

	partition_table_t::const_iterator pos;
	stream << "PMG daemon on: " << System::LocalHost::full_local_name() << std::endl;
	for(pos = m_partition_table.begin(); pos != m_partition_table.end(); ++pos) {
		stream << "---------------------------------------" << std::endl;
		const Partition *p = pos->second;
		ERS_ASSERT(p);
		stream << (*p);
	} // for

} // void

// Method used to start the process

void Daemon::really_start(const pmg::Handle& h) {

	// Get the application using the handle and some info from manifest

	Application* app_ptr = 0;
	Manifest* pManifest = 0;
	std::string _swobject_;
	std::string user;
	{
		boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
		boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);
		app_ptr = application(h);
		if(app_ptr != 0) {
			boost::recursive_mutex::scoped_lock _scoped_lock(app_ptr->mutex());
			pManifest = app_ptr->manifest();
			if(pManifest != 0) {
				pManifest->process_state(PMGProcessState_REQUESTED);
				_swobject_ = pManifest->rm_swobject();
				user = (pManifest->user()).name_safe();
			} else {
				pmg::Internal_Server_Error ex(ERS_HERE, "Manifest is unmapped and it should not!");
				ers::error(ex);
				app_ptr->invalid(true);
				throw ex;
			}
		} else {
			pmg::Internal_Server_Error ex(ERS_HERE, "Invalid Application pointer!");
			ers::error(ex);
			throw ex;
		}
	}

	// Ask the RM, if needed

	long rm_id = 0;
	if(_swobject_.size() > 0) {

		// If the _swobject_ string is not empty then the RM is used

		const std::string _part_name_(h.partitionName());
		const std::string _user_name_(user);
		const std::string _app_name_(h.applicationName());

		// Ask the RM and store the answer in 'rm_id'
		// This call can throw pmg::No_RM_Resources

		try {
			RMBridge askRM;
			rm_id = askRM.get_resources(_part_name_,
			                            _swobject_,
			                            System::LocalHost::full_local_name(),
			                            _user_name_,
			                            _app_name_);
		}
		catch(pmg::No_RM_Resources &e) {
			{
				boost::recursive_mutex::scoped_lock scoped_lock(app_ptr->mutex());
				try {
				    if(pManifest) {
				        if(pManifest->is_mapped()) {
				            pManifest->unmap();
				        }
				        pManifest->unlink();
				    }
				}
				catch(System::PosixIssue &ex) {
					ERS_DEBUG(1, ex);
				}
				catch(...) {
				}
			}
			app_ptr->invalid(true);
			throw ;
		}

	}

	// Store RM token into the manifest

	{
		boost::recursive_mutex::scoped_lock scoped_lock(app_ptr->mutex());
		Manifest* pManifest = app_ptr->manifest();
		if(pManifest != 0) {
			pManifest->rm_token(rm_id);
		} else {
			if(rm_id != 0) {
				RMBridge askRM;
				askRM.free_resources(rm_id);
			}
			pmg::Internal_Server_Error ex(ERS_HERE, "Manifest is unmapped and it should not!");
			ers::error(ex);
			app_ptr->invalid(true);
			throw ex;
		}
	}

	// Start the process

	try {
		app_ptr->start();
	}
	catch(...) {
		if(rm_id != 0) {
			RMBridge askRM;
			askRM.free_resources(rm_id);
		}
		app_ptr->invalid(true);
		throw;
	}

} // really_start()

char* Daemon::request_start(const pmgpriv::ProcessRequestInfo& requestInfo) {

	std::string handle_str;

	ERS_DEBUG(4, "Get down to business");

	// Ask the AM if I am authorized to start the process
	// Do that only if the server and the client run under the same user

	const std::string user_name(requestInfo.user.name);
	const std::string loginName(pmg::Server::login_name());

	ERS_DEBUG(5, "Server user name: " << loginName << " - Client user name: " << user_name);

	if(loginName != user_name) {
		//  if(true) {

		const std::string client_host(requestInfo.client_host);
		const std::string app_name(requestInfo.app_name);
		const std::string run_host(requestInfo.host);
		const std::string partition_name(requestInfo.partition);

		std::string _app_args_;
		int arg_size = requestInfo.start_args.length();
		for (int p = 0; p < arg_size; ++p) {
			_app_args_ += std::string(requestInfo.start_args[p]);
			_app_args_ += " ";
		}

		const std::string app_args(_app_args_);

		AMBridge askAM(user_name,
				client_host,
				app_name,
				run_host,
				app_args,
				partition_name,
				false);

		try {
			bool mayI = askAM.allowStart();
			if(mayI == false) {
				throw pmg::AM_Not_Allowed(ERS_HERE, app_name, partition_name, "authorization not granted to the user");
			} else {
				ERS_DEBUG(1,"Starting of " << app_name << " in partition " << partition_name << " allowed by AM");
			}
		}
		catch(ers::Issue& ex) {
			throw pmg::AM_Not_Allowed(ERS_HERE, app_name, partition_name, ex.message(), ex);
		}
		catch(std::exception& ex) {
			throw pmg::AM_Not_Allowed(ERS_HERE, app_name, partition_name, ex.what(), ex);
		}
		catch(...) {
			throw pmg::AM_Not_Allowed(ERS_HERE, app_name, partition_name, "received an unexpected exception");
		}

	}

	// Create the application
	Application* app_ptr = 0;

	{
		boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
		app_ptr = this->application_factory(std::string(requestInfo.partition), std::string(requestInfo.app_name));
	}

	if(app_ptr == 0) {
		pmg::Internal_Server_Error ex(ERS_HERE, "Not able to build the Application object!");
		ers::error(ex);
		throw ex;
	}

	// Initialize the manifest, get it and fill it according to the request
	// init_manifest() may throw an exception in case of problems

	try {
		app_ptr->init_manifest();
	}
	catch(...) {
		app_ptr->invalid(true);
		throw;
	}

	{
		boost::recursive_mutex::scoped_lock scoped_lock(app_ptr->mutex());

		Manifest* manifest_ptr = app_ptr->manifest();

		if(manifest_ptr == 0) {
			pmg::Internal_Server_Error ex(ERS_HERE, "Manifest is unmapped and it should not!");
			ers::error(ex);
			app_ptr->invalid(true);
			throw ex;
		}

		// Find the first executable which is valid
		BinaryPath bin_path(std::string(requestInfo.exec_list));
		std::string full_binary(bin_path.get_binary());
		if (full_binary == "") {
			{
				//	boost::recursive_mutex::scoped_lock scoped_lock(app_ptr->mutex());
				try {
				    if(manifest_ptr->is_mapped()) {
				        manifest_ptr->unmap();
				    }
				}
				catch(System::PosixIssue &ex) {
					ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, manifest_ptr->full_name(), std::string(requestInfo.app_name), std::string(requestInfo.partition), ex.message(), ex));
				}
				catch(...) {
					ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, manifest_ptr->full_name(), std::string(requestInfo.app_name), std::string(requestInfo.partition), "Unknown"));
				}
				try {
					manifest_ptr->unlink();
				}
				catch(...) {
				}
			}
			// Flag the Application Object as invalid
			app_ptr->invalid(true);

			// Throw the exception
			throw pmg::Binary_Not_Found(ERS_HERE, std::string(requestInfo.app_name), std::string(requestInfo.partition), std::string(requestInfo.exec_list));
		}

		ERS_DEBUG(4,"Binary is: "<< full_binary);

		try {

			// Check if the RM authorization is needed

			std::string _swobject_(requestInfo.rm_swobject);

			if (_swobject_.size() > 0) {

				// If the _swobject_ string is not empty then the RM is used.
				// Store the string in the manifest, it will be uset at really_start()

				manifest_ptr->rm_swobject(_swobject_);

			}

			// The final user
			System::User finalUser(std::string(requestInfo.user.name));

			// Fill the manifest

			manifest_ptr->executable(full_binary);

			System::Executable::param_collection params;
			int size = requestInfo.start_args.length();
			int pos = 0;
			ERS_DEBUG(3,"Start arguments size is "<< size);
			for (pos = 0; pos < size; ++pos) {
				ERS_DEBUG(4,"Arg at pos " << pos << " is "<< requestInfo.start_args[pos]);
				params.push_back(std::string(requestInfo.start_args[pos]));
			}
			manifest_ptr->parameters(params);
			ERS_DEBUG(4,"Added parameters to manifest.");

			System::Executable::env_collection envs;
			size = requestInfo.envs.length();
			ERS_DEBUG(4,"envs length is "<< size);
			for (pos = size-1; pos >= 0; --pos) {
				ERS_DEBUG(4,"Env at pos " << pos<< " is " << requestInfo.envs[pos].name
						<< " with value "<<requestInfo.envs[pos].value);
				envs.insert(std::make_pair(std::string(requestInfo.envs[pos].name),
								std::string(requestInfo.envs[pos].value)));
			}

			// Insert env variable for sync file
			std::string syncFile = PMG_SYNC_FILE_DIR + loginName + "/" + manifest_ptr->handle() + ".sync";
			envs.insert(std::make_pair(PMG_SYNC_ENV_VAR_NAME,syncFile.c_str()));
			ERS_DEBUG(5,"set PMG_SYNC_FILE environment to " << syncFile);

			// Add env variable for Kerberos token
			const std::string krb5Var("KRB5CCNAME");
			if(envs.find(krb5Var) == envs.end()) {
			    const std::string value = std::string("KEYRING:persistent:") +
			                              std::to_string(finalUser.identity()) +
			                              std::string(":pmg");
			    envs.insert(std::make_pair(krb5Var, value));
			}

			manifest_ptr->environnements(envs);
			ERS_DEBUG(4,"Added environment to manifest.");

			std::string wd(requestInfo.wd);
			if(wd.empty())
			manifest_ptr->working_directory("/tmp");
			else
			manifest_ptr->working_directory(std::string(requestInfo.wd));

			ERS_DEBUG(4,"Added working directory to manifest.");

			manifest_ptr->user(finalUser);
			ERS_DEBUG(4,"Added username to manifest.");

			if(requestInfo.streams.out_is_null == true) {
				manifest_ptr->output_file(System::File("/dev/null"));
				manifest_ptr->error_file(System::File("/dev/null"));
			}
			else {
				std::string path(requestInfo.streams.log_path);
				path += "/";
				path += std::string(requestInfo.app_name);

				OWLTime owl_time;
				std::ostringstream logst;
				logst << path << "_" << System::LocalHost::full_local_name() << "_" << owl_time.c_time();

				std::string out_path = logst.str() + ".out";
				std::string err_path = logst.str() + ".err";

				manifest_ptr->output_file(System::File(out_path));
				manifest_ptr->error_file(System::File(err_path));
				manifest_ptr->output_file_permission((mode_t) requestInfo.streams.perms);
			}
			ERS_DEBUG(4,"Added out/err/perms to manifest.");

			std::string in_file(requestInfo.streams.in_path);
			if (in_file.empty()) {
				manifest_ptr->input_file(System::File("/dev/null"));
			} else {
				manifest_ptr->input_file(System::File(in_file));
			}

			ERS_DEBUG(4,"Added stdin to manifest.");

			manifest_ptr->init_timeout(requestInfo.init_time_out);
			ERS_DEBUG(4,"Added init_timeout to manifest.");

			manifest_ptr->auto_kill_timeout(requestInfo.auto_kill_time_out);
            ERS_DEBUG(4,"Added auto_kill_timeout to manifest.");

			manifest_ptr->requesting_host(std::string(requestInfo.client_host));
			ERS_DEBUG(4,"Added requesting host to manifest.");
		}
		catch(System::PosixIssue &issue) {
			std::ostringstream exString;
			exString << "Got exception in request_start for app " << std::string(requestInfo.app_name) << " in partition "
			<< std::string(requestInfo.partition) << ": " << issue.message() << ". Dumping the manifest:\n" << *manifest_ptr;
			ers::info(pmg::Exception(ERS_HERE, exString.str()));
			{
				try {
				    if(manifest_ptr->is_mapped()) {
				        manifest_ptr->unmap();
				    }
				}
				catch(System::PosixIssue &ex) {
					ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, manifest_ptr->full_name(), std::string(requestInfo.app_name), std::string(requestInfo.partition), issue.message(), issue));
				}
				catch(...) {
					ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, manifest_ptr->full_name(), std::string(requestInfo.app_name), std::string(requestInfo.partition), "Unknown"));
				}
				try {
					manifest_ptr->unlink();
				}
				catch(...) {
				}
			}
			app_ptr->invalid(true);
			throw;
		}
		catch(ers::Issue &issue) {
			std::ostringstream exString;
			exString << "Got exception in request_start for app " << std::string(requestInfo.app_name) << " in partition "
			<< std::string(requestInfo.partition) << ": " << issue.message() << ". Dumping the manifest:\n" << *manifest_ptr;
			ers::info(pmg::Exception(ERS_HERE, exString.str()));
			{
				try {
				    if(manifest_ptr->is_mapped()) {
				        manifest_ptr->unmap();
				    }
				}
				catch(System::PosixIssue &ex) {
					ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, manifest_ptr->full_name(), std::string(requestInfo.app_name), std::string(requestInfo.partition), ex.message(), ex));
				}
				catch(...) {
					ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, manifest_ptr->full_name(), std::string(requestInfo.app_name), std::string(requestInfo.partition), "Unknown"));
				}
				try {
					manifest_ptr->unlink();
				}
				catch(...) {
				}
			}
			app_ptr->invalid(true);
			throw;
		}
		catch(std::exception &issue) {
			std::ostringstream exString;
			exString << "Got exception in request_start for app " << std::string(requestInfo.app_name) << " in partition "
			<< std::string(requestInfo.partition) << ": " << issue.what() << ". Dumpig the manifest:\n" << *manifest_ptr;
			ers::info(pmg::Exception(ERS_HERE, exString.str()));
			{
				try {
				    if(manifest_ptr->is_mapped()) {
				        manifest_ptr->unmap();
				    }
				}
				catch(System::PosixIssue &ex) {
					ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, manifest_ptr->full_name(), std::string(requestInfo.app_name), std::string(requestInfo.partition), ex.message(), ex));
				}
				catch(...) {
					ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, manifest_ptr->full_name(), std::string(requestInfo.app_name), std::string(requestInfo.partition), "Unknown"));
				}
				try {
					manifest_ptr->unlink();
				}
				catch(...) {
				}
			}
			app_ptr->invalid(true);
			throw;
		}
		catch(...) {
			std::ostringstream exString;
			exString << "Unknown exception in request_start for app " << std::string(requestInfo.app_name) << " in partition "
			<< std::string(requestInfo.partition) << ". Dumping the manifest:\n" << *manifest_ptr;
			ers::info(pmg::Exception(ERS_HERE, exString.str()));
			{
				try {
				    if(manifest_ptr->is_mapped()) {
				        manifest_ptr->unmap();
				    }
				}
				catch(System::PosixIssue &ex) {
					ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, manifest_ptr->full_name(), std::string(requestInfo.app_name), std::string(requestInfo.partition), ex.message(), ex));
				}
				catch(...) {
					ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, manifest_ptr->full_name(), std::string(requestInfo.app_name), std::string(requestInfo.partition), "Unknown"));
				}
				try {
					manifest_ptr->unlink();
				}
				catch(...) {
				}
			}
			app_ptr->invalid(true);
			throw;
		}

	} // close scope

	// Add reference to the client for callbacks
	app_ptr->add_client_ref(requestInfo.client_ref);
	ERS_DEBUG(4,"Added client_ref to clientList.");

	handle_str = app_ptr->toHandle()->toString();

	// Return the handle

	ERS_DEBUG(3,"Handle string: " << handle_str);

	return CORBA::string_dup(handle_str.c_str());

} // request_start()


void Daemon::signal(const pmg::Handle& handle, const CORBA::Long signum, const std::string& userName, const std::string& hostName) const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
	boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

	ERS_DEBUG(3,"signal(handle=\"" << handle.toString() << "\", signum="<< signum);

	const Application* target_app = application(handle);

	if (target_app && !(target_app->invalid())) {
		target_app->signal(signum, userName, hostName);
	} else {
		ERS_DEBUG(1,"Could not find app with handle="<< handle.toString());
		throw pmg::Application_NotFound_Invalid(ERS_HERE, handle.toString(), handle.applicationName(), handle.partitionName());
	}

} // signal()


void Daemon::stop(const pmg::Handle& handle, const std::string& userName, const std::string& hostName) const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
	boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

	ERS_DEBUG(3,"stop(handle=" << handle.toString() <<")");

	const Application* target_app = application(handle);

	if (target_app && !(target_app->invalid())) {
		target_app->stop(userName, hostName);
	} else {
		ERS_DEBUG(1,"Could not find app with handle=" << handle.toString());
		throw pmg::Application_NotFound_Invalid(ERS_HERE, handle.toString(), handle.applicationName(), handle.partitionName());
	}

} // signal()


void Daemon::kill(const pmg::Handle& handle, const std::string& userName, const std::string& hostName) const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
	boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

	ERS_DEBUG(3,"kill(handle=" << handle.toString() <<")");

	const Application* target_app = application(handle);

	if (target_app && !(target_app->invalid())) {
		target_app->kill(userName, hostName);
	} else {
		ERS_DEBUG(1,"Could not find app with handle=" << handle.toString());
		throw pmg::Application_NotFound_Invalid(ERS_HERE, handle.toString(), handle.applicationName(), handle.partitionName());
	}

} // kill()

void Daemon::kill_soft(const pmg::Handle& handle, const CORBA::Long timeout, const std::string& userName, const std::string& hostName) const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
	boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

	ERS_DEBUG(3,"kill_soft(handle=" << handle.toString() <<")");

	const Application* target_app = application(handle);

	if (target_app && !(target_app->invalid())) {
		target_app->kill_soft(timeout, userName, hostName);
	} else {
		ERS_DEBUG(1,"Could not find app with handle=" << handle.toString());
		throw pmg::Application_NotFound_Invalid(ERS_HERE, handle.toString(), handle.applicationName(), handle.partitionName());
	}

} // kill_soft()

bool Daemon::kill_partition(const std::string& part, const CORBA::Long timeout, const std::string& userName, const std::string& hostName) const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);

	const Partition* p = partition(part);

	bool done = false;

	if (p) {
		done = true;
		p->kill_soft(timeout, userName, hostName);
	}

	return done;

} // kill_partition()


void Daemon::kill_all(const CORBA::Long timeout, const std::string& userName, const std::string& hostName) const {

	this->kill(timeout, userName, hostName);

} // kill_all()

char* Daemon::lookup(const std::string& app_name, const std::string& part_name) const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
	boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

	std::string _handle_;

	partition_table_t::const_iterator pos;
	Application* app = 0;

	for(pos=m_partition_table.begin();pos!=m_partition_table.end();++pos) {
		if(pos->first == part_name) {
			if(pos->second) {
				app = (pos->second)->lookup(app_name);
				if(app) {
					boost::recursive_mutex::scoped_lock scopedLock(app->mutex());
					if(app->is_active() && !(app->invalid())) {
						_handle_ = app->toHandle()->toString();
					}
					break;
				}
			}
		}
	} // for

	ERS_DEBUG(3,"Handle string: " << _handle_);

	return CORBA::string_dup(_handle_.c_str());

}

void Daemon::processes_info(const std::string& partition, pmgpriv::proc_info_list* procInfoList) const {

	std::map<std::string, std::shared_ptr<pmgpub::ProcessStatusInfo>> procInfoMap;

	{
		boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
		boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

		application_collection_t procCollection;
		partition_table_t::const_iterator pos = m_partition_table.find(partition);
		if(pos != m_partition_table.end() && (pos->second != 0)) {
			procCollection = pos->second->all_applications(procCollection);
		}

		for(application_collection_t::const_iterator iter = procCollection.begin(); iter != procCollection.end(); ++iter) {
			if((*iter)) {
				boost::recursive_mutex::scoped_lock scopedLock((*iter)->mutex());
				if((*iter)->is_active() && !(*iter)->invalid()) {
					if(((*iter)->manifest()->process_state() == PMGProcessState_RUNNING)) {
						const pmg::Handle* procHandle = (*iter)->toHandle();
						const auto& procStatusInfo = std::make_shared<pmgpub::ProcessStatusInfo>();
						const bool success = this->get_info(*procHandle, procStatusInfo.get());
						if(success == true) {
							procInfoMap.insert(std::make_pair(procHandle->toString(), procStatusInfo));
							ERS_DEBUG(1, "Found handle " << procHandle->toString());
						}
					}
				}
			}
		}
	}

	const auto entries = procInfoMap.size();
	procInfoList->length(entries);

	const auto& startIt = procInfoMap.cbegin();
	const auto& stopIt = procInfoMap.cend();

	int counter = 0;
	for(auto it = startIt; it != stopIt; ++it) {
		pmgpriv::ProcessInfoRequest req;
		req.app_handle = CORBA::string_dup(it->first.c_str());
		req.proc_info = *(it->second);
		(*procInfoList)[counter++] = req;
	}

}

char* Daemon::processes(const std::string& partition) const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
	boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

	std::string procList;
	application_collection_t procCollection;

	partition_table_t::const_iterator pos = m_partition_table.find(partition);
	if(pos != m_partition_table.end() && (pos->second != 0)) {
		procCollection = pos->second->all_applications(procCollection);
	}

	for(application_collection_t::const_iterator iter = procCollection.begin(); iter != procCollection.end(); ++iter) {
		if((*iter)) {
			boost::recursive_mutex::scoped_lock scopedLock((*iter)->mutex());
			if((*iter)->is_active() && !(*iter)->invalid()) {
				if(((*iter)->manifest()->process_state() == PMGProcessState_RUNNING)) {
					procList += (*iter)->toHandle()->toString();
					procList += pmg::ProcessManagerSeparator;
					ERS_DEBUG(1, "Found handle " << (*iter)->toHandle()->toString());
				}
			}
		}
	}

	return CORBA::string_dup(procList.c_str());

}

void Daemon::all_processes_info(pmgpriv::proc_info_list* procInfoList) const {

	std::map<std::string, std::shared_ptr<pmgpub::ProcessStatusInfo>> procInfoMap;

	{
		boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
		boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

		application_collection_t procCollection(all_applications());

		for(application_collection_t::const_iterator iter = procCollection.begin(); iter != procCollection.end(); ++iter) {
			if((*iter)) {
				boost::recursive_mutex::scoped_lock scopedLock((*iter)->mutex());
				if((*iter)->is_active() && !(*iter)->invalid()) {
					if(((*iter)->manifest()->process_state() == PMGProcessState_RUNNING)) {
						const pmg::Handle* procHandle = (*iter)->toHandle();
						const auto& procStatusInfo = std::make_shared<pmgpub::ProcessStatusInfo>();
						const bool success = this->get_info(*procHandle, procStatusInfo.get());
						if(success == true) {
							procInfoMap.insert(std::make_pair(procHandle->toString(), procStatusInfo));
							ERS_DEBUG(1, "Found handle " << procHandle->toString());
						}
					}
				}
			}
		}
	}

	const auto entries = procInfoMap.size();
	procInfoList->length(entries);

	const auto& startIt = procInfoMap.cbegin();
	const auto& stopIt = procInfoMap.cend();

	int counter = 0;
	for(auto it = startIt; it != stopIt; ++it) {
		pmgpriv::ProcessInfoRequest req;
		req.app_handle = CORBA::string_dup(it->first.c_str());
		req.proc_info = *(it->second);
		(*procInfoList)[counter++] = req;
	}

}

char* Daemon::all_processes() const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
	boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

	std::string procList;
	application_collection_t procCollection;
	procCollection = all_applications();

	for(application_collection_t::const_iterator iter = procCollection.begin(); iter != procCollection.end(); ++iter) {
		if((*iter)) {
			boost::recursive_mutex::scoped_lock scopedLock((*iter)->mutex());
			if((*iter)->is_active() && !(*iter)->invalid()) {
				if(((*iter)->manifest()->process_state() == PMGProcessState_RUNNING)) {
					procList += (*iter)->toHandle()->toString();
					procList += pmg::ProcessManagerSeparator;
					ERS_DEBUG(1, "Found handle " << (*iter)->toHandle()->toString());
				}
			}
		}
	}

	return CORBA::string_dup(procList.c_str());

}

bool Daemon::exists(const pmg::Handle& handle) const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
	boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

	const Application* target_app = application(handle);

	bool is_app_active = false;

	if(target_app) {
		boost::recursive_mutex::scoped_lock scopedLock(target_app->mutex());
		is_app_active = (target_app->is_active() && !(target_app->invalid()));
	}

	return is_app_active;

} // exists()

const std::map<std::string, pmg::Partition *>& Daemon::partitions() const {
	return m_partition_table;
}

bool Daemon::get_info(const pmg::Handle& handle, pmgpub::ProcessStatusInfo* info) const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
	boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

	bool success = false;

	const Application* target_app = application(handle);

	if(target_app) {

		boost::recursive_mutex::scoped_lock scopedLock(target_app->mutex());

		const Manifest* manifest = target_app->manifest();

		if(manifest) {

			PMGProcessState state = manifest->process_state();
			info->state = state;

			info->start_time = *(manifest->start_time_ptr());
			info->stop_time = *(manifest->stop_time_ptr());

			info->failure_str = CORBA::string_dup(state_string(state));
			info->failure_str_hr = CORBA::string_dup(manifest->error_msg());

			info->start_info.client_host = CORBA::string_dup(manifest->requesting_host().c_str());
			info->start_info.host = CORBA::string_dup(handle.server().c_str());
			info->start_info.app_name = CORBA::string_dup(handle.applicationName().c_str());
			info->start_info.partition = CORBA::string_dup(handle.partitionName().c_str());
			info->start_info.executable = CORBA::string_dup(manifest->executable().full_name().c_str());
			info->start_info.wd = CORBA::string_dup(manifest->working_directory().c_full_name());
			info->start_info.rm_token = manifest->rm_token();

			std::string outFile = manifest->output_file().full_name();
			std::string errFile = manifest->error_file().full_name();
			info->start_info.streams.log_path = CORBA::string_dup(manifest->output_file().parent_name().c_str());
			info->start_info.streams.out_path = CORBA::string_dup(outFile.c_str());
			info->start_info.streams.err_path = CORBA::string_dup(errFile.c_str());
			info->start_info.streams.in_path = CORBA::string_dup(manifest->input_file().c_full_name());
			info->start_info.streams.perms = manifest->output_file_permission();
			if(manifest->output_file().full_name().compare("/dev/null") != 0) {
				info->start_info.streams.out_is_null = false;

	            try {
	                info->out_is_available = !std::filesystem::is_empty(outFile);
	            }
	            catch(std::filesystem::filesystem_error& ex) {
	                ERS_DEBUG(1, "Failed to get the status of out log file for process \"" << handle.toString() << "\" (" << outFile << "): " << ex.what());
	                info->out_is_available = false;
	            }

	            try {
	                info->err_is_available = !std::filesystem::is_empty(errFile);
	            }
	            catch(std::filesystem::filesystem_error& ex) {
	                ERS_DEBUG(1, "Failed to get the status of err log file for process \"" << handle.toString() << "\" (" << errFile << "): " << ex.what());
	                info->err_is_available = false;
	            }
			} else {
				info->start_info.streams.out_is_null = true;
				info->out_is_available = false;
				info->err_is_available = false;
			}

			std::vector<std::string> param_collection = manifest->parameters();
			std::vector<std::string>::const_iterator itt;
			info->start_info.start_args.length(param_collection.size());
			int pos = 0;
			for(itt = param_collection.begin(); itt != param_collection.end(); ++itt) {
				info->start_info.start_args[pos] = CORBA::string_dup(itt->c_str());
				++pos;
			}

			std::map<std::string, std::string> envMap = manifest->environnements();
			std::map<std::string, std::string>::const_iterator envIt;
			info->start_info.envs.length(envMap.size());
			pos = 0;
			for (envIt = envMap.begin(); envIt != envMap.end(); ++envIt) {
				info->start_info.envs[pos].name = CORBA::string_dup((envIt->first).c_str());
				info->start_info.envs[pos].value = CORBA::string_dup((envIt->second).c_str());
				++pos;
			}

			info->start_info.init_time_out = manifest->init_timeout();
            info->start_info.auto_kill_time_out = manifest->auto_kill_timeout();

			info->start_info.user.name = CORBA::string_dup(manifest->user().name_safe().c_str());

			info->info.process_id = manifest->process_id().process_id();
			info->info.signal_value = manifest->exit_signal();
			info->info.exit_value = manifest->exit_code();

			info->resource_usage.total_user_time = ((manifest->resource_usage_ptr())->ru_utime).tv_sec;
			info->resource_usage.total_system_time = ((manifest->resource_usage_ptr())->ru_stime).tv_sec;
			info->resource_usage.memory_usage = (manifest->resource_usage_ptr())->ru_maxrss;

			success = true;

		}

	}

	return success;

} // get_info

bool Daemon::link_client(const pmgpriv::LinkRequestInfo& requestInfo) const {

	boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
	boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

	// Look for the application object

	std::string handle_str(requestInfo.app_handle);
	Handle hdl(handle_str); // It may throw pmg::Invalid_Handle (catched by the Server which calls this method)
	Application* app = application(hdl);

	// Let the caller know that the application does not exist anymore
	if(!app) {
	    throw daq::pmg::Application_NotFound_Invalid(ERS_HERE, "", handle_str, hdl.applicationName(), hdl.partitionName());
	}

	// First check if the reference to the client already exists
	bool check_list = app->is_in_client_list(requestInfo.client_ref);

    bool done = false;
	if(check_list == false) {
	    // If the client reference does not exist then add it to the list
	    app->add_client_ref(requestInfo.client_ref);
	    done = true;
	}

	return done;

} // link_client

bool update_all_is(void* daemon_ptr) {

	Daemon* pDaemon = reinterpret_cast<Daemon*>(daemon_ptr);

	// Set the name of the IS server for the Agent info

	const std::string is_agent_server_name = std::string("PMG.") + std::string("AGENT_") + System::LocalHost::full_local_name();

	// We always want to add agent information to the initial partition, even if there are no processes running there
	try {
		AgentIS is_info_agent("initial", is_agent_server_name);
		is_info_agent.updateInfo(true);
		ERS_DEBUG(3,"IS info updated for Agent " << System::LocalHost::full_local_name() << " in partition initial");
	}
	catch(daq::pmg::Failed_Create_IS &ex) {
		ERS_DEBUG(3,"Not updating IS info for the Agent in partition initial. Reason: " << ex.message());
	}
	catch(daq::pmg::Failed_Publish_IS &ex) {
		ERS_DEBUG(3,"Not updating IS info for the Agent in partition initial. Reason: " << ex.message());
	}

	// Get partitions where the Agent is running

	std::map<std::string, pmg::Partition*> p_table;
	{
		boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
		p_table = pDaemon->partitions();
	}

	// Loop over all the partitions

	for(std::map<std::string, pmg::Partition *>::iterator pos = p_table.begin(); pos != p_table.end(); ++pos) {

		if(pos->second == 0) {
			continue;
		}

		const std::string part_name(pos->first);

		// Get the number of active processes

		const unsigned int numOfActProcs(pos->second->active_processes()); // It triggers garbage collection
		ERS_DEBUG(3, numOfActProcs << " active processes in partition " << part_name);

		// Check in IPC if the partition is valid

		IPCPartition partition(part_name);
		bool is_partition_valid = partition.isValid();

		if(is_partition_valid) {

			// Check if the IS PMG repository is present

			bool isRepoValid = false;

			try {
				isRepoValid = partition.isObjectValid<repository>(std::string("PMG"));
				if(isRepoValid == false) {
					ERS_DEBUG(3, "Not updating IS info in partition " << part_name << ". Reason: IS repository not valid");
				}
			}
			catch(daq::ipc::Exception &ex) {
				ERS_DEBUG(3, "Not updating IS info in partition " << part_name << ". Reason: " << ex.message());
			}

			if(isRepoValid == false) {
				continue;
			}

			/****************************** Start updating Agent info *****************************/

			// No need to update or remove the agent info from the initial partition
			// The information is always updated in the initial partition at the beginning
			// of this method and it should never be removed
			if(part_name != "initial") {
				try {
					AgentIS is_info_agent(part_name, is_agent_server_name);
					if(numOfActProcs > 0) {
						is_info_agent.updateInfo(true);
						ERS_DEBUG(3,"IS info updated for Agent " << System::LocalHost::full_local_name() << " in partition " << part_name);
					} else {
						if(is_info_agent.infoExists()) {
							is_info_agent.removeInfo();
							ERS_DEBUG(3,"IS info removed for Agent " << System::LocalHost::full_local_name() << " in partition " << part_name);
						}
					}
				}
				catch(daq::pmg::Failed_Create_IS &ex) {
					ERS_DEBUG(3,"Not updating IS info for the Agent in partition " << part_name << ". Reason: " << ex.message());
				}
				catch(daq::pmg::Failed_Publish_IS &ex) {
					ERS_DEBUG(3,"Not updating IS info for the Agent in partition " << part_name << ". Reason: " << ex.message());
				}
				catch(daq::pmg::Failed_Remove_IS &ex) {
					ERS_DEBUG(3,"Not removing IS info for the Agent in partition " << part_name << ". Reason: " << ex.message());
				}
			}

			/****************************** Start updating process info *****************************/

			// Get all applications in the partition

			boost::recursive_mutex::scoped_lock scoped_lock(Daemon::application_table_mutex);

			application_collection_t partition_apps;
			partition_apps = pos->second->all_applications(partition_apps);

			for(application_collection_t::const_iterator it = partition_apps.begin(); it != partition_apps.end(); ++it) {

				// Check if the Application object is still valid

				if((*it)) {

					const std::string app_name((*it)->toHandle()->applicationName());
					const std::string part_name((*it)->toHandle()->partitionName());

					const std::string is_process_server_name(std::string("PMG.") + (*it)->toHandle()->server() + daq::pmg::IS_SERVER_PROCESS_SEPARATOR + app_name);

					// Check if the application is active
					// If it is update process info in IS

					{
						boost::recursive_mutex::scoped_lock scopedLock((*it)->mutex());

						if((*it)->is_active() && !((*it)->invalid())) {

							const Manifest* const app_manifest = (*it)->manifest();

							if(app_manifest->process_state() != PMGProcessState_NOTAV) {
								try {
									ProcessIS is_info_process(app_manifest, part_name, is_process_server_name);
									is_info_process.updateInfo(true);
									ERS_DEBUG(3,"IS info updated for process " << app_name << " in partition " << part_name);
								}
								catch(daq::pmg::Failed_Create_IS &e) {
									ERS_DEBUG(3,"Not updating IS info for process " << app_name << " in partition " << part_name << ": " << e.message());
								}
								catch(daq::pmg::Failed_Publish_IS &e) {
									ERS_DEBUG(3,"Not updating IS info for process " << app_name << " in partition " << part_name << ": " << e.message());
								}
							}

						}

					} // close scope

				}

			}

		} /* if(is_partition_valid) */else {

			// Remove the Partition object form the map and delete it
			// Check if no more applications are active in the partition

			boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
			boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

			if(numOfActProcs == 0) {

				ERS_DEBUG(1,"Removing partition " << pos->first);

				try {
					ERS_DEBUG(1,"Removing directory " << (pos->second->directory()).full_name());
					(pos->second->directory()).remove();
				}
				catch(System::PosixIssue &ex) {
					ERS_DEBUG(1,"Exception while removing directory " << (pos->second->directory()).full_name() << ": " << ex.what());
				}

				delete(pos->second);
				pos->second = 0;

				pDaemon->removePartition(pos->first);

			}

		}

	} // 'for' partition table

	return true;

} // update_all_is

void Daemon::removePartition(const std::string& partitionName) {
	partition_table_t::iterator pos = m_partition_table.find(partitionName);
	if(pos != m_partition_table.end()) {
		m_partition_table.erase(pos);
	}
}

void Daemon::stopAllReportThreads() const {

	ERS_INFO("Stopping all the report threads (not killing the started processes!). It may take some time...");

	while(active_processes() > 0) { // When the report thread is terminated the Application object is set as invalid
		boost::recursive_mutex::scoped_lock scoped_lock(Daemon::partition_table_mutex);
		boost::recursive_mutex::scoped_lock _scoped_lock(Daemon::application_table_mutex);

		application_collection_t allApps;
		allApps = all_applications();
		for(application_collection_t::const_iterator iter = allApps.begin(); iter != allApps.end(); ++iter) {
			if(*iter) {
				(*iter)->stopReportThread();
			}
		}
		usleep(10000);
	}

}

}} // daq::pmg

std::ostream& operator<<(std::ostream& stream, const daq::pmg::Daemon& daemon) {
	daemon.print_to(stream);
	return stream;
} // operator<<
