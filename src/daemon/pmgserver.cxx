/*
 *  pmgserver.cxx
 *  pmg
 *
 *  Created by Marc Dobson on 22-06-05.
 *
 */

#include <signal.h>
#include <stdlib.h>
#include <system/System.h>

#include "ProcessManager/Server.h"
#include "ProcessManager/Utils.h"

#include <boost/program_options.hpp>

using namespace daq;
namespace po = boost::program_options;

pmg::Server* my_server;

extern "C" {
static void signal_handler(int sig_num) {
	signal(sig_num, SIG_IGN);
	my_server->shutdown();
}
}

int main(int argc, char** argv) {
	::setenv("TDAQ_APPLICATION_NAME", "PMGServer", 1);

	// Command line parsing
	bool allowServerToExit(false);
	try {
		po::options_description desc("pmgserver: it starts, stops and controls processed in the TDAQ system");

		desc.add_options()
				("AllowKill,k", po::bool_switch(&allowServerToExit), "Whether to allow or not the server to exit on explicit (CORBA) user request")
				("help,h", "Print help message");

		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
		po::notify(vm);

		if(vm.count("help")) {
			std::cout << desc << std::endl;
			return EXIT_SUCCESS;
		}
	}
	catch(std::exception& ex) {
		ERS_LOG("Bad command line options: " << ex.what());
		return EXIT_FAILURE;
	}

	try {
		// Extract CORBA command-line options
		std::list<std::pair<std::string, std::string> > optList = IPCCore::extractOptions(argc, argv);

		unsigned int portNumber = 0;

		try {

			// Get the port number to use in CORBA communications
			portNumber = daq::pmg::utils::serverPort();
			if(portNumber != 0) {
				std::ostringstream portNumber_str;
				portNumber_str << "giop:tcp::" << portNumber;
				std::pair<std::string, std::string> addedOpt = std::make_pair("endPoint", portNumber_str.str());
				optList.push_back(addedOpt);
			}

		}
		catch(daq::pmg::IPC_Port_Error &ex) {
			ers::fatal(ex);
			exit(EXIT_FAILURE);
		}

		// Init the IPC system
		IPCCore::init(optList);

		std::string server_name = "AGENT_" + System::LocalHost::full_local_name();
		ERS_INFO("Starting " << server_name);

		// Print the pmgserver initial environment
		std::string env;
		for(int i = 0;; i++) {
			if(environ[i] == NULL) {
				break;
			} else {
				env += environ[i];
				env += "\n";
			}
		}
		ers::info(pmg::Exception(ERS_HERE, "Dumping this pmgserver environment:\n" + env));

		std::string pmgUseDir;
		const char* pmgEnvDir = getenv("TDAQ_PMG_MANIFEST_AND_FIFOS_DIR");
		if(pmgEnvDir != NULL) {
			pmgUseDir = std::string(pmgEnvDir);
		} else {
			pmgUseDir = "/tmp/ProcessManager";
		}

		try {
			my_server = new pmg::Server(server_name, pmgUseDir, portNumber, allowServerToExit);
		}
		catch(daq::pmg::Bad_User_Name &ex) {
			ers::fatal(ex);
			exit(EXIT_FAILURE);
		}

		signal(SIGTERM, signal_handler);
		signal(SIGINT, signal_handler);
		signal(SIGPIPE, SIG_IGN);

		my_server->run();

		my_server->_destroy(true);

		exit(EXIT_SUCCESS);

	}
	catch(CORBA::Exception &ex) {
		ers::error(pmg::Exception(ERS_HERE, std::string("Got CORBA exception ") + std::string(ex._name())));
		exit(EXIT_FAILURE);
	}
	catch(std::exception &ex) {
		ers::error(pmg::Exception(ERS_HERE, std::string("Got exception: ") + std::string(ex.what()), ex));
		exit(EXIT_FAILURE);
	}
	catch(...) {
		ers::error(pmg::Exception(ERS_HERE, "Got an unknown exception"));
		exit(EXIT_FAILURE);
	}

} // main

