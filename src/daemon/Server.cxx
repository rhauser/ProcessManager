/*
 *  Server.cxx
 *
 *  Created by Marc Dobson on 22.06.05.
 *
 */

#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <pwd.h>
#include <signal.h>
#include <fstream>
#include <fcntl.h>
#include <set>

#include <ers/ers.h>
#include <system/System.h>
#include <system/exceptions.h>
#include <ipc/partition.h>
#include <ipc/alarm.h>
#include <is/infodictionary.h>
#include <is/criteria.h>

#include <boost/thread/thread.hpp>
#include <boost/bind/bind.hpp>

#include "ProcessManager/Server.h"
#include "ProcessManager/ISBridge.h"
#include "ProcessManager/RMBridge.h"
#include "ProcessManager/Exceptions.h"
#include "ProcessManager/Daemon.h"
#include "ProcessManager/private_defs.h"
#include "ProcessManager/Utils.h"
#include "ProcessManager/ProcInterface.h"
#include "ProcessManager/Handle.h"

#include "daq_tokens/verify.h"

std::string daq::pmg::Server::server_login_name;

namespace daq {
namespace pmg {

Server::Server(const std::string& server_id, const System::File& directory, const unsigned int port, bool canExit) :
	        IPCNamedObject<POA_pmgpriv::SERVER, ::ipc::multi_thread, ::ipc::persistent> (server_id),
	        m_sem(0),
	        m_i_can_die(canExit),
	        m_port(port)
{
	// Get the name of the user running this server
	configure_login_name();

	// Check if the global IPC server is running
	m_global_part = 0;

	bool isGlobalIPCServerReady = globalIPCServerReady();

	while(!isGlobalIPCServerReady) {
		isGlobalIPCServerReady = globalIPCServerReady();
		usleep(200000);
	}

	// Do no run the server if it is already running
	m_IPC_Ready_Alarm = 0;
	m_host_name = System::LocalHost::full_local_name();
	m_IPC_name = "AGENT_" + m_host_name;

	if(this->checkMyIPCReference()) {
		ERS_INFO("A PMG server is already running on this node.");
		::exit(0);
	}

	this->getIPCRefValue();

	ERS_ASSERT_MSG(m_ipc_ref_value != "", "Failed to find TDAQ_IPC_INIT_REF , exiting...");

	ERS_DEBUG(1,"TDAQ_IPC_INIT_REF variable has value: "<< m_ipc_ref_value);

	// Get the host the global IPC is running on and its PID
	//  ::ipc::servant::info_var info;

	//   bool got_IPC_info = false;
	//   while(!got_IPC_info) {
	//     try {
	//       IPCPartition partition;
	//       ::ipc::partition_var pi = partition.getImplementation();
	//       info = pi->information();
	//     }
	//     catch(...) {
	//       usleep(100000);
	//       continue;
	//     }
	//     got_IPC_info = true;
	//   }

	// Build the directory structure where manifests and FIFOs will be placed.
	std::ostringstream Directory;
	Directory << directory.full_name() << "/" << server_login_name;
	//    Directory << directory.full_name() << "/" << server_login_name << "/" << info->host << "/" << info->pid;

	// Instanciate the Daemon
	m_Daemon_ptr = new Daemon(Directory.str());

	// Try to connect to already existing launchers (i.e., after a Processmanager crash)

	ers::info(pmg::Exception(ERS_HERE, std::string("\nLooking in " + Directory.str()
	        + " for manifets and FIFOs")));

	try {
		connectLaunchers(Directory.str());
		manifestVect.clear();
	}
	catch(System::PosixIssue& ex) {
		ers::error(pmg::Reconnection_Error(ERS_HERE,
		                                   "Exception while trying to re-connect to launchers",
		                                   ex.message(),
		                                   ex));
	}
	catch(ers::Issue& ex) {
		ers::error(pmg::Reconnection_Error(ERS_HERE,
		                                   "Exception while trying to re-connect to launchers",
		                                   ex.message(),
		                                   ex));
	}
	catch(std::exception& ex) {
		ers::error(pmg::Reconnection_Error(ERS_HERE,
		                                   "Exception while trying to re-connect to launchers",
		                                   ex.what(),
		                                   ex));
	}

	// Publish ourself in the global IPC server
	publishInGlobalIPC();

	// Set an alarm to test if the global IPC server is up and do whatever is needed

	if(m_port == 0) {
		m_IPC_Ready_Alarm = new IPCAlarm(IPC_READY_PERIOD, doWhenGlobalIPCReady, this);
		ERS_DEBUG(3, "Started alarm checking IPC reference with a " << IPC_READY_PERIOD << " seconds period;" );
	} else {
		m_IPC_Ready_Alarm = new IPCAlarm(IPC_READY_PERIOD_LONG, doWhenGlobalIPCReady, this);
		ERS_DEBUG(3, "Started alarm checking IPC reference with a " << IPC_READY_PERIOD_LONG << " seconds period." );
	}

	// Mutex unlocked on exit
} // Server()

Server::~Server() {

	ERS_DEBUG(3, "Server destructor called");

	delete m_IPC_Ready_Alarm;

	m_Daemon_ptr->stopAllReportThreads(); // Stop all the report threads at exit

	delete m_global_part;
	delete m_Daemon_ptr;

	// Unpublish this IPC Named Object
	try {
		this->withdraw();
	}
	catch(ers::Issue &e) {
		ers::error(e);
	}
} // ~Server()

bool Server::alive() {
	return true;
}

char* Server::request_start(const pmgpriv::ProcessRequestInfo& requestInfo) {

	try {
        pmgpriv::ProcessRequestInfo requestInfoModified(requestInfo);
        auto real_name = verify_credentials((const char *)requestInfo.user.name);
        requestInfoModified.user.name = real_name.c_str();
		CORBA::String_var _handle_ = m_Daemon_ptr->request_start(requestInfoModified);
		return CORBA::string_dup(_handle_);
	}
	catch(pmg::AM_Not_Allowed& ex) {
		ers::error(ex);
		throw pmgpriv::start_ex(ex.what());
	}
	catch(pmg::Binary_Not_Found& ex) {
		ers::error(ex);
		throw pmgpriv::start_ex(ex.what());
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex );
		throw pmgpriv::start_ex(ex.what());
	}
	catch(System::PosixIssue & ex) {
		ERS_DEBUG(1, ex );
		std::string eMsg = ex.message();
		if(ex.get_error() != 0) {
			eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
		}
		throw pmgpriv::start_ex(eMsg.c_str()); // proper IDL exception here!
	}
	catch(ers::Issue & ex) {
		ERS_DEBUG(1, ex );

		std::ostringstream s;
		s << ex << std::ends;
		const std::string& msg = s.str();

		throw pmgpriv::start_ex(msg.c_str());
	}
	catch(std::exception & ex) {
		ERS_DEBUG(1, "Standard exception thrown in request_start: " << ex.what() );
		std::string what = "Standard exception thrown in request_start: ";
		what.append(ex.what());
		throw pmgpriv::start_ex(what.c_str());
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown" );
		throw pmgpriv::start_ex("Unknown exception thrown in request_start");
	}
} // start()

void Server::really_start(const char* handle) {
	try {
		const pmg::Handle hdl(handle);
		m_Daemon_ptr->really_start(hdl);
	}
	catch(pmg::No_RM_Resources& ex) {
		ers::error(ex);
		throw pmgpriv::start_ex(ex.what());
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex );
		throw pmgpriv::start_ex(ex.what());
	}
	catch(System::NotFoundIssue & ex) {
		ERS_DEBUG(1, ex );
		throw pmgpriv::start_ex(ex.what());
	}
	catch(System::PosixIssue & ex) {
		ERS_DEBUG(1, ex );
		std::string eMsg = ex.message();
		if(ex.get_error() != 0) {
			eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
		}
		throw pmgpriv::start_ex(eMsg.c_str()); // proper IDL exception here!
	}
	catch(ers::Issue & ex) {
		ERS_DEBUG(1, ex );

        std::ostringstream s;
        s << ex << std::ends;
        const std::string& msg = s.str();

		throw pmgpriv::start_ex(msg.c_str());
	}
	catch(std::exception & ex) {
		ERS_DEBUG(1, "Standard exception thrown in really_start: " << ex.what() );
		std::string what = "Standard exception thrown in really_start: ";
		what.append(ex.what());
		throw pmgpriv::start_ex(what.c_str());
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown" );
		throw pmgpriv::start_ex("Unknown exception thrown in request_start");
	}
}

void Server::signal(const char* handle,
                    const CORBA::Long signum,
                    const char* userName,
                    const char* hostName)
{
	try {
		const pmg::Handle hdl(handle);
		m_Daemon_ptr->signal(hdl, signum, verify_credentials(userName), std::string(hostName));
	}
	catch(pmg::AM_Not_Allowed &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_NOT_ALLOWED_BY_AM);
	}
	catch(pmg::Application_NotFound_Invalid &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_APPLICATION_NOTFOUND_INVALID);
	}
	catch(pmg::Application_Manifest_Already_Unmapped &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_MANIFEST_UNMAPPED);
	}
	catch(pmg::Launcher_Not_Running &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_LAUNCHER_NOT_RUNNING);
	}
	catch(pmg::FIFO_Error &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_FIFO_ERROR);
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex);
		std::string errMsg = "Unexpected ProcessManager exception thrown in signal(): "
		        + ex.message();
		throw pmgpriv::signal_ex(errMsg.c_str(), PMGSignalException_UNEXPECTED);
	}
    catch(ers::Issue & ex) {
        ERS_DEBUG(1, ex);

        std::ostringstream s;
        s << ex << std::ends;
        const std::string& msg = s.str();

        throw pmgpriv::signal_ex(msg.c_str(), PMGSignalException_UNEXPECTED);
    }
	catch(std::exception& ex) {
		ERS_DEBUG(1, "Standard exception thrown in signal(): " << ex.what() );
		std::string what = "Standard exception thrown in signal(): ";
		what.append(ex.what());
		throw pmgpriv::signal_ex(what.c_str(), PMGSignalException_UNEXPECTED);
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown in signal()");
		throw pmgpriv::signal_ex("Unknown exception thrown in signal()", PMGSignalException_UNKNOWN);
	}
} // signal()

void Server::stop(const char* handle, const char* userName, const char* hostName) {
    try {
		const pmg::Handle hdl(handle);
		m_Daemon_ptr->stop(hdl, verify_credentials(userName), std::string(hostName));
	}
	catch(pmg::AM_Not_Allowed &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_NOT_ALLOWED_BY_AM);
	}
	catch(pmg::Application_NotFound_Invalid &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_APPLICATION_NOTFOUND_INVALID);
	}
	catch(pmg::Application_Manifest_Already_Unmapped &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_MANIFEST_UNMAPPED);
	}
	catch(pmg::Launcher_Not_Running &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_LAUNCHER_NOT_RUNNING);
	}
	catch(pmg::FIFO_Error &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_FIFO_ERROR);
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex);
		std::string errMsg = "Unexpected ProcessManager exception thrown in stop(): "
		        + ex.message();
		throw pmgpriv::signal_ex(errMsg.c_str(), PMGSignalException_UNEXPECTED);
	}
    catch(ers::Issue & ex) {
        ERS_DEBUG(1, ex);

        std::ostringstream s;
        s << ex << std::ends;
        const std::string& msg = s.str();

        throw pmgpriv::signal_ex(msg.c_str(), PMGSignalException_UNEXPECTED);
    }
	catch(std::exception& ex) {
		ERS_DEBUG(1, "Standard exception thrown in stop(): " << ex.what() );
		std::string what = "Standard exception thrown in stop(): ";
		what.append(ex.what());
		throw pmgpriv::signal_ex(what.c_str(), PMGSignalException_UNEXPECTED);
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown in stop()");
		throw pmgpriv::signal_ex("Unknown exception thrown in stop()", PMGSignalException_UNKNOWN);
	}
} // stop()

void Server::kill(const char* handle, const char* userName, const char* hostName) {
	try {
		const pmg::Handle hdl(handle);
		m_Daemon_ptr->kill(hdl, verify_credentials(userName), std::string(hostName));
	}
	catch(pmg::AM_Not_Allowed &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_NOT_ALLOWED_BY_AM);
	}
	catch(pmg::Application_NotFound_Invalid &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_APPLICATION_NOTFOUND_INVALID);
	}
	catch(pmg::Application_Manifest_Already_Unmapped &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_MANIFEST_UNMAPPED);
	}
	catch(pmg::Launcher_Not_Running &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_LAUNCHER_NOT_RUNNING);
	}
	catch(pmg::FIFO_Error &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_FIFO_ERROR);
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex);
		std::string errMsg = "Unexpected ProcessManager exception thrown in kill(): "
		        + ex.message();
		throw pmgpriv::signal_ex(errMsg.c_str(), PMGSignalException_UNEXPECTED);
	}
    catch(ers::Issue & ex) {
        ERS_DEBUG(1, ex);

        std::ostringstream s;
        s << ex << std::ends;
        const std::string& msg = s.str();

        throw pmgpriv::signal_ex(msg.c_str(), PMGSignalException_UNEXPECTED);
    }
	catch(std::exception& ex) {
		ERS_DEBUG(1, "Standard exception thrown in kill(): " << ex.what() );
		std::string what = "Standard exception thrown in kill(): ";
		what.append(ex.what());
		throw pmgpriv::signal_ex(what.c_str(), PMGSignalException_UNEXPECTED);
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown in kill()");
		throw pmgpriv::signal_ex("Unknown exception thrown in kill()", PMGSignalException_UNKNOWN);
	}
} // kill()

void Server::kill_soft(const char* handle,
                       const CORBA::Long timeout,
                       const char* userName,
                       const char* hostName)
{
	try {
		const pmg::Handle hdl(handle);
		m_Daemon_ptr->kill_soft(hdl, timeout, verify_credentials(userName), std::string(hostName));
	}
	catch(pmg::AM_Not_Allowed &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_NOT_ALLOWED_BY_AM);
	}
	catch(pmg::Application_NotFound_Invalid &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_APPLICATION_NOTFOUND_INVALID);
	}
	catch(pmg::Application_Manifest_Already_Unmapped &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_MANIFEST_UNMAPPED);
	}
	catch(pmg::Launcher_Not_Running &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_LAUNCHER_NOT_RUNNING);
	}
	catch(pmg::FIFO_Error &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::signal_ex(ex.what(), PMGSignalException_FIFO_ERROR);
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex);
		std::string errMsg = "Unexpected ProcessManager exception thrown in kill_soft(): "
		        + ex.message();
		throw pmgpriv::signal_ex(errMsg.c_str(), PMGSignalException_UNEXPECTED);
	}
    catch(ers::Issue & ex) {
        ERS_DEBUG(1, ex);

        std::ostringstream s;
        s << ex << std::ends;
        const std::string& msg = s.str();

        throw pmgpriv::signal_ex(msg.c_str(), PMGSignalException_UNEXPECTED);
    }
	catch(std::exception& ex) {
		ERS_DEBUG(1, "Standard exception thrown in kill_soft(): " << ex.what() );
		std::string what = "Standard exception thrown in kill_soft(): ";
		what.append(ex.what());
		throw pmgpriv::signal_ex(what.c_str(), PMGSignalException_UNEXPECTED);
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown in kill_soft()");
		throw pmgpriv::signal_ex("Unknown exception thrown in kill_soft()",
		                         PMGSignalException_UNKNOWN);
	}
} // kill()

bool Server::kill_partition(const char* partition,
                            const CORBA::Long timeout,
                            const char* userName,
                            const char* hostName)
{
	try {
		return m_Daemon_ptr->kill_partition(std::string(partition),
		                                    timeout,
		                                    verify_credentials(userName),
		                                    std::string(hostName));
	}
    catch(daq::tokens::CannotVerifyToken &ex) {
        std::ostringstream s;
        s << ex << std::ends;
        const std::string& msg = s.str();

  		throw pmgpriv::kill_partition_ex(msg.c_str(), "");
    }
	catch(pmg::Partition_Signal_Exception &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::kill_partition_ex(ex.what(), ex.get_proc_handles().c_str());
	}
} // kill_partition()


void Server::kill_all(const CORBA::Long timeout, const char* userName, const char* hostName) {
	try {
		m_Daemon_ptr->kill_all(timeout, verify_credentials(userName), std::string(hostName));
	}
    catch(daq::tokens::CannotVerifyToken &ex) {
        std::ostringstream s;
        s << ex << std::ends;
        const std::string& msg = s.str();

   		throw pmgpriv::kill_all_ex(msg.c_str(), "");
    }
	catch(pmg::Failed_Kill_All &ex) {
		ERS_DEBUG(1,ex);
		throw pmgpriv::kill_all_ex(ex.what(), ex.get_proc_handles().c_str());
	}
} // kill_all()


char* Server::lookup(const char* app_name, const char* part_name) {
	try {
		CORBA::String_var _handle_ = m_Daemon_ptr->lookup(std::string(app_name),
		                                                  std::string(part_name));
		return CORBA::string_dup(_handle_);
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex);
		std::string errMsg = "Unexpected ProcessManager exception thrown in signal(): "
		        + ex.message();
		throw pmgpriv::lookup_ex(errMsg.c_str());
	}
	catch(System::NotFoundIssue & ex) {
		ERS_DEBUG(1, ex );
		throw pmgpriv::lookup_ex(ex.what());
	}
	catch(System::PosixIssue & ex) {
		ERS_DEBUG(1, ex );
		std::string eMsg = ex.message();
		if(ex.get_error() != 0) {
			eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
		}
		throw pmgpriv::lookup_ex(eMsg.c_str());
	}
	catch(ers::Issue & ex) {
		ERS_DEBUG(1, ex );

		std::ostringstream s;
		s << ex << std::ends;
		const std::string& msg = s.str();

		throw pmgpriv::lookup_ex(msg.c_str());
	}
	catch(std::exception & ex) {
		ERS_DEBUG(1, "Standard exception thrown in lookup: " << ex.what() );
		std::string what = "Standard exception thrown in lookup: ";
		what.append(ex.what());
		throw pmgpriv::lookup_ex(what.c_str());
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown" );
		throw pmgpriv::lookup_ex("Unknown exception thrown in lookup");
	}
} // lookup()

void Server::processes_info(const char* partition, pmgpriv::proc_info_list_out procInfoList) {
	try {
		procInfoList = new pmgpriv::proc_info_list{};
		m_Daemon_ptr->processes_info(std::string(partition), procInfoList);
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex );
		std::string errMsg = "Unexpected ProcessManager exception thrown in processes_info(): "
		        + ex.message();
		throw pmgpriv::processes_ex(errMsg.c_str());
	}
	catch(System::PosixIssue & ex) {
		ERS_DEBUG(1, ex );
		std::string eMsg = ex.message();
		if(ex.get_error() != 0) {
			eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
		}
		throw pmgpriv::processes_ex(eMsg.c_str());
	}
	catch(ers::Issue & ex) {
		ERS_DEBUG(1, ex );

		std::ostringstream s;
		s << ex << std::ends;
		const std::string& msg = s.str();

		throw pmgpriv::processes_ex(msg.c_str());
	}
	catch(std::exception & ex) {
		ERS_DEBUG(1, "Standard exception thrown in processes_info(): " << ex.what() );
		std::string what = "Standard exception thrown in processes_info(): ";
		what.append(ex.what());
		throw pmgpriv::processes_ex(what.c_str());
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown" );
		throw pmgpriv::processes_ex("Unknown exception thrown in processes_info()");
	}
}

char* Server::processes(const char* partition) {
	try {
		CORBA::String_var procList = m_Daemon_ptr->processes(std::string(partition));
		return CORBA::string_dup(procList);
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex );
		std::string errMsg = "Unexpected ProcessManager exception thrown in processes(): "
		        + ex.message();
		throw pmgpriv::processes_ex(errMsg.c_str());
	}
	catch(System::NotFoundIssue & ex) {
		ERS_DEBUG(1, ex );
		throw pmgpriv::processes_ex(ex.what());
	}
	catch(System::PosixIssue & ex) {
		ERS_DEBUG(1, ex );
		std::string eMsg = ex.message();
		if(ex.get_error() != 0) {
			eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
		}
		throw pmgpriv::processes_ex(eMsg.c_str());
	}
	catch(ers::Issue & ex) {
		ERS_DEBUG(1, ex );

		std::ostringstream s;
		s << ex << std::ends;
		const std::string& msg = s.str();

		throw pmgpriv::processes_ex(msg.c_str());
	}
	catch(std::exception & ex) {
		ERS_DEBUG(1, "Standard exception thrown in processes(): " << ex.what() );
		std::string what = "Standard exception thrown in processes(): ";
		what.append(ex.what());
		throw pmgpriv::processes_ex(what.c_str());
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown" );
		throw pmgpriv::processes_ex("Unknown exception thrown in processes()");
	}
}

void Server::all_processes_info(pmgpriv::proc_info_list_out procInfoList) {
	try {
		procInfoList = new pmgpriv::proc_info_list{};
		m_Daemon_ptr->all_processes_info(procInfoList);
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex );
		std::string errMsg = "Unexpected ProcessManager exception thrown in all_processes_info(): "
		        + ex.message();
		throw pmgpriv::all_processes_ex(errMsg.c_str());
	}
	catch(System::PosixIssue & ex) {
		ERS_DEBUG(1, ex );
		std::string eMsg = ex.message();
		if(ex.get_error() != 0) {
			eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
		}
		throw pmgpriv::all_processes_ex(eMsg.c_str());
	}
	catch(ers::Issue & ex) {
		ERS_DEBUG(1, ex );

		std::ostringstream s;
		s << ex << std::ends;
		const std::string& msg = s.str();

		throw pmgpriv::all_processes_ex(msg.c_str());
	}
	catch(std::exception & ex) {
		ERS_DEBUG(1, "Standard exception thrown in all_processes_info(): " << ex.what() );
		std::string what = "Standard exception thrown in all_processes_info(): ";
		what.append(ex.what());
		throw pmgpriv::all_processes_ex(what.c_str());
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown");
		throw pmgpriv::all_processes_ex("Unknown exception thrown in all_processes_info()");
	}
}

char* Server::all_processes() {
	try {
		CORBA::String_var procList = m_Daemon_ptr->all_processes();
		return CORBA::string_dup(procList);
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex );
		std::string errMsg = "Unexpected ProcessManager exception thrown in all_processes(): "
		        + ex.message();
		throw pmgpriv::all_processes_ex(errMsg.c_str());
	}
	catch(System::NotFoundIssue & ex) {
		ERS_DEBUG(1, ex );
		throw pmgpriv::all_processes_ex(ex.what());
	}
	catch(System::PosixIssue & ex) {
		ERS_DEBUG(1, ex );
		std::string eMsg = ex.message();
		if(ex.get_error() != 0) {
			eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
		}
		throw pmgpriv::all_processes_ex(eMsg.c_str());
	}
	catch(ers::Issue & ex) {
		ERS_DEBUG(1, ex );

		std::ostringstream s;
		s << ex << std::ends;
		const std::string& msg = s.str();

		throw pmgpriv::all_processes_ex(msg.c_str());
	}
	catch(std::exception & ex) {
		ERS_DEBUG(1, "Standard exception thrown in all_processes(): " << ex.what() );
		std::string what = "Standard exception thrown in all_processes(): ";
		what.append(ex.what());
		throw pmgpriv::all_processes_ex(what.c_str());
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown" );
		throw pmgpriv::all_processes_ex("Unknown exception thrown in all_processes()");
	}
}

bool Server::exists(const char* handle) {
	try {
		const pmg::Handle hdl(handle);
		return m_Daemon_ptr->exists(hdl);
	}
	catch(System::NotFoundIssue & ex) {
		ERS_DEBUG(1, ex );
		throw pmgpriv::exists_ex(ex.what());
	}
	catch(System::PosixIssue & ex) {
		ERS_DEBUG(1, ex );
		std::string eMsg = ex.message();
		if(ex.get_error() != 0) {
			eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
		}
		throw pmgpriv::exists_ex(eMsg.c_str());
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex);
		std::string errMsg = "Unexpected ProcessManager exception thrown in exists(): "
		        + ex.message();
		throw pmgpriv::exists_ex(errMsg.c_str());
	}
	catch(ers::Issue & ex) {
		ERS_DEBUG(1, ex );

		std::ostringstream s;
		s << ex << std::ends;
		const std::string& msg = s.str();

		throw pmgpriv::exists_ex(msg.c_str());
	}
	catch(std::exception & ex) {
		ERS_DEBUG(1, "Standard exception thrown in exists: " << ex.what() );
		std::string what = "Standard exception thrown in exists: ";
		what.append(ex.what());
		throw pmgpriv::exists_ex(what.c_str());
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown" );
		throw pmgpriv::exists_ex("Unknown exception thrown in exist");
	}
} // exists()

bool Server::link_client(const pmgpriv::LinkRequestInfo& requestInfo) {
	try {
		return m_Daemon_ptr->link_client(requestInfo);
	}
	catch(pmg::Application_NotFound_Invalid&) {
	    throw pmgpriv::app_not_found_ex();
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex );
		std::string errMsg = "Unexpected ProcessManager exception thrown in link_client(): "
		        + ex.message();
		throw pmgpriv::link_client_ex(errMsg.c_str());
	}
	catch(System::NotFoundIssue & ex) {
		ERS_DEBUG(1, ex );
		throw pmgpriv::link_client_ex(ex.what());
	}
	catch(System::PosixIssue & ex) {
		ERS_DEBUG(1, ex );
		std::string eMsg = ex.message();
		if(ex.get_error() != 0) {
			eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
		}
		throw pmgpriv::link_client_ex(eMsg.c_str());
	}
	catch(ers::Issue & ex) {
		ERS_DEBUG(1, ex );

        std::ostringstream s;
        s << ex << std::ends;
        const std::string& msg = s.str();

		throw pmgpriv::link_client_ex(msg.c_str());
	}
	catch(std::exception & ex) {
		ERS_DEBUG(1, "Standard exception thrown in link_client: " << ex.what() );
		std::string what = "Standard exception thrown in link_client: ";
		what.append(ex.what());
		throw pmgpriv::link_client_ex(what.c_str());
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown" );
		throw pmgpriv::link_client_ex("Unknown exception thrown in link_client");
	}
} // link_client


void Server::procFiles(const char* fileName, CORBA::Long maxMsgSize, pmgpriv::File_out binFile) {

	errno = 0;

	const long msgSizeTollerance = 512; // Do not use all the CORBA msg size. Leave room for CORBA headers and so on.

	// Get the max msg size taking into account both server and client limit
	long serverOrbMaxMsgSize = (long) omniORB::giopMaxMsgSize() - msgSizeTollerance;
	long clientOrbMaxMsgSize = (long) maxMsgSize - msgSizeTollerance;

	long orbMaxMsgSize;
	if(serverOrbMaxMsgSize <= clientOrbMaxMsgSize) {
		orbMaxMsgSize = serverOrbMaxMsgSize;
	} else {
		orbMaxMsgSize = clientOrbMaxMsgSize;
	}

	if(orbMaxMsgSize <= 0) {
		throw pmgpriv::procFiles_ex("Value of omniORB::giopMaxMsgSize() too small.", 0);
	}

	// Open the file and set the FD_CLOEXEC flag.
	int fd = ::open(fileName, O_RDONLY);
	if(fd == -1) {
	    const int code = errno;
		const std::string errMsg = "Cannot open file " + std::string(fileName) + "("
		        + daq::pmg::utils::errno2String(code) + ")";
		throw pmgpriv::procFiles_ex(errMsg.c_str(), code);
	}
	int fileFlags = ::fcntl(fd, F_GETFD);
	if(fileFlags != -1) {
		::fcntl(fd, F_SETFD, fileFlags | FD_CLOEXEC);
	}

	// Get the file size
	struct stat st;
	int statRes = ::fstat(fd, &st);

	if(statRes == -1) {
	    const int code = errno;

		::close(fd);

		const std::string errMsg = "Cannot get the size of file " + std::string(fileName) + "("
		        + daq::pmg::utils::errno2String(code) + ")";
		throw pmgpriv::procFiles_ex(errMsg.c_str(), code);
	}

	off_t fileSize = st.st_size;

	// Find which part of the file to send
	size_t bytesToRead;
	off_t startReadingFrom;
	int buffSize;

	if((long) fileSize < orbMaxMsgSize) {
		// Send the whole file
		buffSize = fileSize + 1;
		bytesToRead = fileSize;
		startReadingFrom = 0;
	} else {
		// Tail the file
		buffSize = orbMaxMsgSize;
		bytesToRead = orbMaxMsgSize - 1;
		startReadingFrom = fileSize - orbMaxMsgSize + 1;
	}

	// Allocate the buffer to store the file content
	CORBA::Octet* buf = pmgpriv::File::allocbuf(buffSize);

	if(buf == nullptr) {
		::close(fd);
		throw pmgpriv::procFiles_ex("Cannot allocate memory buffer", 0);
	}

	// Read the file
	int readRes = ::pread(fd, buf, bytesToRead, startReadingFrom);
	if(readRes == -1) {
	    const int code = errno;

		::close(fd);

		const std::string errMsg = "Cannot read from file " + std::string(fileName) + "("
		        + daq::pmg::utils::errno2String(code) + ")";
		throw pmgpriv::procFiles_ex(errMsg.c_str(), code);
	}

	// Add '\0' at the end of the buffer
	buf[buffSize - 1] = '\0';

	ERS_DEBUG(3, "Read file " << fileName << " with the following parameters: " <<
			"\nBuffer size = " << buffSize <<
			"\nBytes to read = " << bytesToRead <<
			"\nStart reading from = " << startReadingFrom <<
			"\nBytes read = " << readRes);

	// Close file descriptor
	::close(fd);

	// Init the CORBA sequence with the file buffer
	binFile = new pmgpriv::File((CORBA::ULong) buffSize, (CORBA::ULong) buffSize, buf, 1);

}

void Server::agentInfo(pmgpub::AgentInfo_out info) {
	info = new pmgpub::AgentInfo{};

	info->mntPoints = CORBA::string_dup(daq::pmg::utils::mntPointInfo().c_str());

	daq::pmg::utils::HostInfo hInfo{};
	bool infoSuccess = daq::pmg::utils::hostInfo(hInfo);
	if(infoSuccess == true) {
		info->hostProcs = hInfo.procs;
		info->hostTotalRam = hInfo.totalram;
		info->hostTotalSwap = hInfo.totalswap;
		info->hostUsedRam = (unsigned long) hInfo.used_ram;
		info->hostUsedSwap = (unsigned long) hInfo.used_swap;
	} else {
		const std::string errMsg("Failed to retrieve information about the host memory status");
		throw pmgpriv::agentInfo_ex(errMsg.c_str());
	}

	double load[3];
	int loadAvg = getloadavg(load, 3);
	if(loadAvg != -1) {
		info->hostAvgLoad[0] = load[0];
		info->hostAvgLoad[1] = load[1];
		info->hostAvgLoad[2] = load[2];
	} else {
		const std::string errMsg = std::string("Failed executing system call \"getloadavg\" (")
		        + daq::pmg::utils::errno2String(errno) + std::string(")");
		throw pmgpriv::agentInfo_ex(errMsg.c_str());
	}

	try {
		pmg::ProcInterface proc((int) getpid());
		pmg::proc_usage p_stat = proc.procInfo();

		info->memUsageTotal = (float) (p_stat.statm_info).size * (float(getpagesize()) / (1024.
		        * 1024.));
		info->memUsageResident = (float) (p_stat.statm_info).resident * (float(getpagesize())
		        / (1024. * 1024.));
	}
	catch(pmg::Exception& ex) {
		const std::string errMsg = "Error getting info from proc file system: " + ex.message();
		throw pmgpriv::agentInfo_ex(errMsg.c_str());
	}
}

bool Server::get_info(const char* handle, pmgpub::ProcessStatusInfo_out info) {
	try {
		const pmg::Handle hdl(handle);
		info = new pmgpub::ProcessStatusInfo{};
		return m_Daemon_ptr->get_info(hdl, info);
	}
	catch(System::NotFoundIssue & ex) {
		ERS_DEBUG(1, ex );
		throw pmgpriv::get_info_ex(ex.what());
	}
	catch(System::PosixIssue & ex) {
		ERS_DEBUG(1, ex );
		std::string eMsg = ex.message();
		if(ex.get_error() != 0) {
			eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
		}
		throw pmgpriv::get_info_ex(eMsg.c_str());
	}
	catch(pmg::Exception &ex) {
		ERS_DEBUG(1, ex );
		std::string errMsg = "Unexpected ProcessManager exception thrown in get_info(): "
		        + ex.message();
		throw pmgpriv::get_info_ex(errMsg.c_str());
	}
	catch(ers::Issue & ex) {
		ERS_DEBUG(1, ex );

        std::ostringstream s;
        s << ex << std::ends;
        const std::string& msg = s.str();

		throw pmgpriv::get_info_ex(msg.c_str());
	}
	catch(std::exception & ex) {
		ERS_DEBUG(1, "Standard exception thrown in get_info: " << ex.what() );
		std::string what = "Standard exception thrown in get_info: ";
		what.append(ex.what());
		throw pmgpriv::get_info_ex(what.c_str());
	}
	catch(...) {
		ERS_DEBUG(1, "Unknown exception thrown" );
		throw pmgpriv::get_info_ex("Unknown exception thrown in get_info");
	}
}

void Server::shutdown() {
	m_sem.post();
} // shutdown()

void Server::run() {
    m_sem.wait();
} // shutdown()

void Server::exit() {
	if(m_i_can_die == true) {
		ers::warning(pmg::Exception(ERS_HERE, "This agent has been asked to exit, and it is going to do it..."));
		shutdown();
	} else {
		ers::error(pmg::Exception(ERS_HERE, "This agent has been asked to exit, but this is not going to happen"));
		throw pmgpriv::exit_ex("The server has not been configured to exit on user request");
	}
}

bool Server::globalIPCServerReady() {
	// Check if the Global IPC Server is up
	if(m_global_part == 0)
		m_global_part = new IPCPartition();
	bool validity = m_global_part->isValid();
	if(!validity) {
		delete m_global_part;
		m_global_part = 0;
	}
	return validity;
} // globalIPCServerReady()


bool Server::checkMyIPCReference() {
	ERS_PRECONDITION(m_global_part);

	try {
		if(m_global_part->isObjectValid<pmgpriv::SERVER, ::ipc::no_cache, ::ipc::non_existent> (m_IPC_name)) {
			return true;
		}
	}
	catch(ipc::InvalidPartition &ex) {
		ERS_DEBUG(1,ex);
	}

	return false;
} // checkMyIPCReference()


void Server::publishInGlobalIPC() {
	if(!this->checkMyIPCReference()) {
		//  if(1) {
		try {
			this->publish();
		}
		catch(ers::Issue &e) {
			// Publication in IPC was not succesful so exit
			ers::fatal(e);
			::exit(1);
		}

		if(!this->checkMyIPCReference()) {
			// Cannot read a reference for myself so exit
			std::string errorString =
			        "Can not get a reference for myself in IPC after publishing. Exiting ...";
			ers::error(pmg::Exception(ERS_HERE, errorString));
			::exit(1);
		}

		// Publish was successful
		ERS_DEBUG(1,"Publication in IPC was succesful.");
	}
	ERS_DEBUG(30, "IPC Partition up and PMG published correctly");
	return;
} // publishInGlobalIPC()


bool Server::getIPCRefValue() {
	m_ipc_ref_value = ::ipc::util::getInitialReference();
	if(m_ipc_ref_value == "") {
		ERS_DEBUG(1,"TDAQ_IPC_INIT_REF is not defined. This is a fatal error.");
		return false;
	}

	return true;
}

void Server::connectLaunchers(const std::string& dirName) {

	// Look for valid manifest files in the system (i.e., after a Processmanager crash)

	scan(dirName);

	std::vector<std::string>::iterator vectManifestIter;
	std::string manifestNames;

	for(vectManifestIter = manifestVect.begin(); vectManifestIter != manifestVect.end(); ++vectManifestIter) {
		manifestNames += *vectManifestIter + "\n";
	}

	// Partitions hosting running applications before the server has been (re)started
	std::set<std::string> partitions;

	int notRunning = 0;
	int manifestsNum = manifestVect.size();
	std::ostringstream manNum;
	manNum << manifestsNum;
	ers::info(pmg::Exception(ERS_HERE, std::string("Found " + manNum.str()
	        + " valid manifest files:\n" + manifestNames)));

	for(vectManifestIter = manifestVect.begin(); vectManifestIter != manifestVect.end(); ++vectManifestIter) {
		ers::info(pmg::Exception(ERS_HERE, std::string("Reconnecting to launcher using "
		        + *vectManifestIter + " manifest")));
		System::File manifestFile(*vectManifestIter);
		Manifest manifest(manifestFile, true, false);

		try {
			manifest.map();
			try {
				(manifest.fd())->closeOnExec();
			}
			catch(System::SystemCallIssue &ex) { // Exception thrown by closeOnExec()
				std::string msg = ex.message()
				        + ". File descriptor will stay open in the child process";
				ers::warning(pmg::Exception(ERS_HERE, msg));
			}
		}
		catch(System::PosixIssue &ex) {
			ers::error(pmg::Reconnection_Error(ERS_HERE,
			                                   "manifest not mapped",
			                                   pmg::Manifest_Not_Mapped(ERS_HERE,
			                                                            *vectManifestIter,
			                                                            ex.message())));
			continue;
		}
		catch(ers::Issue &ex) {
			ers::error(pmg::Reconnection_Error(ERS_HERE,
			                                   "manifest not mapped",
			                                   pmg::Manifest_Not_Mapped(ERS_HERE,
			                                                            *vectManifestIter,
			                                                            ex.message())));
			continue;
		}
		catch(...) {
			ers::error(pmg::Reconnection_Error(ERS_HERE,
			                                   "manifest not mapped",
			                                   pmg::Manifest_Not_Mapped(ERS_HERE,
			                                                            *vectManifestIter,
			                                                            "Unknown")));
			continue;
		}

		bool launcher_exists = false;

		pid_t launcherPID = (manifest.launcher_pid()).process_id();
		if(launcherPID > 0) {
			std::string appName;
			std::ostringstream cmdlnFile;
			cmdlnFile << "/proc/" << launcherPID << "/cmdline";
			std::ifstream fileStream(cmdlnFile.str().c_str(), std::ios::in);
			std::getline(fileStream, appName, '\0');
			if(appName.find(LAUNCHER_NAME) != std::string::npos) {
				int signalResult = ::kill(launcherPID, 0);
				int error = errno;
				if(signalResult == 0) {
					// The Process still exists
					launcher_exists = true;
				} else {
					if(error != ESRCH) {
						// ESRCH means that the pid or process group does not exist: the launcher exited
						launcher_exists = true;
					}
				}
			}
		}

		if(launcher_exists) {

			Application* app_ptr = 0;

			try {
				std::string handle = manifest.handle();
				const Handle h(handle);
				app_ptr = m_Daemon_ptr->build_app(h);
				app_ptr->open_report_fifo();
			}
			catch(ers::Issue &e) {
				std::string reason =
				        "Exception while building the Application object using manifest "
				                + *vectManifestIter;
				ers::error(pmg::Reconnection_Error(ERS_HERE, reason, e));
				try {
					manifest.unmap();
				}
				catch(System::PosixIssue &ex) {
					ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE,
					                                        *vectManifestIter,
					                                        "",
					                                        "",
					                                        ex.message(),
					                                        ex));
				}
				catch(ers::Issue &ex) {
					ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE,
					                                        *vectManifestIter,
					                                        "",
					                                        "",
					                                        ex.message(),
					                                        ex));
				}
				catch(...) {
					ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE,
					                                        *vectManifestIter,
					                                        "",
					                                        "",
					                                        "Unknown"));
				}
				if(app_ptr) {
					app_ptr->invalid(true);
				}
				continue;
			}

			try {
				manifest.unmap();
			}
			catch(System::PosixIssue &ex) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE,
				                                        *vectManifestIter,
				                                        "",
				                                        "",
				                                        ex.message(),
				                                        ex));
			}
			catch(ers::Issue &ex) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE,
				                                        *vectManifestIter,
				                                        "",
				                                        "",
				                                        ex.message(),
				                                        ex));
			}
			catch(...) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE,
				                                        *vectManifestIter,
				                                        "",
				                                        "",
				                                        "Unknown"));
			}

			try {
				boost::thread thrd(boost::bind(&runReportThread, app_ptr));
				thrd.detach();
			}
			catch(boost::thread_resource_error &ex) {
				ers::error(pmg::Reconnection_Error(ERS_HERE,
				                                   "report thread not started while reconnecting to an application whose manifest is " + (*vectManifestIter),
				                                   ex));
				app_ptr->invalid(true);
			}

			ers::info(pmg::Exception(ERS_HERE, std::string("Reconnected to the launcher using "
			        + *vectManifestIter)));

		} else {
			// The launcher does not exists
			try {
			    const Handle h(manifest.handle());
			    partitions.insert(h.partitionName());

				::unlink(manifest.full_name().c_str());
				manifest.unmap();
			}
			catch(System::PosixIssue &ex) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE,
				                                        *vectManifestIter,
				                                        "",
				                                        "",
				                                        ex.message(),
				                                        ex));
			}
			catch(ers::Issue &ex) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE,
				                                        *vectManifestIter,
				                                        "",
				                                        "",
				                                        ex.message(),
				                                        ex));
			}
			catch(...) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE,
				                                        *vectManifestIter,
				                                        "",
				                                        "",
				                                        "Unknown"));
			}
			ers::info(pmg::Exception(ERS_HERE,
			                         std::string("Not reconnecting to the launcher using "
			                                 + *vectManifestIter
			                                 + " manifest because launcher is no more running")));
			notRunning++;
		}
	}

	if(notRunning == manifestsNum) {
		// Clean the directory
		try {
			const System::File dir(dirName);
			dir.remove();
		}
		catch(System::Exception& ex) {
			// Ignore
		}

		// Clean resources
		ers::info(pmg::Exception(ERS_HERE,
		                         std::string("Asking RM to free all the resources associated to the local host")));
		RMBridge askRM;
		askRM.freeLocalHostResources();

		// Clean IS
		ers::info(pmg::Exception(ERS_HERE,
		                                 std::string("Cleaning IS server(s) from old left-over processes")));

		for(auto it = partitions.begin(); it != partitions.end(); ++it) {
		    try {
		        ISInfoDictionary dict(IPCPartition(it->c_str()));
		        dict.removeAll("PMG", ISCriteria(m_host_name + "\\" + daq::pmg::IS_SERVER_PROCESS_SEPARATOR + ".*"));
		    }
		    catch(ers::Exception& ex) {
		        ers::log(ex);
		    }
		}
	}

}

void Server::scan(const std::string& dirName) {

	std::string manifest("pmg.manifest");
	std::string::size_type manifestSize = manifest.size();
	std::string report("pmg.report");
	std::string::size_type reportSize = report.size();
	std::string control("pmg.control");
	std::string::size_type controlSize = control.size();

	struct dirent **namelist;
	int n;
	std::vector<std::string> tempfileVect;
	std::string manifest_file;
	bool has_manifest;
	bool has_report;
	bool has_control;

	n = scandir(dirName.c_str(), &namelist, 0, alphasort);

	if(n < 0) {

		perror("scandir");

	} else {

		tempfileVect.clear();
		manifest_file.clear();

		has_manifest = false;
		has_control = false;
		has_report = false;

		while(n--) {

			std::string fileName = dirName + "/" + std::string(namelist[n]->d_name);

			std::string::size_type len = fileName.size();

			struct stat st;
			int lstatResult = lstat(fileName.c_str(), &st);

			if(lstatResult == 0) {
				if(fileName.substr(len - 1, 1) != std::string(".")) {
					if(S_ISDIR (st.st_mode)) {
						scan(fileName);
					} else {
						tempfileVect.push_back(fileName);
					}
				}
			}

			free(namelist[n]);

		}

		free(namelist);

		std::vector<std::string>::iterator tempfileIter;

		struct stat st;

		for(tempfileIter = tempfileVect.begin(); tempfileIter != tempfileVect.end(); ++tempfileIter) {

			std::string::size_type Len = tempfileIter->size();

			lstat(tempfileIter->c_str(), &st);

			if(! (Len < manifestSize)) {
				if(tempfileIter->substr(Len - manifestSize, manifestSize) == manifest) {
					//	std::cout << "Manifest: " << tempfileIter->substr(Len - manifest.size(), manifest.size()) << std::endl;
					has_manifest = true;
					manifest_file = *tempfileIter;
				}
			}

			if(! (Len < controlSize)) {
				if(tempfileIter->substr(Len - controlSize, controlSize) == control
				        && S_ISFIFO (st.st_mode)) {
					has_control = true;
					//	std::cout << "Control: " << tempfileIter->substr(Len - control.size(), control.size()) << std::endl;
				}
			}

			if(! (Len < reportSize)) {
				if(tempfileIter->substr(Len - reportSize, reportSize) == report
				        && S_ISFIFO (st.st_mode)) {
					has_report = true;
					//	std::cout << "Report: " << tempfileIter->substr(Len - report.size(), report.size()) << std::endl;
				}
			}
		}

		if(has_manifest && has_report && has_control) {
			manifestVect.push_back(manifest_file);
		} else {
			// Remove the manifest file, it is no more needed
			::unlink(manifest_file.c_str());
		}

	}

} // scan

void Server::configure_login_name() const{
	server_login_name = daq::pmg::utils::loginName();
}

const std::string& Server::login_name() {
	return server_login_name;
}

std::string Server::verify_credentials(std::string_view credentials)
{
    static bool s_tokens_enabled = daq::tokens::enabled();
    return s_tokens_enabled ? daq::tokens::verify(credentials).get_subject() : std::string(credentials);
}

bool doWhenGlobalIPCReady(void* server_p) {
	ERS_PRECONDITION(server_p);

	// Check if IPC server is up
	if(((Server*) server_p)->globalIPCServerReady()) {
		// IPC server present so recover and register with IPC
		// then exit the alarm (return false)
		((Server*) server_p)->publishInGlobalIPC();
	}

	return true;

} // doWhenGlobalIPCReady()

}
} // daq::pmg
