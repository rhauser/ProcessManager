/*
 *  Partition.cxx
 *
 *  Created by Matthias Wiesmann on 08.04.05.
 *
 */

#include <ers/ers.h>

#include "ProcessManager/Daemon.h"
#include "ProcessManager/Partition.h"
#include "ProcessManager/ApplicationList.h"
#include "ProcessManager/Application.h"
#include "ProcessManager/Exceptions.h"


namespace daq {
namespace pmg {


System::File Partition::directory_name(const std::string &name, Daemon *parent) {
  ERS_PRECONDITION(parent); 
  return parent->directory().child(name);  
} // directory_name

Partition::Partition(const std::string &name, Daemon *parent) : m_directory(directory_name(name,parent)) {
  m_name = name  ;
  m_parent =  parent; 
} // Partition
    
Partition::~Partition() {
  ERS_DEBUG(3,"Deleting partition " << m_name); 
  for(partition_map_t::iterator pos = m_partition_map.begin(); pos != m_partition_map.end(); ++pos) {
    delete(pos->second); 
    pos->second = 0; 
  } // for
} // ~Partition

const System::File &  Partition::directory() const {
    return m_directory ; 
} // directory

const std::string & Partition::name() const {
  return m_name ; 
} // get_name

const Daemon *Partition::parent() const {
  return m_parent ; 
} // parent

unsigned int Partition::active_processes() {
  unsigned int count = 0 ; 
  partition_map_t::const_iterator pos ;
  for(pos=m_partition_map.begin();pos!=m_partition_map.end();++pos) {
    if(pos->second) { 
      count+=pos->second->active_processes();
    }     
  } // for
  return count ; 
} // running_nbr

ApplicationList *Partition::application_list(const std::string &name) const {
  partition_map_t::const_iterator pos = m_partition_map.find(name);
  if (pos!=m_partition_map.end()) {
    return pos->second ; 
  } // if
  return 0 ; 
} // application_list

Application * Partition::application(const std::string &name, long identity) const {
  const ApplicationList *list = application_list(name);
  if (! list) return 0 ; 
  return list->application(identity); 
} // application

application_collection_t & Partition::all_applications(application_collection_t & collection) const {
  partition_map_t::const_iterator pos ; 
  for(pos=m_partition_map.begin();pos!=m_partition_map.end();++pos) {
    if(pos->second) {
      collection = pos->second->all_applications(collection);
    } 
  } // for    
  return collection ; 
} // all_applications

ApplicationList *Partition::application_list_factory(const std::string &name, long identity) {
  ApplicationList *list = application_list(name) ;
  if (list) return list ; 
  list = new ApplicationList(name,this,identity) ; 
  m_partition_map[name] = list ; 
  return list ; 
} // application_list_factory

Application *Partition::application_factory(const std::string & name, long identity) {
  ApplicationList *list = application_list_factory(name, identity);
  ERS_ASSERT(list);
  Application* app= list->application_factory(identity);
  return app;
} // application_factory

void Partition::stop(const std::string& userName, const std::string& hostName) const {
  partition_map_t::const_iterator pos;

  unsigned int failures = 0;
  std::string handles;
 
  for(pos=m_partition_map.begin();pos!=m_partition_map.end();++pos) {
    if(pos->second) {
      try {
	pos->second->stop(userName, hostName);
      }
      catch(pmg::ApplicationList_Signal_Exception &ex) {
	++failures;
	handles += ex.get_proc_handles();
      }
    } 
  } // for

  if(failures > 0) {
    throw pmg::Partition_Signal_Exception(ERS_HERE, this->name(), handles);
  }

} // stop
  
void Partition::signal(int signum, const std::string& userName, const std::string& hostName) const {
  partition_map_t::const_iterator pos;
  
  unsigned int failures = 0;
  std::string handles;
 
  for(pos=m_partition_map.begin();pos!=m_partition_map.end();++pos) {
    if(pos->second) {
      try {
	pos->second->signal(signum, userName, hostName);
      }
      catch(pmg::ApplicationList_Signal_Exception &ex) {
	++failures;
	handles += ex.get_proc_handles();
      }
    } 
  } // for

  if(failures > 0) {
    throw pmg::Partition_Signal_Exception(ERS_HERE, this->name(), handles);
  }

} // signal

void Partition::kill(const std::string& userName, const std::string& hostName) const {
  partition_map_t::const_iterator pos;

  unsigned int failures = 0;
  std::string handles;

  for(pos=m_partition_map.begin();pos!=m_partition_map.end();++pos) {
    if(pos->second) {
      try {
	pos->second->kill(userName, hostName);
      }
      catch(pmg::ApplicationList_Signal_Exception &ex) {
	++failures;
	handles += ex.get_proc_handles();
      }
    }
  } // for

  if(failures > 0) {
    throw pmg::Partition_Signal_Exception(ERS_HERE, this->name(), handles);
  }

} // kill

void Partition::kill_soft(int timeout, const std::string& userName, const std::string& hostName) const {
  partition_map_t::const_iterator pos;
  
  unsigned int failures = 0;
  std::string handles;
  
  for(pos=m_partition_map.begin();pos!=m_partition_map.end();++pos) {
    if(pos->second) {
      try {
	pos->second->kill_soft(timeout, userName, hostName);
      }
      catch(pmg::ApplicationList_Signal_Exception &ex) {
	++failures;
	handles += ex.get_proc_handles();
      }
    }
  } // for
  
  if(failures > 0) {
    throw pmg::Partition_Signal_Exception(ERS_HERE, this->name(), handles);
  }

} // kill_soft

Application* Partition::lookup(const std::string& app_name) const {
  partition_map_t::const_iterator pos ;
  Application* app = 0;
  for(pos=m_partition_map.begin();pos!=m_partition_map.end();++pos) {
    if(pos->second) {
      app = pos->second->lookup(app_name);
      if(app) {
	break;
      }
    }
  } // for
  return app;
}

void Partition::print_to(std::ostream& stream) const {
  partition_map_t::const_iterator pos ;
  stream << "Partition \"" << m_name << "\" on " << m_directory << std::endl ; 
  for(pos=m_partition_map.begin();pos!=m_partition_map.end();++pos) {
    const std::string name = pos->first ; 
    const ApplicationList *list = pos->second ; 
    ERS_ASSERT(list); 
    stream << (*list)  ; 
  } // for
} // print_to

}} // daq::pmg

std::ostream& operator<<(std::ostream& stream, const daq::pmg::Partition& partition) {
    partition.print_to(stream) ;
    return stream ;
}  // operator<<


