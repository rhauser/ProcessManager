#include <sstream>
#include <fstream>

#include <unistd.h>
#include <stdlib.h>
#include <sys/sysinfo.h>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>

#include <system/Host.h>

#include "ProcessManager/ISBridge.h"
#include "ProcessManager/ProcInterface.h"
#include "ProcessManager/ProcStats.h"
#include "ProcessManager/Manifest.h"
#include "ProcessManager/Handle.h"
#include "ProcessManager/Utils.h"
#include "ProcessManager/Exceptions.h"

using namespace daq;

using boost::posix_time::ptime;
using boost::posix_time::from_time_t;
using boost::posix_time::to_simple_string;
using boost::date_time::c_local_adjustor;

//////////////////////////////////
// AgentIS class implementation //
//////////////////////////////////

pmg::AgentIS::AgentIS(const std::string& partName, const std::string& isServerName)
try :
	ISBridge<PMGPublishedAgentDataNamed> (partName, isServerName)
{
}
catch(ers::Issue& ex) {
	throw daq::pmg::Failed_Create_IS(ERS_HERE, "Agent", ex.message(), ex);
}
catch(CORBA::Exception& ex) {
	throw daq::pmg::Failed_Create_IS(ERS_HERE, "Agent", std::string("CORBA exception ")
	        + std::string(ex._name()));
}
catch(std::exception& ex) {
	throw daq::pmg::Failed_Create_IS(ERS_HERE, "Agent", ex.what(), ex);
}
catch(...) {
	throw daq::pmg::Failed_Create_IS(ERS_HERE, "Agent", "Unknown exception");
}

pmg::AgentIS::AgentIS(const std::string& partName,
                      const std::string& isServerName,
                      PMGPublishedAgentDataNamed* pData) :
	ISBridge<PMGPublishedAgentDataNamed> (partName, isServerName, pData)
{
}

pmg::AgentIS::~AgentIS() {
}

void pmg::AgentIS::collectInfo(bool lowLevelInfo) {
    try {
        // Get info about the server
        data()->name = serverName().substr(std::string("PMG.").size());

        // Get statistics about the PC running the agent

        // Local mounted file systems info
        data()->fs = daq::pmg::utils::mntPointInfo();

        // Get info about used ram, swap and number of current procs

        // Get system average load
        double load[3] = {0};
        int avgRes = getloadavg(load, 3);
        if(avgRes == -1) {
            ers::log(pmg::Failed_Collect_Info(ERS_HERE, "error executing \"getloadavg\" system call"));
        }

        // Get memory and procs info
        daq::pmg::utils::HostInfo hInfo{};
        bool infoSuccess = daq::pmg::utils::hostInfo(hInfo);
        if(infoSuccess == false) {
            ers::log(pmg::Failed_Collect_Info(ERS_HERE, "error retrieving information about the host memory status"));
        }

        data()->totalram = hInfo.totalram;
        data()->totalswap = hInfo.totalswap;
        data()->ram = hInfo.used_ram;
        data()->swap = hInfo.used_swap;
        data()->nCPU = (uint32_t) ::get_nprocs_conf();
        data()->procs = hInfo.procs;
        data()->load1 = load[0];
        data()->load5 = load[1];
        data()->load15 = load[2];
        data()->freeram = hInfo.freeram;
        data()->sharedram = hInfo.sharedram;
        data()->bufferram = hInfo.bufferram;
        data()->cachedram = hInfo.cachedram;
        data()->freeswap = hInfo.freeswap;

        // Collecting Agent resource usage (acces to the /proc fs)
        if(lowLevelInfo) {
            try {
                const int _pid_ = (int) getpid();
                pmg::ProcInterface proc(_pid_);
                pmg::proc_usage p_stat = proc.procInfo();
                const uint64_t pageSize = (uint64_t) (::getpagesize() / 1024); // Page size in kB

                data()->utime = (p_stat.stat_info).utime * 10;
                data()->stime = (p_stat.stat_info).stime * 10;
                data()->size = (p_stat.statm_info).size * pageSize;
                data()->resident = (p_stat.statm_info).resident * pageSize;
                data()->num_threads = (p_stat.stat_info).num_threads;
            }
            catch(pmg::Exception& ex) {
                ERS_DEBUG(1, ex);
            }
        } // if
    }
    catch(pmg::Failed_Collect_Info& ex) {
        throw;
    }
    catch(ers::Issue& ex) {
        throw pmg::Failed_Collect_Info(ERS_HERE, ex.message(), ex);
    }
    catch(std::exception& ex) {
        throw pmg::Failed_Collect_Info(ERS_HERE, ex.what(), ex);
    }
}

////////////////////////////////////
// ProcessIS class implementation //
////////////////////////////////////

pmg::ProcessIS::ProcessIS(const Manifest* const manifest,
                          const std::string& partName,
                          const std::string& isServerName)
try :
	ISBridge<PMGPublishedProcessDataNamed> (partName, isServerName), pManifest_(manifest)
{
}
catch(ers::Issue& ex) {
	throw daq::pmg::Failed_Create_IS(ERS_HERE, "Process", ex.message(), ex);
}
catch(CORBA::Exception& ex) {
	throw daq::pmg::Failed_Create_IS(ERS_HERE, "Process", std::string("CORBA exception ")
	        + std::string(ex._name()));
}
catch(std::exception& ex) {
	throw daq::pmg::Failed_Create_IS(ERS_HERE, "Process", ex.what(), ex);
}
catch(...) {
	throw daq::pmg::Failed_Create_IS(ERS_HERE, "Process", "Unknown exception");
}

pmg::ProcessIS::ProcessIS(const Manifest* const manifest,
                          const std::string& partName,
                          const std::string& isServerName,
                          PMGPublishedProcessDataNamed* pData) :
	ISBridge<PMGPublishedProcessDataNamed> (partName, isServerName, pData), pManifest_(manifest)
{
}

pmg::ProcessIS::~ProcessIS() {
}

void pmg::ProcessIS::collectInfo(bool lowLevelInfo) {
	try {
		// Get info about process from the manifest
		data()->user_name = pManifest_->user().name_safe();
		data()->handle = pManifest_->handle();

		pmg::Handle h(pManifest_->handle()); // It may throw pmg::Invalid_Handle

		data()->pid = pManifest_->process_id().process_id();
		data()->partition = partitionName();
		data()->host = System::LocalHost::full_local_name();
		data()->host_requesting = pManifest_->requesting_host();
		data()->app_name = h.applicationName();
		data()->app_id = h.id();
		data()->state = std::string(state_string(pManifest_->process_state()));
		data()->state_code = pManifest_->process_state();
		data()->exit = pManifest_->exit_code();
		data()->signal = std::string(strsignal(pManifest_->exit_signal()));
		data()->error_msg = std::string(pManifest_->error_msg());

		ptime
		        startTime(c_local_adjustor<ptime>::utc_to_local(from_time_t(*(pManifest_->start_time_ptr()))));
		data()->start = to_simple_string(startTime);

		const time_t stopTime = *(pManifest_->stop_time_ptr());
		if(stopTime != 0) {
			ptime
			        stopTime(c_local_adjustor<ptime>::utc_to_local(from_time_t(*(pManifest_->stop_time_ptr()))));
			data()->stop = to_simple_string(stopTime);
		} else {
			data()->stop = std::string("");
		}

		data()->token = (unsigned long) pManifest_->rm_token();
		data()->executable = pManifest_->executable().full_name();

		std::map<std::string, std::string>::const_iterator it;
		std::map<std::string, std::string> env_map = pManifest_->environnements();
		std::string ld_path("null");
		std::string env_str("");

		for(it = env_map.begin(); it != env_map.end(); ++it) {
			if(it->first == std::string("LD_LIBRARY_PATH")) {
				ld_path.clear();
				ld_path = it->second;
			}
			env_str += it->first + "=" + it->second + "; ";
		}

		data()->library_paths = ld_path;
		data()->workingdir = pManifest_->working_directory().full_name();

		std::vector<std::string>::const_iterator itt;
		std::vector<std::string> param_collection = pManifest_->parameters();
		std::string args("");

		for(itt = param_collection.begin(); itt != param_collection.end(); itt++) {
			args += *itt;
			args += " ";
		}

		data()->arguments = args;
		data()->environment = env_str;
		data()->std_out = pManifest_->output_file().full_name();
		data()->std_err = pManifest_->error_file().full_name();
		data()->std_in = pManifest_->input_file().full_name();

		// Collecting process resource usage (acces to the /proc fs)
		if(lowLevelInfo) {
			try {
				pmg::ProcInterface proc(pManifest_->process_id().process_id());
				pmg::proc_usage p_stat = proc.procInfo();
				const uint64_t pageSize = (uint64_t) (::getpagesize() / 1024); // Page size in kB

				data()->minflt = (p_stat.stat_info).minflt;
				data()->cminflt = (p_stat.stat_info).cminflt;
				data()->majflt = (p_stat.stat_info).majflt;
				data()->cmajflt = (p_stat.stat_info).cmajflt;
				data()->utime = (p_stat.stat_info).utime * 10;
				data()->stime = (p_stat.stat_info).stime * 10;
				data()->cutime = (p_stat.stat_info).cutime * 10;
				data()->cstime = (p_stat.stat_info).cstime * 10;
				data()->num_threads = (p_stat.stat_info).num_threads;
				data()->nswap = (p_stat.stat_info).nswap;
				data()->cnswap = (p_stat.stat_info).cnswap;

				data()->size = (p_stat.statm_info).size * pageSize;
				data()->resident = (p_stat.statm_info).resident * pageSize;
				data()->share = (p_stat.statm_info).share * pageSize;
				data()->text = (p_stat.statm_info).text * pageSize;
				data()->data = (p_stat.statm_info).data * pageSize;
				data()->rsslim = (p_stat.stat_info).rsslim / 1024;
			}
			catch(pmg::Exception& ex) {
				ERS_DEBUG(1, ex);
			}
		} // if
	}
	catch(pmg::Invalid_Handle& ex) {
		throw pmg::Failed_Collect_Info(ERS_HERE, ex.message(), ex);
	}
	catch(ers::Issue& ex) {
		throw pmg::Failed_Collect_Info(ERS_HERE, ex.message(), ex);
	}
	catch(std::exception& ex) {
	    throw pmg::Failed_Collect_Info(ERS_HERE, ex.what(), ex);
	}
}
