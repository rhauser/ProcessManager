/*
 *  Application.cxx
 *
 *  Created by Matthias Wiesmann on 07.04.05.
 *
 */

#include <fcntl.h>
#include <unistd.h>
#include <sstream>
#include <time.h>
#include <signal.h>
#include <sys/types.h>

#include <fstream>
#include <ostream>

#include <system/FIFOConnection.h>
#include <system/Environment.h>
#include <system/Path.h>
#include <system/Host.h>
#include <system/exceptions.h>
#include <ers/ers.h>

#include "ProcessManager/Application.h"
#include "ProcessManager/ApplicationList.h"
#include "ProcessManager/Partition.h"
#include "ProcessManager/defs.h"
#include "ProcessManager/private_defs.h"
#include "ProcessManager/Handle.h"
#include "ProcessManager/ISBridge.h"
#include "ProcessManager/RMBridge.h"
#include "ProcessManager/AMBridge.h"
#include "ProcessManager/Exceptions.h"
#include "ProcessManager/Server.h"
#include "ProcessManager/Utils.h"

namespace daq {

void pmg::Application::invalid(bool isInvalid) {
	isAppInvalid = isInvalid;
}

bool pmg::Application::invalid() const {
	return isAppInvalid;
}

void pmg::Application::open_report_fifo() {

	try {
		boost::mutex::scoped_lock _scoped_lock(m_openFIFOMutex);
		System::Descriptor* fifoDescriptor = m_report_fifo->open_rw(false);
		m_fifo_open = true;
		fifoDescriptor->closeOnExec();
	}
	catch(System::OpenFileIssue &ex) {
		throw ;
	}
	catch(System::SystemCallIssue &ex) { // Exception thrown by closeOnExec()
		std::string msg = ex.message() + ". File descriptor will stay open in the child process";
		ers::warning(pmg::Exception(ERS_HERE, msg));
	}

}

System::File pmg::Application::get_directory(pmg_id_t identity, const System::File &parent_dir) {
	char buffer[256];
	snprintf(buffer,sizeof(buffer),"%04ld",identity);
	return parent_dir.child(buffer);
} // get_directory


System::File pmg::Application::get_manifest_file(const System::File &dir) {
	return dir.child(pmg::Manifest::FILENAME);
} // get_manifest_file

void pmg::Application::stopReportThread() const {

	boost::mutex::scoped_lock scoped_lock(m_condVarMutex);
	while(m_readStopMessage == false) {
		{
			boost::mutex::scoped_lock _scoped_lock_(m_openFIFOMutex);
			try {
				if(m_report_fifo != 0 && m_fifo_open == true) {
					m_report_fifo->send(m_stop_thread_code + std::string("\n"));
				}
			}
			catch(System::PosixIssue& ex) {
				ers::warning(pmg::Exception(ERS_HERE, std::string("Error while stopping report thread for application ") + toHandle()->toString()));
			}
		}
		boost::xtime xt;
		boost::xtime_get(&xt, boost::TIME_UTC_);

		xt.sec += 0;
		xt.nsec += 10000000;
		bool waitCode = m_condVar.timed_wait(scoped_lock, xt);
		if(waitCode == false) {
			break;
		}
	}

}

pmg::Application::Application(pmg_id_t identity, ApplicationList *parent, bool hasManifest, bool hasReport) : m_directory(get_directory(identity,parent->directory())) {

	m_fifo_open = false;

	m_readStopMessage = false;

	{
		unsigned int now = (unsigned int) time(0);
		int rndm = ::rand_r(&now);
		std::ostringstream rndmStr;
		rndmStr << rndm;
		m_stop_thread_code = rndmStr.str();
	}

	isAppInvalid = false;

	is_info_process = 0;

	m_id = identity;

	if(hasManifest) {
		m_manifest = new pmg::Manifest(get_manifest_file(m_directory),true,false);
		if(!m_manifest->is_mapped()) {
			m_manifest->map();
			try {
				m_manifest->fd()->closeOnExec();
			}
			catch(System::SystemCallIssue &ex) {
				std::string msg = ex.message() + ". File descriptor will stay open in the child process";
				ers::warning(pmg::Exception(ERS_HERE, msg));
			}
		}
	} else {
		m_manifest = 0;
	}

	m_parent = parent;

	if(hasReport && hasManifest) {
		m_report_fifo = new System::FIFOConnection(m_manifest->report_file());
		m_launcher_process = m_manifest->launcher_pid();
	} else {
		m_report_fifo = 0;
	}

	m_handle = new pmg::Handle(buildHandle());

} // Application

pmg::Application::~Application() {

	{
		boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);
		if(m_manifest && m_manifest->is_mapped()) {
			try {
				ERS_DEBUG(1,"Unmapping manifest for app " << toString());
				m_manifest->unmap();
			}
			catch(...) {
			}
		}
	}

	ERS_DEBUG(1,"Deleting manifest object for app " << toString());
	delete(m_manifest);

	ERS_DEBUG(1,"Deleting FIFO object for app " << toString());
	{
		boost::mutex::scoped_lock _scoped_lock_(m_openFIFOMutex);
		delete(m_report_fifo);
	}

	delete(m_handle);

} // ~Application

boost::recursive_mutex& pmg::Application::mutex() const {
	return m_mutex;
}

pmg::pmg_id_t pmg::Application::get_id() const {
	return m_id;
} // get_id

pmg::Manifest* pmg::Application::manifest() const {

	if(m_manifest && m_manifest->is_mapped()) {
		return m_manifest;
	} else {
		return 0;
	}

} // manifest

const pmg::Partition * pmg::Application::partition() const {
	ERS_ASSERT(m_parent);
	const Partition* parent_partition = m_parent->parent();
	return parent_partition;
} // Partition

System::Process pmg::Application::launcher() const {
	return m_launcher_process;
} // launcher

bool pmg::Application::is_active() const {

	if (!m_manifest) {
		return false;
	}

	// extra check for safety
	if(!(m_manifest->is_loaded())) {
		return false;
	}

	if(!(m_manifest->is_mapped())) {
		return false;
	}

	PMGProcessState state = m_manifest->process_state();

	switch(state) {
		case PMGProcessState_NOTAV:
		case PMGProcessState_REQUESTED:
		case PMGProcessState_RUNNING:
		case PMGProcessState_LAUNCHING:
		case PMGProcessState_CREATED:
		return true;
		default:
		return false;
	}

} // is_running

std::string pmg::Application::toString() const {
	return m_directory.full_name();
} // toString

pmg::Handle pmg::Application::buildHandle() const {
	ERS_ASSERT(m_parent);
	const std::string & app_name = m_parent->name();
	const std::string hostName = System::LocalHost::full_local_name();
	const std::string & part_name = partition()->name();
	pmg::Handle h(hostName,part_name,app_name,m_id);
	return h;
} // buildHandle

pmg::Handle* pmg::Application::toHandle() const {

	return m_handle;

} // toHandle

void pmg::Application::init_manifest() {

	boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

	try {

		ERS_ASSERT_MSG(0==m_manifest,"manifest " << m_manifest->c_full_name()<< " already initialised");
		m_manifest = new pmg::Manifest(get_manifest_file(m_directory),true,true);
		m_manifest->ensure_path(0777);

		ERS_DEBUG(1,"Init manifest: " << toHandle()->toString());

		m_manifest->zero();
		m_manifest->map();
		try {
			m_manifest->fd()->closeOnExec();
		}
		catch(System::SystemCallIssue &ex) { // Exception thrown by closeOnExec()
			std::string msg = ex.message() + ". File descriptor will stay open in the child process";
			ers::warning(pmg::Exception(ERS_HERE, msg));
		}
		m_manifest->sign();
		m_manifest->handle(toHandle()->toString());
		m_manifest->report_file(m_directory.child(pmg::Manifest::DEFAULT_REPORT));
		m_manifest->process_state(PMGProcessState_NOTAV);
		m_manifest->control_file(m_directory.child(pmg::Manifest::DEFAULT_CONTROL));
		m_manifest->control_file_permission(0777);
		m_manifest->output_file(pmg::Manifest::DEFAULT_OUT);
		m_manifest->output_file_permission(0664);
        m_manifest->input_file("/dev/null");
		m_manifest->error_file(pmg::Manifest::DEFAULT_OUT);
		m_manifest->error_msg(pmg::Manifest::DEFAULT_ERR_MSG);
		m_manifest->working_directory(m_directory);
		m_manifest->rm_token(0);
		m_manifest->rm_swobject("");
		m_manifest->executable("/dev/null");
		m_manifest->requesting_host("");

		// Add something to the env and the parameters otehrwise
		// the StringMemoryArea (in the system package) has troubles
		std::map<std::string, std::string> dummy_env{{"", ""}};
		m_manifest->environnements(dummy_env);

		std::vector<std::string> dummy_params(1);
		m_manifest->parameters(dummy_params);

		m_manifest->permissions(0777);

		m_manifest->user(System::User::ROOT);
		m_manifest->process_id(0);
		m_manifest->launcher_pid(0);
		m_manifest->exit_code(0);
		m_manifest->exit_signal(0);
        m_manifest->exit_signal(0);
        m_manifest->auto_kill_timeout(0);

		const time_t now = time(0);
		*(m_manifest->start_time_ptr()) = now;
		*(m_manifest->stop_time_ptr()) = (time_t) 0;

		//time(m_manifest->start_time_ptr());
		//time(m_manifest->stop_time_ptr());

	}
	catch(System::PosixIssue &e) {
		ERS_DEBUG(1,"Exception in init_manifest: " << e.what());
		try {
			if(m_manifest && m_manifest->is_mapped()) {
				m_manifest->unmap();
			}
		}
		catch(System::PosixIssue &ex) {
			ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), ex.message(), ex));
		}
		catch(...) {
			ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), "Unknown"));
		}
		try {
			if(m_manifest) m_manifest->unlink();
		}
		catch(...) {
		}
		throw;
	}
	catch(ers::Issue &e) {
		ERS_DEBUG(1,"Exception in init_manifest: " << e.what());
		try {
			if(m_manifest && m_manifest->is_mapped()) {
				m_manifest->unmap();
			}
		}
		catch(System::PosixIssue &ex) {
			ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), ex.message(), ex));
		}
		catch(...) {
			ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), "Unknown"));
		}
		try {
			if(m_manifest) m_manifest->unlink();
		}
		catch(...) {
		}
		throw;
	}
	catch(std::exception &e) {
		ERS_DEBUG(1,"Exception in init_manifest: " << e.what());
		try {
			if(m_manifest && m_manifest->is_mapped()) {
				m_manifest->unmap();
			}
		}
		catch(System::PosixIssue &ex) {
			ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), ex.message(), ex));
		}
		catch(...) {
			ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), "Unknown"));
		}
		try {
			if(m_manifest) m_manifest->unlink();
		}
		catch(...) {
		}
		throw;
	}
	catch(...) {
		ERS_DEBUG(1,"Unknown exception in init_manifest." );
		try {
			if(m_manifest && m_manifest->is_mapped()) {
				m_manifest->unmap();
			}
		}
		catch(System::PosixIssue &ex) {
			ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), ex.message(), ex));
		}
		catch(...) {
			ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), "Unknown"));
		}
		try {
			if(m_manifest) m_manifest->unlink();
		}
		catch(...) {
		}
		throw;
	}

} // init_manifest

void pmg::Application::start() {

	try {

		uid_t launcher_user_id;

		{
			boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

			ERS_ASSERT(m_manifest && m_manifest->is_mapped());

			// Now I unmap the manifest and delete its local image
			m_manifest->unmap();
			delete(m_manifest);
			m_manifest = 0;

			// I create a manifest from an esisting manifest file and map it in readonly mode... Only the launcher can change it now.
			m_manifest = new pmg::Manifest(get_manifest_file(m_directory),true,false);
			m_manifest->map();
			try {
				m_manifest->fd()->closeOnExec();
			}
			catch(System::SystemCallIssue &ex) { // Exception thrown by closeOnExec()
				std::string msg = ex.message() + ". File descriptor will stay open in the child process";
				ers::warning(pmg::Exception(ERS_HERE, msg));
			}

			// Create the report fifo
			m_report_fifo = new System::FIFOConnection(m_manifest->report_file());
			m_report_fifo->make(0777);

			// Get the launcher user
			launcher_user_id = m_manifest->user().identity();

		}

		// Open the report fifo in read/write non-blocking mode

		open_report_fifo();

		// Start launching the first stage of the launcher

		// Get the LD_LIBRARY_PATH and the manifest dir
		const std::string ld_str = System::Environment::get("LD_LIBRARY_PATH");
		const std::string manifest_dir = m_directory.full_name();

		// Find the user who will be the owner of the started process
		uid_t final_user_id = launcher_user_id;
		std::ostringstream final_uid;
		final_uid << final_user_id;

		// Find the first stage launcher executable
		const std::string path_str = System::Environment::get("PATH");
		const System::Path path(path_str);
		const System::Executable launcher_file(path.which(pmg::STG1_LAUNCHER_NAME));

		// Build the agument vector to pass to the process
		System::Executable::param_collection params;
		params.push_back(ld_str);
		params.push_back(manifest_dir);
		params.push_back(final_uid.str());

		// Actually start the first stage launcher
		m_launcher_process = launcher_file.start_and_forget(params);

		// Start the thread reading on report FIFO
		boost::thread thrd(boost::bind(&runReportThread,this));
		thrd.detach();
	}
	catch(boost::thread_resource_error &e) {
		std::ostringstream infoMsg;
		infoMsg << "Exception in Application::start() for process " << m_parent->name() << " in partition " << this->partition()->name() << ": " << e.what();
		{
			boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);
			try {
				if(m_manifest && m_manifest->is_mapped()) {
					infoMsg << ". Dumping the manifest:\n" << (*m_manifest);
					m_manifest->unmap();
				}
			}
			catch(System::PosixIssue &ex) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), ex.message(), ex));
			}
			catch(...) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), "Unknown"));
			}
			try {
				if(m_manifest) m_manifest->unlink();
			}
			catch(...) {
			}
			try {
				if(m_report_fifo) m_report_fifo->unlink();
			}
			catch(...) {
			}
		}
		ers::info(pmg::Exception(ERS_HERE, infoMsg.str(), e));
		throw pmg::Failed_Start_Thread(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), e.what(), e);
	}
	catch(System::PosixIssue &e) {
		std::ostringstream infoMsg;
		infoMsg << "Exception in Application::start() for process " << m_parent->name() << " in partition " << this->partition()->name() << ": " << e.message();
		{
			boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);
			try {
				if(m_manifest && m_manifest->is_mapped()) {
					infoMsg << ". Dumping the manifest:\n" << (*m_manifest);
					m_manifest->unmap();
				}
			}
			catch(System::PosixIssue &ex) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), ex.message(), ex));
			}
			catch(...) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), "Unknown"));
			}
			try {
				if(m_manifest) m_manifest->unlink();
			}
			catch(...) {
			}
			try {
				if(m_report_fifo) m_report_fifo->unlink();
			}
			catch(...) {
			}
		}
		ers::info(pmg::Exception(ERS_HERE, infoMsg.str()));
		throw;
	}
	catch(ers::Issue &e) {
		std::ostringstream infoMsg;
		infoMsg << "Exception in Application::start() for process " << m_parent->name() << " in partition " << this->partition()->name() << ": " << e.message();
		{
			boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);
			try {
				if(m_manifest && m_manifest->is_mapped()) {
					infoMsg << ". Dumping the manifest:\n" << (*m_manifest);
					m_manifest->unmap();
				}
			}
			catch(System::PosixIssue &ex) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), ex.message(), ex));
			}
			catch(...) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), "Unknown"));
			}
			try {
				if(m_manifest) m_manifest->unlink();
			}
			catch(...) {
			}
			try {
				if(m_report_fifo) m_report_fifo->unlink();
			}
			catch(...) {
			}
		}
		ers::info(pmg::Exception(ERS_HERE, infoMsg.str()));
		throw;
	}
	catch(std::exception &e) {
		std::ostringstream infoMsg;
		infoMsg << "Exception in Application::start() for process " << m_parent->name() << " in partition " << this->partition()->name() << ": " << e.what();
		{
			boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);
			try {
				if(m_manifest && m_manifest->is_mapped()) {
					infoMsg << ". Dumping the manifest:\n" << (*m_manifest);
					m_manifest->unmap();
				}
			}
			catch(System::PosixIssue &ex) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), ex.message(), ex));
			}
			catch(...) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), "Unknown"));
			}
			try {
				if(m_manifest) m_manifest->unlink();
			}
			catch(...) {
			}
			try {
				if(m_report_fifo) m_report_fifo->unlink();
			}
			catch(...) {
			}
		}
		ers::info(pmg::Exception(ERS_HERE, infoMsg.str()));
		throw;
	}
	catch(...) {
		std::ostringstream infoMsg;
		infoMsg << "Unknown exception in Application::start() for process " << m_parent->name() << " in partition " << this->partition()->name();
		{
			boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);
			try {
				if(m_manifest && m_manifest->is_mapped()) {
					infoMsg << ". Dumping the manifest:\n" << (*m_manifest);
					m_manifest->unmap();
				}
			}
			catch(System::PosixIssue &ex) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), ex.message(), ex));
			}
			catch(...) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, m_manifest->full_name(), m_parent->name(), this->partition()->name(), "Unknown"));
			}
			try {
				if(m_manifest) m_manifest->unlink();
			}
			catch(...) {
			}
			try {
				if(m_report_fifo) m_report_fifo->unlink();
			}
			catch(...) {
			}
		}
		ers::info(pmg::Exception(ERS_HERE, infoMsg.str()));
		throw;
	}

} // start

void pmg::Application::update_data() const {

	boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

	if(is_active()) {

		ERS_ASSERT(m_manifest);
		System::FIFOConnection control_file = System::FIFOConnection(m_manifest->control_file());
		if(! control_file.exists()) {
			ERS_DEBUG(1, m_manifest->control_file() << " does not exist: the associated Launcher is not running.");
			return;
		}
		try {
			System::Descriptor* c_fifoDescriptor = control_file.open_w(false);
			try {
				c_fifoDescriptor->closeOnExec();
			}
			catch(System::SystemCallIssue &ex) { // Exception thrown by closeOnExec()
				std::string msg = ex.message() + ". File descriptor will stay open in the child process";
				ers::warning(pmg::Exception(ERS_HERE, msg));
			}
			control_file.send(pmg::UPDATE_COMMAND);
			control_file.close();
		}
		catch(System::WriteIssue &ex) {
			ers::warning(pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), ex.message(), ex));
		}
		catch(System::OpenFileIssue &ex) {
			ers::warning(pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), ex.message(), ex));
		}
		catch(System::PosixIssue &ex) {
			ers::warning(pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), ex.message(), ex));
		}
		catch(...) {
			ers::warning(pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), "Unknown"));
		}

	} else {
		ERS_DEBUG(1,"Update call skipped because app not active (handle=" << toHandle()->toString() <<")");
	}

} // stop

void pmg::Application::stop(const std::string& userName, const std::string& hostName) const {

	boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

	if(is_active()) {
		ERS_ASSERT(m_manifest);

		bool allowed = false;
		try {
			allowed = this->signalAllowed(m_manifest, SIGKILL, userName, hostName);
		}
		catch(ers::Issue& ex) {
			ERS_DEBUG(1,"Access Manager does not allow you to terminate the process (handle= " << toHandle()->toString() << "): " << ex.message());
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), ex.message(), ex);
		}
		catch(std::exception& ex) {
			ERS_DEBUG(1,"Access Manager does not allow you to terminate the process (handle= " << toHandle()->toString() << "): " << ex.what());
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), ex.what(), ex);
		}
		catch(...) {
			ERS_DEBUG(1,"Access Manager does not allow you to terminate the process (handle= " << toHandle()->toString() << ")");
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), "received an unexpected exception");
		}

		if(allowed == true) {
			System::FIFOConnection control_file = System::FIFOConnection(m_manifest->control_file());
			if(! control_file.exists()) {
				ERS_DEBUG(1, m_manifest->control_file() << " does not exist: the associated Launcher is not running.");
				throw pmg::Launcher_Not_Running(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName());
			}
			try {
				System::Descriptor* c_fifoDescriptor = control_file.open_w(false);
				try {
					c_fifoDescriptor->closeOnExec();
				}
				catch(System::SystemCallIssue &ex) { // Exception thrown by closeOnExec()
					std::string msg = ex.message() + ". File descriptor will stay open in the child process";
					ers::warning(pmg::Exception(ERS_HERE, msg));
				}
				control_file.send(pmg::TERM_COMMAND);
				control_file.close();
			}
			catch(System::WriteIssue &ex) {
				ERS_DEBUG(1,"Error writing on control FIFO " << control_file.full_name() << ": " << ex);
				std::string fifoError(ex.message());
				if(ex.get_error() != 0) {
					fifoError += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
				}
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), fifoError, ex);
			}
			catch(System::OpenFileIssue &ex) {
				ERS_DEBUG(1,"Error opening control FIFO " << control_file.full_name() << ": " << ex);
				std::string fifoError(ex.message());
				if(ex.get_error() != 0) {
					fifoError += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
				}
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), fifoError, ex);
			}
			catch(System::PosixIssue &ex) {
				ERS_DEBUG(1,ex);
				std::string fifoError(ex.message());
				if(ex.get_error() != 0) {
					fifoError += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
				}
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), fifoError, ex);
			}
			catch(...) {
				ERS_DEBUG(1,"Unknown exception while writing on control FIFO " << control_file.full_name());
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), "unknown exception");
			}

		} else {
			ERS_DEBUG(1,"Access Manager does not allow you to terminate the process (handle= " << toHandle()->toString() << ")");
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), "authorization not granted to the user");
		}

	} else {
		ERS_DEBUG(1,"Stop call skipped because app not active (handle=" << toHandle()->toString() <<")");
		throw pmg::Application_Manifest_Already_Unmapped(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName());
	}

} // stop

void pmg::Application::signal(int signum, const std::string& userName, const std::string& hostName) const {

	boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

	if(is_active()) {
		ERS_RANGE_CHECK(0,signum,128);
		ERS_ASSERT(m_manifest);

		bool allowed = false;
		try {
			allowed = this->signalAllowed(m_manifest, SIGKILL, userName, hostName);
		}
		catch(ers::Issue& ex) {
			ERS_DEBUG(1,"Access Manager does not allow you to signal the process (handle= " << toHandle()->toString() << "): " << ex.message());
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), ex.message(), ex);
		}
		catch(std::exception& ex) {
			ERS_DEBUG(1,"Access Manager does not allow you to signal the process (handle= " << toHandle()->toString() << "): " << ex.what());
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), ex.what(), ex);
		}
		catch(...) {
			ERS_DEBUG(1,"Access Manager does not allow you to signal the process (handle= " << toHandle()->toString() << ")");
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), "received an unexpected exception");
		}

		if(allowed == true) {
			std::ostringstream message_stream;
			message_stream << pmg::SIGNAL_CODE << signum << "\n";
			System::FIFOConnection control_file = System::FIFOConnection(m_manifest->control_file());
			if(! control_file.exists()) {
				ERS_DEBUG(1, m_manifest->control_file() << " does not exist: the associated Launcher is not running.");
				throw pmg::Launcher_Not_Running(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName());
			}
			try {
				System::Descriptor* c_fifoDescriptor = control_file.open_w(false);
				try {
					c_fifoDescriptor->closeOnExec();
				}
				catch(System::SystemCallIssue &ex) { // Exception thrown by closeOnExec()
					std::string msg = ex.message() + ". File descriptor will stay open in the child process";
					ers::warning(pmg::Exception(ERS_HERE, msg));
				}
				control_file.send(message_stream.str());
				control_file.close();
			}
			catch(System::WriteIssue &ex) {
				ERS_DEBUG(1,"Error writing on control FIFO " << control_file.full_name() << ": " << ex);
				std::string fifoError(ex.message());
				if(ex.get_error() != 0) {
					fifoError += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
				}
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), fifoError, ex);
			}
			catch(System::OpenFileIssue &ex) {
				ERS_DEBUG(1,"Error opening control FIFO " << control_file.full_name() << ": " << ex);
				std::string fifoError(ex.message());
				if(ex.get_error() != 0) {
					fifoError += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
				}
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), fifoError, ex);
			}
			catch(System::PosixIssue &ex) {
				ERS_DEBUG(1,ex);
				std::string fifoError(ex.message());
				if(ex.get_error() != 0) {
					fifoError += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
				}
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), fifoError, ex);
			}
			catch(...) {
				ERS_DEBUG(1,"Unknown exception while writing on control FIFO " << control_file.full_name());
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), "unknown exception");
			}

		} else {
			ERS_DEBUG(1,"Access Manager does not allow you to signal the process (handle= " << toHandle()->toString() << ")");
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), "authorization not granted to the user");
		}

	} else {
		ERS_DEBUG(1,"Signal call skipped because app not active (handle=" << toHandle()->toString() <<")");
		throw pmg::Application_Manifest_Already_Unmapped(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName());
	}

} // signal

void pmg::Application::kill(const std::string& userName, const std::string& hostName) const {

	boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

	if(is_active()) {
		ERS_ASSERT(m_manifest);

		bool allowed = false;
		try {
			allowed = this->signalAllowed(m_manifest, SIGKILL, userName, hostName);
		}
		catch(ers::Issue& ex) {
			ERS_DEBUG(1,"Access Manager does not allow you to kill the process (handle= " << toHandle()->toString() << "): " << ex.message());
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), ex.message(), ex);
		}
		catch(std::exception& ex) {
			ERS_DEBUG(1,"Access Manager does not allow you to kill the process (handle= " << toHandle()->toString() << "): " << ex.what());
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), ex.what(), ex);
		}
		catch(...) {
			ERS_DEBUG(1,"Access Manager does not allow you to kill the process (handle= " << toHandle()->toString() << ")");
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), "received an unexpected exception");
		}

		if(allowed == true) {
			System::FIFOConnection control_file = System::FIFOConnection(m_manifest->control_file());
			if(!control_file.exists()) {
				ERS_DEBUG(1, m_manifest->control_file() << " does not exist: the associated Launcher is not running.");
				throw pmg::Launcher_Not_Running(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName());
			}
			try {
				System::Descriptor* c_fifoDescriptor = control_file.open_w(false);
				try {
					c_fifoDescriptor->closeOnExec();
				}
				catch(System::SystemCallIssue &ex) { // Exception thrown by closeOnExec()
					std::string msg = ex.message() + ". File descriptor will stay open in the child process";
					ers::warning(pmg::Exception(ERS_HERE, msg));
				}
				control_file.send(pmg::KILL_COMMAND);
				control_file.close();
			}
			catch(System::WriteIssue &ex) {
				ERS_DEBUG(1,"Error writing on control FIFO " << control_file.full_name() << ": " << ex);
				std::string fifoError(ex.message());
				if(ex.get_error() != 0) {
					fifoError += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
				}
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), fifoError, ex);
			}
			catch(System::OpenFileIssue &ex) {
				ERS_DEBUG(1,"Error opening control FIFO " << control_file.full_name() << ": " << ex);
				std::string fifoError(ex.message());
				if(ex.get_error() != 0) {
					fifoError += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
				}
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), fifoError, ex);
			}
			catch(System::PosixIssue &ex) {
				ERS_DEBUG(1,ex);
				std::string fifoError(ex.message());
				if(ex.get_error() != 0) {
					fifoError += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
				}
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), fifoError, ex);
			}
			catch(...) {
				ERS_DEBUG(1,"Unknown exception while writing on control FIFO " << control_file.full_name());
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), "unknown exception");
			}

		} else {
			ERS_DEBUG(1,"Access Manager does not allow you to kill the process (handle= " << toHandle()->toString() << ")");
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), "authorization not granted to the user");
		}

	} else {
		ERS_DEBUG(1,"Kill call skipped because app not active (handle=" << toHandle()->toString() <<")");
		throw pmg::Application_Manifest_Already_Unmapped(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName());
	}

} // kill

void pmg::Application::kill_soft(int timeout, const std::string& userName, const std::string& hostName) const {

	boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

	if(is_active()) {
		ERS_ASSERT(m_manifest);

		bool allowed = false;
		try {
			allowed = this->signalAllowed(m_manifest, SIGKILL, userName, hostName);
		}
		catch(ers::Issue& ex) {
			ERS_DEBUG(1,"Access Manager does not allow you to softly kill the process (handle= " << toHandle()->toString() << "): " << ex.message());
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), ex.message(), ex);
		}
		catch(std::exception& ex) {
			ERS_DEBUG(1,"Access Manager does not allow you to softly kill the process (handle= " << toHandle()->toString() << "): " << ex.what());
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), ex.what(), ex);
		}
		catch(...) {
			ERS_DEBUG(1,"Access Manager does not allow you to softly kill the process (handle= " << toHandle()->toString() << ")");
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), "received an unexpected exception");
		}

		if(allowed == true) {
			std::ostringstream message_stream;
			message_stream << pmg::KILL_SOFT_CODE << timeout << "\n";
			System::FIFOConnection control_file = System::FIFOConnection(m_manifest->control_file());
			if(! control_file.exists()) {
				ERS_DEBUG(1, m_manifest->control_file() << " does not exist: the associated Launcher is not running.");
				throw pmg::Launcher_Not_Running(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName());
			}
			try {
				System::Descriptor* c_fifoDescriptor = control_file.open_w(false);
				try {
					c_fifoDescriptor->closeOnExec();
				}
				catch(System::SystemCallIssue &ex) { // Exception thrown by closeOnExec()
					std::string msg = ex.message() + ". File descriptor will stay open in the child process";
					ers::warning(pmg::Exception(ERS_HERE, msg));
				}
				control_file.send(message_stream.str());
				control_file.close();
			}
			catch(System::WriteIssue &ex) {
				ERS_DEBUG(1,"Error writing on control FIFO " << control_file.full_name() << ": " << ex);
				std::string fifoError(ex.message());
				if(ex.get_error() != 0) {
					fifoError += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
				}
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), fifoError, ex);
			}
			catch(System::OpenFileIssue &ex) {
				ERS_DEBUG(1,"Error opening control FIFO " << control_file.full_name() << ": " << ex);
				std::string fifoError(ex.message());
				if(ex.get_error() != 0) {
					fifoError += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
				}
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), fifoError, ex);
			}
			catch(System::PosixIssue &ex) {
				ERS_DEBUG(1,ex);
				std::string fifoError(ex.message());
				if(ex.get_error() != 0) {
					fifoError += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
				}
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), fifoError, ex);
			}
			catch(...) {
				ERS_DEBUG(1,"Unknown exception while writing on control FIFO " << control_file.full_name());
				throw pmg::FIFO_Error(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName(), "unknown exception");
			}

		} else {
			ERS_DEBUG(1,"Access Manager does not allow you to softly kill the process (handle= " << toHandle()->toString() << ")");
			throw pmg::AM_Not_Allowed(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), "authorization not granted to the user");
		}

	} else {
		ERS_DEBUG(1,"Kill_soft call skipped because app not active (handle=" << toHandle()->toString() <<")");
		throw pmg::Application_Manifest_Already_Unmapped(ERS_HERE, toHandle()->toString(), toHandle()->applicationName(), toHandle()->partitionName());
	}

} // kill_soft

void pmg::Application::notify_update(const PMGProcessState state) {

	boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

	if(m_manifest && m_manifest->is_mapped()) {

		m_clientList.notify_update(*m_handle, m_manifest, state);

		// Update IS info (low level process info only when in running state)

		bool updateLowLevelInfo = false;
		if(state == PMGProcessState_RUNNING) {
			updateLowLevelInfo = true;
		}

		if(is_info_process == 0) {
			std::string is_process_server_name = std::string("PMG.") + m_handle->server() + daq::pmg::IS_SERVER_PROCESS_SEPARATOR + m_parent->name();
			const std::string part_name(m_handle->partitionName());
			try {
				is_info_process = new ProcessIS(m_manifest, part_name, is_process_server_name);
				is_info_process->updateInfo(updateLowLevelInfo);
			}
			catch(daq::pmg::Failed_Create_IS &e) {
				ERS_DEBUG(2, e);
				//ers::warning(e);
			}
			catch(daq::pmg::Failed_Publish_IS &e) {
				ERS_DEBUG(2, e);
				//ers::warning(e);
			}
		} else {
			try {
				is_info_process->updateInfo(updateLowLevelInfo);
			}
			catch(daq::pmg::Failed_Publish_IS &e) {
				ERS_DEBUG(2, e);
				//ers::warning(e);
			}
		}

	} else {
		std::string msgReason = "Manifest already unmapped";
		ers::error(pmg::Failed_Status_Update(ERS_HERE, toHandle()->applicationName(), toHandle()->partitionName(), toHandle()->toString(), msgReason));
	}

} // notify_update

void pmg::Application::add_client_ref(const pmgpriv::CLIENT_var& client_ref) {
	m_clientList.add(client_ref);
} // add_client_ref

bool pmg::Application::is_in_client_list(const pmgpriv::CLIENT_var& client_ref) const {
	return m_clientList.check(client_ref);
} // is_in_client_list

void pmg::Application::print_to(std::ostream &stream) const {

	stream << "pmg::Application [";
	stream << m_directory.full_name();

	{
		boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

		if (m_manifest && m_manifest->is_mapped()) {
			stream << ", manifest=" << (*m_manifest);
		} // if
	}

	stream << "]";
} // print_to

void pmg::runReportThread(Application* pApp) {

	{
		boost::recursive_mutex::scoped_lock scoped_lock(pApp->m_mutex);

		// Insert IS info

		if (pApp->m_manifest && pApp->m_manifest->is_mapped()) {

			// Create the ISBridge object to put process info in IS

			const std::string is_process_server_name = std::string("PMG.") + pApp->m_handle->server() + daq::pmg::IS_SERVER_PROCESS_SEPARATOR + pApp->m_parent->name();
			const std::string part_name(pApp->m_handle->partitionName());

			try {
				pApp->is_info_process = new ProcessIS(pApp->m_manifest, part_name, is_process_server_name);
				// The IS info could already exist after a reconnection to launchers due to a crash
				if(!pApp->is_info_process->infoExists()) {
					pApp->is_info_process->updateInfo(false);
				}
			}
			catch(daq::pmg::Failed_Create_IS &e) {
				pApp->is_info_process = 0;
				ERS_DEBUG(2, e);
				//ers::warning(e);
			}
			catch(daq::pmg::Failed_Publish_IS &e) {
				ERS_DEBUG(2, e);
				//ers::warning(e);
			}

			// Agent info

			std::string is_agent_server_name = std::string("PMG.") + std::string("AGENT_") + pApp->toHandle()->server();
			try {
				AgentIS is_info_agent(part_name, is_agent_server_name);
				if(!is_info_agent.infoExists()) {
					is_info_agent.updateInfo(true);
				}
			}
			catch(daq::pmg::Failed_Create_IS &e) {
				ERS_DEBUG(2, e);
				//ers::warning(e);
			}
			catch(daq::pmg::Failed_Publish_IS &e) {
				ERS_DEBUG(2, e);
				//ers::warning(e);
			}

		}

	}

	ERS_DEBUG(2, "Checking the pmglauncher has been started");

	struct timeval tv;
	gettimeofday(&tv,NULL);

	bool is_launcher_ok = false;
	bool timeout_elapsed = false;

	while((!is_launcher_ok) && (!timeout_elapsed)) {

		// Check if the process status has changed

		if(pApp->m_manifest && (pApp->m_manifest->process_state() > PMGProcessState_REQUESTED)) {
			is_launcher_ok = true;
			continue;
		}

		// Check if the timeout's elapsed

		struct timeval _tv_;
		gettimeofday(&_tv_,NULL);
		time_t elapsed_sec = _tv_.tv_sec - tv.tv_sec;
		if(elapsed_sec > LAUNCHER_TIMEOUT) {
			ers::warning(pmg::Launcher_Timeout(ERS_HERE, (pApp->toHandle())->applicationName(), (pApp->toHandle())->partitionName() , (pApp->toHandle())->toString(), LAUNCHER_TIMEOUT));
			timeout_elapsed = true;
			continue;
		}

		// Slow down the loop

		usleep(100000);

	}

	ERS_DEBUG(2, "Start reading from Report FIFO " << *(pApp->m_report_fifo));

	bool quit_not_requested = true;
	int failedLauncherCheck(2);

	std::string Message;
	bool rmFreed = false;

	while(quit_not_requested) {

		fd_set rfds;
		struct timeval tv;
		int retval;

		int fifo_fd = pApp->m_report_fifo->fd();

		FD_ZERO(&rfds);
		FD_SET(fifo_fd, &rfds);

		tv.tv_sec = FIFO_SELECT_PERIOD;
		tv.tv_usec = 0;

		retval = select(fifo_fd + 1, &rfds, NULL, NULL, &tv);

		// If the timeout's elapsed then check if the launcher is alive
		// If the launcher is not alive (i.e. it crashed) then exit from the loop

		if(retval == 0) { // Timeout elapsed. Insert here the code to check if the launcher is still alive
			bool launcher_exists(false);

			pid_t launcherPID = pApp->m_launcher_process.process_id();
			if(launcherPID > 0) {
				int signalResult = ::kill(launcherPID, 0);
				int error = errno;
				if(signalResult == 0 || ((signalResult != 0) && (error != ESRCH))) { // ESRCH means that the pid or process group does not exist: the launcher exited
					// The Process still exists, check its name
					std::ostringstream cmdlnFile;
					cmdlnFile << "/proc/" << launcherPID << "/cmdline";
					std::ifstream fileStream(cmdlnFile.str().c_str(), std::ios::in);
					if(fileStream.good()) {
						std::string appName;
						std::getline(fileStream, appName, '\0');
						if(appName.find(LAUNCHER_NAME) != std::string::npos) {
							launcher_exists = true;
						}
					} else {
						// If the /proc/PID/cmdline file cannot be opened then trust the kill result
						// This is to handle in the right way the situation in which the launcher exits
						// just after the kill call: in this case the /proc/PID/cmdline file cannot be opened
						// but we must read the report FIFO for status notification update (i.e., we do not
						// have to exit from the loop).
						launcher_exists = true;
					}
				}
			}

			if(!launcher_exists) {
				failedLauncherCheck--;
				if(failedLauncherCheck <= 0) { // Check 2 times
					// The launcher exited
					quit_not_requested = false;
					std::string errorMessage = "The launcher for the application " + pApp->toString() + " (handle " + (pApp->toHandle())->toString() +
					") exited unexpectedly. Application control lost. A manual cleanup may be needed.";
					ers::error(pmg::Exception(ERS_HERE,errorMessage));
				}
			}

			continue;
		} else if(retval == -1) { // 'select' error
			const int selectErrno(errno);
			if(selectErrno != EINTR) {
				// The EINTR error can be ignored
				// Any other kind of error cannot be recovered (usually a problem with the fd)
				quit_not_requested = false;
				const std::string selectErrorStr = "Error (" + pmg::utils::errno2String(selectErrno) + ") calling \"select\" on report FIFO for application "
				+ pApp->toString() + " (handle " + (pApp->toHandle())->toString() + "). Application control lost. A manual cleanup may be needed.";
				ers::error(pmg::Exception(ERS_HERE, selectErrorStr));
			}

			continue;
		}

		try {
			Message = pApp->m_report_fifo->read();
			if(Message.empty()) {
				continue;
			}
		}
		catch(System::ReadIssue &e) {
			if(e.get_error() == EAGAIN) {
				continue;
			} else {
				ers::error(pmg::Failed_Status_Update(ERS_HERE, (pApp->toHandle())->applicationName(), (pApp->toHandle())->partitionName(), (pApp->toHandle())->toString(), e.message(), e));
				continue;
			}
		}
		catch(System::PosixIssue &e) {
			ers::error(pmg::Failed_Status_Update(ERS_HERE, (pApp->toHandle())->applicationName(), (pApp->toHandle())->partitionName(), (pApp->toHandle())->toString(), e.message(), e));
			continue;
		}
		catch(std::exception &e) {
			ers::error(pmg::Failed_Status_Update(ERS_HERE, (pApp->toHandle())->applicationName(), (pApp->toHandle())->partitionName(), (pApp->toHandle())->toString(), e.what(), e));
			continue;
		}

		std::string::size_type first = 0;
		std::string::size_type last = 0;
		std::string::size_type len = Message.length();

		while((first < len) && (last != std::string::npos)) { // Parse the Message string to look for multiple commands in the FIFO
			last = Message.find("\n",first);
			if (last == std::string::npos) {
				continue;
			} else {
				if(Message.substr(first,last-first) == pApp->m_stop_thread_code) {
					// In this case stop the thread
					{
						ERS_DEBUG(3, "Received command to stop this thread (" << pApp->toHandle()->toString() << ")");
					}
					boost::mutex::scoped_lock scoped_lock(pApp->m_condVarMutex);
					pApp->m_readStopMessage = true;
					pApp->m_condVar.notify_all();
					pApp->invalid(true); // This lowers the number of active Applications
					return;
				}

				std::string state = Message.substr(first,last-first).c_str();

				ERS_DEBUG(3, "Update status message for handle " << pApp->toHandle()->toString() << " --> " << state);

				if(state[0] == pmg::QUIT_CODE) {
					quit_not_requested = false;
					first = last + 1;
					break;
				}

				PMGProcessState newState = (PMGProcessState) atoi(state.c_str());
				if(newState == PMGProcessState_EXITED || newState == PMGProcessState_SIGNALED ||
				   newState == PMGProcessState_SYNCERROR || newState == PMGProcessState_FAILED)
				{
				    pApp->freeRMResources();
				    rmFreed = true;
				}

				pApp->notify_update(newState);

				first = last + 1;

			} //else

		} // while on Message

	} // while quit not requested

	pApp->m_report_fifo->close();
	{
		boost::mutex::scoped_lock _scoped_lock_(pApp->m_openFIFOMutex);
		pApp->m_fifo_open = false;
	}

	ERS_DEBUG(1,"Finished reading from Report FIFO " << *(pApp->m_report_fifo));

	ERS_DEBUG(1,"Starting cleanup");

	{
		boost::recursive_mutex::scoped_lock scoped_lock(pApp->m_mutex);

		try {
			pApp->m_report_fifo->unlink();
		}
		catch(System::RemoveFileIssue &ex) {
			ERS_DEBUG(2,"Got exception " << ex.what() << " while unlinking report FIFO " << pApp->m_report_fifo->full_name());
		}
		catch(System::PosixIssue &ex) {
			ERS_DEBUG(2,"Got exception " << ex.what() << " while unlinking report FIFO " << pApp->m_report_fifo->full_name());
		}

		if(rmFreed == false) {
		    pApp->freeRMResources();
		}

		if (pApp->m_manifest) {

			try {
				if(pApp->m_manifest->is_loaded() && pApp->m_manifest->is_mapped()) {
					pApp->m_manifest->unmap();
					ERS_DEBUG(2,"Manifest " << pApp->m_manifest->full_name().c_str() << " unmapped");
				}
			}
			catch(System::PosixIssue &e) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, pApp->m_manifest->full_name(), pApp->m_parent->name(), pApp->partition()->name(), e.message(), e));
			}
			catch(std::exception &e) {
				ers::warning(pmg::Manifest_Not_Unmapped(ERS_HERE, pApp->m_manifest->full_name(), pApp->m_parent->name(), pApp->partition()->name(), e.what(), e));
			}

			try {
				pApp->m_manifest->unlink();
				ERS_DEBUG(2,"Manifest " << pApp->m_manifest->full_name().c_str() << " unlinked");
			}
			catch(System::PosixIssue &e) {
				ERS_DEBUG(2, "Got exception " << e.what() << " while unlinking manifest " << pApp->m_manifest->full_name().c_str());
			}
			catch(std::exception &e) {
				ERS_DEBUG(2, "Got exception " << e.what() << " while unlinking manifest " << pApp->m_manifest->full_name().c_str());
			}

		}

		// Remove dir containing FIFOs and manifest
		try {
			std::string dir = pApp->m_report_fifo->parent_name();
			System::File dir_to_remove(dir);
			dir_to_remove.remove();
		}
		catch(System::SystemCallIssue &ex) {
			ERS_DEBUG(2,"Got exception while removing dir " << pApp->m_report_fifo->parent_name() << ": " << ex);
		}
		catch(System::PosixIssue &ex) {
			ERS_DEBUG(2,"Got exception while removing dir " << pApp->m_report_fifo->parent_name() << ": " << ex);
		}
		// Remove process info from IS and delete the is_info_process object

		try {
			if(pApp->is_info_process != 0) pApp->is_info_process->removeInfo();
		}
		catch(daq::pmg::Failed_Remove_IS &e) {
			ERS_DEBUG(1, pmg::Failed_Remove_IS(ERS_HERE, pApp->m_parent->name(), e.message(), e));
		}

		delete(pApp->is_info_process);

	}

	// At the end ask the ApplicationList to delete the Application object

	ApplicationList* pParentList = pApp->m_parent;
	pmg::pmg_id_t appID = pApp->get_id();
	pParentList->removeAndDelete(appID);

} // runReportThread

void pmg::Application::freeRMResources() {
    if(m_manifest) {
        try {
            RMBridge askRM;
            const long handleId = m_manifest->rm_token();
            const pid_t processPID = m_manifest->process_id().process_id();
            const std::string& partName = m_handle->partitionName();
            const std::string& hostName = m_handle->server();
            if(handleId > 0 ) {
                if(processPID > 0) {
                    askRM.free_resources(handleId, partName, hostName, processPID);
                } else {
                    askRM.free_resources(handleId);
                }
            } else {
                if(processPID > 0) {
                    askRM.free_resources_by_pid(partName, hostName, processPID);
                }
            }
        }
        catch(ers::Issue &e) {
        }
    }
}

bool pmg::Application::signalAllowed(const Manifest* pManifest,
                                     int signal,
                                     const std::string& userName,
                                     const std::string& hostName) const
{
	bool mayI = false;

	const std::string loginName(pmg::Server::login_name());

	ERS_DEBUG(5, "Server user name: " << loginName << " - Client user name: " << userName);

	if(loginName != userName) {
		std::string app_args;
		const System::Executable::param_collection p_coll = pManifest->parameters();
		for(std::vector<std::string>::const_iterator ii = p_coll.begin(); ii != p_coll.end(); ++ii) {
			app_args += *ii;
			app_args += " ";
		}

		bool appOwnedByRequester(false);
		const std::string app_owner(pManifest->user().name_safe());
		if(app_owner == userName) {
			appOwnedByRequester = true;
		}

		AMBridge askAM(userName,
		               hostName,
		               m_handle->applicationName(),
		               m_handle->server(),
		               app_args,
		               m_handle->partitionName(),
		               appOwnedByRequester);


		mayI = askAM.allowSignal(signal); // This may throw
		if(mayI == false) {
			ERS_DEBUG(1,"Signaling of " << m_handle->applicationName() << " with signal " << signal << " not allowed by AM: authorization not granted to the user");
		} else {
			ERS_DEBUG(1,"Signaling of " << m_handle->applicationName() << " with signal " << signal << " allowed by AM");
		}
	} else {
		mayI = true;
	}

	return mayI;

}

} // daq

std::ostream& operator<<(std::ostream& stream, const daq::pmg::Application& app) {
	app.print_to(stream);
	return stream;
} // operator<<


