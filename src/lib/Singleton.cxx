#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>

#include <ipc/partition.h>
#include <ipc/alarm.h>
#include <ipc/applicationcontext.h>

#include <ers/ers.h>

#include "ProcessManager/ProxyTable.h"
#include "ProcessManager/Proxy.h"
#include "ProcessManager/Singleton.h"
#include "ProcessManager/Process.h"
#include "ProcessManager/Utils.h"
#include "ProcessManager/private_defs.h"

#include "daq_tokens/acquire.h"

#include <set>

namespace daq {
namespace pmg {

namespace {
std::set<PMGProcessState> validStatesForCallbacks {PMGProcessState_RUNNING,
                                                   PMGProcessState_FAILED,
                                                   PMGProcessState_EXITED,
                                                   PMGProcessState_SIGNALED,
                                                   PMGProcessState_SYNCERROR};
}

Singleton* Singleton::s_instance = 0;
bool Singleton::m_failed_server_port = true;
boost::shared_mutex Singleton::m_static_mutex;
std::string Singleton::m_port_failed_reason;

void singletonCleanup() {
    Singleton::s_instance->m_PMG_Server_Ready_Alarm->suspend();
    Singleton::s_instance->_destroy();
    Singleton::s_instance = 0;
}

const pmgpriv::CLIENT_var& Singleton::reference() const {
    return m_corba_ref;
}

void Singleton::setReference() {
    m_corba_ref = _this(); // Getting the reference the CORBA object is activated
}

void Singleton::init() {

    boost::lock_guard<boost::shared_mutex> lock(m_static_mutex);

    if(s_instance == 0) {
        s_instance = new Singleton();
        try {
            s_instance->setReference();
        }
        catch(CORBA::Exception& ex) {
            throw daq::pmg::Invalid_CORBA_Ref(ERS_HERE, std::string(ex._name()));
        }
        int atexitRes = ::atexit(singletonCleanup);
        if(atexitRes != 0) {
            ers::warning(pmg::Exception(ERS_HERE, "Unable to register cleanup function with atexit()"));
        }
        ERS_DEBUG(1, "ProcessManager client Singleton initialised.");
    } else {
        throw daq::pmg::Already_Init(ERS_HERE);
    }
    if(m_failed_server_port == true) {
        throw daq::pmg::IPC_Port_Error(ERS_HERE, m_port_failed_reason);
    }

}

Singleton* Singleton::instance() {

    boost::shared_lock<boost::shared_mutex> lock(m_static_mutex);

    if(s_instance != 0) {
        return s_instance;
    } else {
        throw daq::pmg::Not_Initialized(ERS_HERE);
    }

} // instance

Singleton::Singleton() :
        m_login_name(configure_login_name()), m_local_host(configure_local_host()), m_proxy_table(new ProxyTable()), m_global_partition(IPCPartition()), m_corba_ref(0)
{
    try {
        m_server_port = daq::pmg::utils::serverPort(); // Try to get the port to communicate with the server
        m_failed_server_port = false;
    }
    catch(daq::pmg::IPC_Port_Error &ex) {
        ers::warning(ex);
        m_server_port = 0;
        m_failed_server_port = true;
        m_port_failed_reason = ex.get_reason();
    }

    m_PMG_Server_Ready_Alarm.reset(new IPCAlarm(SINGLETON_SERVER_QUERY_PERIOD, doWhenPMGServerReady, this));
} // Singleton

bool doWhenPMGServerReady(void* pSingleton) {

    Singleton* sing_p = reinterpret_cast<Singleton*>(pSingleton);

    if(sing_p->reference() == 0) {
        return true;
    }

    // Vector to contain the Handle of the processes to check
    std::vector<std::shared_ptr<Process>> procVect;

    // First remove Process objects referring to processes not running and not linked
    {
        boost::recursive_mutex::scoped_lock scoped_lock(sing_p->m_mutex);

        for(std::map<std::string, std::shared_ptr<Process>>::iterator pos = sing_p->m_process_table.begin();
                pos != sing_p->m_process_table.end();) {
            if((pos->second != nullptr) && (pos->second->exited()) && !(pos->second->is_linked())) {
                sing_p->m_proxy_table->remove(pos->second->handle().toString());
                sing_p->m_process_table.erase(pos++);
            } else {
                if(pos->second != nullptr && !(pos->second->exited())) {
                    procVect.push_back(pos->second);
                }

                ++pos;
            }
        }
    } // Close scope

    int proc_size = procVect.size();

    std::set<std::string> serverSet;

    for(int i = 0; i < proc_size; i++) {
        std::string proc_server_name = ProcessManagerServerPrefix + procVect[i]->handle().server();

        std::set<std::string>::iterator it = serverSet.find(proc_server_name);
        if(it != serverSet.end()) {
            // Processes for this server have already been checked
            continue;
        }
        serverSet.insert(proc_server_name);

        try {
            pmgpriv::SERVER_var proc_server = sing_p->get_server(proc_server_name); // It may throw pmg::No_PMG_Server
            IPCApplicationContext appCtx(proc_server);

            // Server is alive: add the client to the ClientList (do it
            // for all processes running on the same server).
            for(int ii = 0; ii < proc_size; ii++) {
                const Handle& h_ = procVect[ii]->handle();
                std::string selected_proc_server_name = ProcessManagerServerPrefix + h_.server();

                if(selected_proc_server_name == proc_server_name) {
                    pmgpriv::LinkRequestInfo reqLink;
                    reqLink.app_handle = CORBA::string_dup(h_.toString().c_str());
                    reqLink.client_ref = sing_p->reference();

                    try {
                        const bool link_status = proc_server->link_client(reqLink);
                        if(link_status == true) {
                            ERS_DEBUG(3, "Client linked to the server for the application " << h_.toString());

                            // If the link has been successful, then update the local information about the process
                            pmgpub::ProcessStatusInfo_var info = 0;
                            bool done = proc_server->get_info(h_.toString().c_str(), info);
                            const std::shared_ptr<const Process>& p_ = procVect[ii];
                            if(done) {
                                // Information acquired: execute the call-backs if the freshly acquired state
                                // is different than the one locally cached
                                if((p_->exited() == false) && (p_->status().state != info->state)
                                        && (validStatesForCallbacks.find(info->state) != validStatesForCallbacks.end())) {
                                    sing_p->m_proxy_table->callProxyCallbacks(h_.toString(), info);
                                }
                            } else {
                                // The process is gone in the while
                                if(p_->exited() == false) {
                                    sing_p->m_proxy_table->callProxyCallbacks(h_.toString(), PMGProcessState_EXITED);
                                }
                            }
                        } else {
                            ERS_DEBUG(9, "No need to link client to the server for the application " << h_.toString());
                        }
                    }
                    catch(pmgpriv::app_not_found_ex&) {
                        ERS_DEBUG(0,
                                  "Exception in link_client for process " << h_.toString() << ": it looks like it does not exist anymore");

                        // Update the state only if the application is not running any more and the server
                        // has been started later that the process (i.e., the server or the machine died)
                        const std::shared_ptr<const Process>& p_ = procVect[ii];

                        if((p_->exited() == false)
                                && (p_->status().start_time < std::chrono::system_clock::to_time_t(appCtx.m_time))) {
                            ERS_LOG("Application  \"" + h_.toString() + " started at \""
                                    + std::to_string((long ) p_->status().start_time) + " while server started at "
                                    + std::to_string(std::chrono::system_clock::to_time_t(appCtx.m_time)));

                            sing_p->m_proxy_table->callProxyCallbacks(h_.toString(), PMGProcessState_EXITED);
                        }

                        continue;
                    }
                    catch(pmgpriv::link_client_ex &ex) {
                        ERS_DEBUG(0,
                                  "Exception in link_client for process " << h_.toString() << ": " << std::string(ex.error_string));
                        continue;
                    }
                    catch(CORBA::Exception &ex) {
                        ERS_DEBUG(0,
                                  "CORBA exception in link_client for process " << h_.toString() << ": " << ex._name());
                        continue;
                    }
                    catch(...) {
                        ERS_DEBUG(0, "Unexpected exception in link_client for process " << h_.toString());
                        continue;
                    }
                }
            }
        }
        catch(pmg::No_PMG_Server& ex) {
            ERS_DEBUG(0, ex);
        }
        catch(...) {
        }
    }

    return true;
}

Singleton::~Singleton() {
    ERS_DEBUG(3, "Singleton destructor called");
} // ~Singleton

void Singleton::callback(const char* c_handle, const PMGProcessStatusInfo& info) {

    std::string hdl(c_handle);
    m_proxy_table->callProxyCallbacks(hdl, info);

} // callback

std::shared_ptr<Process> Singleton::start_finished(const pmg::Handle& hdl, const PMGProcessStatusInfo& info) {

    boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

    // Call the ProxyTable factory to get the proxy

    std::shared_ptr<Proxy> pProxy(m_proxy_table->factory(hdl));
    pProxy->info(info, true);

    // Create a new Process object

    const std::string hdlStr(hdl.toString());
    std::shared_ptr<Process> proc_p(Process::create(pProxy));

    process_table_t::iterator it = m_process_table.find(hdlStr);
    if(it != m_process_table.end()) {
        ERS_DEBUG(0,
                  "Already found Process object associated with handle " << hdlStr << ". Removing old Process object and replacing with the fresh one");
        m_process_table.erase(it);
    }

    m_process_table.insert(make_pair(hdlStr, proc_p));

    return proc_p;

} // start_finished

std::shared_ptr<Process> Singleton::get_process(const Handle& handle) {
    boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

    std::shared_ptr<Process> proc_p;
    const std::string& handleStr = handle.toString();

    process_table_t::const_iterator it = m_process_table.find(handleStr);
    if(it != m_process_table.end()) {
        // The Process already exists
        if(!(it->second->exited()) || it->second->is_linked()) {
            proc_p = it->second;
        }
    } else {
        // Create a new Process
        std::shared_ptr<Proxy> proxy(m_proxy_table->find(handleStr));

        if(proxy == nullptr) {
            // process not started by me; try to find it contacting
            // the real host as described in the handle
            try {
                pmgpriv::SERVER_var server = get_server(ProcessManagerServerPrefix + handle.server()); // Throws pmg::No_PMG_Server
                bool app_exists = server->exists(handleStr.c_str());
                if(app_exists) {
                    // Update process status structure, create the Proxy and
                    // ask the server to put the client into the ClientList
                    pmgpub::ProcessStatusInfo_var info = 0;
                    bool done = server->get_info(handleStr.c_str(), info);
                    if(done) {
                        proxy = m_proxy_table->factory(handle);
                        proxy->info(info, true);
                        pmgpriv::LinkRequestInfo link_request;
                        link_request.client_ref = this->reference();
                        link_request.app_handle = CORBA::string_dup(handleStr.c_str());
                        try {
                            server->link_client(link_request);
                        }
                        catch(pmgpriv::app_not_found_ex&) {
                            ERS_DEBUG(0, "Process with handle " << handle.toString() << " does not exist anymore");
                        }
                    }
                }
            }
            catch(pmgpriv::exists_ex &ex) {
                ERS_DEBUG(0,
                          "Exception in get_process for process " << handle.applicationName() << ": " << std::string(ex.error_string));
                throw pmg::Failed_Get_Process(ERS_HERE,
                                              handle.applicationName(),
                                              handle.partitionName(),
                                              std::string(ex.error_string));
            }
            catch(pmgpriv::get_info_ex &ex) {
                ERS_DEBUG(0,
                          "Exception in get_process for process " << handle.applicationName() << ": " << std::string(ex.error_string));
                throw pmg::Failed_Get_Process(ERS_HERE,
                                              handle.applicationName(),
                                              handle.partitionName(),
                                              std::string(ex.error_string));
            }
            catch(pmgpriv::link_client_ex &ex) {
                ERS_DEBUG(0,
                          "Exception in get_process for process " << handle.applicationName() << ": " << std::string(ex.error_string));
                throw pmg::Failed_Get_Process(ERS_HERE,
                                              handle.applicationName(),
                                              handle.partitionName(),
                                              std::string(ex.error_string));
            }
            catch(CORBA::TRANSIENT& ex) {
                ERS_DEBUG(0,
                          "CORBA exception in get_process for process " << handle.applicationName() << ": " << ex._name());
                throw pmg::No_PMG_Server(ERS_HERE,
                                         "No PMG server on host " + handle.server(),
                                         handle.server(),
                                         ex._name());
            }
            catch(CORBA::Exception &ex) {
                ERS_DEBUG(0,
                          "CORBA exception in get_process for process " << handle.applicationName() << ": " << ex._name());
                throw pmg::Bad_PMG_Server(ERS_HERE, handle.server(), ex._name());
            }
        } // if(proxy==0)

        if(proxy != nullptr) { // If proxy == nullptr then the process is not running
            proc_p = Process::create(proxy);
            m_process_table.insert(make_pair(handleStr, proc_p));
        }
    }

    return proc_p;
} // get_process

std::shared_ptr<Process> Singleton::process(const std::string& name) const {

    boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

    std::shared_ptr<Process> pProcess;

    process_table_t::const_iterator pos = m_process_table.find(name);
    if(pos != m_process_table.end()) {
        pProcess = pos->second;
    }

    return pProcess;
}

void Singleton::askRunningProcesses(const std::string& partition,
                                    std::map<std::string, pmgpub::ProcessStatusInfo>& procMap) const
{
    unsigned int failures = 0;
    procMap.clear();

    ERS_DEBUG(5, "Get all agents.");
    std::map<std::string, pmgpriv::SERVER_var> objMap;

    try {
        m_global_partition.getObjects<pmgpriv::SERVER>(objMap);
        ERS_DEBUG(5, "Done.");
    }
    catch(daq::ipc::Exception & ex) {
        ERS_DEBUG(0, ex);
        throw pmg::No_Agents(ERS_HERE, ex.message(), ex);
    }

    std::string agentList;

    for(std::map<std::string, pmgpriv::SERVER_var>::const_iterator i = objMap.begin(); i != objMap.end(); ++i) {
        try {
            ERS_DEBUG(2, "Asking agent " << i->first << " about all the running processes in partition " << partition);

            pmgpriv::proc_info_list_var procList = 0;
            i->second->processes_info(partition.c_str(), procList);

            CORBA::ULong size = procList->length();
            for(CORBA::ULong i = 0; i < size; ++i) {
                procMap[std::string(procList[i].app_handle)] = procList[i].proc_info;
            }
        }
        catch(pmgpriv::processes_ex &ex) {
            std::string msg = "Error while contacting " + i->first + ": " + std::string(ex.error_string);
            ers::warning(pmg::Exception(ERS_HERE, msg));
            agentList += i->first;
            agentList += pmg::ProcessManagerSeparator;
            ++failures;
        }
        catch(CORBA::Exception& ex) {
            std::string msg = "Error while contacting " + i->first + ": " + ex._name();
            ers::warning(pmg::Exception(ERS_HERE, msg));
            agentList += i->first;
            agentList += pmg::ProcessManagerSeparator;
            ++failures;
        }
    }

    if(failures > 0) {
        throw pmg::Failed_Listing_Partition(ERS_HERE, partition, agentList);
    }
}

void Singleton::askRunningProcesses(const std::string& partition, std::string& procList) const {

    unsigned int failures = 0;

    procList.erase();

    ERS_DEBUG(5, "Get all agents.");
    std::map<std::string, pmgpriv::SERVER_var> objMap;

    try {
        m_global_partition.getObjects<pmgpriv::SERVER>(objMap);
        ERS_DEBUG(5, "Done.");
    }
    catch(daq::ipc::Exception & ex) {
        ERS_DEBUG(0, ex);
        throw pmg::No_Agents(ERS_HERE, ex.message(), ex);
    }

    std::string agentList;

    for(std::map<std::string, pmgpriv::SERVER_var>::const_iterator i = objMap.begin(); i != objMap.end(); ++i) {
        try {
            ERS_DEBUG(2, "Asking agent " << i->first << " about all the running processes in partition " << partition);
            CORBA::String_var procHandles = i->second->processes(partition.c_str());
            procList.append(std::string(procHandles));
        }
        catch(pmgpriv::processes_ex &ex) {
            std::string msg = "Error while contacting " + i->first + ": " + std::string(ex.error_string);
            ers::warning(pmg::Exception(ERS_HERE, msg));
            agentList += i->first;
            agentList += pmg::ProcessManagerSeparator;
            ++failures;
        }
        catch(CORBA::Exception& ex) {
            std::string msg = "Error while contacting " + i->first + ": " + ex._name();
            ers::warning(pmg::Exception(ERS_HERE, msg));
            agentList += i->first;
            agentList += pmg::ProcessManagerSeparator;
            ++failures;
        }
    }

    if(failures > 0) {
        throw pmg::Failed_Listing_Partition(ERS_HERE, partition, agentList);
    }

}

void Singleton::askRunningProcesses(const std::string& hostname,
                                    const std::string& partition,
                                    std::map<std::string, pmgpub::ProcessStatusInfo>& procMap) const
{

    procMap.clear();

    pmgpriv::SERVER_var server = get_server(ProcessManagerServerPrefix + hostname); // Throws pmg::No_PMG_Server

    try {
        ERS_DEBUG(2,
                  "Asking agent on host " << hostname << " about all the running processes in partition " << partition);

        pmgpriv::proc_info_list_var procList = 0;
        server->processes_info(partition.c_str(), procList);

        CORBA::ULong size = procList->length();
        for(CORBA::ULong i = 0; i < size; ++i) {
            procMap[std::string(procList[i].app_handle)] = procList[i].proc_info;
        }
    }
    catch(pmgpriv::processes_ex &ex) {
        ERS_DEBUG(0, "Error while contacting " << hostname << ": " << ex.error_string);
        throw pmg::Failed_Listing_Server(ERS_HERE, hostname, std::string(ex.error_string));
    }
    catch(CORBA::TRANSIENT& ex) {
        ERS_DEBUG(0, "Error while contacting " << hostname << ": " << ex._name());
        throw pmg::No_PMG_Server(ERS_HERE, "No PMG server on host " + hostname, hostname, ex._name());
    }
    catch(CORBA::Exception& ex) {
        ERS_DEBUG(0, "Error while contacting " << hostname << ": " << ex._name());
        throw pmg::Bad_PMG_Server(ERS_HERE, hostname, ex._name());
    }

}

void Singleton::askRunningProcesses(const std::string& hostname,
                                    const std::string& partition,
                                    std::string& procList) const
{

    procList.erase();

    pmgpriv::SERVER_var server = get_server(ProcessManagerServerPrefix + hostname); // Throws pmg::No_PMG_Server

    try {
        ERS_DEBUG(2,
                  "Asking agent on host " << hostname << " about all the running processes in partition " << partition);
        CORBA::String_var procHandles = server->processes(partition.c_str());
        procList.append(std::string(procHandles));
    }
    catch(pmgpriv::processes_ex &ex) {
        ERS_DEBUG(0, "Error while contacting " << hostname << ": " << ex.error_string);
        throw pmg::Failed_Listing_Server(ERS_HERE, hostname, std::string(ex.error_string));
    }
    catch(CORBA::TRANSIENT& ex) {
        ERS_DEBUG(0, "Error while contacting " << hostname << ": " << ex._name());
        throw pmg::No_PMG_Server(ERS_HERE, "No PMG server on host " + hostname, hostname, ex._name());
    }
    catch(CORBA::Exception& ex) {
        ERS_DEBUG(0, "Error while contacting " << hostname << ": " << ex._name());
        throw pmg::Bad_PMG_Server(ERS_HERE, hostname, ex._name());
    }

}

void Singleton::kill_partition(const std::string& partition, const int timeout) {

    unsigned int failures = 0;
    unsigned int signal_failures = 0;
    std::string handles;
    std::string agents;

    std::map<std::string, pmgpriv::SERVER_var> objMap;
    try {
        m_global_partition.getObjects<pmgpriv::SERVER>(objMap);
    }
    catch(daq::ipc::Exception & ex) {
        ERS_DEBUG(0, ex);
        throw pmg::No_Agents(ERS_HERE, ex.message(), ex);
    }

    auto creds = credentials();

    for(std::map<std::string, pmgpriv::SERVER_var>::const_iterator i = objMap.begin(); i != objMap.end(); ++i) {
        try {
            ERS_DEBUG(0, "Kill processes of partition " << partition << " on host " << i->first);
            i->second->kill_partition(partition.c_str(),
                                      timeout,
                                      creds.c_str(),
                                      this->local_host().c_str());
        }
        catch(pmgpriv::kill_partition_ex &ex) {
            std::string msg = "Error while contacting " + i->first + ": " + std::string(ex.error_string);
            ers::warning(pmg::Exception(ERS_HERE, msg));
            ++signal_failures;
            handles += std::string(ex.handle_list);
        }
        catch(CORBA::Exception& ex) {
            std::string msg = "Error while contacting " + i->first + ": " + ex._name();
            ers::warning(pmg::Exception(ERS_HERE, msg));
            ++failures;
            agents += i->first;
            agents += pmg::ProcessManagerSeparator;
        }
    }

    if((signal_failures > 0) || (failures > 0)) {
        throw pmg::Failed_Kill_Partition(ERS_HERE, partition, handles, agents);
    }

}

std::unique_ptr<Handle> Singleton::lookup(const std::string& hostname,
                                          const std::string& appname,
                                          const std::string& partname) const
{

    std::unique_ptr<Handle> pH;

    pmgpriv::SERVER_var server = get_server(ProcessManagerServerPrefix + hostname); // Throws pmg::No_PMG_Server

    std::string handle_str;

    try {
        CORBA::String_var Hdl = server->lookup(appname.c_str(), partname.c_str());
        handle_str = std::string(Hdl);
    }
    catch(pmgpriv::lookup_ex &ex) {
        ERS_DEBUG(0, "Exception in lookup for process " << appname << ": " << std::string(ex.error_string));
        throw pmg::Failed_Lookup(ERS_HERE, appname, partname, std::string(ex.error_string));
    }
    catch(CORBA::TRANSIENT& ex) {
        ERS_DEBUG(0, "CORBA Exception in lookup for process " << appname << ": " << ex._name());
        throw pmg::No_PMG_Server(ERS_HERE, "No PMG server on host " + hostname, hostname, ex._name());
    }
    catch(CORBA::Exception &ex) {
        ERS_DEBUG(0, "CORBA Exception in lookup for process " << appname << ": " << ex._name());
        throw pmg::Bad_PMG_Server(ERS_HERE, ProcessManagerServerPrefix + hostname, ex._name());
    }

    try {
        if(!handle_str.empty()) {
            pH.reset(new Handle(handle_str));
        }
    }
    catch(pmg::Invalid_Handle& ex) {
        throw pmg::Failed_Lookup(ERS_HERE, appname, partname, ex.message(), ex);
    }

    return pH;

} // lookup

std::unique_ptr<Handle> Singleton::lookup(const std::string& appname, const std::string& partname) const {

    std::unique_ptr<Handle> pH;

    unsigned int failures = 0;
    std::string handle_str;
    std::string badAgents;

    std::map<std::string, pmgpriv::SERVER_var> objMap;
    try {
        m_global_partition.getObjects<pmgpriv::SERVER>(objMap);
    }
    catch(daq::ipc::Exception & ex) {
        ers::warning(pmg::Exception(ERS_HERE, ex));
        throw pmg::No_Agents(ERS_HERE, ex.message(), ex);
    }

    for(std::map<std::string, pmgpriv::SERVER_var>::const_iterator i = objMap.begin(); i != objMap.end(); ++i) {
        try {
            CORBA::String_var Hdl = i->second->lookup(appname.c_str(), partname.c_str());
            handle_str = std::string(Hdl);
            if(!handle_str.empty()) {
                break;
            }
        }
        catch(pmgpriv::lookup_ex &ex) {
            std::string msg = "Exception in lookup for process " + appname + " on agent " + i->first + ": "
                    + std::string(ex.error_string);
            ers::warning(pmg::Exception(ERS_HERE, msg));
            ++failures;
            badAgents += i->first;
            badAgents += pmg::ProcessManagerSeparator;
        }
        catch(CORBA::Exception &ex) {
            std::string msg = "Communication error in lookup for process " + appname + " on agent " + i->first + ": "
                    + ex._name();
            ers::warning(pmg::Exception(ERS_HERE, msg));
            ++failures;
            badAgents += i->first;
            badAgents += pmg::ProcessManagerSeparator;
        }
    }

    if(failures > 0) {
        throw pmg::Failed_Multiple_Lookup(ERS_HERE, appname, handle_str, badAgents);
    }

    try {
        if(!handle_str.empty()) {
            pH.reset(new Handle(handle_str));
        }
    }
    catch(pmg::Invalid_Handle& ex) {
        throw pmg::Failed_Multiple_Lookup(ERS_HERE, appname, handle_str, badAgents, ex);
    }

    return pH;
}

std::unique_ptr<Handle> Singleton::lookup(const std::vector<std::string>& hosts,
                                          const std::string& appname,
                                          const std::string& partname) const
{

    std::unique_ptr<Handle> pH;

    unsigned int failures = 0;
    std::string handle_str;
    std::string badAgents;

    for(std::vector<std::string>::const_iterator i = hosts.begin(); i != hosts.end(); ++i) {
        try {
            pmgpriv::SERVER_var server = get_server(ProcessManagerServerPrefix + (*i));
            CORBA::String_var Hdl = server->lookup(appname.c_str(), partname.c_str());
            handle_str = std::string(Hdl);
            if(!handle_str.empty()) {
                break;
            }
        }
        catch(pmgpriv::lookup_ex &ex) {
            std::string msg = "Exception in lookup for process " + appname + " on host " + (*i) + ": "
                    + std::string(ex.error_string);
            ers::warning(pmg::Exception(ERS_HERE, msg));
            ++failures;
            badAgents += ProcessManagerServerPrefix + (*i);
            badAgents += pmg::ProcessManagerSeparator;
        }
        catch(pmg::No_PMG_Server &ex) {
            std::string msg = "Exception in lookup for process " + appname + " on host " + (*i) + ": " + ex.message();
            ers::warning(pmg::Exception(ERS_HERE, msg));
            ++failures;
            badAgents += ProcessManagerServerPrefix + (*i);
            badAgents += pmg::ProcessManagerSeparator;
        }
        catch(CORBA::Exception &ex) {
            std::string msg = "Communication error in lookup for process " + appname + " on host " + (*i) + ": "
                    + ex._name();
            ers::warning(pmg::Exception(ERS_HERE, msg));
            ++failures;
            badAgents += ProcessManagerServerPrefix + (*i);
            badAgents += pmg::ProcessManagerSeparator;
        }
    }

    if(failures > 0) {
        throw pmg::Failed_Multiple_Lookup(ERS_HERE, appname, handle_str, badAgents);
    }

    try {
        if(!handle_str.empty()) {
            pH.reset(new Handle(handle_str));
        }
    }
    catch(pmg::Invalid_Handle& ex) {
        throw pmg::Failed_Multiple_Lookup(ERS_HERE, appname, handle_str, badAgents, ex);
    }

    return pH;
}

bool Singleton::exists_somewhere(const Handle& handle) const {

    pmgpriv::SERVER_var server = get_server(ProcessManagerServerPrefix + handle.server()); // Throws pmg::No_PMG_Server

    bool app_exists = false;

    try {
        app_exists = server->exists(handle.toString().c_str());
    }
    catch(pmgpriv::exists_ex &ex) {
        ERS_DEBUG(0,
                  "Exception in exists for process " << handle.applicationName() << ": " << std::string(ex.error_string));
        throw pmg::Failed_Exists(ERS_HERE,
                                 handle.applicationName(),
                                 handle.partitionName(),
                                 std::string(ex.error_string));
    }
    catch(CORBA::TRANSIENT& ex) {
        ERS_DEBUG(0, "CORBA exception in exists for process " << handle.applicationName() << ": " << ex._name());
        throw pmg::No_PMG_Server(ERS_HERE, "No PMG server on host " + handle.server(), handle.server(), ex._name());
    }
    catch(CORBA::Exception &ex) {
        ERS_DEBUG(0, "CORBA exception in exists for process " << handle.applicationName() << ": " << ex._name());
        throw pmg::Bad_PMG_Server(ERS_HERE, handle.server(), ex._name());
    }

    return app_exists;

} // exists_somewhere

pmgpriv::SERVER_var Singleton::get_server(const std::string& pmgserver_name) const {

    pmgpriv::SERVER_var server = 0;

    try {

        if(m_server_port != 0) {

            ERS_DEBUG(10, "NOT using IPC lookup to get server reference");

            std::vector<std::string> strVect;
            daq::pmg::utils::tokenize(pmgserver_name, "_", strVect);

            std::vector<std::pair<std::string, int> > endpoints;
            endpoints.push_back(std::make_pair(strVect[1], m_server_port));

            std::string reference = ::ipc::util::constructReference(m_global_partition.name(),
                                                                    pmgserver_name,
                                                                    ::ipc::util::getTypeName<pmgpriv::SERVER>(),
                                                                    endpoints);

            CORBA::Object_var object = IPCCore::stringToObject(reference);
            server = pmgpriv::SERVER::_narrow(object);

            if(CORBA::is_nil(server)) {
                throw pmg::No_PMG_Server(ERS_HERE,
                                         "Cannot contact PMG server on host " + strVect[1]
                                                 + ": invalid CORBA reference",
                                         strVect[1],
                                         "Invalid CORBA reference");
            }

        } else {

            ERS_DEBUG(10, "Using IPC lookup to get server reference");
            server = m_global_partition.lookup<pmgpriv::SERVER>(pmgserver_name);

        }

    }
    catch(daq::ipc::Exception & ex) {
        throw pmg::No_PMG_Server(ERS_HERE, pmgserver_name, ex.message(), ex);
    }
    catch(CORBA::Exception &ex) {
        throw pmg::No_PMG_Server(ERS_HERE,
                                 pmgserver_name + " is not reachable (" + std::string(ex._name()) + ")",
                                 pmgserver_name,
                                 ex._name());
    }

    return server;

} // get_server

const System::LocalHost* Singleton::configure_local_host() const {
    System::LocalHost* lh = new System::LocalHost();
    // Work-around in order to oblige the library to build the full name
    // That is why the call is not thread-safe
    lh->full_name();

    return lh;
}

const std::string Singleton::configure_login_name() const {
    return daq::pmg::utils::loginName();
}

const std::string& Singleton::login_name() const {
    return m_login_name;
}

std::string Singleton::credentials() const {
    static bool s_tokens_enabled = daq::tokens::enabled();
    return s_tokens_enabled ? daq::tokens::acquire(daq::tokens::Mode::Reuse) : login_name();
}

const std::string& Singleton::local_host() const {
    return m_local_host->full_name();
}

}
} // daq::pmg
