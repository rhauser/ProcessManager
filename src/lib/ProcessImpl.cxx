#include <ers/ers.h>

#include "ProcessManager/ProcessImpl.h"
#include "ProcessManager/Singleton.h"
#include "ProcessManager/Proxy.h"
#include "ProcessManager/CallBack.h"

namespace daq {
namespace pmg {

// this copy constructor is dangerous because it allows to unlink the same
// callback twice if we also copy m_callback_ptr. The preferred way to get
// new instances is to query Singleton to get fresh copies.
/*
 ProcessImpl::ProcessImpl(const ProcessImpl &other)
 : m_callback_ptr(other.m_callback_ptr), m_proxy_ptr(other.proxy())
 {
 //m_callback_ptr = 0;
 } // ProcessImpl
 */

ProcessImpl::ProcessImpl(const std::shared_ptr<Proxy>& proxy) :
        m_callback_ptr(nullptr), m_proxy_ptr(proxy)
{
    ERS_ASSERT(proxy);
} // ProcessImpl

ProcessImpl::~ProcessImpl() {
    // unlink this Process
    ProcessImpl::unlink();
} // ~ProcessImpl

bool ProcessImpl::operator==(const ProcessImpl &other) const {
    if(!(m_callback_ptr == other.m_callback_ptr))
        return false;
    if(!(m_proxy_ptr == other.m_proxy_ptr))
        return false; // compare only ptrs here
    return true;
} // operator==

bool ProcessImpl::operator!=(const ProcessImpl &other) const {
    return (!((*this) == other));
} // operator!=

std::shared_ptr<Proxy> ProcessImpl::proxy() const {
    return m_proxy_ptr;
} // proxy

const std::string ProcessImpl::name() const {
    const std::string name_str(m_proxy_ptr->info().start_info.app_name);
    return name_str;
} // name

const Handle& ProcessImpl::handle() const {
    return m_proxy_ptr->handle();
} // handle

PMGProcessStatusInfo ProcessImpl::status() const {
    return m_proxy_ptr->info();
} // status

const std::string ProcessImpl::path() const {
    const std::string path_str(m_proxy_ptr->info().start_info.executable);
    return path_str;
} // path

std::map<std::string, std::string> ProcessImpl::env() const {
    std::map<std::string, std::string> envs;
    const pmgpub::env_list_type& corba_envs = m_proxy_ptr->info().start_info.envs;
    const int size = corba_envs.length();
    for(int i = 0; i < size; i++) {
        // add each mapping to envs
        const pmgpub::EnvironmentPair& current = corba_envs[i];
        const std::string var(current.name);
        const std::string val(current.value);
        envs[var] = val;
    }
    return envs;
} // env

std::vector<std::string> ProcessImpl::params() const {
    std::vector<std::string> params;
    const pmgpub::arg_list_type& corba_params = m_proxy_ptr->info().start_info.start_args;
    const int size = corba_params.length();
    for(int i = 0; i < size; i++) {
        // add params
        const std::string val(corba_params[i]);
        params.push_back(val);
    }
    return params;
} // params

PMGResourceInfo ProcessImpl::resource() const {
    return m_proxy_ptr->info().resource_usage;
} // resource

bool ProcessImpl::is_linked() const {
    return m_callback_ptr != nullptr;
} // is_linked

bool ProcessImpl::exited() const {
    return m_proxy_ptr->exited();
} // is_running

void ProcessImpl::link(const std::shared_ptr<Process>& callbackowner, CallBackFncPtr callback, void* callbackparameter)
{
    if(m_callback_ptr == nullptr) {
        // Set the Process to be linked
        m_callback_ptr.reset(new CallBack(callbackowner, callback, callbackparameter));
        // Register callback in the proxy
        m_proxy_ptr->link(m_callback_ptr.get());
    } else {
        ERS_DEBUG(3, "Method called when the process is already linked to a callback.");
    }
} // link

void ProcessImpl::unlink() {
    if(m_callback_ptr != nullptr) {
        // Unregister call back in the proxy
        m_proxy_ptr->unlink(m_callback_ptr.get());
        m_callback_ptr.reset();
    } else {
        ERS_DEBUG(3, "Method called when the process is already unlinked.");
    }
} // unlink

void ProcessImpl::stop() {
    m_proxy_ptr->stop();
} // stop

void ProcessImpl::kill() {
    m_proxy_ptr->kill();
} // kill

void ProcessImpl::kill_soft(const int timeout) {
    m_proxy_ptr->kill_soft(timeout);
} // kill_soft

void ProcessImpl::signal(const int signum) {
    m_proxy_ptr->signal(signum);
} // signal

void ProcessImpl::errFile(pmgpriv::File_var& fileContent) const {
    m_proxy_ptr->errFile(fileContent);
}

void ProcessImpl::outFile(pmgpriv::File_var& fileContent) const {
    m_proxy_ptr->outFile(fileContent);
}

}
} // daq::pmg
