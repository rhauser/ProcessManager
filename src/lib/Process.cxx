#include "ProcessManager/Process.h"
#include "ProcessManager/ProcessImpl.h"
#include "ProcessManager/Proxy.h"

namespace daq {
namespace pmg {

// dangerous: allowing a copy constr. causes issues with the invariant
// 'at most one callback per process' -> could unlink same callback
// twice. Preferred method is to query Singleton to get another Process
/*
 Process::Process(const Process &other)
 : m_impl(new ProcessImpl(*(other.impl())))
 {
 } // Process
 */

struct ProcessDeleter {
        void operator()(Process* p) const {
            delete p;
        }
};

std::shared_ptr<Process> Process::create(const std::shared_ptr<Proxy>& proxy) {
    return std::shared_ptr<Process>(new Process(proxy), ProcessDeleter());
}

Process::Process(const std::shared_ptr<Proxy>& proxy) :
        std::enable_shared_from_this<Process>(), m_impl(new ProcessImpl(proxy))
{
} // Process

Process::~Process() {
} // ~Process

bool Process::operator==(const Process &other) const {
    return ((*m_impl) == (*(other.impl())));
} // operator==

bool Process::operator!=(const Process &other) const {
    return ((*m_impl) != (*(other.impl())));
}

const std::string Process::name() const {
    return m_impl->name();
} // name

const Handle& Process::handle() const {
    return m_impl->handle();
} // handle

PMGProcessStatusInfo Process::status() const {
    return m_impl->status();
} // status

const std::string Process::path() const {
    return m_impl->path();
} //

std::map<std::string, std::string> Process::env() const {
    return m_impl->env();
} // env

std::vector<std::string> Process::params() const {
    return m_impl->params();
} // params

PMGResourceInfo Process::resource() const {
    return m_impl->resource();
} // resource

bool Process::is_linked() const {
    return m_impl->is_linked();
} // is_linked

bool Process::exited() const {
    return m_impl->exited();
} // is_running

bool Process::is_valid() const {
    // is this the right way to check if the process exists?
    // normally constructors' visibility ensures that you don't
    // get invalid Process objects (have to query Singleton)
    return m_impl->proxy() != 0;
} // is_valid

void Process::link(CallBackFncPtr callback, void* callbackparameter) {
    m_impl->link(shared_from_this(), callback, callbackparameter);
} // link

void Process::unlink() {
    m_impl->unlink();
} // unlink

void Process::stop() {
    m_impl->stop();
} // stop

void Process::kill() {
    m_impl->kill();
} // kill

void Process::kill_soft(const int timeout) {
    m_impl->kill_soft(timeout);
} // kill_soft

void Process::signal(const int signum) {
    m_impl->signal(signum);
} // signal

void Process::errFile(pmgpriv::File_var& fileContent) const {
    m_impl->errFile(fileContent);
}

void Process::outFile(pmgpriv::File_var& fileContent) const {
    m_impl->outFile(fileContent);
}

ProcessImpl* Process::impl() const {
    return m_impl.get();
}

}
} // daq::pmg

