#include <cstdio>

#include <sys/time.h>

#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>
#include <pthread.h>
#include <errno.h>

#include <sstream>
#include <iostream>
#include <map>
#include <memory>

#include <boost/program_options.hpp>

#include <ers/ers.h>

#include <ipc/core.h>

#include "ProcessManager/client_simple.h"
#include "ProcessManager/Exceptions.h"

namespace po = boost::program_options;
using namespace daq;
using namespace std;
using namespace pmg;


int Tasks = 0;

int thread_flag;
pthread_cond_t thread_flag_cv;
pthread_mutex_t thread_flag_mutex;

void testcallback(std::shared_ptr<Process> process, void* test) {
	if(test != 0) {
	} // avoids the unused param warning

	ERS_DEBUG(0,"Received callback for handle " << process->handle().toString()
			<< " with state " << p_state_strings[(unsigned int) process->status().state] );
	if(process->exited()) {

		//      process->unlink();

		pthread_mutex_lock(&thread_flag_mutex);

		Tasks--;

		ERS_DEBUG(0,"Tasks left running = " << Tasks);

		pthread_cond_signal(&thread_flag_cv);

		pthread_mutex_unlock(&thread_flag_mutex);

	}
}

int main(int argc, char** argv) {
	try {
		IPCCore::init(argc, argv);
		pmg::Singleton::init();

		std::string host, AppName, ExecName;
		int procs, loops, Timeout;

		try {
			po::options_description desc("Test application for the ProcessDescription class");

			desc.add_options()
			("host,H", po::value<string>(&host), "Host name")
			("procs,N", po::value<int>(&procs),"Number of processes (1 - 3)")
			("loops,n", po::value<int>(&loops), "number of loops")
			("Timeout,t", po::value<int>(&Timeout), "Max time to wait to kill processes")
			("AppName,a", po::value<string>(&AppName), "Radix to built application names")
			("ExecName,e", po::value<string>(&ExecName), "Executable name (full path)")
			("help,h", "Print help message");

			po::variables_map vm;
			po::store(po::parse_command_line(argc, argv, desc), vm);
			po::notify(vm);

			if(vm.count("help")) {
				std::cout << desc << std::endl;
				return EXIT_SUCCESS;
			}

			if((!vm.count("host")) || (!vm.count("procs")) || (!vm.count("loops")) || (!vm.count("Timeout"))
			        || (!vm.count("AppName")) || (!vm.count("ExecName"))) {
				ERS_INFO("ERROR: please specify all the command line options!");
				std::cout << desc << std::endl;
				return EXIT_FAILURE;
			}
		}
		catch(std::exception& ex) {
			ERS_INFO("Bad command line options: " << ex.what());
			return EXIT_FAILURE;
		}

		cout << "Test will start a few processes, wait for a random number of seconds (between 0 and  " << Timeout
		        << ")  and kill them " << loops << " times." << endl
		        << "2 callbacks should be obtained for each process: RUNNING->->EXITED." << endl << endl;

		const char* partition = getenv("TDAQ_PARTITION");

		std::string app_name_1 = AppName + std::string("_1");
		std::string app_name_2 = AppName + std::string("_2");
		std::string app_name_3 = AppName + std::string("_3");

		// Get the local host name
		System::Host h(host);

		// Get my login name
		std::string login_name = "";
		{
			char* loginName;
			struct passwd* userPasswd;

			if((loginName = getlogin()) == 0) {
				userPasswd = getpwuid(getuid());
				if(userPasswd->pw_name != 0)
					loginName = userPasswd->pw_name;
			}

			if(loginName != 0) {
				login_name = loginName;
			} else {
				login_name = "unknown";
			}
		}

		std::string logpath = "/tmp/" + login_name + "/pmg_test_logs";

		std::string libpath(getenv("LD_LIBRARY_PATH"));
		libpath += ":/lib;/usr/lib;/usr/local/lib";
		std::map<std::string, std::string> env;
		env["LD_LIBRARY_PATH"] = libpath;

		int rn1, rn2, rn3, rns;
		;
		const int ran = Timeout;
		char str[256];

		srand(time(0));

		pmg::ProcessDescription* d1 = 0;
		pmg::ProcessDescription* d2 = 0;
		pmg::ProcessDescription* d3 = 0;

		std::shared_ptr<Process> pD1, pD2, pD3;

		pthread_mutex_init(&thread_flag_mutex, NULL);
		pthread_cond_init(&thread_flag_cv, NULL);

		for(int i = 0; i < loops; i++) {

			ERS_INFO("------------------> START LOOP NUMBER " << i);

			// Create a ProcessDescription Object

			int pippo = rand();
			double temp = ran * (pippo / (RAND_MAX + 1.0));
			rn1 = (int) temp;

			pippo = rand();
			temp = ran * (pippo / (RAND_MAX + 1.0));
			rn2 = (int) temp;

			pippo = rand();
			temp = ran * (pippo / (RAND_MAX + 1.0));
			rn3 = (int) temp;

			pippo = rand();
			temp = ran * (pippo / (RAND_MAX + 1.0));
			rns = (int) temp;

			ERS_INFO("------------------> " << rn1 << " " << rn2 << " " << rn3 << " " << rns);

			sprintf(str, "%d", rn1);

			vector<string> temp_args;
			temp_args.push_back(str);

			//      std::cout << "O>STR() " << str << " " << rn << " " << pippo << " " << RAND_MAX << std::endl;


			d1 = new pmg::ProcessDescription(h.full_name(), // hostname
			                                 app_name_1, // appname
			                                 partition, // partition
			                                 ExecName, // exec
			                                 "/tmp", // wd
			                                 temp_args, // start args
			                                 env, // env
			                                 logpath, // logpath
			                                 "/dev/null", // stdin
			                                 "", // use rm
			                                 true, // null log
			                                 0); // timeout

			if(procs > 1) {

				sprintf(str, "%d", rn2);

				temp_args.clear();
				temp_args.push_back(str);

				//      std::cout << "O>STR() " << str << " " << rn << " " << pippo << " " << RAND_MAX << std::endl;


				d2 = new pmg::ProcessDescription(h.full_name(),
				                                 app_name_2,
				                                 partition,
				                                 ExecName,
				                                 "/tmp",
				                                 temp_args,
				                                 env,
				                                 logpath,
				                                 "/dev/null", // stdin
				                                 "", // use rm
				                                 true,
				                                 0);
			}

			if(procs > 2) {

				sprintf(str, "%d", rn3);

				temp_args.clear();
				temp_args.push_back(str);

				//      std::cout << "O>STR() " << str << " " << rn << " " << pippo << " " << RAND_MAX << std::endl;


				d3 = new pmg::ProcessDescription(h.full_name(),
				                                 app_name_3,
				                                 partition,
				                                 ExecName,
				                                 "/tmp",
				                                 temp_args,
				                                 env,
				                                 logpath,
				                                 "/dev/null", // stdin
				                                 "", // use rm
				                                 true,
				                                 0);

			}

			pthread_mutex_lock(&thread_flag_mutex);

			Tasks = procs;

			ERS_INFO("Starting " << Tasks << " processes now.");

			pthread_mutex_unlock(&thread_flag_mutex);

			try {
				pD1 = d1->start(testcallback);
				if(procs > 1)
					pD2 = d2->start(testcallback);
				if(procs > 2)
					pD3 = d3->start(testcallback);
			}
			catch(pmg::Failed_Start& ex) {
				ers::warning(ex);
				exit(1);
			}

			if(rns > 0) {
				sleep(rns);
			}

			ERS_INFO( "Kill all left over processes");

			try {
				pD1->kill_soft(1);
			}
			catch(pmg::Signal_Not_Allowed &ex) {
				ers::warning(ex);
				exit(1);
			}
			catch(pmg::Exception &ex) {
				ers::warning(ex);
			}

			try {
				if(procs > 1)
					pD2->kill_soft(1);
			}
			catch(pmg::Signal_Not_Allowed &ex) {
				ers::warning(ex);
				exit(1);
			}
			catch(pmg::Exception &ex) {
				ers::warning(ex);
			}

			try {
				if(procs > 2)
					pD3->kill_soft(1);
			}
			catch(pmg::Signal_Not_Allowed &ex) {
				ers::warning(ex);
				exit(1);
			}
			catch(pmg::Exception &ex) {
				ers::warning(ex);
			}
			// }

			pthread_mutex_lock(&thread_flag_mutex);

			ERS_INFO("Finished test calls, running server, and waiting for " << Tasks << " to finish.");

			struct timeval now;
			struct timespec timeout;
			int retcode;

			gettimeofday(&now, NULL);
			timeout.tv_sec = now.tv_sec + 15;
			timeout.tv_nsec = now.tv_usec * 1000;
			retcode = 0;

			while(Tasks > 0) {
				retcode = pthread_cond_timedwait(&thread_flag_cv, &thread_flag_mutex, &timeout);
				if(retcode == ETIMEDOUT) {
					ERS_INFO("Wait on TASK timed out");
					exit(1);
					break;
				}
			}

			pthread_mutex_unlock(&thread_flag_mutex);

			pD1->unlink();
			if(procs > 1)
				pD2->unlink();
			if(procs > 2)
				pD3->unlink();

			delete d1;
			delete d2;
			delete d3;

			ERS_INFO("------------------> END LOOP NUMBER " << i);

		}

	}
	catch(ers::Issue &i) {
		ers::error(i);
	}
	exit(0);
}

