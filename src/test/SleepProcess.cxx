/* ||===========================================================||
   ||	File : pmg_syncSleepProcess.cc                              ||
   ||                                                           ||
   ||	Authors : Pierre-Yves Duval   Laurent Cohen		||
   ||	Date : 24-11-99						||
   ||								||
   ||								||
   ||===========================================================||*/

// Test program
//--------------
// argument  -w is the sleeping delay after synchronization
// argument  -x the exit status
// arguments -W the synchronization delay
//==============================================

#include <iostream>
/*
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
*/
#include <signal.h>

#include <ers/ers.h>

#include "pmg/pmg_initSync.h"
#include "pmg/pmg_syncMacros.h"

#include <boost/program_options.hpp>

namespace po = boost::program_options;

extern "C" void signalHandler(int sig) {
  ERS_INFO("Received signal " << sig << ". Exiting.");
  exit(0);
}

int main(int argc, char **argv)
{
  unsigned int waitDelay, exitValue, syncDelay;
  po::variables_map vm;

  try {
    po::options_description desc("This program is a simple program that sleeps for the time specified as parameter and then just exit with the specified exit status value");
    
    desc.add_options()
      ("waitDelay,w", po::value<unsigned int>(&waitDelay), "time in seconds to sleep before the process exit, default 3 secs")
      ("exitValue,x", po::value<unsigned int>(&exitValue), "exit value the process returns when it finishes")
      ("syncDelay,W", po::value<unsigned int>(&syncDelay), "time in seconds to sleep before the synchronization is made with the pmg_agent, default no synchronization")      
      ("help,h", "Print help message")
      ;
    
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);    
    
    if(vm.count("help")) {
      std::cout << desc << std::endl;
      return EXIT_SUCCESS; 
    }
  }
  catch(std::exception& ex) {
    ERS_INFO("Bad command line options: " << ex.what());
    return EXIT_FAILURE;
  }

  ERS_INFO("My Pid is "<< getpid() );
  unsigned wTime = 3;
  unsigned WTime = 0;

  // Register exit handler for SIGTERM
  signal(SIGINT,  signalHandler);
  signal(SIGTERM, signalHandler);

  if(vm.count("syncDelay")) {
    WTime = syncDelay;
    ERS_INFO("Waiting for process initialization "<<WTime<< " seconds  ... ");
    sleep( WTime );
    std::string pmg_env_var="";
    char* c = getenv( PMG_SYNC_ENV_VAR_NAME );
    if (c != NULL) {
      pmg_env_var = getenv( PMG_SYNC_ENV_VAR_NAME );

      ERS_INFO(" ... Signaling to pmg_agent the end of initialization: create " << pmg_env_var);
    }
    else {
      ERS_INFO("No environment to create sync file set");
    }
    pmg_initSync();
  }

  if(vm.count("waitDelay")) {
    wTime = waitDelay;
  }
  ERS_INFO("sleeping for "<< wTime << " seconds ... ");
  sleep( wTime );

  unsigned xValue = 0;

  if(vm.count("exitValue")) {
    xValue = exitValue;
  }
  ERS_INFO("returning "<< xValue << " as exit status value");

  return ( xValue );
}
