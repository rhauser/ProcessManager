#include <iostream>
#include <vector>
#include <memory>
#include <ers/ers.h>
#include <pwd.h>
#include <sys/types.h>

#include <ipc/partition.h>
#include <ipc/core.h>
#include <ipc/threadpool.h>

#include <daq_tokens/acquire.h>

#include <boost/thread/mutex.hpp>

#include "ProcessManager/defs.h"
#include "ProcessManager/Singleton.h"
#include "ProcessManager/Process.h"
#include "ProcessManager/Utils.h"
#include "ProcessManager/Exceptions.h"
#include <pmgpriv/pmgpriv.hh>

#include <system/Host.h>

using namespace daq;
using namespace pmg;

std::map<std::string, pmgpriv::SERVER_var> objMap; // Map containing references to all the pmgservers
std::string procList;

unsigned int failures = 0;

boost::mutex mutex;

const unsigned int KILL_TIMEOUT = 60;

class AskServer {
    public:
        AskServer(const std::string& server,
                  const unsigned int killTimeout,
                  const std::string& hostName,
                  const std::string& loginName) :
                serverName(server), timeout(killTimeout), hostName(hostName), loginName(loginName)
        {
        }

        void operator()() {
            try {
                objMap[this->serverName]->kill_all(timeout, loginName.c_str(), hostName.c_str());
                ERS_LOG(this->serverName << ": killed all my processes!");
            }
            catch(pmgpriv::kill_all_ex &ex) {
                {
                    boost::mutex::scoped_lock lk(mutex);
                    ++failures;
                    procList += std::string(ex.handle_list);
                }
            }
            catch(CORBA::Exception& ex) {
                std::string msg = "Communication problem while contacting agent " + this->serverName + ": "
                        + std::string(ex._name());
                ers::warning(pmg::Exception(ERS_HERE, msg));
            }
            catch(...) {
                std::string msg = "Unexpected exception while contacting agent " + this->serverName;
                ers::warning(pmg::Exception(ERS_HERE, msg));
            }

        }

    private:
        const std::string serverName;
        const unsigned int timeout;
        const std::string hostName;
        const std::string loginName;
    };


int main(int argc, char** argv) {
    try {
        std::string answer;
        bool rightAnswer = false;

        std::cout
                << "You are going to ask all the ProcessManager servers published in the global IPC to terminate all the processes."
                << std::endl;
        std::cout << "Are you sure? (y/n)" << std::endl;
        std::cin >> answer;

        while(!rightAnswer) {
            if(answer == std::string("y")) {
                rightAnswer = true;
            } else {
                if(answer == std::string("n")) {
                    rightAnswer = true;
                    std::cout << "Exiting..." << std::endl;
                    exit(0);
                } else {
                    rightAnswer = false;
                    std::cout << "Please type \"y\" or \"n\": " << std::endl;
                    std::cin >> answer;
                }
            }
        }

        // Init IPC

        IPCCore::init(argc, argv);

        const std::string hostName(System::LocalHost::full_local_name());
        const std::string loginName(daq::tokens::enabled() ? daq::tokens::acquire(daq::tokens::Mode::Reuse) : daq::pmg::utils::loginName());

        // Init ProcessManager client library

        daq::pmg::Singleton::init();

        /** We are going to talk to multiple servers: use the IPCPipeline **/

        // Get the agent list from IPC
        IPCPartition p;
        try {
            p.getObjects<pmgpriv::SERVER>(objMap);
        }
        catch(daq::ipc::Exception& ex) {
            std::string errStr("Failed getting agent list from IPC: " + ex.message());
            ers::error(pmg::Exception(ERS_HERE, errStr));
            return (EXIT_FAILURE);
        }

        // Create the pipeline
        IPCPipeline pipe(10);

        // Add jobs to the pipeline
        std::map<std::string, pmgpriv::SERVER_var>::const_iterator it;
        for(it = objMap.begin(); it != objMap.end(); ++it) {
            pipe.addJob(AskServer(it->first, KILL_TIMEOUT, hostName, loginName));
        }

        // Wait for jobs
        pipe.waitForCompletion();

        /********************************************************************/

        if(failures > 0) {
            std::vector<std::string> handleVector;
            daq::pmg::utils::tokenize(procList, pmg::ProcessManagerSeparator, handleVector);

            if(handleVector.size() > 0) {
                std::cout << "\nFailed killing some processes. Here is the list:" << std::endl;
                daq::pmg::Singleton * Sg = Singleton::instance();
                for(std::vector<std::string>::const_iterator it = handleVector.begin(); it != handleVector.end();
                        ++it) {
                    try {
                        Handle h(*it);
                        std::string server = h.server();
                        std::string appName = h.applicationName();
                        std::shared_ptr<Process> proc = Sg->get_process(h);
                        if(proc) {
                            try {
                                proc->kill_soft(KILL_TIMEOUT);
                            }
                            catch(pmg::Bad_PMG_Server &ex) {
                                std::cout << server << ": " << appName << "\tbecause of no answer from the server"
                                        << std::endl;
                            }
                            catch(pmg::No_PMG_Server &ex) {
                                std::cout << server << ": " << appName << "\tbecause the server is not present in IPC"
                                        << std::endl;
                            }
                            catch(pmg::Process_Already_Exited &ex) {
                                std::cout << server << ": " << appName << "\tbecause the process already exited"
                                        << std::endl;
                            }
                            catch(pmg::Signal_Not_Allowed &ex) {
                                std::cout << server << ": " << appName << "\tbecause not allowed by the AccessManager"
                                        << std::endl;
                            }
                            catch(pmg::Server_Internal_Error &ex) {
                                std::cout << server << ": " << appName << "\tbecause of an internal server error"
                                        << std::endl;
                            }
                        }
                    }
                    catch(pmg::Exception &ex) {
                        std::cerr << "Error while getting process with handle " << (*it) << ": " << ex.message()
                                << std::endl;
                    }

                }

            }

        }
    }
    catch(ers::Issue &ex) {
        ers::error(ex);
        return (EXIT_FAILURE);
    }
    catch(CORBA::Exception& ex) {
        std::string msg = "Communication error: " + std::string(ex._name());
        ers::warning(pmg::Exception(ERS_HERE, msg));
        return (EXIT_FAILURE);
    }
}

