#include <iostream>
#include <vector>
#include <memory>
#include <ers/ers.h>
#include <pwd.h>
#include <sys/types.h>

#include <ipc/partition.h>
#include <ipc/core.h>
#include <system/Host.h>

#include <daq_tokens/acquire.h>

#include <boost/program_options.hpp>

#include "pmgpriv/pmgpriv.hh"
#include "ProcessManager/defs.h"
#include "ProcessManager/Singleton.h"
#include "ProcessManager/Process.h"
#include "ProcessManager/Utils.h"

namespace po = boost::program_options;
using namespace std;
using namespace daq;
using namespace pmg;

int main(int argc, char** argv) {
    try {
        IPCCore::init(argc, argv);

        string hostName, partitionName;
        unsigned int killTimeout;

        try {
            po::options_description desc("Kills all the process running on a host (if the partition is defined only processes belonging to that partition will be killed)");

            desc.add_options()
                    ("Host,H", po::value<std::string>(&hostName), "Host name")
                    ("Partition,p", po::value<std::string>(&partitionName), "Partition name of the processes to kill")
                    ("Timeout,t", po::value<unsigned int>(&killTimeout)->default_value(60), "Timeout - in seconds - to graceful kill applications")
                    ("help,h", "Print help message");

            po::variables_map vm;
            po::store(po::parse_command_line(argc, argv, desc), vm);
            po::notify(vm);

            if(vm.count("help")) {
                std::cout << desc << std::endl;
                return EXIT_SUCCESS;
            }

            if(!vm.count("Host")) {
                ERS_LOG("ERROR: please specify a host name");
                std::cout << desc << std::endl;
                return EXIT_FAILURE;
            }
        }
        catch(std::exception& ex) {
            ERS_LOG("Bad command line options: " << ex.what());
            return EXIT_FAILURE;
        }

        System::Host h(hostName);
        string fn = h.full_name();

        string loginName = daq::tokens::enabled() ? daq::tokens::acquire(daq::tokens::Mode::Reuse) : daq::pmg::utils::loginName();
        string localHostName = System::LocalHost::full_local_name();

        daq::pmg::Singleton::init();
        std::string pmgServerName = daq::pmg::ProcessManagerServerPrefix + h.full_name();
        pmgpriv::SERVER_var server = daq::pmg::Singleton::instance()->get_server(pmgServerName);

        bool ok = false;
        std::string procList;
        try {
            if(!partitionName.empty()) {
                ERS_LOG("About to kill processes on host " << h.full_name() << " for partition " << partitionName);
                server->kill_partition(partitionName.c_str(), killTimeout, loginName.c_str(), localHostName.c_str());
                ok = true;
            } else {
                ERS_LOG("About to kill all processes on host " << h.full_name());
                server->kill_all(killTimeout, loginName.c_str(), localHostName.c_str());
                ok = true;
            }
        }
        catch(pmgpriv::kill_partition_ex &ex) {
            procList = ex.handle_list;
        }
        catch(pmgpriv::kill_all_ex &ex) {
            procList = ex.handle_list;
        }
        catch(CORBA::Exception &ex) {
            ERS_LOG("Communication error while contacting agent on host " << h.full_name() << ": " << ex._name());
            return (EXIT_FAILURE);
        }

        std::vector<std::string> handleVector;
        daq::pmg::utils::tokenize(procList, pmg::ProcessManagerSeparator, handleVector);

        if(handleVector.size() > 0) {
            std::cout << "Failed killing some processes. Here is the list:" << std::endl;
            daq::pmg::Singleton * Sg = Singleton::instance();
            for(std::vector<std::string>::const_iterator it = handleVector.begin(); it != handleVector.end(); ++it) {
                try {
                    Handle h(*it);
                    std::string server = h.server();
                    std::string appName = h.applicationName();
                    std::shared_ptr<Process> proc = Sg->get_process(h);
                    if(proc) {
                        try {
                            proc->kill_soft(killTimeout);
                        }
                        catch(pmg::Bad_PMG_Server &ex) {
                            std::cout << server << ": " << appName << "\tbecause of no answer from the server"
                                    << std::endl;
                        }
                        catch(pmg::No_PMG_Server &ex) {
                            std::cout << server << ": " << appName << "\tbecause the server is not present in IPC"
                                    << std::endl;
                        }
                        catch(pmg::Process_Already_Exited &ex) {
                            std::cout << server << ": " << appName << "\tbecause the process already exited"
                                    << std::endl;
                        }
                        catch(pmg::Signal_Not_Allowed &ex) {
                            std::cout << server << ": " << appName << "\tbecause not allowed by the AccessManager"
                                    << std::endl;
                        }
                        catch(pmg::Server_Internal_Error &ex) {
                            std::cout << server << ": " << appName << "\tbecause of an internal server error"
                                    << std::endl;
                        }
                    }
                }
                catch(pmg::Exception &ex) {
                    std::cerr << "Error while getting process with hande " << (*it) << ": " << ex.message()
                            << std::endl;
                }
            }
        }

        ERS_LOG("Call succeeded: " << (ok ? "yes" : "no"));
        return (ok ? EXIT_SUCCESS : EXIT_FAILURE);

    }
    catch(ers::Issue &i) {
        ers::error(i);
        return (EXIT_FAILURE);
    }
}
