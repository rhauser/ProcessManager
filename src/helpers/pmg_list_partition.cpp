#include <iostream>
#include <vector>

#include <ipc/core.h>
#include <ipc/threadpool.h>
#include <ers/ers.h>
#include <system/Host.h>

#include <boost/algorithm/minmax.hpp>
#include <boost/algorithm/minmax_element.hpp>
#include <boost/program_options.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>

#include "ProcessManager/Singleton.h"
#include "ProcessManager/Handle.h"
#include "ProcessManager/Utils.h"

namespace po = boost::program_options;
using boost::posix_time::ptime;
using boost::posix_time::from_time_t;
using boost::posix_time::to_simple_string;
using boost::date_time::c_local_adjustor;
using namespace daq;
using namespace pmg;

std::map<std::string, pmgpriv::SERVER_var> objMap; // Map containing references to all the pmgservers
std::map<std::string, pmgpub::ProcessStatusInfo> procMap;
std::string procList;
std::string agentList;

boost::mutex procMutex;
boost::mutex agentMutex;

class AskServer {

	public:

		AskServer(const std::string& server, const std::string& partition, const bool moreInfo) :
			additionalInfo(moreInfo), serverName(server), partitionName(partition)
		{
		}

		void operator()() {
			try {
				ERS_DEBUG(0, "Asking agent " << this->serverName << " about all the running processes in partition " << this->partitionName);

				if(additionalInfo == false) {
				CORBA::String_var procHandles =
				        objMap[this->serverName]->processes(this->partitionName.c_str());
					{
						boost::mutex::scoped_lock lk(procMutex);
						procList.append(std::string(procHandles));
					}
				} else {
					pmgpriv::proc_info_list_var procList = 0;
					objMap[this->serverName]->processes_info(this->partitionName.c_str(), procList);
					{
						boost::mutex::scoped_lock lk(procMutex);
						const CORBA::ULong size = procList->length();
						for(CORBA::ULong i = 0; i < size; ++i) {
							procMap[std::string(procList[i].app_handle)] = procList[i].proc_info;
						}
					}
				}
			}
			catch(pmgpriv::processes_ex &ex) {
				std::string msg = "Error while contacting " + this->serverName + ": "
				        + std::string(ex.error_string);
				ers::warning(pmg::Exception(ERS_HERE, msg));
				{
					boost::mutex::scoped_lock lk(agentMutex);
					agentList += this->serverName;
					agentList += pmg::ProcessManagerSeparator;
				}
			}
			catch(CORBA::Exception& ex) {
				std::string msg = "Error while contacting " + this->serverName + ": " + ex._name();
				ers::warning(pmg::Exception(ERS_HERE, msg));
				{
					boost::mutex::scoped_lock lk(agentMutex);
					agentList += this->serverName;
					agentList += pmg::ProcessManagerSeparator;
				}
			}
			catch(...) {
				std::string msg = "Unexpected exception while contacting " + this->serverName;
				ers::warning(pmg::Exception(ERS_HERE, msg));
				{
					boost::mutex::scoped_lock lk(agentMutex);
					agentList += this->serverName;
					agentList += pmg::ProcessManagerSeparator;
				}
			}
		}

	private:

		const bool additionalInfo;
		const std::string serverName;
		const std::string partitionName;

};

void printProcessTable(const std::vector<std::string>& header, const std::vector<std::vector<std::string> >& content) {
	const unsigned int columns(header.size());

	// Vector containing the column sizes
	std::vector<std::vector<unsigned int> > columnSizes(columns);

	// Add the header label sizes
	for(unsigned int i = 0; i < columns; ++i) {
		columnSizes[i].push_back(header[i].size());
	}

	// Add the column content sizes
	const std::vector<std::vector<std::string> >::const_iterator contentStartIt = content.begin();
	const std::vector<std::vector<std::string> >::const_iterator contentStopIt = content.end();
	std::vector<std::vector<std::string> >::const_iterator it;
	for(it = contentStartIt; it != contentStopIt; ++it) {
		for(unsigned int i = 0; i < columns; ++i) {
			columnSizes[i].push_back((*it)[i].size());
		}
	}

	// Compute column sizes
	std::vector<unsigned int> maxValues(columns);
	for(unsigned int i = 0; i < columns; ++i) {
		maxValues[i] = *(boost::minmax_element(columnSizes[i].begin(), columnSizes[i].end())).second;
	}

	// Print headers now
	for(unsigned int i = 0; i < columns; ++i) {
		std::cout << std::setw(maxValues[i] + 3) << std::left << std::setfill(' ') << header[i];
	}
	std::cout << std::endl;

	// Print content now
	for(it = contentStartIt; it != contentStopIt; ++it) {
		for(unsigned int i = 0; i < columns; ++i) {
			std::cout << std::setw(maxValues[i] + 3) << std::left << std::setfill(' ') << (*it)[i];
		}
		std::cout << std::endl;
	}
}

int main(int argc, char** argv) {

	try {

		IPCCore::init(argc, argv);

		std::string partName, hostName;
		bool printTime = false;

		try {
			po::options_description
			        desc("Lists all the process running within a partition (if the host is defined only processes running on that host will be listed)");

			desc.add_options()

			("Partition,p", po::value<std::string>(&partName), "Partition name")
			("Host,H", po::value<std::string>(&hostName), "Host name")
			("startTime,v", po::bool_switch(&printTime), "Whether to print the processes start time (default is FALSE)")
			("help,h", "Print help message");

			po::variables_map vm;
			po::store(po::parse_command_line(argc, argv, desc), vm);
			po::notify(vm);

			if(vm.count("help")) {
				std::cout << desc << std::endl;
				return EXIT_SUCCESS;
			}

			if(!vm.count("Partition")) {
				ERS_LOG("ERROR: please specify the partition names");
				std::cout << desc << std::endl;
				return EXIT_FAILURE;
			}
		}
		catch(std::exception& ex) {
			ERS_LOG("Bad command line options: " << ex.what());
			return EXIT_FAILURE;
		}

		Singleton::init();
		Singleton* pSing = Singleton::instance();

		if(!hostName.empty()) {
			try {
				System::Host h(hostName);
				std::string fullHostName = h.full_name();
				std::cout << "Asking host " << fullHostName
				        << " about processes running in partition " << partName << std::endl;
				if(printTime == false) {
					pSing->askRunningProcesses(fullHostName, partName, procList);
				} else {
					pSing->askRunningProcesses(fullHostName, partName, procMap);
				}
			}
			catch(pmg::No_PMG_Server &ex) {
				ers::error(ex);
				return (EXIT_FAILURE);
			}
			catch(pmg::Bad_PMG_Server &ex) {
				ers::error(ex);
				return (EXIT_FAILURE);
			}
			catch(pmg::Failed_Listing_Server &ex) {
				ers::error(ex);
				return (EXIT_FAILURE);
			}
		} else {
			std::cout << "Asking all the agents about processes running in partition " << partName
			        << std::endl;

			/** We are going to talk to multiple servers: use the IPCPipeline**/

			// Get the agent list from IPC
			IPCPartition p;
			try {
				p.getObjects<pmgpriv::SERVER> (objMap);
			}
			catch(daq::ipc::Exception& ex) {
				std::string errStr("Failed getting agent list from IPC: " + ex.message());
				ers::error(pmg::Exception(ERS_HERE, errStr));
				return (EXIT_FAILURE);
			}

			// Create the pipeline
			IPCPipeline pipe(10);

			// Add jobs to the pipeline
			std::map<std::string, pmgpriv::SERVER_var>::const_iterator it;
			for(it = objMap.begin(); it != objMap.end(); ++it) {
				pipe.addJob(AskServer(it->first, partName, printTime));
			}

			// Wait for jobs
			pipe.waitForCompletion();
		}

		// Print results
		std::vector<std::string> header;
		std::vector<std::vector<std::string> > contents;

		if(printTime == false) {
			header.push_back("APPLICATION");
			header.push_back("PARTITION");
			header.push_back("HANDLE");

			std::vector<std::string> handleVector;
			daq::pmg::utils::tokenize(procList, pmg::ProcessManagerSeparator, handleVector);
			contents.resize(handleVector.size());

			unsigned int headerSize = header.size();
			for(unsigned int i = 0; i < contents.size(); ++i) {
				const std::string& handle(handleVector[i]);
				try {
					Handle h(handle);
					contents[i].push_back(h.applicationName());
					contents[i].push_back(h.partitionName());
					contents[i].push_back(handle);
				}
				catch(pmg::Invalid_Handle& ex) {
					contents[i].resize(headerSize);
					ers::error(ex);
				}
			}
		} else {
			header.push_back("APPLICATION");
			header.push_back("PARTITION");
			header.push_back("START TIME");
			header.push_back("HANDLE");

			contents.resize(procMap.size());

			const std::map<std::string, pmgpub::ProcessStatusInfo>::const_iterator begin(procMap.begin());
			const std::map<std::string, pmgpub::ProcessStatusInfo>::const_iterator end(procMap.end());
			std::map<std::string, pmgpub::ProcessStatusInfo>::const_iterator it;

			unsigned int headerSize(header.size());
			unsigned int counter(0);
			for(it = begin; it != end; ++it, ++counter) {
				const std::string& handle(it->first);
				try {
					Handle h(handle);
					contents[counter].push_back(h.applicationName());
					contents[counter].push_back(h.partitionName());

					const pmgpub::ProcessStatusInfo& procInfo(it->second);
					const ptime t(c_local_adjustor<ptime>::utc_to_local(from_time_t(procInfo.start_time)));
					contents[counter].push_back(to_simple_string(t));

					contents[counter].push_back(handle);
				}
				catch(pmg::Invalid_Handle& ex) {
					contents[counter].resize(headerSize);
					ers::error(ex);
				}
			}
		}

		if(contents.size() == 0) {
			std::cout << "No process found" << std::endl;
		} else {
			printProcessTable(header, contents);
		}

		if(agentList.size() > 0) {
			std::cerr << "\nSome errors occurred while contacting some agents. Here is the list: "
			        << std::endl;
			std::vector<std::string> agentVector;
			daq::pmg::utils::tokenize(agentList, pmg::ProcessManagerSeparator, agentVector);
			for(std::vector<std::string>::const_iterator it = agentVector.begin(); it
			        != agentVector.end(); ++it) {
				std::cerr << *it << std::endl;
			}
		}

		return (EXIT_SUCCESS);

	}
	catch(ers::Issue &ex) {
		ers::error(ex);
		return (EXIT_FAILURE);
	}

}
