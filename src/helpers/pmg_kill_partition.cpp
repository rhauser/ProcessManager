#include <iostream>
#include <vector>
#include <memory>
#include <ers/ers.h>

#include <ipc/partition.h>
#include <ipc/core.h>
#include <ipc/threadpool.h>

#include <daq_tokens/acquire.h>

#include <boost/program_options.hpp>
#include <boost/thread/mutex.hpp>

#include "pmgpriv/pmgpriv.hh"
#include "ProcessManager/defs.h"
#include "ProcessManager/Singleton.h"
#include "ProcessManager/Process.h"
#include "ProcessManager/Utils.h"
#include "ProcessManager/Exceptions.h"

namespace po = boost::program_options;
using namespace std;
using namespace daq;
using namespace pmg;

std::map<std::string, pmgpriv::SERVER_var> objMap; // Map containing references to all the pmgservers
std::string procList;
std::string agentList;

boost::mutex procMutex;
boost::mutex agentMutex;

class AskServer {
    public:
        AskServer(const std::string& server,
                  const std::string& partition,
                  unsigned int killTimeout,
                  const std::string& hostName,
                  const std::string& loginName) :
                serverName(server), partitionName(partition), timeout(killTimeout),
                hostName(hostName), loginName(loginName)
        {
        }

        void operator()() {
            try {
                ERS_DEBUG(0, "Kill processes of partition " << this->partitionName << " on host " << this->serverName);
                objMap[this->serverName]->kill_partition(this->partitionName.c_str(),
                                                         timeout,
                                                         loginName.c_str(),
                                                         hostName.c_str());
            }
            catch(pmgpriv::kill_partition_ex &ex) {
                std::string msg = "Error while contacting " + this->serverName + ": " + std::string(ex.error_string);
                ers::warning(pmg::Exception(ERS_HERE, msg));
                {
                    boost::mutex::scoped_lock lk(procMutex);
                    procList += std::string(ex.handle_list);
                }
            }
            catch(CORBA::Exception& ex) {
                std::string msg = "Error while contacting " + this->serverName + ": " + ex._name();
                ers::warning(pmg::Exception(ERS_HERE, msg));
                {
                    boost::mutex::scoped_lock lk(agentMutex);
                    agentList += this->serverName;
                    agentList += pmg::ProcessManagerSeparator;
                }
            }
            catch(...) {
                std::string msg = "Unexpected exception while contacting " + this->serverName;
                ers::warning(pmg::Exception(ERS_HERE, msg));
                {
                    boost::mutex::scoped_lock lk(agentMutex);
                    agentList += this->serverName;
                    agentList += pmg::ProcessManagerSeparator;
                }
            }
        }

    private:
        const std::string serverName;
        const std::string partitionName;
        const unsigned int timeout;
        const std::string hostName;
        const std::string loginName;
};

int main(int argc, char** argv) {
    try {
        IPCCore::init(argc, argv);

        const std::string hostName(System::LocalHost::full_local_name());
        const std::string loginName(daq::tokens::enabled() ? daq::tokens::acquire(daq::tokens::Mode::Reuse) : daq::pmg::utils::loginName());

        string partName;
        unsigned int killTimeout;

        try {
            po::options_description desc("Looks up in the IPC server which PMG servers include processes in the passed partition and asks them to kill the partition");

            desc.add_options()
                    ("Partition,p", po::value<std::string>(&partName), "Name of the partition")
                    ("Timeout,t", po::value<unsigned int>(&killTimeout)->default_value(60), "Timeout - in seconds - to graceful kill applications")
                    ("help,h", "Print help message");

            po::variables_map vm;
            po::store(po::parse_command_line(argc, argv, desc), vm);
            po::notify(vm);

            if(vm.count("help")) {
                std::cout << desc << std::endl;
                return EXIT_SUCCESS;
            }

            if(!vm.count("Partition")) {
                ERS_LOG("ERROR: please specify the partition names");
                std::cout << desc << std::endl;
                return EXIT_FAILURE;
            }
        }
        catch(std::exception& ex) {
            ERS_LOG("Bad command line options: " << ex.what());
            return EXIT_FAILURE;
        }

        ERS_LOG("About to kill partition " << partName);

        Singleton::init();
        daq::pmg::Singleton * Sg = Singleton::instance();

        /** We are going to talk to multiple servers: use the IPCPipeline **/

        // Get the agent list from IPC
        IPCPartition p;
        try {
            p.getObjects<pmgpriv::SERVER>(objMap);
        }
        catch(daq::ipc::Exception& ex) {
            std::string errStr("Failed getting agent list from IPC: " + ex.message());
            ers::error(pmg::Exception(ERS_HERE, errStr));
            return (EXIT_FAILURE);
        }

        // Create the pipeline
        IPCPipeline pipe(10);

        // Add jobs to the pipeline
        std::map<std::string, pmgpriv::SERVER_var>::const_iterator it;
        for(it = objMap.begin(); it != objMap.end(); ++it) {
            pipe.addJob(AskServer(it->first, partName, killTimeout, hostName, loginName));
        }

        // Wait for jobs
        pipe.waitForCompletion();

        /********************************************************************/

        // Look at kill partition result
        std::vector<std::string> handleVector;
        daq::pmg::utils::tokenize(procList, pmg::ProcessManagerSeparator, handleVector);

        std::vector<std::string> agentVector;
        daq::pmg::utils::tokenize(agentList, pmg::ProcessManagerSeparator, agentVector);

        if(handleVector.size() > 0) {
            std::cout << "\nFailed killing some processes. Here is the list:" << std::endl;
            for(std::vector<std::string>::const_iterator it = handleVector.begin(); it != handleVector.end(); ++it) {
                Handle h(*it);
                std::string server = h.server();
                std::string appName = h.applicationName();
                try {
                    std::shared_ptr<Process> proc = Sg->get_process(h);
                    if(proc) {
                        try {
                            proc->kill_soft(killTimeout);
                        }
                        catch(pmg::Bad_PMG_Server &ex) {
                            std::cout << server << ": " << appName << "\tbecause of no answer from the server"
                                    << std::endl;
                        }
                        catch(pmg::No_PMG_Server &ex) {
                            std::cout << server << ": " << appName << "\tbecause the server is not present in IPC"
                                    << std::endl;
                        }
                        catch(pmg::Process_Already_Exited &ex) {
                            std::cout << server << ": " << appName << "\tbecause the process already exited"
                                    << std::endl;
                        }
                        catch(pmg::Signal_Not_Allowed &ex) {
                            std::cout << server << ": " << appName << "\tbecause not allowed by the AccessManager"
                                    << std::endl;
                        }
                        catch(pmg::Server_Internal_Error &ex) {
                            std::cout << server << ": " << appName << "\tbecause of an internal server error"
                                    << std::endl;
                        }
                    }
                }
                catch(pmg::Exception &ex) {
                    std::cerr << "Error while getting process " << appName << ": " << ex.message() << std::endl;
                }
            }
        }

        if(agentVector.size() > 0) {
            std::cout << "\nFailed contacting some agents. Here is the list: " << std::endl;
            for(std::vector<std::string>::const_iterator it = agentVector.begin(); it != agentVector.end(); ++it) {
                std::cout << *it << std::endl;
            }
        }

    }
    catch(ers::Issue &i) {
        ers::error(i);
    }
}
