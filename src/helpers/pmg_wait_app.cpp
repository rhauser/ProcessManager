#include <unistd.h>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <ers/ers.h>
#include <tmgr/tmresult.h>
#include <ipc/core.h>

#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/xtime.hpp>
#include <boost/program_options.hpp>

#include "ProcessManager/client_simple.h"
#include "ProcessManager/Exceptions.h"
#include "ProcessManager/defs.h"
#include "ProcessManager/Utils.h"

namespace po = boost::program_options;
using namespace daq;
using namespace pmg;

boost::condition condVar;
boost::mutex condVarMutex;

const int EXIT_APP_EXITED(0);
const int EXIT_TIMEOUT_ELAPSED(1);
const int EXIT_APP_NOT_FOUND(2);
const int EXIT_GENERIC_ERROR(3);

void testcallback(std::shared_ptr<Process> process, void* test) {
	if(test != 0) {
	} // It avoids the unused param warning

	boost::mutex::scoped_lock scoped_lock(condVarMutex);

	unsigned int state_int = (unsigned int) process->status().state;
	ERS_LOG("Received callback for handle " << process->handle().toString()
			<< " with state " << p_state_strings[state_int]);

	condVar.notify_all();
}

void writeInfoToFile(const Process* const pProc, const std::string& fileName) {
	const PMGProcessStatusInfo& procStatus = pProc->status();
	const PMGProcessState procState = procStatus.state;
	const pmgpub::ProcessInfo& procInfo = procStatus.info;

	std::ofstream fileStream(fileName.c_str());
	if(fileStream.good() == true) {
		fileStream.exceptions(std::ios_base::badbit | std::ios_base::failbit);
		try {
			fileStream << "STATE\t" << procState << "\n" << "SIGNAL\t" << procInfo.signal_value
			        << "\n" << "EXIT\t" << procInfo.exit_value << "\n";
		}
		catch(std::ios_base::failure& ex) {
			ERS_LOG("ERROR! Exception \"" << ex.what() << "\" while writing to file \"" << fileName << "\n");
		}
	} else {
		ERS_LOG("ERROR! Cannot open file \"" << fileName << "\"");
	}
}

int main(int argc, char** argv) {

	bool waitForEver(false);
	unsigned int killTimeout;
	std::string appName, partName, hostName, fileName;

	// Manage command line options
	try {
		std::string
		        descStr("\nWait for a process to exit.\n\nReturn codes:\n0 - The application exited\n1 - Timeout elapsed while waiting\n");
		descStr.append("2 - Application not found\n3 - Some error occurred (i.e., while contacting the pmgserver)\n");
		descStr.append("\nCommand line options");
		po::options_description desc(descStr);

		desc.add_options()("AppName,n",
		                   po::value<std::string>(&appName),
		                   "The name of the application")
		("Partition,p", po::value<std::string>(&partName), "The name of the partition")
		("Timeout,t", po::value<unsigned int>(&killTimeout), "Time to wait in seconds (if none wait for ever)")
		("Host,H", po::value<std::string>(&hostName), "Host the application should be running on")
		("OutFile,f", po::value<std::string>(&fileName)->default_value(""), "Name of the file where process information (SIGNAL and EXIT code) should be written in")
		("help,h", "Print help message");

		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);

		if(vm.count("help")) {
			std::cout << desc << std::endl;
			return (EXIT_GENERIC_ERROR);
		}

		if((!vm.count("AppName")) || (!vm.count("Partition"))) {
			ERS_LOG("ERROR: please specify both application and partition names");
			std::cout << desc << std::endl;
			return (EXIT_GENERIC_ERROR);
		}

		if(!vm.count("Timeout")) {
			waitForEver = true;
		}

	}
	catch(std::exception& ex) {
		ERS_LOG("Bad command line options: " << ex.what());
		return (EXIT_GENERIC_ERROR);
	}

	int returnCode(EXIT_APP_EXITED);

	try {
		// Init IPC
		IPCCore::init(argc, argv);

		// Init the Singleton object
		Singleton::init();
		Singleton* pS = Singleton::instance();

		// Get the process handle asking the ProcessManager server(s)
		std::unique_ptr<Handle> pH;

		if(!hostName.empty()) {
			System::Host h(hostName);
			pH = pS->lookup(h.full_name(), appName, partName);
		} else {
			try {
				pH = pS->lookup(appName, partName);
			}
			catch(pmg::Failed_Multiple_Lookup& ex) {
				const std::string agentList(ex.get_agent_list());
				std::vector<std::string> agentVect;
				daq::pmg::utils::tokenize(agentList, ProcessManagerSeparator, agentVect);
				for(std::vector<std::string>::const_iterator it = agentVect.begin(); it
				        != agentVect.end(); ++it) {
					ERS_LOG("Error while contacting agent " << *it);
				}
				const std::string handle(ex.get_handle());
				if(!handle.empty()) {
					pH.reset(new Handle(handle));
				} else {
					throw;
				}
			}
		}

		// Do your job
		if(pH != nullptr) {
			{
				// Lock the mutex on the cond var before registering the callback.
				boost::mutex::scoped_lock scoped_lock(condVarMutex);

				// Register the callback
				pS->get_process(*pH)->link(testcallback);

				// Wait for the process to exit
				if(!waitForEver) {
					while(!pS->get_process(*pH)->exited()) {
						boost::xtime xt;
						boost::xtime_get(&xt, boost::TIME_UTC_);

						xt.sec += killTimeout;
						bool waitCode = condVar.timed_wait(scoped_lock, xt);
						if(waitCode == false) {
							returnCode = EXIT_TIMEOUT_ELAPSED;
							break;
						}
					}
				} else {
					while(!pS->get_process(*pH)->exited()) {
						condVar.wait(scoped_lock);
					}
				}
			} // Close scope

			// Write info to file
			if(fileName.empty() == false) {
				writeInfoToFile(pS->get_process(*pH).get(), fileName);
			}

			// Unlink the Process
			pS->get_process(*pH)->unlink();
		} else {
			ERS_LOG("The process has not been found");
			returnCode = EXIT_APP_NOT_FOUND;
		}
	}
	catch(ers::Issue& ex) {
		ers::error(ex);
		returnCode = EXIT_GENERIC_ERROR;
	}

	return(returnCode);
}
