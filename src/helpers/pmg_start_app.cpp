#include <unistd.h>
#include <errno.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <memory>

#include <ers/ers.h>
#include <tmgr/tmresult.h>
#include <ipc/core.h>

#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/program_options.hpp>

#include "ProcessManager/client_simple.h"
#include "ProcessManager/Exceptions.h"
#include "ProcessManager/defs.h"
#include "ProcessManager/Utils.h"

namespace po = boost::program_options;
using namespace daq;
using namespace pmg;

PMGProcessState procStatus = PMGProcessState_NOTAV;
std::string failureString;

boost::condition condVar;
boost::mutex condVarMutex;

void testcallback(std::shared_ptr<Process> process, void* test) {
	if(test != 0) {
	} // It avoids the unused param warning

	boost::mutex::scoped_lock scoped_lock(condVarMutex);

	unsigned int state_int = (unsigned int) process->status().state;
	ERS_LOG("Received callback for handle " << process->handle().toString()
			<< " with state " << p_state_strings[state_int] );

	procStatus = process->status().state;
	failureString = process->status().failure_str_hr;

	condVar.notify_all();
}

void printProcessLogs(const Process* const process) {
	try {
		pmgpriv::File_var outLog = 0;
		process->outFile(outLog);
		ERS_LOG("---> Process OUT log:\n" + std::string((const char*) outLog->get_buffer()));
	}
	catch(pmg::Cannot_Get_File& ex) {
	    if(ex.get_code() == ENOENT) {
	        ERS_LOG("The process did not generate any output (the out file does not exist)");
	    } else {
	        ERS_LOG("Failed getting the process OUT log");
	        ers::error(ex);
	    }
	}
	catch(pmg::Exception& ex) {
		ERS_LOG("Failed getting the process OUT log");
		ers::error(ex);
	}

	try {
		pmgpriv::File_var errLog = 0;
		process->errFile(errLog);
		ERS_LOG("---> Process ERR log:\n" + std::string((const char*) errLog->get_buffer()));
	}
    catch(pmg::Cannot_Get_File& ex) {
        if(ex.get_code() == ENOENT) {
            ERS_LOG("The process did not generate any error (the err file does not exist)");
        } else {
            ERS_LOG("Failed getting the process OUT log");
            ers::error(ex);
        }
    }
	catch(pmg::Exception& ex) {
		ERS_LOG("Failed getting the process ERR log");
		ers::error(ex);
	}
}

void writeInfoToFile(const Process* const pProc, const std::string& fileName) {
	const PMGProcessStatusInfo& procStatus = pProc->status();
	const PMGProcessState procState = procStatus.state;
	const pmgpub::ProcessInfo& procInfo = procStatus.info;

	std::ofstream fileStream(fileName.c_str());
	if(fileStream.good() == true) {
		fileStream.exceptions(std::ios_base::badbit | std::ios_base::failbit);
		try {
			fileStream << "STATE\t" << procState << "\n" << "SIGNAL\t" << procInfo.signal_value
			        << "\n" << "EXIT\t" << procInfo.exit_value << "\n";
		}
		catch(std::ios_base::failure& ex) {
			ERS_LOG("ERROR! Exception \"" << ex.what() << "\" while writing to file \"" << fileName << "\n");
		}
	} else {
		ERS_LOG("ERROR! Cannot open file \"" << fileName << "\"");
	}
}

int main(int argc, char** argv) {
	// Manage command line options
	std::string hostName, binName, appName, partName, workDir, libPath, procLog, procStdin, swObj, fileName;
	bool nullLog = false;
	bool waitForExit = false;
	unsigned long timeout, waitTimeout, autokillTimeout;
	std::vector<std::string> procArgs, procEnv;

	try {
		po::options_description
		        desc(std::string("Asks the ProcessManager server running on a certain host to start a defined application.\n") +
		        	 std::string("- If the LD_LIBRARY_PATH is not defined neither in the \"LibPath\" or in the \"ProcEnv\" then the current LD_LIBRARY_PATH value is used.\n") +
		        	 std::string("  The library path eventually defined in \"ProcEnv\" is appended to the \"LibPath\"") +
		        	 std::string("- The PATH variable is extracted from \"ProcEnv\"; if not defined then the current PATH value is used."));

		desc.add_options()
		("Host,H", po::value<std::string>(&hostName), "Host the application should be started on")
		("Bin,b", po::value<std::string>(&binName), "Name of the binary to run")
		("App,n", po::value<std::string>(&appName), "Name to associate with the process (can be different than the binary name)")
		("Partition,p", po::value<std::string>(&partName), "Name of the partition")
		("WorkDir,w", po::value<std::string>(&workDir), "Directory where the process will be executed")
		("LibPath,l", po::value<std::string>(&libPath), "The process LD_LIBRARY_PATH (use the standard ':' path separator)")
		("ProcEnv,e", po::value<std::vector<std::string> >(&procEnv), "Additional environment variables to pass to the process (i.e.: \"-e Value1=Key1 -e Value2=Key2\")")
		("LogDir,d", po::value<std::string>(&procLog)->default_value("/tmp"), "Directory to write process logs (default is /tmp)")
		("Stdin,i", po::value<std::string>(&procStdin)->default_value("/dev/null"), "The redirection of stdin (default is /dev/null)")
		("SwObj,s", po::value<std::string>(&swObj), "The swobject describing the process: empty string (default) -> DO not use RM; Not empty string -> USE RM")
		("NullLogs,x", po::bool_switch(&nullLog), "Whether to not write out the log at all (default is FALSE)")
		("Timeout,t", po::value<unsigned long>(&timeout)->default_value(0), "Timeout for the pmg sync mechanism (default is \"0\": do not use the sync)")
        ("Auto-kill timeout,k", po::value<unsigned long>(&autokillTimeout)->default_value(0), "Timeout to be elapsed for the process to be automatically killed (default is \"0\": the process is not killed)")
		("Args,a", po::value<std::vector<std::string> >(&procArgs), "Arguments to pass to the process (i.e.: to run 'xterm' with the 'title' option \"-a -title -a THE_TITLE_VALUE\")")
		("Wait,W", po::bool_switch(&waitForExit), "Whether to wait for the process to exit (default is FALSE). If TRUE then the process err and out logs will be printed on the terminal")
		("WaitTimeout,T", po::value<unsigned long>(&waitTimeout)->default_value(0), "Time to wait for the process to start or exit (default is \"0\": wait forever)")
		("OutFile,f", po::value<std::string>(&fileName)->default_value(""), "Name of the file where process information (SIGNAL and EXIT code) should be written in (only valid if used with the \"-W\" option)")
		("help,h", "Print help message");

		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);

		if(vm.count("help")) {
			std::cout << desc << std::endl;
			return daq::tmgr::TmUntested;
		}

		if((!vm.count("Partition")) || (!vm.count("Host")) || (!vm.count("Bin"))
		        || (!vm.count("App")) || (!vm.count("WorkDir"))) {
			ERS_LOG("ERROR: please specify the host, partition, binary, application and working dir names");
			std::cout << desc << std::endl;
			return daq::tmgr::TmUntested;
		}
	}
	catch(std::exception& ex) {
		ERS_LOG("Bad command line options: " << ex.what());
		return daq::tmgr::TmUntested;
	}

	// Return code of this application
	unsigned int returnCode = daq::tmgr::TmUntested;

	try {

		// Init IPC
		IPCCore::init(argc, argv);

		// Singleton init
		Singleton::init();

		// Get command line options
		System::Host h(hostName);
		std::string path;

		// Build the env map from command line values
		std::map<std::string, std::string> envMap;
		for(std::vector<std::string>::const_iterator it = procEnv.begin(); it != procEnv.end(); ++it) {
		    auto separatorPos = it->find_first_of('=');
			if(separatorPos != std::string::npos) {
			    const std::string& key = it->substr(0, separatorPos);
			    const std::string& value = it->substr(separatorPos + 1);
				if(key == std::string("LD_LIBRARY_PATH")) { // If LD_LIBRARY_PATH var then add to 'libPath'...
					if(!libPath.empty()) {
						libPath.append(":");
					}
					libPath.append(value);
				} else if(key == std::string("PATH")) {
					if(!path.empty()) {
						path.append(":");
					}
					path.append(value);
				} else {
					envMap[key] = value;
				}
			}
		}

		// Add default PATH if not defined
		if(path.empty()) {
			ERS_DEBUG(3, "Adding current PATH to the process environment.");
			const char* defPath = getenv("PATH");
			if(defPath != NULL) {
				path.append(defPath);
			}
		}
		envMap["PATH"] = path;


		// Add default LD_LIBRARY_PATH if not defined
		if(libPath.empty()) {
			ERS_DEBUG(3, "Adding current LD_LIBRARY_PATH to the process environment.");
			const char* ldPath = getenv("LD_LIBRARY_PATH");
			if(ldPath != NULL) {
				libPath.append(ldPath);
			}
		}
		envMap["LD_LIBRARY_PATH"] = libPath;

		// Build the binary list
		std::string binaryList;
		std::vector<std::string> binVect;
		daq::pmg::utils::tokenize(path, ":", binVect);
		for(std::vector<std::string>::iterator it = binVect.begin(); it != binVect.end(); ++it) {
			it->append("/");
			it->append(binName);
			binaryList.append(*it);
			binaryList.append(":");
		}

		ERS_DEBUG(0, "Host name: " << h.full_name());
		ERS_DEBUG(0, "Application Name: " << appName);
		ERS_DEBUG(0, "Binary Name: " << binName);
		ERS_DEBUG(0, "Binary List: " << binaryList);
		ERS_DEBUG(0, "Working Directory: " << workDir);
		ERS_DEBUG(0, "Library Path: " << libPath);
		ERS_DEBUG(0, "Log Directory: " << procLog);
		ERS_DEBUG(0, "Stdin: " << procStdin);
		ERS_DEBUG(0, "Software Object: " << swObj);
		ERS_DEBUG(0, "PMG Sync Timeout: " << timeout);
        ERS_DEBUG(0, "Auto-kill Timeout: " << autokillTimeout);
        ERS_DEBUG(0, "Null Log: " << nullLog);

		for(std::vector<std::string>::const_iterator it = procArgs.begin(); it != procArgs.end(); it++) {
			ERS_DEBUG(0, "Process Arguments: " << *it);
		}

		for(std::map<std::string, std::string>::const_iterator it = envMap.begin(); it
		        != envMap.end(); it++) {
			ERS_DEBUG(0, "Process Environment: " << it->first << "=" << it->second);
		}

		// Create the ProcessDescription
		ProcessDescription* pDescr = new pmg::ProcessDescription(h.full_name(),
		                                                         appName,
		                                                         partName,
		                                                         binaryList,
		                                                         workDir,
		                                                         procArgs,
		                                                         envMap,
		                                                         procLog,
		                                                         procStdin,
		                                                         swObj,
		                                                         nullLog,
		                                                         timeout,
		                                                         autokillTimeout);

		std::shared_ptr<Process> pProc;
		{
			// Lock the mutex on the cond var before asking the ProcessManager to start the process
			boost::mutex::scoped_lock scoped_lock(condVarMutex);

			// Start the process
			pProc = pDescr->start(testcallback);

			ERS_LOG("Request to start the process " << appName << " sent to the agent running on " << h.full_name()
					<< ". Waiting for RUNNING callback");

			// Wait for callbacks
			while(true) {
				bool waitCode = true;
				if(waitTimeout == 0) {
					condVar.wait(scoped_lock);
				} else {
					boost::xtime xt;
					boost::xtime_get(&xt, boost::TIME_UTC_);

					xt.sec += waitTimeout;
					waitCode = condVar.timed_wait(scoped_lock, xt);
				}
				if(waitCode == false) {
					ERS_LOG("Timeout elapsed while waiting for process callbacks. Printing process OUT and ERR logs (if any).");
					printProcessLogs(pProc.get());
					returnCode = daq::tmgr::TmUntested;
					break;
				}
				if(waitForExit == true) {
					// Check if the process exited
					if(pProc->exited() == true) {
						returnCode = daq::tmgr::TmPass;
						// Print process err and out logs
						printProcessLogs(pProc.get());
						break;
					}
				} else {
					// Check the process status and set the return code
					if(procStatus == PMGProcessState_RUNNING) {
						ERS_LOG("Process successfully started!");
						returnCode = daq::tmgr::TmPass;
					} else {
						ERS_LOG("Failed starting the process: " << failureString);
						returnCode = daq::tmgr::TmFail;
					}
					break;
				}
			}

			// Here the scoped lock on the mutex is un-locked.
			// Do not call unlink() with the lock on the mutex:
			// the callback function uses the same mutex and it
			// could cause a dead-lock.
		}

		// Write process info to file
		if((waitForExit == true) && (fileName.empty() == false)) {
			writeInfoToFile(pProc.get(), fileName);
		}

		// Unlink the Process
		pProc->unlink();

		// Delete the ProcessDescripotion
		delete pDescr;
	}
	catch(ers::Issue &ex) {
		ERS_LOG("Failed starting the process");
		ers::error(ex);
		returnCode = daq::tmgr::TmFail;
	}

	// return
	return (returnCode);
}
