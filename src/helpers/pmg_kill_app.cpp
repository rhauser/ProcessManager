#include <unistd.h>

#include <iostream>
#include <string>
#include <vector>

#include <ers/ers.h>
#include <tmgr/tmresult.h>
#include <ipc/core.h>

#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/xtime.hpp>
#include <boost/program_options.hpp>

#include "ProcessManager/client_simple.h"
#include "ProcessManager/Exceptions.h"
#include "ProcessManager/defs.h"
#include "ProcessManager/Utils.h"

namespace po = boost::program_options;
using namespace daq;
using namespace pmg;

PMGProcessState procStatus = PMGProcessState_NOTAV;
std::string failureString;

boost::condition condVar;
boost::mutex condVarMutex;

void testcallback(std::shared_ptr<Process> process, void* test) {
    if(test != 0) {
    } // It avoids the unused param warning

    boost::mutex::scoped_lock scoped_lock(condVarMutex);

    unsigned int state_int = (unsigned int) process->status().state;
    ERS_LOG("Received callback for handle " << process->handle().toString() << " with state " << p_state_strings[state_int]);

    procStatus = process->status().state;
    failureString = process->status().failure_str_hr;

    condVar.notify_all();
}

int main(int argc, char** argv) {
    unsigned int returnCode = daq::tmgr::TmUntested;

    unsigned int killTimeout;
    std::string appName, partName, hostName;

    // Manage command line options

    try {
        po::options_description desc(std::string("Asks the ProcessManager server to terminate a defined application\n.")
                + std::string("This application will wait for the process to exit for an amount of time equal to \"Timeout\" plus additional 5 seconds"));

        desc.add_options()
                ("AppName,n", po::value<std::string>(&appName), "The name of the application to kill")
                ("Partition,p", po::value<std::string>(&partName), "Name of the partition")
                ("Timeout,t", po::value<unsigned int>(&killTimeout)->default_value(60), "Timeout - in seconds - to pass to the kill_soft method (default is 60 s)")
                ("Host,H", po::value<std::string>(&hostName), "Host the application should be running on")
                ("help,h", "Print help message");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if(vm.count("help")) {
            std::cout << desc << std::endl;
            return daq::tmgr::TmUntested;
        }

        if((!vm.count("AppName")) || (!vm.count("Partition"))) {
            ERS_LOG("ERROR: please specify both application and partition names");
            std::cout << desc << std::endl;
            return daq::tmgr::TmUntested;
        }
    }
    catch(std::exception& ex) {
        ERS_LOG("Bad command line options: " << ex.what());
        return daq::tmgr::TmUntested;
    }

    try {
        bool lookupError = false;

        // Init IPC

        IPCCore::init(argc, argv);

        // Init the Singleton object

        Singleton::init();
        Singleton * pS = Singleton::instance();

        // Get the process handle asking the ProcessManager server(s)

        std::unique_ptr<Handle> pH;

        if(!hostName.empty()) {
            System::Host h(hostName);
            pH = pS->lookup(h.full_name(), appName, partName);
        } else {
            try {
                pH = pS->lookup(appName, partName);
            }
            catch(pmg::Failed_Multiple_Lookup &ex) {
                lookupError = true;
                ers::error(ex);
                std::string agentList = ex.get_agent_list();
                std::vector<std::string> agentVect;
                daq::pmg::utils::tokenize(agentList, ProcessManagerSeparator, agentVect);
                for(std::vector<std::string>::const_iterator it = agentVect.begin(); it != agentVect.end(); it++) {
                    ERS_LOG("Error while contacting agent " << *it);
                }
                std::string handle = ex.get_handle();
                if(!handle.empty()) {
                    pH.reset(new Handle(handle));
                    lookupError = false;
                }
            }
        }

        // Get the Process pointer

        std::shared_ptr<Process> pP;

        if(pH != nullptr) {
            pP = pS->get_process(*pH);
        }

        // Check if the process has been found

        if(pH == nullptr || pP == nullptr) {
            ERS_LOG("The process has not been found");
            returnCode = daq::tmgr::TmFail;
        } else {
            unsigned int state_int = (unsigned int) pP->status().state;
            ERS_LOG("The process is running on host " << pP->handle().server());
            ERS_LOG("Process status is " << p_state_strings[state_int]);

            {
                // Lock the mutex on the cond var before registering the callback.

                boost::mutex::scoped_lock scoped_lock(condVarMutex);

                // Register the callback

                pP->link(testcallback);

                // Ask the server to kill the process

                pP->kill_soft(killTimeout);

                ERS_LOG("Request to kill the process sent to the server");

                // Wait for a callback after sending the kill request
                // Return if the callback is not received after a timeout

                int totalTimeout = killTimeout + 5;

                boost::xtime xt;
                boost::xtime_get(&xt, boost::TIME_UTC_);

                xt.sec += totalTimeout;

                ERS_LOG("Waiting " << totalTimeout << " seconds to receive a callback from the process");

                while(procStatus == PMGProcessState_NOTAV) {
                    bool retcode = condVar.timed_wait(scoped_lock, xt);
                    if(retcode == false) {
                        ERS_LOG("Not received any callback after killing the process. Please, try again to be sure the process has really been killed.");
                        returnCode = daq::tmgr::TmUnresolved;
                        break;
                    } else {
                        returnCode = daq::tmgr::TmPass;
                    }
                }
            }

            // Unlink the process

            pP->unlink();

        }

        if((lookupError == false) && (returnCode == daq::tmgr::TmPass)) {
            ERS_LOG("The process has been terminated!");
        } else {
            ERS_LOG("Some errors occurred. Please, look at the application messages");
        }
    }
    catch(ers::Issue &ex) {
        ERS_LOG("Failed killing the process.");
        ers::error(ex);
        returnCode = daq::tmgr::TmFail;
    }

    // return

    return (returnCode);
}
