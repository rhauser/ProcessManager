#include <memory>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <ers/ers.h>
#include <ipc/core.h>
#include <system/Host.h>
#include <daq_tokens/acquire.h>

#include <boost/program_options.hpp>
#include <boost/algorithm/minmax.hpp>
#include <boost/algorithm/minmax_element.hpp>

#include "ProcessManager/defs.h"
#include "ProcessManager/Exceptions.h"
#include "ProcessManager/Singleton.h"
#include "ProcessManager/Utils.h"
#include "ProcessManager/Handle.h"

#include <vector>
#include <string>
#include <iostream>
#include <memory>

#include <pmgpriv/pmgpriv.hh>

namespace po = boost::program_options;

const unsigned int KILL_TIMEOUT = 60;

void listProcesses(const std::string& procList) {
	std::vector<std::string> handleVector;
	daq::pmg::utils::tokenize(procList, daq::pmg::ProcessManagerSeparator, handleVector);

	// Get information about strings for nice printing
	std::vector<int> appNameSize;
	std::vector<int> handleSize;
	std::vector<int> partNameSize;

	for(std::vector<std::string>::const_iterator it = handleVector.begin(); it != handleVector.end(); ++it) {
		try {
			daq::pmg::Handle h(*it);
			std::string server = h.server();
			std::string appName = h.applicationName();
			std::string partName = h.partitionName();
			appNameSize.push_back(appName.size());
			partNameSize.push_back(partName.size());
			handleSize.push_back(it->size());
		}
		catch(daq::pmg::Invalid_Handle& ex) {
			ers::error(ex);
		}
	}

	std::string appLabel("APPLICATION");
	std::string partLabel("PARTITION");
	std::string handleLabel("HANDLE");

	appNameSize.push_back(appLabel.size());
	partNameSize.push_back(partLabel.size());
	handleSize.push_back(handleLabel.size());

	typedef std::vector<int>::const_iterator iterator;
	std::pair<iterator, iterator> appNameMinMax = boost::minmax_element(appNameSize.begin(), appNameSize.end());
	std::pair<iterator, iterator> partNameMinMax = boost::minmax_element(partNameSize.begin(), partNameSize.end());
	std::pair<iterator, iterator> handleMinMax = boost::minmax_element(handleSize.begin(), handleSize.end());

	int maxAppName = *(appNameMinMax.second);
	int maxPartName = *(partNameMinMax.second);
	int maxHandle = *(handleMinMax.second);

	std::cout << std::setw(maxAppName + 3) << std::left << std::setfill(' ') << appLabel << std::setw(maxPartName + 3)
	        << std::left << std::setfill(' ') << partLabel << std::setw(maxHandle + 3) << std::left << std::setfill(' ')
	        << handleLabel << std::endl;

	// Print information about found applications

	for(std::vector<std::string>::const_iterator it = handleVector.begin(); it != handleVector.end(); ++it) {
		try {
			daq::pmg::Handle h(*it);
			std::string server = h.server();
			std::string appName = h.applicationName();
			std::string partName = h.partitionName();
			std::cout << std::setw(maxAppName + 3) << std::left << std::setfill(' ') << appName
			        << std::setw(maxPartName + 3) << std::left << std::setfill(' ') << partName
			        << std::setw(maxHandle + 3) << std::left << std::setfill(' ') << *it << std::endl;
		}
		catch(daq::pmg::Invalid_Handle& ex) {
			ers::error(ex);
		}
	}
}

int main(int argc, char** argv) {
	// Command line parsing
	std::string targetMachine;
	try {
		po::options_description desc("This program is used to cleanly stop an agent");

		desc.add_options()
				("Host,H", po::value<std::string>(&targetMachine), "Target machine on which the agent is running: default is local machine")
				("help,h", "Print help message");

		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);

		if(vm.count("help")) {
			std::cout << desc << std::endl;
			return EXIT_SUCCESS;
		}
	}
	catch(std::exception& ex) {
		ERS_LOG("Bad command line options: " << ex.what());
		return EXIT_FAILURE;
	}

	try {
		// Init IPC and the Singleton
		IPCCore::init(argc, argv);
		daq::pmg::Singleton::init();

		// Get login name and local host name
		const std::string loginName = daq::tokens::enabled() ? daq::tokens::acquire(daq::tokens::Mode::Reuse) : daq::pmg::utils::loginName();
		const std::string localHostName = System::LocalHost::full_local_name();

		// Set the selected host
		std::unique_ptr<System::Host> h;
		if(!targetMachine.empty()) {
			h.reset(new System::Host(targetMachine));
		} else {
			h.reset(new System::Host(System::LocalHost::full_local_name()));
		}

		// Get the agent reference
		const std::string serverName(daq::pmg::ProcessManagerServerPrefix + h->full_name());
		ERS_LOG("Looking for agent " << serverName);
		pmgpriv::SERVER_var serverRef = daq::pmg::Singleton::instance()->get_server(serverName);

		// Check if there is some running process controlled by this server
		bool stopServer(false);
		const std::string procList(serverRef->all_processes());
		if(procList.empty()) {
			// There is no running process, ask the server to shutdown
			ERS_LOG("The agent " << serverName << " is not controlling any process");
			stopServer = true;
		} else {
			// There are running processes, list all of them and ask the user
			ERS_LOG("The agent " << serverName << " has some running processes, here is the list:");
			listProcesses(procList);
			std::string answer;
			bool rightAnswer = false;
			std::cout << "If you decide to kill the agent then all the listed processes will be terminated."
			        << std::endl;
			std::cout << "Are you sure (y/n)?" << std::endl;
			std::cin >> answer;
			while(!rightAnswer) {
				if(answer == std::string("y")) {
					rightAnswer = true;
					stopServer = true;
				} else {
					if(answer == std::string("n")) {
						rightAnswer = true;
						stopServer = false;
					} else {
						rightAnswer = false;
						std::cout << "Please type \"y\" or \"n\": " << std::endl;
						std::cin >> answer;
					}
				}
			}
		}

		try {
			if(stopServer) {
			    if(procList.empty() == false) {
			        ERS_LOG("Asking the agent " << serverName << " to terminate all its processes");
			        serverRef->kill_all(KILL_TIMEOUT, loginName.c_str(), localHostName.c_str());
			        ERS_LOG("Asking the agent " << serverName << " to stop");
			    }

				try {
					// First try the "normal" way: if the pmgserver does not belong to the same user
					// then the CORBA interceptors will not allow to shutdown it
					serverRef->shutdown();
				}
				catch(CORBA::NO_PERMISSION& ex) {
					// Not allowed, then ask the pmgserver using its own "exit" implementation
					ERS_LOG("Stopping the server is not allowed by CORBA interceptors, now trying a different way");
					serverRef->exit();
				}

				ERS_LOG("The agent has been asked to shutdown");
			} else {
				ERS_LOG("The agent " << serverName << " will NOT be asked to stop");
			}
		}
		catch(pmgpriv::kill_all_ex& ex) {
			ERS_LOG("Some error occurred while killing all the agent processes: " + std::string(ex.error_string));
			ERS_LOG("The following processes could not be killed:");
			listProcesses(std::string(ex.handle_list));
			ERS_LOG("The agent will NOT be stopped, try again");

			return EXIT_FAILURE;
		}
		catch(pmgpriv::exit_ex& ex) {
			ERS_LOG("The agent could not be stopped: " + std::string(ex.error_string));

			return EXIT_FAILURE;
		}
	}
	catch(ers::Issue& ex) {
		ERS_LOG("An error occurred, agent not killed: " << ex.message());
		return EXIT_FAILURE;
	}
	catch(CORBA::Exception& ex) {
		ERS_LOG("CORBA exception contacting the agent, agent not killed: " << ex._name());
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

