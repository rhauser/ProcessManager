#include <iostream>
#include <map>
#include <condition_variable>
#include <mutex>
#include <limits>
#include <chrono>
#include <memory>

#include <boost/program_options.hpp>

#include <ers/ers.h>
#include <ipc/partition.h>
#include <ipc/core.h>
#include <tmgr/tmresult.h>
#include <system/Host.h>
#include <system/User.h>

#include "ProcessManager/client_simple.h"
#include "ProcessManager/Exceptions.h"
#include "ProcessManager/defs.h"
#include "ProcessManager/Process.h"

#include "pmgpriv/pmgpriv.hh"

namespace po = boost::program_options;
using namespace daq;

// global objects
unsigned int returnStatus = daq::tmgr::TmUndef;

std::condition_variable cv;
std::mutex mutex;

void testcallback(std::shared_ptr<pmg::Process> process, void* test) {
	if(test != 0) {
	} // avoids the unused param warning

	unsigned int state_int = (unsigned int) process->status().state;

	{
        std::lock_guard<std::mutex> lk(mutex);

        if(state_int == pmgpub::RUNNING) {
            ERS_DEBUG(1, "Test process running.");
            return;
        } else if(state_int == pmgpub::FAILED) {
            std::string msg = "WARNING: Starting Process failed: " + std::string(process->status().failure_str_hr);
            ERS_LOG(msg);
            returnStatus = daq::tmgr::TmFail;
        } else if((state_int == pmgpub::EXITED) || (state_int == pmgpub::SIGNALED)) {
            ERS_DEBUG(0, "Test succeeded.");
            returnStatus = daq::tmgr::TmPass;
        }
	}

	cv.notify_all();
}

int main(int argc, char** argv) {
	try {
		IPCCore::init(argc, argv);

		std::string hostName;
		unsigned int timeout = std::numeric_limits<unsigned int>::max();
		bool verbose = false;

		try {
			po::options_description desc("Checks if pmgserver is running on the passed host");

			desc.add_options()
			("host,H", po::value<std::string>(&hostName), "Host name")
			("Timeout,t", po::value<unsigned int>(&timeout),
			              "Timeout to wait for a call-back from the server (default is waiting for ever)")
			("verbose,v", po::bool_switch(&verbose), "Turn on 'debug'-mode")
			("help,h", "Print help message");

			po::variables_map vm;
			po::store(po::parse_command_line(argc, argv, desc), vm);
			po::notify(vm);

			if(vm.count("help")) {
				std::cout << desc << std::endl;
				return daq::tmgr::TmUntested;
			}

			if(!vm.count("host")) {
				ERS_LOG("ERROR: please specify the host name!");
				std::cout << desc << std::endl;
				return daq::tmgr::TmUntested;
			}
		}
		catch(std::exception& ex) {
			ERS_LOG("Bad command line options: " << ex.what());
			return daq::tmgr::TmUntested;
		}

		System::Host h(hostName);
		ERS_DEBUG(0, "Testing pmgserver on host " << h.full_name());

		IPCPartition part;
		pmg::Singleton::init();
		std::string pmgServerName = daq::pmg::ProcessManagerServerPrefix + h.full_name();
		pmgpriv::SERVER_var server = daq::pmg::Singleton::instance()->get_server(pmgServerName);

		if(server->alive()) {
			ERS_DEBUG(1,"Server is alive");

			System::User user;

			std::string libpath(getenv("LD_LIBRARY_PATH"));
			libpath += ":/lib;/usr/lib;/usr/local/lib";
			std::map<std::string, std::string> env;
			env["LD_LIBRARY_PATH"] = libpath;

			std::vector<std::string> args;
			args.push_back("0");

			std::unique_ptr<pmg::ProcessDescription> d1(new pmg::ProcessDescription(h.full_name(), // hostname
			                                                                        "sleep-app", // appname
			                                                                        part.name(), // partition
			                                                                        "/bin/sleep", // exec
			                                                                        "/tmp", // wd
			                                                                        args, // start args
			                                                                        env, // env
			                                                                        "/tmp", // logpath
			                                                                        "/dev/null", // stdin
			                                                                        "", // use rm
			                                                                        true, // null log
			                                                                        0)); // timeout


			d1->start(testcallback);

			std::unique_lock<std::mutex> lk(mutex);
			const bool timeoutExpired = !cv.wait_for(lk, std::chrono::seconds(timeout), []() { return returnStatus != daq::tmgr::TmUndef; });
			if(timeoutExpired == true) {
			    ERS_LOG("Timeout expired waiting for the server's call-back");
			    returnStatus = daq::tmgr::TmUnresolved;
			}

			return returnStatus;
		} else {
			ERS_LOG("Server is not responding");
			return daq::tmgr::TmFail;
		}
	}
	catch(ers::Issue &i) {
		ERS_LOG(i);
		return daq::tmgr::TmFail;
	}
	catch(CORBA::Exception &ex) {
		return daq::tmgr::TmFail;
	}

}
