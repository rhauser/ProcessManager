package pmgClient;


/**
 * Thrown to indicate that the String representation of a Handle given as argument is not in the expected format
 *
 */
@SuppressWarnings("serial")
public class InvalidHandleStringException extends PmgClientException {
	
	InvalidHandleStringException(final String message, final Exception cause) {
		super(message, cause);
	}

	InvalidHandleStringException(final String message) {
		super(message);
	}
}
