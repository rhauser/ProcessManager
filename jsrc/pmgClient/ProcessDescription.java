package pmgClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import pmgpriv.ProcessRequestInfo;
import pmgpriv.SERVER;
import pmgpriv.start_ex;
import pmgpub.EnvironmentPair;
import pmgpub.ProcessInfo;
import pmgpub.ProcessStartInfo;
import pmgpub.ProcessStatusInfo;
import pmgpub.ResourceInfo;
import pmgpub.StreamInfo;
import pmgpub.UserInfo;
import pmgpub.ProcessState;


/**
 * Class with the all the information regarding the process to be started by the pmg server.
 * <p>
 * Users of the pmg client library must user this class to generate a valid description for the process they want to start.
 * This class makes use of the Builder Design Pattern in order to construct a description. @see {@link ProcessDescription.ProcessDescriptionBuilder}
 *
 */
public final class ProcessDescription {

	private final String appName;			// Name of the process. Selected by the client
	private final String partition;			// Name of the partition the process will belong to.
	private final String execList;			// A String of possible fully qualified executables separated by ':', e.g. "/bin/smthg:/usr/bin/smthg"
	private final String wd;				// The working directory from which the process should be started.
	private final List<String> startArgs;	// The command line arguments used to start the process.
	private final Map<String, String> env;	// The environment variables which are needed to start process.
	private final String logPath;			// The log file directory path.
	private final String inputDev;			// The redirection of stdin.
	private final String rmSwobject;		// The swobject describing the process.
	private final int initTimeout;			// The initial timeout after which to give up
	private final int autoKillTimeout;		// Time the ProcessManager will wait before killing the process 
	private final boolean nullLogs;			// If TRUE logs will not be written.
	private final String localhost;			// The client local host.
	private final String host;				// The host the process will be started on. Must be the FULL name!
	private final PmgClient pmgClient;		// Reference to the pmg client itself (PmgClient class)
	

	/**
	 * Entry point class for creating a process description. @see {@link ProcessDescriptionBuilder}
	 * <p>
	 * see package-info.java for an example
	 *
	 */
	public final static class ProcessDescriptionBuilder {
		
		//Mandatory arguments. Must be provided by the user.
		private final PmgClient pmgClient;
		private final String host;
		private final String partition;
		private final String appName;
		private final String execList;
		private final Map<String, String> env;
		
		//Arguments provided by the implementation.
		private final String localhost;
		
		// Optional arguments. Default values will be set if they are omitted.
		private String wd;				// default value: "/tmp"
		private List<String> startArgs;	// default value: an empty ArrayList<String>
		private String logPath;			// default value: "/tmp"
		private String inputDev;		// default value: "/dev/null"
		private String rmSwobject;		// default value: ""
		private int initTimeout;		// default value: 0
		private int autoKillTimeout;	// default value: 0
		private boolean nullLogs;		// default value: false

		/**
		 * Builder's constructor which includes all the mandatory arguments for creating a new process description.
		 * <p>
		 * Usage: ProcessDescription desc = new ProcessDescription.ProcessDescriptionBuilder(....)
		 * <p>
		 * @see package-info.java for an example
		 * 
		 * @param pmgClient a reference to the instance of PmgClient.
		 * @param host the host the process will be started on. Must be the FULL name! for example "pc-tbed-pub-05.cern.ch".
		 * @param partition the name of the partition the process will belong to.
		 * @param appName the name that will be given to the process. Selected by the client
		 * @param execList a String of possible fully qualified executables separated by ':', e.g. "/bin/smthg:/usr/bin/smthg"
		 * @param env a Map<String, String> with the the environment variables which are needed to start process.
		 *		Empty expected in case of no env vars
		 */
		public ProcessDescriptionBuilder(final PmgClient pmgClient, final String host, final String partition, 
				final String appName, final String execList, final Map<String, String> env) {

			this.pmgClient = pmgClient;
			this.host = host;
			this.partition = partition;
			this.appName = appName;
			this.execList = execList;
			this.env = env;
			
			this.localhost = pmgClient.getLocalhost();
			
			this.wd	= "/tmp";
			this.startArgs = null;
			this.logPath = "/tmp";
			this.inputDev = "/dev/null";
			this.rmSwobject = "";
			this.initTimeout = 0;
			this.autoKillTimeout = 0;
			this.nullLogs = false;
		}


		/**
		 * Method to be called AFTER the ProcessDescriptionBuilder constructor (and maybe other methods too).
		 * This is the method that actually returns the ProcessDescription instance corresponding .
		 * <p>
		 * <pre>
		 * e.g. ProcessDescription d = new ProcessDescription.ProcessDescriptionBuilder( client, "pc-tbed-pub-05.cern.ch",
					"lyk2",  "MyAppName_52_", "/afs/cern.ch/user/l/lpapaevg/public/lykTest/lyk2.sh", env)
				.initTimeout(0) // optional
				.build();
		 * </pre>
		 * @return a ProcessDescription instance that can be used to start the process
		 */
		public ProcessDescription build() {
			if( this.startArgs == null ) {	//if not set by the user
				this.startArgs = new ArrayList<String>();			// TODO addcheck for arguments etc..
			}
			return new ProcessDescription(this.appName, this.partition, this.execList, this.wd, this.startArgs, this.env, this.logPath, this.inputDev,
					this.rmSwobject, this.initTimeout, this.autoKillTimeout, this.nullLogs, this.localhost, this.host, this.pmgClient);
		}
		
		/**
		 * Adds to the builder a working directory from which the process should be started.
		 * 
		 * @param workdir the path of that directory
		 * @return the ProcessDescriptionBuilder itself
		 */
		public ProcessDescriptionBuilder wd(final String workdir) {
			this.wd = workdir;
			return this;
		}
		
		/**
		 * Adds to the builder the command line arguments for the process (1 arg per listnode).
		 * 
		 * @param startArguments a List with the arguments that will be passed to the process
		 * @return the ProcessDescriptionBuilder itself
		 */
		public ProcessDescriptionBuilder startArgs(final List<String> startArguments) {
			this.startArgs = startArguments;
			return this;
		}
		
		/**
		 * Adds to the builder the command line arguments for the process.
		 * 
		 * @param startArguments a String with the arguments that will be passed to the process. Space separated argumnents
		 * @return the ProcessDescriptionBuilder itself
		 */
		public ProcessDescriptionBuilder startArgs(final String startArguments) {
			this.startArgs = new ArrayList<>();
			for( String arg : startArguments.split(" ") ) {
				this.startArgs.add(arg);
			}
			return this;
		}
		
		/**
		 * Adds to the builder a log file directory path.
		 * 
		 * @param lp the path where the log files will be saved
		 * @return the ProcessDescriptionBuilder itself
		 */
		public ProcessDescriptionBuilder logPath(final String lp) {
			this.logPath = lp;
			return this;
		}
		
		/**
		 * Adds to the builder the redirection for the standard input.
		 * 
		 * @param inputd the stream to be used
		 * @return the ProcessDescriptionBuilder itself
		 */
		public ProcessDescriptionBuilder inputDev(final String inputd) {
			this.inputDev = inputd;
			return this;
		}
		
		/**
		 * Adds to the builder the swobject describing the process.
		 * 
		 * @param rmSwObject the swobject
		 * @return the ProcessDescriptionBuilder itself
		 */
		public ProcessDescriptionBuilder rmSwobject(final String rmSwObject) {
			this.rmSwobject = rmSwObject;
			return this;
		}
		
		/**
		 * Adds to the builder an initial timeout for the process
		 * 
		 * @param initT the number of seconds for the timeout
		 * @return the ProcessDescriptionBuilder itself
		 */
		public ProcessDescriptionBuilder initTimeout(final int initT) {
			this.initTimeout = initT;
			return this;
		}
		
		/**
		 * Adds to the builder an autoKillTimeout, after which the process will be killed by the server.
		 * 
		 * @param killTimeout number of seconds before the process is killed
		 * @return the ProcessDescriptionBuilder itself
		 */
		public ProcessDescriptionBuilder autoKillTimeout(final int killTimeout) {
			this.autoKillTimeout = killTimeout;
			return this;
		}
		
		/**
		 * Sets a flag about whether to keep logs or not. If TRUE logs will not be written.
		 * 
		 * @param nullLog	boolean flag for having 'null' (true) logs or not (false)
		 * @return the ProcessDescriptionBuilder itself
		 */
		public ProcessDescriptionBuilder nullLogs(final boolean nullLog) {
			this.nullLogs = nullLog;
			return this;
		}
	}  // ProcessDescriptionBuilder
	

	/**
	 * One must use ProcessDescriptionBuilder too create a process description and call build() on that object.
	 * The build call on a ProcessDescriptionBuilder will use this private constructor
	 * 
	 * @param appName
	 * @param partition
	 * @param execList
	 * @param wd
	 * @param startArgs
	 * @param env
	 * @param logPath
	 * @param inputDev
	 * @param rmSwobject
	 * @param initTimeout
	 * @param nullLogs
	 * @param localhost
	 * @param host
	 * @param globalPartition
	 * @param pmgClient
	 */
	private ProcessDescription(
			final String appName, 
			final String partition, 
			final String execList, 
			final String wd, 
			final List<String> startArgs,
			final Map<String, String> env, 
			final String logPath, 
			final String inputDev, 
			final String rmSwobject, 
			final int initTimeout,
			final int autoKillTimeout,
			final boolean nullLogs, 
			final String localhost, 
			final String host, 
			final PmgClient pmgClient)
	{
		this.appName = appName;
		this.partition = partition;
		this.execList = execList;
		this.wd = wd;
		this.startArgs = startArgs;
		this.env = env;
		this.logPath = logPath;
		this.inputDev = inputDev;
		this.rmSwobject = rmSwobject;
		this.initTimeout = initTimeout;
		this.autoKillTimeout = autoKillTimeout;
		this.nullLogs = nullLogs;

		this.localhost = localhost;

		this.host = host;

		this.pmgClient = pmgClient;
	}

	
	/**
	 * Method used to start a process without linking with a callback.
	 * This is a shortcut of @see {@link ProcessDescription#start(Callback)} with null as an parameter
	 * 
	 * @return The Process object representing the process that was started if no exception happened
	 * @throws NoSuchPmgServerException @see {@link ProcessDescription#start(Callback)}
	 * @throws ServerException @see {@link ProcessDescription#start(Callback)}
	 * @throws CorbaException @see {@link ProcessDescription#start(Callback)}
	 * @throws BadUserException Cannot get user Identity
	 */
	public Process start() throws NoSuchPmgServerException, ServerException, CorbaException, BadUserException {
		return this.start(null);
	}
	
	
	/**
	 * Method used to start a process and link with a specific callback.
	 * 
	 * @param callback an object of a class that implements the Callback interface. @see {@link Callback}
	 * @return the Process object representing the process that was started if no exception happened
	 * @throws NoSuchPmgServerException if the host of the ProcessDescription was not found.
	 * @throws ServerException if there was some CORBA exception in the request_start and really_start method calls on the server
	 * @throws CorbaException  if there was some IDL exception in the request_start and really_start method calls on the server
	 * @throws BadUserException Cannot get user identity
	 */
	public Process start(final Callback callback) throws NoSuchPmgServerException, ServerException, CorbaException, BadUserException {
		Process process = null;

		// prepare the arguments' array
		String[] args = new String[this.startArgs.size()];
		args = this.startArgs.toArray(args);

		// prepare the environmental vars' array
		EnvironmentPair[] envs = new EnvironmentPair[this.env.size()];
		int i = 0;
		for (Map.Entry<String, String> entry : this.env.entrySet()) {
			envs[i] = new EnvironmentPair(entry.getKey(), entry.getValue());
			i++;
		}
		
		StreamInfo streamInfo = new StreamInfo(this.logPath, "", "", this.inputDev, 256+128+32+16+4, this.nullLogs);
		// TODO perms in c++ they are S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH

		ProcessRequestInfo procRequestInfo = new ProcessRequestInfo(
				this.pmgClient.getCorbaRef(),
				this.localhost,
				this.host,
				this.appName,
				this.partition,
				this.execList,
				this.wd,
				this.rmSwobject,
				streamInfo, args, envs,
				this.initTimeout,
				this.autoKillTimeout,
				new UserInfo(UserIdentityFactory.getUserIdentity().getAuthToken()));		
		
		ers.Logger.debug(2, "Ask server : " +  this.host + " to start app: " + this.appName + " on host: " + this.host);
		SERVER server;
		try {
			server = this.pmgClient.pmgServerFromName( this.host );
		} catch (NoSuchPmgServerException e) {
			throw e;
		}
		
		
		String handleStr;
		Handle handle;
		try { 
			handleStr = server.request_start(procRequestInfo);		// throws start_ex & Corba Exceptions
			ers.Logger.debug(3, "Received process handle from server. Handle: " + handleStr);
			handle = new Handle(handleStr);							// throws InvalidHandleStringException	
		} catch (start_ex e) {
			throw new ServerException("Exception from IDL method  request_start failed", e);
		} catch (InvalidHandleStringException e) {	// not expected, since handleStr is given from the server
			ers.Logger.fatal(e);
			throw new IllegalStateException("Handle returned by request_start was invalid.", e);
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during Process start (server.request_start)", e);
		}
		
		ProcessStatusInfo procStatusInfo = new ProcessStatusInfo();
		procStatusInfo.state = ProcessState.REQUESTED;
		procStatusInfo.start_time = 0;
		procStatusInfo.stop_time = 0;
		procStatusInfo.failure_str = "requested";
		procStatusInfo.failure_str_hr = "None";
		procStatusInfo.start_info = new ProcessStartInfo();
		procStatusInfo.start_info.client_host = procRequestInfo.client_host;
		procStatusInfo.start_info.host = procRequestInfo.host;
		procStatusInfo.start_info.app_name = procRequestInfo.app_name;
		procStatusInfo.start_info.partition = procRequestInfo.partition;
		procStatusInfo.start_info.executable = "None";
		procStatusInfo.start_info.wd = procRequestInfo.wd;
		procStatusInfo.start_info.rm_token = -999;
		procStatusInfo.start_info.streams = new StreamInfo();
		procStatusInfo.start_info.streams.log_path = procRequestInfo.streams.log_path;
		procStatusInfo.start_info.streams.out_path = procRequestInfo.streams.out_path;
		procStatusInfo.start_info.streams.err_path = procRequestInfo.streams.err_path;
		procStatusInfo.start_info.streams.in_path = procRequestInfo.streams.in_path;
		procStatusInfo.start_info.streams.perms = procRequestInfo.streams.perms;
		procStatusInfo.start_info.streams.out_is_null = procRequestInfo.streams.out_is_null;
		procStatusInfo.start_info.start_args = procRequestInfo.start_args;
		procStatusInfo.start_info.envs = procRequestInfo.envs;
		procStatusInfo.start_info.init_time_out = procRequestInfo.init_time_out;
		procStatusInfo.start_info.user = new UserInfo();
		procStatusInfo.start_info.user.name = procRequestInfo.user.name;
		procStatusInfo.resource_usage = new ResourceInfo();
		procStatusInfo.resource_usage.total_user_time = -999;
		procStatusInfo.resource_usage.total_system_time = -999;
		procStatusInfo.resource_usage.memory_usage = -999;
		procStatusInfo.info = new ProcessInfo();
		procStatusInfo.info.process_id = -99999;
		procStatusInfo.info.signal_value = -999;
		procStatusInfo.info.exit_value = -999;			
		
		process = this.pmgClient.requestProcess(handle, procStatusInfo);
		
		if(callback != null ) {
			process.link(callback);
		}		
		
		ers.Logger.debug(3, "Starting process with Handle: " + handleStr);
		try {
			server.really_start(handleStr); 
		} catch (start_ex e) {
			throw new ServerException("Exception from IDL method really_start failed", e);
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during Process start (server.really_start)", e);
		}
		ers.Logger.debug(3, "Successfully started process with Handle: " + handleStr);

		return process;
	}

	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(1024);
		builder.append("ProcessDescription [\nappName=");
		builder.append(this.appName);
		builder.append(",\npartition=");
		builder.append(this.partition);
		builder.append(",\nexecList=");
		builder.append(this.execList);
		builder.append(",\nwd=");
		builder.append(this.wd);
		builder.append(",\nstartArgs=");
		builder.append(this.startArgs);
		builder.append(",\nenv=");
		builder.append(this.env);
		builder.append(",\nlogPath=");
		builder.append(this.logPath);
		builder.append(",\ninputDev=");
		builder.append(this.inputDev);
		builder.append(",\nrmSwobject=");
		builder.append(this.rmSwobject);
		builder.append(",\ninitTimeout=");
		builder.append(this.initTimeout);
		builder.append(",\nautoKillTimeout=");
		builder.append(this.autoKillTimeout);
		builder.append(",\nnullLogs=");
		builder.append(this.nullLogs);
		builder.append(",\nlocalhost=");
		builder.append(this.localhost);
		builder.append("],\nhost=");
		builder.append(this.host);
		builder.append(",\npmgClient=");
		builder.append(this.pmgClient);
		builder.append("\n]");
		return builder.toString();
	}

	
	
	/**
	 * Returns the application name of the process that was set via the pmg client
	 * 
	 * @return the application name
	 */
	public String getAppName() {		
		return this.appName;
	}

	
	/**
	 * Returns the hostname (full name) of the machine where the process will be started on
	 * 
	 * @return the hostname of the machine
	 */
	public String getHost() {
		return this.host;
	}


	/**
	 * 
	 * @return the name of the partition in which the process belongs to
	 */
	public String getPartition() {
		return this.partition;
	}


	/**
	 * 
	 * @return a String of possible fully qualified executables separated by ':', e.g. "/bin/smthg:/usr/bin/smthg"
	 */
	public String getExecList() {
		return this.execList;
	}


	/**
	 * 
	 * @return the working directory from which the process should be started.
	 */
	public String getWd() {
		return this.wd;
	}


	/**
	 * 
	 * @return The command line arguments used to start the process.
	 */
	public List<String> getStartArgs() {
		return this.startArgs;
	}


	/**
	 * 
	 * @return The environment variables which are needed to start process.
	 */
	public Map<String, String> getEnv() {
		return this.env;
	}

	/**
	 * 
	 * @return The log file directory path.
	 */
	public String getLogPath() {
		return this.logPath;
	}


	/**
	 * 
	 * @return The redirection of stdin.
	 */
	public String getInputDev() {
		return this.inputDev;
	}


	/**
	 * 
	 * @return The swobject describing the process.
	 */
	public String getRmSwobject() {
		return this.rmSwobject;
	}


	/**
	 * 
	 * @return The initial timeout after which to give up
	 */
	public int getInitTimeout() {
		return this.initTimeout;
	}


	/**
	 * 
	 * @return Time the ProcessManager will wait before killing the process 
	 */
	public int getAutoKillTimeout() {
		return this.autoKillTimeout;
	}


	/**
	 * 
	 * @return true if no logs are written, false otherwise
	 */
	public boolean isNullLogs() {
		return this.nullLogs;
	}


	/**
	 * 
	 * @return the client localhost
	 */
	public String getLocalhost() {
		return this.localhost;
	}	
}
