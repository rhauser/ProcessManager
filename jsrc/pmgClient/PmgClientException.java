package pmgClient;

import ers.Issue;



/**
 * Used as a base class for all the Exceptions thrown by the library.
 * <p>
 * User code can safely assume that all the exceptions thrown by the pmg client will inherit from this class.
 * However, it is recommended to catch each specific pmg client exception separately<br>
 * <p>
 * Library maintenance notes: 
 * Every exception thrown by the pmg client must inherit from this exception. <br>
 * Not to be used directly in the library code. Use other exception that extend the PmgClientException or write new ones that do.
 */
public class PmgClientException extends Issue {

	private static final long serialVersionUID = 1243555555L;
	

	PmgClientException(final String message) {
		super(message);
	}

	PmgClientException(final Exception cause) {
		super(cause);
	}	
	
	PmgClientException(final String message, final Exception cause) {
		super(message, cause);
	}	
}
