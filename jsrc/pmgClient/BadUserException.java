package pmgClient;

@SuppressWarnings("serial")
public class BadUserException extends PmgClientException {
    public BadUserException(final String message) {
        super(message);
    }

    public BadUserException(final Exception cause) {
        super(cause);
    }

    public BadUserException(final String message, final Exception cause) {
        super(message, cause);
    }
}
