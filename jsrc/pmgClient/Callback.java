package pmgClient;

/**
 * Interface that must be implemented by user's callback class.
 * <p>
 * 
 * When a callback is received, the method callbackFunction of this interface is executed
 */
public interface Callback {
	
	/**
	 * The client method which will be invoked remotely from the PMG server when a callback for the particular process is received 
	 * 
	 * @param linkedProcess The Process object representing the process for which the callback was received
	 */
	void callbackFunction(final Process linkedProcess);
	
}
