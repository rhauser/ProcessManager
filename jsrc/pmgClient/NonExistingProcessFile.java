package pmgClient;


/**
 * Thrown when a process produces not output in stdout or stderr
 * 
 */
@SuppressWarnings("serial")
public class NonExistingProcessFile extends PmgClientException {

	public NonExistingProcessFile(String message, Exception cause) {
		super(message, cause);
	}

}
