package pmgClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

import ipc.InvalidObjectException;
import ipc.InvalidPartitionException;
import ipc.InvalidReferenceException;
import ipc.ObjectEnumeration;
import ipc.Partition;
import pmgpriv.LinkRequestInfo;
import pmgpriv.SERVER;
import pmgpriv.CLIENT;
import pmgpriv.CLIENTPOA;
import pmgpriv.agentInfo_ex;
import pmgpriv.all_processes_ex;
import pmgpriv.app_not_found_ex;
import pmgpriv.exists_ex;
import pmgpriv.exit_ex;
import pmgpriv.get_info_ex;
import pmgpriv.kill_all_ex;
import pmgpriv.kill_partition_ex;
import pmgpriv.link_client_ex;
import pmgpriv.lookup_ex;
import pmgpriv.proc_info_listHolder;
import pmgpriv.processes_ex;
import pmgpub.AgentInfo;
import pmgpub.AgentInfoHolder;
import pmgpub.ProcessState;
import pmgpub.ProcessStatusInfoHolder;
import pmgpub.ProcessStatusInfo;


/**
 * The class representing the actual pmgClient; library users will need exactly one instance of this.
 * @author Lykourgos Papaevgeniou
 */
public final class PmgClient extends CLIENTPOA {
	
	public static final String PROCESS_MANAGER_SERVER_PREFIX = "AGENT_";
	public static final int CHECK_SERVER_THREAD_DELAY		= 10;
	public static final Partition GLOBAL_PARTITION			= new Partition(); // The IPC global partition, i.e. initial
	
	private static final String PROCESS_MANAGER_SEPARATOR	= "|PMGD|";
	private static final String TDAQ_INST_PATH_ENV			= "TDAQ_INST_PATH";
	private static final String SERVER_PORT_FILE			= "/com/pmg_port";

	private final CLIENT corbaRef;
	private final int serverPort;
	private final String localhost;
	private final ConcurrentMap <Handle, Process> processesMap;
	
	private boolean suppressPeriodicLoging = false;
	
	
	
	/**
	 * PmgClient constructor, will make the initial configurations for the client. 
	 * <p>
	 * Also responsible for scheduling a thread to check the server status regularly (every 5 secs) and take appropriate actions.
	 * 
	 * Must be used before anything else.
	 * 
	 * @throws ConfigurationException if there was some error with getting the server port from the file in TDAQ_INST_PATH_ENV diretory
	 */
	public PmgClient() throws ConfigurationException {
		this.processesMap = new ConcurrentHashMap<Handle, Process>();
		this.corbaRef  = _this(ipc.Core.getORB());	// "enables" the CORBA object
		
		String portFilePath = System.getenv(TDAQ_INST_PATH_ENV) + SERVER_PORT_FILE;
		
		//String portFilePath = "/afs/cern.ch/user/l/lpapaevg/public/lykTest/port.txt";
		
		File portFile = new File( portFilePath );
		
		if( portFile.isFile() ) {
			if( ! portFile.canRead() ) {
				ers.Logger.error(new ers.Issue("File \"" + portFilePath + "\" found," +
				"but but cannot be read. Check file permissions"));
				throw new ConfigurationException("File \"" + portFilePath + "\" found," +
						" but but cannot be read. Check file permissions");
			}
			int tmpPort = 0;
			try (Scanner scanner = new Scanner(portFile) ){
				tmpPort = scanner.nextInt();
			} catch (FileNotFoundException e) {
				throw new ConfigurationException("File \"" + portFilePath + "\" NOT found.");
			} catch (InputMismatchException e) {
				throw new ConfigurationException("Token in file \"" + portFilePath + "\" does not match " +
						"the Integer regular expression, or is out of range Strange.");
			} catch (NoSuchElementException e) {
				throw new ConfigurationException("Input in file \"" + portFilePath + "\"  was exhausted ");
			} 
			this.serverPort = tmpPort;
		} else {
			ers.Logger.info("File \"" + portFilePath + "\" does not exist or is not a normal file");
			this.serverPort = 0;
		}
		
				
		String lh = "unknown";
		try {
			lh = java.net.InetAddress.getLocalHost().getHostName(); 
		} catch (java.net.UnknownHostException e) {
			ers.Logger.warning(new ers.Issue("Local hostname could not be resolved into an address", e));
		}
		this.localhost = lh;
		
		ScheduledExecutorService ex = Executors.newSingleThreadScheduledExecutor(		// Thread to regularly check the server's status
				new ThreadFactory() {
					@Override
					public Thread newThread(Runnable r) {
						Thread thread = new Thread(r);
						thread.setDaemon(true);
						return thread;
					}
				});
		
		ex.scheduleWithFixedDelay(
				new Runnable() {
					@Override
					public void run() {
						linkClient();
					}
				},
				CHECK_SERVER_THREAD_DELAY,
				CHECK_SERVER_THREAD_DELAY,
				java.util.concurrent.TimeUnit.SECONDS
			);
	}



	/**
	 * The general callback function that is called remotely from the server and then calls the callack of the appropriate Process.<br>
	 * <b>Not to be used by users of the library!</b><br>
	 * (If one receives no callbacks, check local firewall for disallowing incoming connections)
	 */
	@Override
	public void callback(final String handleStr, final ProcessStatusInfo info) {
		
		ers.Logger.debug(2, "Executing PMG_Client callback. Handle : " + handleStr );
		
		final Handle handle;
		try {
			handle = new Handle(handleStr);
		} catch (InvalidHandleStringException e) {
			ers.Logger.fatal(new ers.Issue("Handle with which the callback was called seemed invalid(!)", e));
			// handle should always be in the correct format
			assert false : "Handle with which the callback was called seemed invalid(!)";
			return;
		}
					
		final Process p = this.processesMap.get(handle);
		assert p != null : "Handle with which the callback was called was not found in the local map(!)" ;
		
		synchronized (p) {			
			final ProcessState oldState = p.getProcessStatusInfo().state;
			p.setProcessStatusInfo(info);
			
			if( p.isLinked() ) {
				ers.Logger.debug(5, "Process with handle " + handleStr + " is linked");
				p.callback();
			}
				
			
			if( PmgClient.isEndState(p) ) {
				final ProcessState currentState = p.getProcessStatusInfo().state;
				
					// check if process state is SIGNALED or EXITED but was not in RUNNING (probably out of order callbacks
				if( (currentState == ProcessState.EXITED || currentState == ProcessState.SIGNALED)
						&& oldState != ProcessState.RUNNING ){
						// then do NOT remove from the map
					ers.Logger.debug(5, "End state EXITED or SIGNALED reached by " + handleStr + " but previous state was NOT RUNNING." +
							"Probably out of order callbacks ");
				} else {
					this.processesMap.remove(handle);
					ers.Logger.debug(5, "End state \"" + currentState + "\" reached by " + handleStr);
				}
			}
		}
	}


	
	/**
	 * Given a Handle, user can get a Process object
	 * 
	 * @param handle of the process
	 * @return the corresponding Process object
	 * @throws ProcessNotFoundException if no such process exists
	 * @throws ServerException 
	 * @throws CorbaException if some CORBA exception happened
	 * @throws IllegalArgumentException if the given handle was null
	 */

	public Process getProcess(final Handle handle) throws ProcessNotFoundException, ServerException, CorbaException{
		if( handle == null ) {
			throw new IllegalArgumentException("PmgClient's getProcess was called with a null handle!");
		}
		Process process = this.processesMap.get(handle);
		
		
		if( process != null ) {		// Process already existed locally
			ers.Logger.debug(2, "Process with handle: " + handle.toString() + " found in the local map");
			return process;
		}
		
		ers.Logger.debug(2, "Process with handle: " + handle.toString() + " NOT found in the local map");
		SERVER server;
		try {
		    server = pmgServerFromName(handle.getServer());
		} catch (NoSuchPmgServerException e1) {
		    throw new ProcessNotFoundException("No pmgServer found with name :" + handle.getServer(), e1);
		}

		try {
		    String handleStr = handle.toString();
		    if (server.exists(handleStr) ) {		// thows exists_ex
		        ers.Logger.debug(2, "Handle: " + handleStr + " exists in server.");

		        ProcessStatusInfoHolder infoHolder = new ProcessStatusInfoHolder();
		        boolean done = server.get_info(handleStr, infoHolder);		// thorws get_info_ex
		        if (done) {
		            process = new Process(handle, infoHolder.value, this);
		            final Process retProcess = this.processesMap.putIfAbsent(handle, process);
		            if(retProcess != null) {
                                process = retProcess;
                            }
		            
		            try {
                                server.link_client(new LinkRequestInfo(handle.toString(), this.corbaRef));
                            }
                            catch(app_not_found_ex e) {
                                ers.Logger.debug(1, "Process with handle " + handle.toString() + " NOT linked because it does not exist on the server: " + e);
                            }		            		            
		        }

		    } else {
		        ers.Logger.debug(2, "Handle: " + handleStr + " does not exist in server.");
		        throw new ProcessNotFoundException("Handle: " + handleStr + " does not exist in server.");
		    }
		} catch(link_client_ex e) {
		    throw new ServerException("link_client_ex Exception during getProcess call for: " + handle, e);
		} catch (exists_ex e) {
		    throw new ServerException("exists_ex Exception during getProcess call for: " + handle, e);
		} catch (get_info_ex e) {
		    throw new ServerException("get_info_ex Exception during getProcess call for: " + handle, e);
		} catch (org.omg.CORBA.SystemException e) {
		    throw new CorbaException("CORBA Exception during getProcess call for: " + handle, e);
		}

		return process;
	}
	
	
	/**
	 * Performs a server lookup for a particular process given its name, partition and server.
	 * 
	 * @param pmgServerName the fully qualified domain name of the server host.
	 * @param appName the name of the application.
	 * @param partition the partition the application is running.
	 * @return the Handle of the corresponding process or null otherwise.
	 * @throws LookupException if server was not found or an IDL exception happened.
	 * @throws CorbaException if some CORBA exception happened.
	 */
	public Handle lookup(final String appName, final String partition, final String pmgServerName) throws LookupException, CorbaException {
		try {
			SERVER server = pmgServerFromName(pmgServerName);			// throws NoSuchPmgServerException
			String handleStr = server.lookup(appName, partition);		// throws lookup_ex & Corba exceptions
			return handleStr.equals("") ? null : new Handle(handleStr);	// throws InvalidHandleStringException
		} catch (NoSuchPmgServerException e) {
			throw new LookupException("Bad pmgServer name: " + pmgServerName, e);
		} catch (lookup_ex e) {
			throw new LookupException(e);
		} catch (InvalidHandleStringException e) {
			ers.Logger.fatal(new ers.Issue("Handle String returned by server.lookup was invalid(!)", e));
			throw new RuntimeException("Handle String returned by server.lookup was invalid(!)", e);
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during lookup call for app: " + appName
					+ " in partition: " + partition + " and server: " + pmgServerName, e);
		}
	}

	
	/**
	 * Performs a server lookup for a particular process given its name, partition and a List of servers where it is expected to be found.
	 * Internally it uses { @link PmgClient#lookup(String appName, String partition, String pmgServerName) },
	 * so one should use that one provided the specific server is known. 
	 * 
	 * @param appName the name of tha application
	 * @param partition the name of the partition
	 * @param pmgServersList a List of servers among which the requested process is expected to be found
	 * @return the Handle of the corresponding process if found, null otherwise.
	 * @throws AggregatePmgClientException  if one or more exceptions happened. @see {@link AggregatePmgClientException#getPmgClientExceptionList()}
	 */
	public Handle lookup(final String appName, final String partition, final List<String> pmgServersList) throws AggregatePmgClientException {
		Handle handle = null;
		AggregatePmgClientException exceptionsList = null;
		
		for (String pmgServerName : pmgServersList) {
			try {
				handle = lookup(appName, partition, pmgServerName);
				if (handle != null) {
					return handle;
				}
			} catch (LookupException e) {
				ers.Logger.error(e);
				if( exceptionsList == null ) {
					exceptionsList = new AggregatePmgClientException("One or more exceptions occured in lookup and logged as errors.");
				}
				exceptionsList.addPmgClientException(e);
				continue;
			}  catch (CorbaException e) {
				ers.Logger.error(e);
				if( exceptionsList == null ) {
					exceptionsList = new AggregatePmgClientException("One or more exceptions occured in lookup and logged as errors.");
				}
				exceptionsList.addPmgClientException(e);
				continue;
			}
		}
		
		// Handle was not found. If there was/were some exception(s), throw an AggregatePmgClientException
		if( exceptionsList != null ) {
			throw exceptionsList;
		}
		return null;
	}
	
	
	/**
	 * Performs a server lookup for a particular process given its name and the partition it is running.
	 * Will perform a lookup for each server in the global partition (initial).
	 * 
	 * @param appName the name of the application.
	 * @param partition the partition the application is running.
	 * @return the Handle of the corresponding process if found, null otherwise.
	 * @throws AggregatePmgClientException  if one or more exceptions happened. @see {@link AggregatePmgClientException#getPmgClientExceptionList()}
	 */
	public Handle lookup(final String appName, final String partition) throws AggregatePmgClientException {
		
		ObjectEnumeration<SERVER> servers = null;
		try {				//get the pmgservers running in initial
			servers = new ObjectEnumeration<SERVER>(PmgClient.GLOBAL_PARTITION, SERVER.class);
		} catch (InvalidPartitionException e) {
			ers.Logger.fatal(e);
			throw new RuntimeException("IPC threw InvalidPartitionException regarding gloalPartition (initial)", e);
		}
		
		AggregatePmgClientException exceptionsList = null;

		while( servers.hasMoreElements() ) {
			SERVER server = servers.nextElement().reference;
			try {
				String handleStr = server.lookup(appName, partition);	// throws lookup_ex & corba exceptions
				if( ! handleStr.isEmpty()) {
					return new Handle(handleStr);					// throws InvalidHandleStringException
				}
			} catch (lookup_ex e) {
				LookupException lookupEx = new LookupException("IDL lookup_ex during lookup of app:" + appName
						+ " in partition: " + partition, e);
				ers.Logger.error(lookupEx);
				if( exceptionsList == null ) {
					exceptionsList = new AggregatePmgClientException("One or more exceptions occured in lookup and logged as errors.");
				}
				exceptionsList.addPmgClientException(lookupEx);
				continue;
			} catch (InvalidHandleStringException e) {
				LookupException lookupEx = new LookupException(e);
				ers.Logger.error(lookupEx);
				if( exceptionsList == null ) {
					exceptionsList = new AggregatePmgClientException("One or more exceptions occured in lookup and logged as errors.");
				}
				exceptionsList.addPmgClientException(lookupEx);
				continue;
			} catch (org.omg.CORBA.SystemException e) {
				CorbaException corbaEx =  new CorbaException("CORBA Exception during lookup of app:" + appName
						+ " in partition: " + partition, e);
				ers.Logger.error(corbaEx);
				if( exceptionsList == null ) {
					exceptionsList = new AggregatePmgClientException("One or more exceptions occured in lookup and logged as errors.");
				}
				exceptionsList.addPmgClientException(corbaEx);
				continue;
			}
		}
		
		// Handle was not found. If there was/were some exception(s), throw an AggregatePmgClientException
		if( exceptionsList != null ) {
			throw exceptionsList;
		}

		return null;
	}
	

	/**
	 * Returns an ArrayList containing the handles as String of all the  processes running on the given host within the given partition.
	 *
	 * @param hostname   the fully qualified domain name of the server host where the processes are running
	 * @param partition The name of the partition the processes belong to
	 * 
	 * @return an ArrayList containing the string representations of the processes' Handles
	 * @throws NoSuchPmgServerException if the requested pmgServer has not been found in IPC
	 * 			 (or there is a communication problem with IPC itself)
	 * @throws ServerException 
	 * @throws CorbaException if some CORBA exception happened
	 *
	 */
	public ArrayList<String> runningProcesses(final String hostname, final String partition ) throws NoSuchPmgServerException,
			ServerException, CorbaException{
		
		String handles = "";
		SERVER server = pmgServerFromName(hostname); 	// throws NoSuchPmgServerException
		
		try {
			handles = server.processes(partition);
		} catch (processes_ex e) {
			throw new ServerException("processes_ex Exception during askRunningProcesses of partition:" + partition 
					+ " and host: " + hostname, e);
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during askRunningProcesses of partition:" + partition 
					+ " and host: " + hostname, e);
		} 
		
		return parseHandlesToList(handles);
	}
	
	
	/**
	 * Returns an ArrayList containing the handles as String of all the  processes running on the given host.
	 * 
	 * @param pmgserver_name the fully qualified domain name of the server host where the processes are running on
	 * @return an ArrayList containing the string representations of the processes' Handles
	 * @throws NoSuchPmgServerException if the requested pmgServer has not been found in IPC
	 * 			 (or there is a communication problem with IPC itself)
	 * @throws ServerException  
	 * @throws CorbaException  if some CORBA exception happened
	 */
	public ArrayList<String> runningProcessesForHost(final String pmgServerName) throws NoSuchPmgServerException,
			ServerException, CorbaException {
		SERVER server = pmgServerFromName(pmgServerName);		 	// throws NoSuchPmgServerException
		String procs = null;
		
		try {
			procs = server.all_processes();
		} catch (all_processes_ex e) {
			throw new ServerException("all_processes_ex Exception during askRunningProcessesInHost of server:" + pmgServerName, e);
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during askRunningProcessesInHost of server:" + pmgServerName, e);
		} 

		return parseHandlesToList(procs);
	}
	
	
	/**
	 *  
	 * Returns an ArrayList containing the handles as String of all the  processes running on the given partition.
	 *
	 * @param partition The name of the partition the processes belong to
	 * 
	 * @return an ArrayList containing the string representations of the processes' Handles
	 * @throws InvalidPartitionException if the partition name is invalid
	 * @throws ServerException 
	 * @throws CorbaException  if some CORBA exception happened
	 *
	 */
	public ArrayList<String> runningProcessesForPartition( final String partition ) throws InvalidPartitionException, ServerException, CorbaException{

		
		try {
			//get the pmgservers running in initial
			ObjectEnumeration<SERVER> enu = new ObjectEnumeration<SERVER>(PmgClient.GLOBAL_PARTITION, SERVER.class);// throws InvalidPartitionException
			
			StringBuilder builder = new StringBuilder(386);
			
			while( enu.hasMoreElements() ) {
				SERVER server = enu.nextElement().reference;
					builder.append(server.processes(partition));		// throws processes_ex & corba Exceptions
			}
			
			return parseHandlesToList(builder.toString());
			
		} catch (InvalidPartitionException e) {
			throw e;
		} catch (processes_ex e) {
			throw new ServerException("processes_ex Exception during askRunningProcesses of partition:" + partition, e);		
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("Corba Exception during askRunningProcesses of partition:" + partition, e);
		}
	}

	
	/**
	 * Returns a map containing the handle as String and ProcessStatusInfo of all the  processes running on the
	 *       given host within the given partition .
	 *
	 * @param hostname   the fully qualified domain name of the server host where the processes are running
	 * @param partition the name of the partition the processes belong to
	 * 
	 * @return the aforementioned map
	 * @throws NoSuchPmgServerException if the requested pmgServer has not been found in IPC
	 * 			 (or there is a communication problem with IPC itself)
	 * @throws ServerException 
	 * @throws CorbaException  if some CORBA exception happened
	 *
	 */
	public HashMap<String, ProcessStatusInfo> runningProcessesInfo(final String hostname, final String partition )
			throws NoSuchPmgServerException, ServerException, CorbaException{
		
		HashMap<String, ProcessStatusInfo> pm = new HashMap<String, ProcessStatusInfo>();
		
		SERVER server = pmgServerFromName(hostname);	// throws NoSuchPmgServerException
		
		proc_info_listHolder procInfoList = new proc_info_listHolder();
		
		try {
			server.processes_info(partition, procInfoList);
		} catch (processes_ex e) {
			throw new ServerException("processes_ex Exception during askRunningProcessesMap of partition:" + partition 
					+ " and host: " + hostname, e);
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during askRunningProcessesMap of partition:" + partition 
					+ " and host: " + hostname, e);
		} 
		
		for( pmgpriv.ProcessInfoRequest i : procInfoList.value ) {
			pm.put(i.app_handle, i.proc_info);
		}

		return pm;		
	}
	
	
	/**
	 * Returns a map containing the handle as String and ProcessStatusInfo of all the  processes running on the
	 *       given host.
	 * 
	 * @param pmgServerName  the fully qualified domain name of the server host where the processes are running on
	 * @return the aforementioned map
	 * @throws NoSuchPmgServerException if the requested pmgServer has not been found in IPC
	 * 			 (or there is a communication problem with IPC itself)
	 * @throws ServerException 
	 * @throws CorbaException  if some CORBA exception happened
	 */
	public HashMap<String, ProcessStatusInfo> runningProcessesInfoForHost(final String pmgServerName)
			throws NoSuchPmgServerException, ServerException, CorbaException {
		
		SERVER server = pmgServerFromName(pmgServerName);	 	// throws NoSuchPmgServerException
		proc_info_listHolder procInfoList = new proc_info_listHolder();
		
		HashMap<String, ProcessStatusInfo> pm = new HashMap<String, ProcessStatusInfo>();
		
		try {
			server.all_processes_info(procInfoList);
		} catch (all_processes_ex e) {
			throw new ServerException("all_processes_ex Exception during askRunningProcessesMapOfHost of server:" + pmgServerName, e);
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during askRunningProcessesMapOfHost of server:" + pmgServerName, e);
		} 
		
		for( pmgpriv.ProcessInfoRequest i : procInfoList.value ) {
			pm.put(i.app_handle, i.proc_info);
		}
		
		return pm;
	}
	
	
	/**
	 * Returns a map containing the handle as String and ProcessStatusInfo of all the processes running on the
	 *		given partition.
	 *        
	 * @param partition the name of the partition the processes belong to
	 * @return the aforementioned map
	 * @throws InvalidPartitionException if the partition name is invalid
	 * @throws ServerException 
	 * @throws CorbaException  if some CORBA exception happened
	 */
	public HashMap<String, ProcessStatusInfo> runningProcessesInfoForPartition( final String partition )
			throws InvalidPartitionException, ServerException, CorbaException{
		
		HashMap<String, ProcessStatusInfo> pm = new HashMap<String, ProcessStatusInfo>();
		
		ObjectEnumeration<SERVER> enu;
		try {
			enu = new ObjectEnumeration<SERVER>(PmgClient.GLOBAL_PARTITION, SERVER.class);	//get the pmgserver running in initial
		} catch (InvalidPartitionException e) {
			throw e;
		}
		
		
		while( enu.hasMoreElements() ) {
			SERVER server = enu.nextElement().reference;
			proc_info_listHolder procInfoList = new proc_info_listHolder();
			
			try {
				server.processes_info(partition, procInfoList);
			}catch (processes_ex e) {
				throw new ServerException("processes_ex Exception during askRunningProcessesMap of partition:" + partition, e);
			} catch (org.omg.CORBA.SystemException e) {
				throw new CorbaException("CORBA Exception during askRunningProcessesMap of partition:" + partition, e);
			}
			
			for( pmgpriv.ProcessInfoRequest i : procInfoList.value ) {
				pm.put(i.app_handle, i.proc_info);
			}
		}
			
		return pm;		
	}
	
	
	/**
	 * Checks whether an application with a given Handle exists somewhere.
	 * 
	 * @param handle the application's Handle.
	 * @return true if it exists, false otherwise.
	 * @throws NoSuchPmgServerException if the requested pmgServer has not been found in IPC
	 * 			 (or there is a communication problem with IPC itself).
	 * @throws ServerException 
	 * @throws CorbaException if some CORBA exception happened.
	 */
	public boolean existsSomewhere(final Handle handle) throws NoSuchPmgServerException, ServerException, CorbaException {
		SERVER server = pmgServerFromName(handle.getServer());
		
		boolean exists = false;
		try {
			exists = server.exists(handle.toString());
		} catch (exists_ex e) {
			throw new ServerException("exists_ex Exception during existsSomewhere for handle:" + handle, e);			
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during existsSomewhere for handle:" + handle, e);
		} 
		
		return exists;
	}
	
	
	/**
	 * Returns an object with all the info about a specific agent/server.
	 * 
	 * 
	 * @param pmgServerName the fully qualified domain name of the server host
	 * @return an AgentInfo object as defined in IDL. All its members are public.
	 * @throws NoSuchPmgServerException if the requested pmgServer has not been found in IPC
	 * 			 (or there is a communication problem with IPC itself)
	 * @throws ServerException 
	 * @throws CorbaException  if some CORBA exception happened
	 */
	public AgentInfo agentInfo(final String pmgServerName) throws NoSuchPmgServerException, ServerException, CorbaException {
		AgentInfoHolder agInfo = new AgentInfoHolder();
		try {
			pmgServerFromName(pmgServerName).agentInfo(agInfo);
			return agInfo.value;
		}  catch (NoSuchPmgServerException e) {
			throw e;
		} catch (agentInfo_ex e) {
			throw new ServerException("agentInfo_ex Exception during agentInfo for server:" + pmgServerName, e);
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during agentInfo for server:" + pmgServerName, e);
		}
	}
	
	
	/**
	 * "Softly" kills all the started processes of all the servers.
	 * 
	 * @param timeout number of seconds to wait before forcing a process to exit
	 * @throws AggregatePmgClientException if one or more exceptions happened. @see {@link AggregatePmgClientException#getPmgClientExceptionList()}
	 */
	public void killAll(final int timeout) throws AggregatePmgClientException {
		//get the pmgserver running in initial
		ObjectEnumeration<SERVER> enu = null;
	
		try {
			enu = new ObjectEnumeration<SERVER>(PmgClient.GLOBAL_PARTITION, SERVER.class);
		} catch (InvalidPartitionException e) {
			throw new RuntimeException(e);
		}
		
		AggregatePmgClientException exceptionsList = null;

		while( enu.hasMoreElements() ) {
			SERVER server = enu.nextElement().reference;
			
			try {
				server.kill_all(timeout, UserIdentityFactory.getUserIdentity().getAuthToken(), this.localhost);
			} catch(BadUserException e) {
			        ers.Logger.error(e);
			        if( exceptionsList == null ) {
                                    exceptionsList = new AggregatePmgClientException("One or more exceptions occurred in killAll" +
                                                    " and logged as errors. This does NOT mean that killAll call failed for all the servers");
                                }
                                exceptionsList.addPmgClientException(e);
                                continue;
			} catch (kill_all_ex e) {
				ServerException serverEx =  new ServerException("kill_all_ex Exception during killAll call of some server", e);
				ers.Logger.error(serverEx);
				if( exceptionsList == null ) {
					exceptionsList = new AggregatePmgClientException("One or more exceptions occurred in killAll" +
							" and logged as errors. This does NOT mean that killAll call failed for all the servers");
				}
				exceptionsList.addPmgClientException(serverEx);
				continue;
			} catch (org.omg.CORBA.SystemException e) {
				CorbaException corbaEx =  new CorbaException("CORBA Exception during killAll call of some server", e);
				ers.Logger.error(corbaEx);
				if( exceptionsList == null ) {
					exceptionsList = new AggregatePmgClientException("One or more exceptions occurred in killAll" +
							" and logged as errors. This does NOT mean that killAll call failed for all the servers");
				}
				exceptionsList.addPmgClientException(corbaEx);
				continue;
			} 
		}
		
		if( exceptionsList != null )
			throw exceptionsList;
	}
	
	
	/**
	 *  "Softly" kills all the started process belonging to the given partition.
	 * 
	 * @param partition name of the partition
	 * @param timeout number of seconds to wait before forcing a process to exit
	 * @throws InvalidPartitionException if there is no such partition in the global partition of the PmgClient
	 * @throws CorbaException if some CORBA exception happened
	 */
	public void killPartition(final String partition, final int timeout) throws InvalidPartitionException, AggregatePmgClientException {
		//get the pmgserver running in initial
		ObjectEnumeration<SERVER> enu = null;
	
		try {
			enu = new ObjectEnumeration<SERVER>(PmgClient.GLOBAL_PARTITION, SERVER.class);
		} catch (InvalidPartitionException e) {
			throw e;
		}
		
		AggregatePmgClientException exceptionsList = null;
		
		while( enu.hasMoreElements() ) {
			SERVER server = enu.nextElement().reference;
			
			try {
				server.kill_partition(partition, timeout, UserIdentityFactory.getUserIdentity().getAuthToken(), this.localhost);
			} catch(BadUserException e) {
			        ers.Logger.error(e);
                                if( exceptionsList == null ) {
                                    exceptionsList = new AggregatePmgClientException("One or more exceptions occurred in killPartition and logged as errors." +
                                                    " This does NOT mean that killPartition call failed for all the servers in partition : " + partition);
                                }
                                exceptionsList.addPmgClientException(e);
                                continue;
			} catch (kill_partition_ex e) {
				ServerException serverEx =  new ServerException("kill_partition_ex Exception during killPartition call some server in partition: "
						+ partition, e);
				ers.Logger.error(serverEx);
				if( exceptionsList == null ) {
					exceptionsList = new AggregatePmgClientException("One or more exceptions occurred in killPartition and logged as errors." +
							" This does NOT mean that killPartition call failed for all the servers in partition : " + partition);
				}
				exceptionsList.addPmgClientException(serverEx);
				continue;
			} catch (org.omg.CORBA.SystemException e) {
				CorbaException corbaEx =  new CorbaException("CORBA Exception during killPartition call for some server in partition: "
						+ partition, e);
				ers.Logger.error(corbaEx);
				if( exceptionsList == null ) {
					exceptionsList = new AggregatePmgClientException("One or more exceptions occurred in killPartition and logged as errors." +
							" This does NOT mean that killPartition call failed for all the servers in partition : " + partition);
				}
				exceptionsList.addPmgClientException(corbaEx);
				continue;
			}
		}
		
		if( exceptionsList != null )
			throw exceptionsList;
	}

	/**
	 * "Softly" kills all the processes in a partition running on a host.
	 * 
	 * @param pmgServerName The fully qualified name of the host
	 * @param partition The name of the partition
	 * @param timeout Number of seconds to wait before forcing a process to exit
	 * @throws NoSuchPmgServerException There are issues talking to the PMG server on the host
	 * @throws ServerException The PMG server on the host did not manage to do its job
	 * @throws CorbaException Some error occurred in the CORBA communication
	 * @throws BadUserException Cannot determine user identity
	 */
        public void killPartitionOnHost(final String pmgServerName, final String partition, final int timeout)
            throws NoSuchPmgServerException,
                ServerException,
                CorbaException, BadUserException
        {
            final SERVER server = pmgServerFromName(pmgServerName); // throws NoSuchPmgServerException

            try {
                server.kill_partition(partition, timeout, UserIdentityFactory.getUserIdentity().getAuthToken(), this.localhost);
            }
            catch(kill_partition_ex e) {
                throw new ServerException("Exception killing processes on server: " + pmgServerName, e);
            }
            catch(org.omg.CORBA.SystemException e) {
                throw new CorbaException("CORBA exception killing processes on server: " + pmgServerName, e);
            }
        }

	/**
	 * Will cause the given server to exit (only if the pmgserver is started with the "-k" option)
	 * 
	 * @param pmgserver_name the fully qualified domain name of the server host
	 * @throws NoSuchPmgServerException if the requested pmgServer has not been found in IPC
	 * 			 (or there is a communication problem with IPC itself)
	 * @throws ServerException
	 * @throws CorbaException if some CORBA exception happened
	 */
	public void exit(final String pmgServerName) throws NoSuchPmgServerException, ServerException, CorbaException {		
		try {
			pmgServerFromName(pmgServerName).exit();
		} catch (NoSuchPmgServerException e) {
			throw e;
		} catch (exit_ex e) {
			throw new ServerException("exit_ex Exception during exit call of server: " + pmgServerName, e);
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during exit call of server: " + pmgServerName, e);
		} 
	}
	
	
	/**
	 * Checks whether a given pmg server is alive.
	 * 
	 * @param pmgserver_name the fully qualified domain name of the server host
	 * @return true is the server is alive, false otherwise
	 * @throws NoSuchPmgServerException if the requested pmgServer has not been found in IPC
	 * 			 (or there is a communication problem with IPC itself)
	 * @throws CorbaException if some CORBA exception happened
	 */
	public boolean isAlive(final String pmgServerName) throws NoSuchPmgServerException, CorbaException {
		try {
			return pmgServerFromName(pmgServerName).alive();
		} catch (NoSuchPmgServerException e) {
			throw e;
		}  catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during isAlive call of server: " + pmgServerName, e);
		}
	}
	
	
	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(386);
		builder.append("PmgClient [\n localhost=");
		builder.append(this.localhost);
		builder.append(",\n corbaRef=");
		builder.append(this.corbaRef);
		builder.append("\n]");
		return builder.toString();
	}
	
	
	
	
	/***   package-private and private methods   ****/
	
	
	
	
	
	/**
	 * Asks the IPC server about the existence of a given ProcessManager server.
	 * 
	 * @param pmgServerName the fully qualified domain name of the server host
	 * 			(without the prefix PROCESS_MANAGER_SERVER_PREFIX, "AGENT_" )
	 * @return A reference to the ProcessManager SERVER.
	 * 
	 * @throws NoSuchPmgServerException if the requested pmgServer has not been found in IPC
	 * 			 (or there is a communication problem with IPC itself)
	 */
	SERVER pmgServerFromName(final String pmgServerName) throws NoSuchPmgServerException {
		
		SERVER server = null;
		
		if( this.serverPort == 0 ) {
			ers.Logger.debug(5, "Using IPC to getServerFromName");
			try {
				server = GLOBAL_PARTITION.lookup(SERVER.class, PmgClient.PROCESS_MANAGER_SERVER_PREFIX + pmgServerName);
			} catch (InvalidPartitionException e) {
				ers.Logger.fatal(e);
				throw new RuntimeException("IPC threw InvalidPartitionException regarding gloalPartition (initial)", e);
			} catch (InvalidObjectException e) {
				throw new NoSuchPmgServerException("PmgServer with name: " + pmgServerName + " was not found", e);
			}
		} else {
			ers.Logger.debug(5, "NOT using IPC to getServerFromName");			
			
			try {
			    final String cRef = ipc.Core.constructReference(GLOBAL_PARTITION.getName(), 
			                                                    "pmgpriv/SERVER", 
			                                                    PmgClient.PROCESS_MANAGER_SERVER_PREFIX + pmgServerName, 
			                                                    pmgServerName, 
			                                                    this.serverPort);
			    
                            server = ipc.Core.stringToObject(SERVER.class, cRef);
                        }
                        catch(final InvalidReferenceException e) {
                            throw new RuntimeException("Cannot build a valid reference for PMG server on host " + pmgServerName, e);
                        }			
		}

		return server;
	}
	
	
	
	
	/***   package-private and private methods   ****/
	
	
	
	
	
	/**
	 * Method used by <code>ProcessDescription</code> in order to get a new <code>Process</code> object
	 * or the one that was already created for that handle.
	 * 
	 * @param handle the Handle object created by the valid handle String returned by the server
	 * @param procStatusInfo
	 * @return the new <code>Process</code> object or one that was already in the map
	 */
	Process requestProcess(final Handle handle, final ProcessStatusInfo procStatusInfo) {
		final Process process = new Process(handle, procStatusInfo, this);
		final Process retProcess = this.processesMap.putIfAbsent(handle, process);
		
		return (retProcess==null ? process : retProcess);
	
	}



	/**
	 * Checks whether a given Process is in one of the end states (exited, signaled, failed, syncerror).
	 * 
	 * @param proc process for which the state will be checked.
	 * @return true if the Process's state is an end state, false otherwise.
	 */
	static boolean isEndState(final Process proc ) {
		final ProcessState state = proc.getProcessStatusInfo().state;
		return state == ProcessState.EXITED
			|| state == ProcessState.SIGNALED
			|| state == ProcessState.FAILED
			|| state == ProcessState.SYNCERROR;
	}
	
	/**
	 * Checks whether a given Process is in a state valid for callbacks (running, exited, signaled, failed, syncerror)
	 * 
	 * @param proc process for which the state will be checked.
	 * @return true if the Process's state is valid for callbacks, false otherwise.
	 */
	private static boolean isValidStateForCallBacks(final Process proc) {
		return proc.getProcessStatusInfo().state == ProcessState.RUNNING || PmgClient.isEndState(proc);
	}
	
	
	/**
	 * Parses a String with handles that are separated by {@link PmgClient#PROCESS_MANAGER_SEPARATOR} and returns an ArrayList
	 * 
	 * @param handlesStr a String in the format HANDLE_1|PMGD|HANDLE_2|PMGD|....|PMGD|  with PROCESS_MANAGER_SEPARATOR as |PMGD|
	 * @return an ArrayList containing the handles as Strings
	 */
	private ArrayList<String> parseHandlesToList(String handlesStr) {
		ArrayList<String> handlesList = new ArrayList<String>();
		
		if(handlesStr.isEmpty())
			return handlesList;
													//escape the SEPARATOR before using it as a regex
		String[] handlesArray = handlesStr.split(java.util.regex.Pattern.quote(PROCESS_MANAGER_SEPARATOR));
		
		for( String handle : handlesArray ) {
			handlesList.add(handle);
		}
		
		return handlesList;
	}
	

	/**
	 * Method that is invoked periodically ( @see {@link PmgClient#PmgClient()} ) and checks whether the pmg servers are up and running.
	 * Re-links all the processes of a server in case the server was down and calls callbacks accordingly. 
	 */
	private void linkClient()  {	

		if (! this.suppressPeriodicLoging) {
			ers.Logger.debug(3, "Scheduled link_client() method called");
		}
		Set<Entry<Handle, Process>> set = this.processesMap.entrySet();

		for( Entry<Handle, Process> entry : set ) {
			SERVER server;
			try {
				server = pmgServerFromName(entry.getKey().getServer());
			} catch (NoSuchPmgServerException e1) {
				ers.Logger.error(e1);
				continue;
			}
			pmgpub.ProcessStatusInfoHolder statusInfoHolder = null;
			
			
			if( PmgClient.isEndState(entry.getValue()))
				continue;
			
			try {						// throws link_client_ex & app_not_found_ex & corba ex
				boolean wasUnlinked = server.link_client(new LinkRequestInfo(entry.getKey().toString(), this.corbaRef));
				if( wasUnlinked ) {
					/*	This means that the pmg Server was down (or even the host of the pmg Server)
					 * So our local info about the process may be outdated.
					 * Update our local info						 
					 */
					ers.Logger.debug(3, "Client linked to the server for the application: " + entry.getKey().toString());
					
					
					statusInfoHolder = new pmgpub.ProcessStatusInfoHolder();
					boolean success  = server.get_info(entry.getKey().toString(), statusInfoHolder); // throws  get_info_ex & corba ex
					
					if(success && entry.getValue().getProcessStatusInfo().state != statusInfoHolder.value.state
							&& PmgClient.isValidStateForCallBacks(entry.getValue())) {	// TODO check condition of C++
						// Information acquired and the new state is different than the locally cached one.
						//   Execute the callbacks
						callback(entry.getKey().toString(), statusInfoHolder.value);
					} else if (! success) {
						// The process is gone in the while
						statusInfoHolder.value.state = ProcessState.EXITED;
						callback(entry.getKey().toString(), statusInfoHolder.value);
					}
				}
			} catch (link_client_ex e) {
				ers.Logger.error(new ers.Issue("Exception at linking of client : " + entry.getKey().toString(),e));
				continue;
			}catch (app_not_found_ex e) {		// the application does not exist anymore, call the callback
			        if(server.app_context().time > entry.getValue().getProcessStatusInfo().start_time) {
			            // Here if the server is "younger" than the application 
			            entry.getValue().getProcessStatusInfo().state = ProcessState.EXITED;
			            ers.Logger.info(new ers.Issue("Application " + entry.getKey().toString() + " was not found." +
			                                          "Changing its status to EXITED", e));
			            
			            callback(entry.getKey().toString(), entry.getValue().getProcessStatusInfo());
			        }
				continue;
			} catch (get_info_ex e) {
				ers.Logger.error(new ers.Issue("Exception at get_info for: " + entry.getKey().toString(), e));
				continue;
			} catch (org.omg.CORBA.SystemException e) {
				ers.Logger.error(new ers.Issue("Corba exception at PmgClient.link_client() for " + entry.getKey().toString(), e));
				continue;
			}
		}
	}

	/**
	 *  Getter for the corba reference if this client
	 *  
	 * @return the corba reference
	 */
	public CLIENT getCorbaRef() {
		return this.corbaRef;
	}


	/**
	 * Getter for the server port
	 * 
	 * @return the server port
	 */
	public int getServerPort() {
		return this.serverPort;
	}


	/**
	 * Getter for the localhost where this is running.
	 * 
	 * @return the localhost of this client
	 */
	public String getLocalhost() {
		return this.localhost;
	}
	
	/**
	 * 
	 * 
	 * @return true when the periodic logging message is not being printed 
	 */
	public boolean getSuppressPeriodicLogging() {
		return this.suppressPeriodicLoging;
	}
	
	
	/**
	 * Can be used by the user to suppress the periodic logging of messages when the pmg client 'pings' the server.
	 * suppressPeriodicLoging set to false by default.
	 * 
	 * @param suppressPeriodicLoging set to true to suppress the logging.
	 */
	public void setSuppressPeriodicLogging(boolean suppressPeriodicLoging) {
		this.suppressPeriodicLoging = suppressPeriodicLoging;
	}
	
}



