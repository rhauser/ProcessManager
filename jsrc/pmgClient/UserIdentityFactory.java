package pmgClient;

import daq.tokens.AcquireTokenException;
import daq.tokens.JWToken;
import daq.tokens.JWToken.MODE;
import daq.tokens.VerifyTokenException;


class UserIdentityFactory {
    private final static boolean IS_AUTH_ENABLED = JWToken.enabled();

    private static class DefaultAuth implements UserIdentity {
        private final static String USER_NAME = System.getProperty("user.name");

        DefaultAuth() {
        }

        @Override
        public String getUserName() {
            return DefaultAuth.USER_NAME;
        }

        @Override
        public String getAuthToken() {
            return DefaultAuth.USER_NAME;
        }

    }

    private static class JWTAuth implements UserIdentity {
        private final String authToken;
        private volatile String userName;

        JWTAuth() throws BadUserException {            
            try {
                this.authToken = JWToken.acquire(MODE.REUSE);
            }
            catch(final AcquireTokenException ex) {
                throw new BadUserException(ex);
            }

            this.userName = null;
        }

        @Override
        public String getUserName() {
            if(this.userName == null) {
                synchronized(this) {
                    if(this.userName == null) {
                        try {
                            this.userName = JWToken.verify(this.authToken).get("sub").toString();
                        }
                        catch(final VerifyTokenException ex) {
                            this.userName = "Untrusted user (" + System.getProperty("user.name") + ")";
                        }
                    }
                }
            }

            return this.userName;
        }

        @Override
        public String getAuthToken() {
            return this.authToken;
        }
    }

    public static UserIdentity getUserIdentity() throws BadUserException {
        if(UserIdentityFactory.IS_AUTH_ENABLED == true) {
            return new JWTAuth();
        }

        return new DefaultAuth();
    }
}
