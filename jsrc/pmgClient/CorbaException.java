package pmgClient;


/**
 * Thrown whenever there was a CORBA layer exception. The Corba exception should always be given as a cause for this exception.
 *
 */
@SuppressWarnings("serial")
public class CorbaException extends PmgClientException {

	/**
	 * @param cause The actual CORBA exception that happened
	 */
	CorbaException(final String message, final Exception cause) {
		super(message, cause);
	}

	/**
	 * 
	 * @param cause The actual CORBA exception that happened
	 */
	CorbaException(final Exception cause) {
		super(cause);
	}
}
